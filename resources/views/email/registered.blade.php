@extends('email.index')

@section('content')
    <tr>
        <td bgcolor="#ffffff"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            Hi {{ $data['user']['name'] }},
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            Thanks for registration. You can check your scheduled appointments in your dashboard. Please make sure to use your logged in or registered first name and last name in the second step for the appointment to be linked to your account.
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            <b>Registered Name: {{ isset($data['user']) && $data['user']['name'] ? ' ' . $data['user']['name'] : '' }}</b>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            <table bgcolor="#2a465f" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
                <tr>
                    <td align="center" height="50"
                        style=" padding: 0 25px 0 25px; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold;"
                        class="button">
                        <a href="{{ url('customer/dashboard') }}" style="color: #ffffff; text-align: center; text-decoration: none;">Dashboard</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection