@extends('email.index')

@section('content')
    <tr>
        <td bgcolor="#ffffff"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            Hi {{ $data['to']['name'] }},
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            Check out this virtual tour at <a href="{{ urldecode($data['appointment_data']['link']) }}">{{ urldecode($data['appointment_data']['link']) }}</a>
            for {{ $data['appointment_data']['address'] }}
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            A message from your friend:
            <p>{!! $data['message'] !!}</p>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            Sent From {{ $data['from']['name'] }}
        </td>
    </tr>
@endsection