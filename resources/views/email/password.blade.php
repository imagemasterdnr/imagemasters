@extends('email.index')

@section('content')
    <tr>
        <td bgcolor="#ffffff" align="center"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            <b>Forgot your password? Let's get you a new one!</b>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            <b>Account:</b> {{ $email }}
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center"
            style="padding: 10px 20px 5px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
            <table bgcolor="#2a465f" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
                <tr>
                    <td align="center" height="50"
                        style=" padding: 0 25px 0 25px; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold;"
                        class="button">
                        <a href="{{ $url }}" style="color: #ffffff; text-align: center; text-decoration: none;">Reset
                            Password</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection