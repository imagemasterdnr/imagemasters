<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>ProUI Email Template - Welcome</title>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .buttonwrapper {
                background-color: transparent !important;
            }

            body[yahoo] .button {
                padding: 0 !important;
            }

            body[yahoo] .button a {
                background-color: #9b59b6;
                padding: 15px 25px !important;
            }
        }

        @media only screen and (min-device-width: 601px) {
            .content {
                width: 650px !important
            }

            .col387 {
                width: 387px !important;
            }
        }
        .im > a[href] {
          color: #ffffff!important;
        }
    </style>
</head>
<body bgcolor="#ffffff" style="margin: 0; padding: 0; padding-top: 30px;" yahoo="fix">
<!--[if (gte mso 9)|(IE)]>
<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
<![endif]-->
<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 600px;"
       class="content">
    <tr>
        <td align="center" bgcolor="#dddddd"
            style="padding: 0 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold; background-image: url('{{ url('public/img/generic-common/exp-e-logo-lightblue.png') }}'); background-repeat:  no-repeat;background-position: right center;background-size: contain;">
            <table align="center" border="0" cellpadding="0" cellspacing="0"
                   style="border-collapse: collapse; width: 100%;" class="content">
                <tr>
                    <img style="vertical-align: middle;margin: 15px;" src="{{ asset('public/uploads/logo.png') }}">
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 20px; background-color: #fff"></td>
    </tr>