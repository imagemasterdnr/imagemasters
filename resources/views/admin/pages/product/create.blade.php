@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/products') }}">Services</a></li>
        <li><span href="javascript:void(0)">Add New Service</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'POST',
            'id' => 'create-product',
            'route' => ['admin.products.store'],
            'class' => 'form-horizontal ',
            'files' => true
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Add new Service</strong></h2>
                </div>
                {{--<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">--}}
                    {{--<label class="col-md-3 control-label" for="product_code">Code</label>--}}

                    {{--<div class="col-md-9">--}}
                        {{--<input type="text" class="form-control" id="product_code" name="code" value="{{ old('code') }}"--}}
                               {{--placeholder="Enter Service code..">--}}
                        {{--@if($errors->has('code'))--}}
                            {{--<span class="help-block animation-slideDown">{{ $errors->first('code') }}</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_name" name="name" value="{{ old('name') }}"
                               placeholder="Enter Service name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('acuity_scheduling_link') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="acuity_scheduling_link">Acuity Scheduling Link</label>

                    <div class="col-md-9">
                        <textarea id="acuity_scheduling_link" name="acuity_scheduling_link" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter Service acuity scheduling link..">{!! old('acuity_scheduling_link') !!}</textarea>
                        @if($errors->has('acuity_scheduling_link'))
                            <span class="help-block animation-slideDown">{{ $errors->first('acuity_scheduling_link') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_id">Project Sample</label>

                    <div class="col-md-9">
                        <select name="project_id" id="project_id"
                                class="project-select"
                                data-placeholder="Choose project..">
                            <option value=""></option>
                            @foreach($projects as $project)
                                <option value="{{ $project->id }}" {{ old('project_id') ? old('project_id') == $project->id ? 'selected' : '' : '' }}>{{ $project->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('project_id'))
                            <span class="help-block animation-slideDown">{{ $errors->first('project_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="banner_image">Banner Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Choose File <input type="file" name="banner_image" style="display: none;">
                            </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('banner_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('banner_image') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('overview_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="overview_image">Overview Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Choose File <input type="file" name="overview_image" style="display: none;">
                            </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('overview_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('overview_image') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="description">Description</label>

                    <div class="col-md-9">
                        <textarea id="description" name="description" rows="9"
                                  class="form-control ckeditor"
                                  placeholder="Enter Service description..">{{ old('description') }}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>
                <!--
                <div class="form-group{{ $errors->has('pricing_content') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="pricing_content">Pricing Content</label>

                    <div class="col-md-9">
                        <textarea id="pricing_content" name="pricing_content" rows="9"
                                  class="form-control ckeditor"
                                  placeholder="Enter Service pricing_content..">{{ old('pricing_content') }}</textarea>
                        @if($errors->has('pricing_content'))
                            <span class="help-block animation-slideDown">{{ $errors->first('pricing_content') }}</span>
                        @endif
                    </div>
                </div>
                -->
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ old('is_active') ? 'checked' : '' }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ url('admin/products') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            @include('admin.pages.product.product_category_fields')
        </div>
        <div class="col-md-6">
            {{--@include('admin.pages.product.image_fields')--}}
            @include('admin.pages.product.meta_fields')
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('extrascripts')
    <script>
        @if (!empty(old('product_images')))
            <?php
                $product_images = [];
                foreach (old('product_images') as $product_image) :
                    $product_images[] = json_decode(urldecode($product_image));
                endforeach;
            ?>
            var sProductImages = '<?php echo json_encode($product_images); ?>';
            console.log(sProductImages)
        @endif
    </script>
    <script type="text/javascript" src="{{ asset('public/proui-backend/js/helpers/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/products.js') }}"></script>
@endsection