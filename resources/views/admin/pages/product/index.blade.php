@extends('admin.pages.index')

@section('content')
    <script>
        var deleteProductURI = "{{ route('admin.products.delete', '') }}";
    </script>
    @if (auth()->user()->can('Create Product'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ url('admin/products/create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Service</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i
                                    class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2><i class="fa fa-shopping-cart sidebar-nav-icon"></i>&nbsp;<strong>Services</strong></h2>
        </div>
        <div class="alert alert-info alert-dismissable product-empty {{$products->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No services found.
        </div>
        <div class="table-responsive {{$products->count() == 0 ? 'johnCena' : '' }}">
            <table id="products-table"
                   class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    {{--<th class="text-center">--}}
                        {{--Code--}}
                    {{--</th>--}}
                    <th class="text-left">
                        Name
                    </th>
                    <th class="text-left">
                        Service Categories
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr data-product-template-id="{{$product->id}}">
                        <td class="text-center">{{ $product->id }}</td>
                        {{--<td class="text-center"><strong>{{ $product->code }}</strong></td>--}}
                        <td class="text-left">{{ $product->name }}</td>
                        <td class="text-left">{{ $product->product_categories()->pluck('name')->implode(', ') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Product'))
                                    <a href="{{ route('admin.products.edit', $product->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Product'))
                                    <a id="delete-product-btn" href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-product-btn"
                                       data-original-title="Delete"
                                       data-product-id="{{ $product->id }}"
                                       data-product-route="{{ route('admin.products.delete', $product->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/products.js') }}"></script>
@endsection