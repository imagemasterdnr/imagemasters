<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-archive"></i> <strong>Service Categories</strong></h2>
    </div>
    <div class="alert alert-info alert-dismissable product-category-empty {{$product_categories->count() == 0 ? '' : 'johnCena' }}">
        <i class="fa fa-info-circle"></i> No service categories found.
    </div>
    @if ($product_categories->count())
        <div class="form-group">
            <div class="col-md-12">
                @foreach($product_categories as $product_category)
                    <div class="panel-group" data-product-category-template-id="{{$product_category->id}}">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    {{ Form::checkbox('product_categories[]', $product_category->id, (!empty($product) && !empty($product->product_categories) ? ($product->product_categories->contains('id', $product_category->id)) : 0), ["class" => "category"]) }}
                                    <a data-toggle="collapse" href="#product-category-{{ $product_category->id }}">
                                        <strong>{{ $product_category->name }}</strong>
                                    </a>
                                </h4>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
