@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/users') }}">Users</a></li>
        <li><span href="javascript:void(0)">Add New User</span></li>
    </ul>
    <div class="row">
        <div class="block">
            <div class="block-title">
                <h2><i class="fa fa-pencil"></i> <strong>Add new User</strong></h2>
            </div>
            {{  Form::open([
                'method' => 'POST',
                'id' => 'create-user',
                'route' => ['admin.users.store'],
                'class' => 'form-horizontal form-bordered'
                ])
            }}
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="first_name">Firstname</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="first_name" name="first_name"
                           value="{{ old('first_name') }}"
                           placeholder="Enter firstname..">
                    @if($errors->has('first_name'))
                        <span class="help-block animation-slideDown">{{ $errors->first('first_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="last_name">Lastname</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="last_name" name="last_name"
                           value="{{ old('last_name') }}"
                           placeholder="Enter lastname..">
                    @if($errors->has('last_name'))
                        <span class="help-block animation-slideDown">{{ $errors->first('last_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="user_name">Username</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="user_name" name="user_name"
                           value="{{ old('user_name') }}"
                           placeholder="Enter username..">
                    @if($errors->has('user_name'))
                        <span class="help-block animation-slideDown">{{ $errors->first('user_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="email">Email</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="email" name="email"
                           value="{{ old('email') }}"
                           placeholder="Enter email..">
                    @if($errors->has('email'))
                        <span class="help-block animation-slideDown">{{ $errors->first('email') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="password">Password</label>

                <div class="col-md-9">
                    <input type="password" class="form-control" id="password" name="password"
                           placeholder="Enter password..">
                    @if($errors->has('password'))
                        <span class="help-block animation-slideDown">{{ $errors->first('password') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="password_confirmation">Password Confirmation</label>

                <div class="col-md-9">
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                           placeholder="Verify Password..">
                </div>
            </div>
            <div class="form-group{{ $errors->has('photographer_calendar_id') ? ' has-error' : '' }} calendar-id-container" style="display: none;">
                <label class="col-md-3 control-label" for="photographer_calendar_id">Calendar ID</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="photographer_calendar_id" name="photographer_calendar_id"
                           value="{{ old('photographer_calendar_id') }}"
                           placeholder="Enter calendar id..">
                    @if($errors->has('photographer_calendar_id'))
                        <span class="help-block animation-slideDown">{{ $errors->first('photographer_calendar_id') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="roles">Assign Roles</label>
                <div class="col-md-9">
                    @if(!$roles->isEmpty())
                        <h4></h4>
                        @foreach ($roles as $role)
                            @if ($role->id != 1 && $role->id != 4)
                                {{ Form::radio('roles[]', $role->id, true) }}
                                {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3">
                    <a href="{{ url('admin/users') }}" class="btn btn-sm btn-warning">Cancel</a>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/users.js') }}"></script>
@endsection