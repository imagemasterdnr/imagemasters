@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/users') }}">Users</a></li>
        <li><span href="javascript:void(0)">View User</span></li>
    </ul>

    <div class="row">
        <div class="{{ $user->hasAnyRole('Customer') ? 'col-lg-5' : 'col-lg-12' }}">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-file-o"></i> <strong>User</strong> Info</h2>
                    @if (!$user->hasAnyRole('Customer'))
                        <div class="block-options pull-right">
                            <a href="{{ route('admin.users.edit', $user->id) }}" class="label label-default"><strong><i class="fa fa-pencil"></i> Edit</strong></a>
                        </div>
                    @endif
                </div>
                <div class="block-section text-center">
                    <a href="javascript:void(0)">
                    </a>
                    <img src="{{ asset('public/proui-backend/img/placeholders/avatars/avatar4@2x.jpg') }}" alt="avatar" class="img-circle">
                    <h3>
                        <strong>{{ $user->first_name.' '.$user->last_name }}</strong><br><small></small>
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                    <tr>
                        <td class="text-right" style="width: 50%;"><strong>Roles</strong></td>
                        <td>{{ $user->roles()->pluck('name')->implode(', ') }}</td>
                    </tr>
                    @if (!$user->hasAnyRole('Customer'))
                        <tr>
                            <td class="text-right"><strong>Username</strong></td>
                            <td>{{ $user->user_name }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td class="text-right"><strong>Email</strong></td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Creation Date</strong></td>
                        <td>{{ $user->created_at->format('F d, Y h:i:s a') }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Last Visit</strong></td>
                        <td>{{ date('F d, Y h:i:s a', strtotime($user->last_login)) }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Status</strong></td>
                        <td>
                            @if ($user->is_active)
                                <span class="label label-success"><i class="fa fa-check"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-times"></i> Inactive</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="{{ $user->hasAnyRole('Customer') ? 'col-lg-7' : 'johnCena' }}">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-calendar"></i> <strong>Appointments</strong></h2>
                </div>
                <div class="alert alert-info alert-dismissable appointment-empty {{ count($appointments) == 0 ? '' : 'johnCena' }}">
                    <i class="fa fa-info-circle"></i> No appointments found.
                </div>
                <div class="table-responsive {{ count($appointments) == 0 ? 'johnCena' : '' }}">
                    <table id="appointments-table"
                           class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr role="row">
                            <th class="text-left">
                                Appointment Date
                            </th>
                            <th class="text-left">
                                Name
                            </th>
                            <th class="text-left">
                                Email
                            </th>
                            <th class="text-center">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($appointments as $appointment)
                            <tr data-appointment-template-id="{{ $appointment->id }}">
                                <td class="text-left">{{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}</td>
                                <td class="text-left">{{ $appointment->user->first_name . ' ' . $appointment->user->last_name }}</td>
                                <td class="text-left">{{ $appointment->user->email }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-xs">
                                        <a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}"
                                           data-toggle="tooltip"
                                           title=""
                                           class="btn btn-default"
                                           data-original-title="View Appointment Info"><i class="fa fa-eye"></i></a>
                                        @if (auth()->user()->hasAnyRole(['Photographer']))
                                            @if (auth()->user()->photographer_calendar_id == $appointment->calendar_id)
                                                <a href="{{ route('admin.appointments.add_attachments.get', $appointment->appointment_id) }}"
                                                   data-toggle="tooltip"
                                                   title=""
                                                   class="btn btn-default"
                                                   data-original-title="Add Photos"><i class="fa fa-file-photo-o"></i></a>
                                                <a href="{{ route('admin.appointments.edit_property_details', $appointment->appointment_id) }}"
                                                   data-toggle="tooltip"
                                                   title=""
                                                   class="btn btn-default"
                                                   data-original-title="Edit Property Details"><i class="fa fa-pencil"></i></a>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/users.js') }}"></script>
@endsection