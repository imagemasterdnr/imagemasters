@include('admin.pages.template.template_start')
@include('admin.pages.template.page_head')

<div class="page-content" id="page-content">
    @yield('content')
</div>

@include('admin.pages.template.page_footer')
@include('admin.pages.template.template_scripts')
@include('admin.pages.template.template_end')
@include('admin.pages.flash')