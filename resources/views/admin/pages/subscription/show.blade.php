@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/subscriptions') }}">Subscriptions</a></li>
        <li><span href="javascript:void(0)">View Subscription</span></li>
    </ul>
    <div class="row">
        <div class="col-lg-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-envelope"></i> <strong>Subscription</strong> Info</h2>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Date</strong></td>
                        <td style="width: 70%">{{ $subscription->created_at->format('F d, Y h:i:s A') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Name</strong></td>
                        <td style="width: 70%">{{ $subscription->first_name . ' ' . $subscription->last_name }}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Email</strong></td>
                        <td style="width: 70%">{{ $subscription->email }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/subscriptions.js') }}"></script>
@endsection