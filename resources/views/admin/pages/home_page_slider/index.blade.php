@extends('admin.pages.index')

@section('content')
    <script>
        var deleteHomePageSliderURI = "{{ route('admin.home_page_sliders.delete', '') }}";
    </script>
    @if (auth()->user()->can('Create Home Page Slider'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ url('admin/home_page_sliders/create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Home Page Slider</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i
                                    class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2><i class="fa fa-sliders sidebar-nav-icon"></i>&nbsp;<strong>Home Page Sliders</strong></h2>
        </div>
        <div class="alert alert-info alert-dismissable home-page-slider-empty {{$home_page_sliders->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No home page sliders found.
        </div>
        <div class="table-responsive {{$home_page_sliders->count() == 0 ? 'johnCena' : '' }}">
            <table id="home-page-sliders-table"
                   class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-left">
                        Name
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($home_page_sliders as $home_page_slider)
                    <tr data-home-page-slider-template-id="{{$home_page_slider->id}}">
                        <td class="text-center">{{ $home_page_slider->id }}</td>
                        <td class="text-left">{{ $home_page_slider->name }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Home Page Slider'))
                                    <a href="{{ route('admin.home_page_sliders.edit', $home_page_slider->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Home Page Slider'))
                                    <a id="delete-home-page-slider-btn" href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-home-page-slider-btn"
                                       data-original-title="Delete"
                                       data-home-page-slider-id="{{ $home_page_slider->id }}"
                                       data-home-page-slider-route="{{ route('admin.home_page_sliders.delete', $home_page_slider->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/home_page_sliders.js') }}"></script>
@endsection