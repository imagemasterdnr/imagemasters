@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/home_page_sliders') }}">Home Page Sliders</a></li>
        <li><span href="javascript:void(0)">Add New Home Page Slider</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'POST',
            'id' => 'create-home-page-slider',
            'route' => ['admin.home_page_sliders.store'],
            'class' => 'form-horizontal ',
            'files' => true
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Add new Home Page Slider</strong></h2>
                </div>
                {{--<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">--}}
                    {{--<label class="col-md-3 control-label" for="home_page_slider_code">Code</label>--}}

                    {{--<div class="col-md-9">--}}
                        {{--<input type="text" class="form-control" id="home_page_slider_code" name="code" value="{{ old('code') }}"--}}
                               {{--placeholder="Enter home_page_slider code..">--}}
                        {{--@if($errors->has('code'))--}}
                            {{--<span class="help-block animation-slideDown">{{ $errors->first('code') }}</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_page_slider_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="home_page_slider_name" name="name" value="{{ old('name') }}"
                               placeholder="Enter home page slider name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="file">Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Choose File <input type="file" name="file" style="display: none;">
                            </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('file'))
                            <span class="help-block animation-slideDown">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="description">Description</label>

                    <div class="col-md-9">
                        <textarea id="description" name="description" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter home page slider description..">{{ old('description') }}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ old('is_active') ? 'checked' : '' }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ url('admin/home_page_sliders') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/proui-backend/js/helpers/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/home_page_sliders.js') }}"></script>
@endsection