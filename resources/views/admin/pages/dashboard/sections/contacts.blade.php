@if (count($contacts) == 0)
    <div class="alert alert-info alert-dismissable contact-empty">
        <i class="fa fa-info-circle"></i> No contacts found.
    </div>
@else
    <div class="table-responsive {{$contacts->count() == 0 ? 'johnCena' : '' }}">
        <table id="contacts-table"
               class="table table-bordered table-striped table-vcenter">
            <thead>
            <tr role="row">
                <th class="text-left">
                    Date
                </th>
                <th class="text-left">
                    Name
                </th>
                <th class="text-left">
                    Email
                </th>
                <th class="text-center">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($contacts as $contact)
                <tr data-contact-template-id="{{$contact->id}}">
                    <td class="text-left">{{ $contact->created_at->format('F d, Y h:i:s A') }}</td>
                    <td class="text-left">{{ $contact->name }}</td>
                    <td class="text-left">{{ $contact->email }}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.contacts.show', $contact->id) }}"
                               data-toggle="tooltip"
                               title=""
                               class="btn btn-default"
                               data-original-title="View"><i class="fa fa-eye"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif