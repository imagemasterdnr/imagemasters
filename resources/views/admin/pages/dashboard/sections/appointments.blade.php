@if (count($appointments) == 0)
    <div class="alert alert-info alert-dismissable request-sample-empty">
        <i class="fa fa-info-circle"></i> No appointments found.
    </div>
@else
    <div class="table-responsive {{$appointments->count() == 0 ? 'johnCena' : '' }}">
        <table id="appointments-table"
               class="table table-bordered table-striped table-vcenter">
            <thead>
            <tr role="row">
                <th class="text-left">
                    Appointment Date
                </th>
                <th class="text-left">
                    Name
                </th>
                <th class="text-left">
                    Email
                </th>
                <th class="text-center">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($appointments as $appointment)
                <tr data-appointment-template-id="{{ $appointment->id }}">
                    <td class="text-left">{{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}</td>
                    <td class="text-left">{{ $appointment->user->first_name . ' ' . $appointment->user->last_name }}</td>
                    <td class="text-left">{{ $appointment->user->email }}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}"
                               data-toggle="tooltip"
                               title=""
                               class="btn btn-default"
                               data-original-title="View Appointment Info"><i class="fa fa-eye"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif