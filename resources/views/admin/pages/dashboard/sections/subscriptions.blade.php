@if (count($subscriptions) == 0)
    <div class="alert alert-info alert-dismissable subscription-empty">
        <i class="fa fa-info-circle"></i> No subscriptions found.
    </div>
@else
    <div class="table-responsive {{$subscriptions->count() == 0 ? 'johnCena' : '' }}">
        <table id="subscriptions-table"
               class="table table-bordered table-striped table-vcenter">
            <thead>
            <tr role="row">
                <th class="text-left">
                    Date
                </th>
                <th class="text-left">
                    Name
                </th>
                <th class="text-left">
                    Email
                </th>
                <th class="text-center">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($subscriptions as $subscription)
                <tr data-subscription-template-id="{{$subscription->id}}">
                    <td class="text-left">{{ $subscription->created_at->format('F d, Y h:i:s A') }}</td>
                    <td class="text-left">{{ $subscription->first_name . ' ' . $subscription->last_name }}</td>
                    <td class="text-left">{{ $subscription->email }}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.subscriptions.show', $subscription->id) }}"
                               data-toggle="tooltip"
                               title=""
                               class="btn btn-default"
                               data-original-title="View"><i class="fa fa-eye"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif