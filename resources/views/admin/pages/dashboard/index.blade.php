@extends('admin.pages.index')

@section('content')
    <div class="row text-center">
        <div class="col-sm-4 text-center">
            <div href="javascript:void(0)" class="widget ">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>All Appointments</strong></h4>
                </div>
                <div class="widget-extra-full"><span
                            class="h2 themed-color-dark animation-expandOpen">{{ (!empty($all_appointments_count) ? $all_appointments_count : 0) }}</span>
                </div>
            </div>
        </div>
        <div class="col-sm-4 text-center">
            <div href="javascript:void(0)" class="widget ">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>All Contacts</strong></h4>
                </div>
                <div class="widget-extra-full"><span
                            class="h2 themed-color-dark animation-expandOpen">{{ (!empty($all_contacts_count) ? $all_contacts_count : 0) }}</span>
                </div>
            </div>
        </div>
        <div class="col-sm-4 text-center">
            <div href="javascript:void(0)" class="widget ">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>All Subscriptions</strong></h4>
                </div>
                <div class="widget-extra-full"><span
                            class="h2 themed-color-dark animation-expandOpen">{{ (!empty($all_subscriptions_count) ? $all_subscriptions_count : 0) }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-calendar"></i> <strong>Recent Appointments</strong></h2>
                </div>
                @include('admin.pages.dashboard.sections.appointments')
            </div>
        </div>
        <div class="col-sm-6">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-phone"></i> <strong>Recent Contacts</strong></h2>
                </div>
                @include('admin.pages.dashboard.sections.contacts')
            </div>
        </div>
        <div class="col-sm-6">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-envelope"></i> <strong>Recent Subscriptions</strong></h2>
                </div>
                @include('admin.pages.dashboard.sections.subscriptions')
            </div>
        </div>
    </div>
@endsection
