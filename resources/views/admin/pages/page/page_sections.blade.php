@foreach($page->page_sections as $section)
    @if ($section->type == 'textarea')
        <div class="form-group{{ $errors->has($section->section) ? ' has-error' : '' }}">
            <label class="col-md-3 control-label"
                   for="page_sections[{{ $section->id }}]">{{ ucwords(str_replace('_', ' ', $section->section)) }}</label>
            <div class="col-md-9">
                                <textarea id="page_sections[{{ $section->id }}]"
                                          name="page_sections[{{ $section->id }}]" rows="9"
                                          class="form-control" style="resize: vertical; min-height: 150px;"
                                          placeholder="Enter page {{ str_replace('_', ' ', $section->section) }}..">{!! $section->content !!}</textarea>
            </div>
        </div>
    @elseif ($section->type == 'ckeditor')
        <div class="form-group{{ $errors->has($section->section) ? ' has-error' : '' }}">
            <label class="col-md-3 control-label"
                   for="page_sections[{{ $section->id }}]">{{ ucwords(str_replace('_', ' ', $section->section)) }}</label>
            <div class="col-md-9">
                            <textarea id="page_sections[{{ $section->id }}]"
                                      name="page_sections[{{ $section->id }}]" rows="9"
                                      class="form-control ckeditor"
                                      placeholder="Enter page {{ str_replace('_', ' ', $section->section) }}..">{!! $section->content !!}</textarea>
            </div>
        </div>
    @elseif ($section->type == 'input')
        <div class="form-group{{ $errors->has($section->section) ? ' has-error' : '' }}">
            <label class="col-md-3 control-label"
                   for="page_sections[{{ $section->id }}]">{{ ucwords(str_replace('_', ' ', $section->section)) }}</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="page_sections[{{ $section->id }}]"
                       name="page_sections[{{ $section->id }}]"
                       placeholder="Enter page {{ str_replace('_', ' ', $section->section) }}.."
                       value="{{ $section->content }}">
            </div>
        </div>
    @elseif ($section->type == 'file' || $section->type == 'video')
        <div class="form-group{{ $errors->has($section->section) ? ' has-error' : '' }}">
            <label class="col-md-3 control-label"
                   for="page_sections[{{ $section->id }}]">{{ ucwords(str_replace('_', ' ', $section->section)) }}</label>
            <div class="col-md-9">
                <div class="input-group">
                    <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                                Choose File <input type="file" name="page_sections[{{ $section->id }}]"
                                                                   style="display: none;">
                                            </span>
                    </label>
                    <input type="text" class="form-control" readonly>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label"
                   for="page_sections[{{ $section->id }}]">&nbsp;</label>
            <div class="col-md-9">
                @if ($section->type == 'video')
                    <div class="gallery" data-toggle="lightbox-gallery-video">
                        <a href="{{ asset($section->content) }}" class="zoom img-thumbnail gallery-link"
                           style="cursor: default !important;">
                            <img src="{{ asset($section->content) }}" data-video="1" alt="{{ $section->content}}"
                                 class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                    </div>
                @else
                    <a href="{{ asset($section->content) }}" class="zoom img-thumbnail"
                       style="cursor: default !important;" data-toggle="lightbox-image">
                        <img src="{{ asset($section->content) }}" alt="{{ $section->content}}"
                             class="img-responsive center-block" style="max-width: 100px;">
                    </a>
                @endif
            </div>
        </div>
    @endif
    @if($errors->has($section->section))
        <span class="help-block animation-slideDown">{{ $errors->first($section->section) }}</span>
    @endif
@endforeach