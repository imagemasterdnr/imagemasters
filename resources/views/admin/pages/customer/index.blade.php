@extends('admin.pages.index')

@section('content')
    <script>

    </script>
    <div class="block full">
        <div class="block-title">
            <h2><i class="fa fa-users sidebar-nav-icon"></i>&nbsp;<strong>Customers</strong></h2>
        </div>
        <div class="alert alert-info alert-dismissable customer-empty {{$customers->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No customers found.
        </div>
        <div class="table-responsive {{$customers->count() == 0 ? 'johnCena' : '' }}">
            <table id="customers-table"
                   class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-left">
                        Name
                    </th>
                    <th class="text-left">
                        Email
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr data-customer-template-id="{{$customer->id}}">
                        <td class="text-left"><strong>{{ $customer->first_name.' '.$customer->last_name }}</strong></td>
                        <td class="text-left">{{ $customer->email }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Read User'))
                                    @if (!$customer->hasAnyRole(['Super Admin', 'Admin', 'Photographer']))
                                        <a href="{{ route('admin.customers.show', $customer->id) }}"
                                           data-toggle="tooltip"
                                           title=""
                                           class="btn btn-default"
                                           data-original-title="View"><i class="fa fa-eye"></i></a>
                                    @endif
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('node_modules/jszip/dist/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/pdfmake/build/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/pdfmake/build/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/customers.js') }}"></script>
@endsection