@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/appointments') }}">Appointments</a></li>
        <li><span href="javascript:void(0)">View Appointment</span></li>
    </ul>
    <div class="row">
        <div class="col-lg-5">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-user"></i> <strong>User</strong> Info</h2>
                </div>
                <div class="block-section text-center">
                    <a href="javascript:void(0)">
                    </a>
                    @if ($appointment->user->profile_image != '')
                        <img src="{{ asset($appointment->user->profile_image) }}" alt="avatar" class="img-thumbnail" style="height: 138px;">
                    @else
                        <img src="{{ asset('public/proui-backend/img/placeholders/avatars/avatar4@2x.jpg') }}"
                             alt="avatar" class="img-thumbnail">
                    @endif
                    <h3>
                        <strong>{{ $appointment->user->first_name.' '.$appointment->user->last_name }}</strong><br>
                        <small></small>
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                    <tr>
                        <td class="text-right"><strong>Email</strong></td>
                        <td>{{ $appointment->user->email }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Appointment Date</strong></td>
                        <td>{!! $appointment['acuity_details']['date']  . ' ' . $appointment['acuity_details']['time'] !!}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Last Visit</strong></td>
                        <td>{{ date('F d, Y h:i:s a', strtotime($appointment->user->last_login)) }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Status</strong></td>
                        <td>
                            @if ($appointment->user->is_active)
                                <span class="label label-success"><i class="fa fa-check"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-times"></i> Inactive</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-file-photo-o"></i>
                        <strong>
                            Images
                        </strong>
                    </h2>
                    @if (auth()->user()->hasAnyRole(['Photographer']))
                        @if (auth()->user()->photographer_calendar_id == $appointment->calendar_id && $appointment->status != 2)
                            @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']))
                                @if(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString())
                                    <div class="block-options pull-right">
                                        <a href="{{ route('admin.appointments.add_attachments.get', $appointment->appointment_id) }}"
                                           class="label label-default"><strong><i class="fa fa-file-photo-o"></i> Add
                                                Photos</strong></a>
                                    </div>
                                @endif
                            @endif
                        @endif
                    @endif
                </div>
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                        @if (!empty($appointment->attachments) && count($appointment->attachments))
                            @if (!empty($appointment->attachments) && count($appointment->attachments))
                                @foreach ($appointment->attachments()->orderby('is_default', 'desc')->get() as $attachment_key => $attachment)
                                    <div class="col-xs-6 col-sm-3 ">
                                        <a href="{{ asset($attachment->file) }}" class="gallery-link"
                                           title="{{ $attachment->name }}">
                                            <img {{ $attachment->extension == 'mp4' ? "data-video=1" : '' }} src="{{ asset($attachment->file) }}"
                                                 alt="{{ $attachment->alt }}"
                                                 class="img-responsive img-thumbnail center-block" height="110"
                                                 width="110"
                                                 style="max-width: 110px;">
                                        </a>
                                    </div>
                                    @if (($attachment_key + 1) % 4 === 0)
                                        <div class="clearfix hidden-xs"></div>
                                    @endif
                                    @if (($attachment_key + 1) % 2 === 0)
                                        <div class="clearfix visible-xs"></div>
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-calendar"></i> <strong>Acuity</strong> Info</h2>
                    @if (!auth()->user()->hasAnyRole(['Photographer']))
                        @if ($appointment->status != 2)
                            @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']))
                                @if(!(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                                    <div class="block-options pull-right">
                                        <a id="cancel-appointment-btn" href="javascript:void(0)" data-toggle="tooltip"
                                           title=""
                                           class="label label-danger cancel-appointment-btn"
                                           data-original-title="Cancel"
                                           data-appointment-id="{{ $appointment->appointment_id }}"
                                           data-appointment-route="{{ route('admin.appointments.cancel_appointment', $appointment->appointment_id) }}">
                                            <i class="fa fa-times"></i> Cancel Appointment
                                        </a>
                                    </div>
                                @endif
                            @endif
                        @endif
                    @endif
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    {{--
                    ID
                    First Name
                    Last Name
                    Phone
                    Email
                    Date
                    Time
                    Endtime
                    Price
                    Paid
                    Amount Paid
                    Type
                    Category
                    Calendar
                    --}}
                    <tbody>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Appointment ID</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['id'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Status</strong></td>
                        <td style="width: 70%">
                            @if ($appointment->status != 2)
                                @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                                    && !(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                                    Ongoing
                                @else
                                    Completed
                                @endif
                            @else
                                @if ($appointment['acuity_details']['canceled'])
                                    Canceled
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>First Name</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['firstName'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Last Name</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['lastName'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Phone</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['phone'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Email</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['email'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Date</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['date'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Time</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['time'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Endtime</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['endTime'] !!}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Date Created</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['dateCreated'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Datetime Created</strong></td>--}}
                    {{--<td style="width: 70%">{!! date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetimeCreated'])) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Datetime</strong></td>--}}
                    {{--<td style="width: 70%">{!! date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Price</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['price'] !!}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Price Sold</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['priceSold'] !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Paid</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['paid'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Amount Paid</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['amountPaid'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Type</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['type'] !!}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Appointment Type ID</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['appointmentTypeID'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Class ID</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['classID'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Addon IDs</strong></td>--}}
                    {{--<td style="width: 70%">{!! json_encode($appointment['acuity_details']['addonIDs']) !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Category</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['category'] !!}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Duration</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['duration'] !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Calendar</strong></td>
                        <td style="width: 70%">{!! $appointment['acuity_details']['calendar'] !!}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Calendar ID</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['calendarID'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Certificate</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['certificate'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Confirmation Page</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['confirmationPage'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Location</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['location'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Notes</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['notes'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Timezone</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['timezone'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Calendar Timezone</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['calendarTimezone'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Canceled</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['canceled'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Can Client Cancel</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['canClientCancel'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Can Client Reschedule</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['canClientReschedule'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Labels</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['labels'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Forms</strong></td>--}}
                    {{--<td style="width: 70%">{!! json_encode($appointment['acuity_details']['forms']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Forms Text</strong></td>--}}
                    {{--<td style="width: 70%">{!! nl2br($appointment['acuity_details']['formsText']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Is Verified</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['isVerified'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td style="width: 30%" class="text-right"><strong>Schedule By</strong></td>--}}
                    {{--<td style="width: 70%">{!! $appointment['acuity_details']['scheduledBy'] !!}</td>--}}
                    {{--</tr>--}}
                    </tbody>
                </table>
            </div>
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-home"></i> <strong>Property</strong> Details</h2>
                    @if (auth()->user()->hasAnyRole(['Photographer']))
                        @if (auth()->user()->photographer_calendar_id == $appointment->calendar_id && $appointment->status != 2)
                            @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']))
                                @if(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString())
                                    <div class="block-options pull-right">
                                        <a href="{{ route('admin.appointments.edit_property_details', $appointment->appointment_id) }}"
                                           class="label label-default"><strong><i class="fa fa-pencil"></i> Edit Property Details</strong></a>
                                    </div>
                                @endif
                            @endif
                        @endif
                    @endif
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Beds</strong></td>
                        <td style="width: 70%">{!! $appointment['beds'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Baths</strong></td>
                        <td style="width: 70%">{!! $appointment['baths'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Availability Status</strong></td>
                        <td style="width: 70%">{!! $appointment['availability_status'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Square Feet</strong></td>
                        <td style="width: 70%">{!! $appointment['square_feet'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Address</strong></td>
                        <td style="width: 70%">{!! $appointment['address'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Google Map Iframe Link</strong></td>
                        <td style="width: 70%">{!! $appointment['google_map_link'] !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 30%" class="text-right"><strong>Video Link</strong></td>
                        <td style="width: 70%">{!! $appointment['video_link'] !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/node_modules/frame-grab.js/client/lib/rsvp.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/node_modules/frame-grab.js/client/frame-grab.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/appointments.js') }}"></script>
@endsection