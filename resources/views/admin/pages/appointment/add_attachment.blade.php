@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/appointments') }}">Appointments</a></li>
        <li><a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}">Appointment ID: {{ $appointment->appointment_id }}</a></li>
        <li><span href="javascript:void(0)">Add Appointment Attachments</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
             'method' => 'POST',
             'id' => 'add-attachment',
             'route' => ['admin.appointments.add_attachments.post', $appointment->id],
             'class' => 'form-horizontal ',
             'files' => true
             ])
        }}
        <div class="col-lg-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-file-photo-o"></i> <strong>Photos
                            "Appointment ID: {{ $appointment->appointment_id }}"</strong></h2>
                </div>
                <div class="form-group">
                    <div id="upload-image" class="">
                        <div class="dz-message" data-dz-message style="margin: 10px 0 !important;">
                            <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                            <div>Drag & drop files here
                                <br/> or
                            </div>
                            <span id="file-upload-btn" class="btn btn-primary">Select a file</span>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple/>
                        </div>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-12 text-right">
                        <a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}"
                           class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('extrascripts')
    <script>
        @if (!empty($appointment->attachments))
            var sAppointmentImages = '<?php echo $appointment->attachments()->get()->toJson(); ?>';
        @endif
    </script>
    <script type="text/javascript" src="{{ asset('public/node_modules/frame-grab.js/client/lib/rsvp.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/node_modules/frame-grab.js/client/frame-grab.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/appointments.js') }}"></script>
@endsection