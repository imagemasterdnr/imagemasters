@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/appointments') }}">Appointments</a></li>
        <li><a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}">Appointment
                ID: {{ $appointment->appointment_id }}</a></li>
        <li><span href="javascript:void(0)">Edit Property Detail</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
             'method' => 'POST',
             'id' => 'edit-property-detail',
             'route' => ['admin.appointments.update_property_details', $appointment->id],
             'class' => 'form-horizontal ',
             'files' => true
             ])
        }}
        <div class="col-lg-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit roperty Detail
                            "Appointment ID: {{ $appointment->appointment_id }}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('beds') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="appointment_beds">Beds</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="appointment_beds" name="beds"
                               value="{{ Request::old('beds') ? : $appointment->beds }}"
                               placeholder="Enter appointment beds..">
                        @if($errors->has('beds'))
                            <span class="help-block animation-slideDown">{{ $errors->first('beds') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('baths') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="appointment_baths">Baths</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="appointment_baths" name="baths"
                               value="{{ Request::old('baths') ? : $appointment->baths }}"
                               placeholder="Enter appointment baths..">
                        @if($errors->has('baths'))
                            <span class="help-block animation-slideDown">{{ $errors->first('baths') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('availability_status') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="appointment_availability_status">Availability Status</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="appointment_availability_status" name="availability_status"
                               value="{{ Request::old('availability_status') ? : $appointment->availability_status }}"
                               placeholder="Enter appointment availability status..">
                        @if($errors->has('availability_status'))
                            <span class="help-block animation-slideDown">{{ $errors->first('availability_status') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('square_feet') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="appointment_square_feet">Square Feet</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="appointment_square_feet" name="square_feet"
                               value="{{ Request::old('square_feet') ? : $appointment->square_feet }}"
                               placeholder="Enter appointment square feet..">
                        @if($errors->has('square_feet'))
                            <span class="help-block animation-slideDown">{{ $errors->first('square_feet') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="address">Address</label>

                    <div class="col-md-9">
                        <textarea id="address" name="address" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter appointment address..">{!! Request::old('address') ? : $appointment->address !!}</textarea>
                        @if($errors->has('address'))
                            <span class="help-block animation-slideDown">{{ $errors->first('address') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('google_map_link') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="google_map_link">Google Map Iframe Link</label>

                    <div class="col-md-9">
                        <textarea id="google_map_link" name="google_map_link" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter appointment google map link..">{!! Request::old('google_map_link') ? : $appointment->google_map_link !!}</textarea>
                        @if($errors->has('google_map_link'))
                            <span class="help-block animation-slideDown">{{ $errors->first('google_map_link') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Branded?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_branded" name="is_branded"
                                   value="1" {{ Request::old('is_branded') ? : ($appointment->is_branded ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('video_link') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="video_link">Video Link</label>

                    <div class="col-md-9">
                        <textarea id="video_link" name="video_link" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter appointment google map link..">{!! Request::old('video_link') ? : $appointment->video_link !!}</textarea>
                        @if($errors->has('video_link'))
                            <span class="help-block animation-slideDown">{{ $errors->first('video_link') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-12 text-right">
                        <a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}"
                           class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/appointments.js') }}"></script>
@endsection