@extends('admin.pages.index')

@section('content')
    <div class="block full">
        <div class="block-title">
            <h2><i class="fa fa-calendar sidebar-nav-icon"></i>&nbsp;<strong>Appointments</strong></h2>
        </div>
        <div class="alert alert-info alert-dismissable appointment-empty {{ count($appointments) == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No appointments found.
        </div>
        <div class="table-responsive {{ count($appointments) == 0 ? 'johnCena' : '' }}">
            <table id="appointments-table"
                   class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-left">
                        Appointment Date
                    </th>
                    <th class="text-left">
                        Appointment ID
                    </th>
                    <th class="text-left">
                        Name
                    </th>
                    <th class="text-left">
                        Email
                    </th>
                    <th class="text-left">
                        Status
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($appointments as $appointment)
                    <tr data-appointment-template-id="{{ $appointment->id }}">
                        <td class="text-left">{{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}</td>
                        <td class="text-left">{{ $appointment->appointment_id }}</td>
                        <td class="text-left">{{ $appointment->user->first_name . ' ' . $appointment->user->last_name }}</td>
                        <td class="text-left">{{ $appointment->user->email }}</td>
                        <td class="text-left">
                            @if ($appointment->status != 2)
                                @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                                    && !(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                                    Ongoing
                                @else
                                    Completed
                                @endif
                            @else
                                @if ($appointment['acuity_details']['canceled'])
                                    Canceled
                                @endif
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                <a href="{{ route('admin.appointments.show', $appointment->appointment_id) }}"
                                   data-toggle="tooltip"
                                   title=""
                                   class="btn btn-default"
                                   data-original-title="View Appointment Info"><i class="fa fa-eye"></i></a>
                                @if (auth()->user()->hasAnyRole(['Photographer']))
                                    @if (auth()->user()->photographer_calendar_id == $appointment->calendar_id && $appointment->status != 2)
                                        @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']))
                                            @if(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString())
                                                <a href="{{ route('admin.appointments.add_attachments.get', $appointment->appointment_id) }}"
                                                   data-toggle="tooltip"
                                                   title=""
                                                   class="btn btn-default"
                                                   data-original-title="Add Photos"><i class="fa fa-file-photo-o"></i></a>
                                                <a href="{{ route('admin.appointments.edit_property_details', $appointment->appointment_id) }}"
                                                   data-toggle="tooltip"
                                                   title=""
                                                   class="btn btn-default"
                                                   data-original-title="Edit Property Details"><i class="fa fa-pencil"></i></a>
                                            @endif
                                        @endif
                                    @endif
                                @else
                                    @if ($appointment->status != 2)
                                        @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                                            && !(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                                            <a id="cancel-appointment-btn" href="javascript:void(0)"
                                               data-toggle="tooltip"
                                               title=""
                                               class="btn btn-xs btn-danger cancel-appointment-btn"
                                               data-original-title="Cancel"
                                               data-appointment-id="{{ $appointment->appointment_id }}"
                                               data-appointment-route="{{ route('admin.appointments.cancel_appointment', $appointment->appointment_id) }}">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        @endif
                                    @endif
                                @endif

                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/appointments.js') }}"></script>
@endsection