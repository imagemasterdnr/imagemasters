<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>
<!-- global variables -->
<script>
    /* used in ckeditor image upload */
    var sBaseURI = '{{ url('/') }}';
    var sAdminBaseURI = '{{ url('admin') }}';
    /* used in ckeditor image upload */
    var sProductImages = null;
    var sProductCategoriesImages = null;
    var sAppointmentImages = null;
    var oPageSections = null;
    var sSettingType = null;
</script>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{asset('public/proui-backend/js/vendor/jquery.min.js')}}"></script>
<script src="{{asset('public/proui-backend/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('public/proui-backend/js/plugins.js')}}"></script>
<script src="{{asset('public/proui-backend/js/app.js')}}"></script>

<!-- Include platform js file -->
<script type="text/javascript" src="{{ asset('public/js/core/platform.js') }}"></script>
<!-- Include ajaxq js file -->
<script type="text/javascript" src="{{ asset('public/js/core/ajaxq.js') }}"></script>
<!-- Include centralAjax js file -->
<script type="text/javascript" src="{{ asset('public/js/core/centralAjax.js') }}"></script>
<!-- Include config js file -->
<script type="text/javascript" src="{{ asset('public/js/core/config.js') }}"></script>
<!-- Include main js file -->
<script type="text/javascript" src="{{ asset('public/js/libraries/main.js') }}"></script>
<!-- Include delete js file -->
<script type="text/javascript" src="{{ asset('public/js/libraries/delete.js') }}"></script>
<!-- Include sweetalert files -->
<link rel="stylesheet" type="text/css" href="{{ asset('public/node_modules/sweetalert/dist/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('public/node_modules/sweetalert/dist/sweetalert.min.js') }}"></script>
<!-- Include moment files -->
<script type="text/javascript" src="{{ asset('public/node_modules/moment/moment.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('public/node_modules/moment-timezone/builds/moment-timezone.min.js') }}"></script>--}}
<!-- Include countdown files -->
<!-- countdown only -->
{{--<script type="text/javascript" src="{{ asset('public/node_modules/jquery-countdown/dist/jquery.countdown.js') }}"></script>--}}
<!-- with other functions -->
{{--<link rel="stylesheet" type="text/css" href="{{ asset('public/node_modules/jquery.countdown.package-2.1.0/css/jquery.countdown.css') }}">--}}
{{--<script type="text/javascript" src="{{ asset('public/node_modules/jquery.countdown.package-2.1.0/js/jquery.plugin.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('public/node_modules/jquery.countdown.package-2.1.0/js/jquery.countdown.js') }}"></script>--}}
<!-- include custom timer -->
{{--<script type="text/javascript" src="{{ asset('public/js/plugins/timer.js') }}"></script>--}}

{{--<!-- Include pusher js file -->--}}
{{--<script type="text/javascript" src="{{ asset('public/js/plugins/pusher.min.js') }}"></script>--}}
{{--<!-- Include websocket js file -->--}}
{{--<script type="text/javascript" src="{{ asset('public/js/core/websocket.js') }}"></script>--}}
{{--<!-- Include websocket_events js file -->--}}
{{--<script type="text/javascript" src="{{ asset('public/js/libraries/websocket_events.js') }}"></script>--}}

@section('extrascripts')
@show