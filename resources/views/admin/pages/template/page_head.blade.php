<?php
/**
 * page_head.php
 *
 * Author: pixelcave
 *
 * Header and Sidebar of each page
 *
 */
?>

<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
    Available classes:

    'page-loading'      enables page preloader
-->
<div id="page-wrapper"{!! ($admin_template['page_preloader']) ? ' class="page-loading"' : '' !!}>
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background">
        <h1 class="push-top-bottom text-light text-center"><strong>{!! $admin_template['name'] !!}</strong></h1>
        <div class="inner">
            <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="preloader-spinner hidden-lt-ie10"></div>
        </div>
    </div>
    <!-- END Preloader -->

    <!-- Page Container -->
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
        Available #page-container classes:

        '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)

        'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
        'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
        'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
        'sidebar-mini sidebar-visible-lg-mini'          for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
        'sidebar-mini sidebar-visible-lg'               for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)

        'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
        'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
        'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)

        'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)

        'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!

        'style-alt'                                     for an alternative main style (without it: the default style)
        'footer-fixed'                                  for a fixed footer (without it: a static footer)

        'disable-menu-autoscroll'                       add this to disable the main menu auto scrolling when opening a submenu

        'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
        'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar

        'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links
    -->
    <?php
    $page_classes = '';

    if ($admin_template['header'] == 'navbar-fixed-top') :
        $page_classes = 'header-fixed-top';
    elseif ($admin_template['header'] == 'navbar-fixed-bottom') :
        $page_classes = 'header-fixed-bottom';
    endif;

    if ($admin_template['sidebar']) :
        $page_classes .= (($page_classes == '') ? '' : ' ') . $admin_template['sidebar'];
    endif;

    if ($admin_template['main_style'] == 'style-alt') :
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'style-alt';
    endif;

    if ($admin_template['footer'] == 'footer-fixed') :
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'footer-fixed';
    endif;

    if (!$admin_template['menu_scroll']) :
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'disable-menu-autoscroll';
    endif;

    if ($admin_template['cookies'] === 'enable-cookies') :
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'enable-cookies';
    endif;
    ?>
    <div id="page-container"{!! ($page_classes)? ' class="' . $page_classes . '"' : '' !!}>

        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="{{ url('admin/dashboard') }}" class="sidebar-brand">
                        <i class="gi gi-flash"></i><span
                                class="sidebar-nav-mini-hide"><strong>{{ $admin_template['name'] }}</strong></span>
                    </a>
                    <!-- END Brand -->

                    <!-- User Info -->
                    <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                        <div class="sidebar-user-avatar">
                            <a href="javascript:void(0)">
                                @if (auth()->user()->profile_image != '')
                                    <img src="{{ asset(auth()->user()->profile_image) }}" alt="avatar">
                                @else
                                <img src="{{asset('public/proui-backend/img/placeholders/avatars/avatar2.jpg')}}"
                                     alt="avatar">
                                @endif
                            </a>
                        </div>
                        <div class="sidebar-user-name">{{$logged_user->first_name}} {{$logged_user->last_name}}</div>
                        <div class="sidebar-user-links">
                            <a href="{{ url('/admin/logout') }}" data-toggle="tooltip" data-placement="bottom"
                               title="Logout"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                        class="gi gi-exit"></i></a>
                        </div>
                    </div>
                    <!-- END User Info -->

                @if ($admin_primary_nav)
                    <!-- Sidebar Navigation -->
                        <ul class="sidebar-nav">

                            <!-- END User Info -->
                            <!-- Ln 129 -->

                            @foreach( $admin_primary_nav as $key => $link )
                                <?php
                                $link_class = '';
                                $li_active = '';
                                $menu_link = '';

                                // Get 1st level link's vital info
                                $url = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
                                $active = (isset($link['url']) && (strpos($admin_template['active_page'], $link['url']) !== FALSE) && (!isset($link['never_active']))) ? ' active' : '';
                                $never_active = isset($link['never_active']) ? ' target="_blank"' : '';
                                $icon = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon"></i>' : '';

                                // Check if the link has a submenu
                                if (isset($link['sub']) && $link['sub']) :
                                    // Since it has a submenu, we need to check if we have to add the class active
                                    // to its parent li element (only if a 2nd or 3rd level link is active)
                                    foreach ($link['sub'] as $sub_link) :
                                        if (isset($sub_link['url'])) :
                                            if (strpos($admin_template['active_page'],$sub_link['url']) !== FALSE) :
                                                $li_active = ' class="active"';
                                                break;
                                            endif;
                                            if ((strpos($admin_template['active_page'], 'options') !== FALSE) && ($sub_link['name'] == 'Product Options Entries')
                                                || (strpos($admin_template['active_page'], 'quote_options') !== FALSE) && ($sub_link['name'] == 'Quote Options Entries'))
                                                :
                                                $li_active = ' class="active"';
                                                break;
                                            endif;
                                        endif;

                                        // 3rd level links
                                        if (isset($sub_link['sub']) && $sub_link['sub']) :
                                            foreach ($sub_link['sub'] as $sub2_link) :
                                                if (strpos($admin_template['active_page'], $sub2_link['url']) !== FALSE) :
                                                    $li_active = ' class="active"';
                                                    break;
                                                endif;
                                            endforeach;
                                        endif;
                                    endforeach;

                                    $menu_link = 'sidebar-nav-menu';
                                endif;

                                // Create the class attribute for our link
                                if ($menu_link || $active) :
                                    $link_class = ' class="' . $menu_link . $active . '"';
                                endif;
                                ?>
                                @if ($url == 'header')
                                    <li class="sidebar-header">
                                        @if (isset($link['opt']) && $link['opt'])
                                            <span class="sidebar-header-options clearfix">{!! $link['opt'] !!}</span>
                                        @endif
                                        <span class="sidebar-header-title">{!! $link['name'] !!}</span>
                                    </li>
                                @else
                                    <li{!! $li_active !!}>
                                        <a href="{!! $url !!}" {!! $link_class !!} {!! $never_active !!}>
                                            @if (isset($link['sub']) && $link['sub'])
                                                <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                                            @endif
                                            {!! $icon !!}
                                            <span class="sidebar-nav-mini-hide">{!! $link['name'] !!}</span>
                                        </a>
                                        @if (isset($link['sub']) && $link['sub'])
                                            <ul>
                                                @foreach ($link['sub'] as $sub_link)
                                                    <?php
                                                    $link_class = '';
                                                    $li_active = '';
                                                    $submenu_link = '';

                                                    // Get 2nd level link's vital info
                                                    $url = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                                    $active = (isset($sub_link['url']) && (strpos($admin_template['active_page'], $sub_link['url']) !== FALSE) && (!isset($sub_link['never_active'])))
                                                    || (strpos($admin_template['active_page'], 'options') !== FALSE && $sub_link['name'] == 'Product Options Entries' && $sub_link['name'] != 'Quote Options Entries' && strpos($admin_template['active_page'], 'quote_options') == FALSE)
                                                    || (strpos($admin_template['active_page'], 'quote_options') !== FALSE && $sub_link['name'] == 'Quote Options Entries')
                                                        ? ' active' : '';

                                                    // Check if the link has a submenu
                                                    if (isset($sub_link['sub']) && $sub_link['sub']) :
                                                        // Since it has a submenu, we need to check if we have to add the class active
                                                        // to its parent li element (only if a 3rd level link is active)
                                                        foreach ($sub_link['sub'] as $sub2_link) :
                                                            if (isset($sub2_link['url'])) :
                                                                if (strpos($admin_template['active_page'], $sub2_link['url']) !== FALSE) :
                                                                    $li_active = ' class="active"';
                                                                    break;
                                                                endif;
                                                            endif;
                                                        endforeach;

                                                        $submenu_link = 'sidebar-nav-submenu';
                                                    endif;

                                                    if ($submenu_link || $active) :
                                                        $link_class = ' class="' . $submenu_link . $active . '"';
                                                    endif;
                                                    ?>
                                                    <li{!! $li_active !!}>
                                                        <a href="{!! $url !!}" {!! $link_class !!}>
                                                            @if (isset($sub_link['sub']) && $sub_link['sub'])
                                                                <i class="fa fa-angle-left sidebar-nav-indicator"></i>
                                                            @endif
                                                            {!! $sub_link['name'] !!}
                                                        </a>
                                                        @if (isset($sub_link['sub']) && $sub_link['sub'])
                                                            <ul>
                                                                <?php
                                                                foreach ($sub_link['sub'] as $sub2_link) :
                                                                // Get 3rd level link's vital info
                                                                $url = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                                                $active = (isset($sub2_link['url']) && (strpos($admin_template['active_page'], $sub2_link['url']) !== FALSE) && (!isset($sub2_link['never_active'])))? ' class="active"' : '';
                                                                ?>
                                                                <li>
                                                                    <a href="{!! $url !!}"{!! $active !!}>{!! $sub2_link['name'] !!}</a>
                                                                </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <!-- END Sidebar Navigation -->
                    @endif
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
                Available header.navbar classes:

                'navbar-default'            for the default light header
                'navbar-inverse'            for an alternative dark header

                'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                    'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                    'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
            -->
            <header class="navbar{!! $admin_template['header_navbar'] ? ' ' . $admin_template['header_navbar'] : '' !!}{!! $admin_template['header'] ? ' ' . $admin_template['header'] : '' !!}">
            @if ( $admin_template['header_content'] == 'horizontal-menu' )

            @else
                <!-- Left Header Navigation -->
                    <ul class="nav navbar-nav-custom">
                        <!-- Main Sidebar Toggle Button -->
                        <li>
                            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                <i class="fa fa-bars fa-fw"></i>
                            </a>
                        </li>
                        <!-- END Main Sidebar Toggle Button -->
                    </ul>
                    <!-- END Left Header Navigation -->

                    <!-- Right Header Navigation -->
                    <ul class="nav navbar-nav-custom pull-right">
                        <!-- User Dropdown -->
                        <li class="dropdown">
                            <a><strong class="server-time"></strong></a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                @if (auth()->user()->profile_image != '')
                                    <img src="{{ asset(auth()->user()->profile_image) }}"
                                         alt="avatar"> <i class="fa fa-angle-down"></i>
                                @else
                                <img src="{{asset('public/proui-backend/img/placeholders/avatars/avatar2.jpg')}}"
                                     alt="avatar"> <i class="fa fa-angle-down"></i>
                                @endif
                            </a>
                            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                <li class="dropdown-header text-center">Account</li>
                                <li>
                                    <a href="{{ url('/admin/logout') }}"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                                </li>
                            </ul>
                        </li>
                        <!-- END User Dropdown -->
                    </ul>
                    <!-- END Right Header Navigation -->
                @endif
            </header>
            <!-- END Header -->

            <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
