@extends('admin.pages.index')

@section('content')
    <script>
        var deleteProjectURI = "{{ route('admin.projects.delete', '') }}";
    </script>
    @if (auth()->user()->can('Create Project'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ url('admin/projects/create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Project</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i
                                    class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2><i class="fa fa-picture-o sidebar-nav-icon"></i>&nbsp;<strong>Projects</strong></h2>
        </div>
        <div class="alert alert-info alert-dismissable project-empty {{$projects->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No projects found.
        </div>
        <div class="table-responsive {{$projects->count() == 0 ? 'johnCena' : '' }}">
            <table id="projects-table"
                   class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-left">
                        Name
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                    <tr data-project-template-id="{{$project->id}}">
                        <td class="text-center">{{ $project->id }}</td>
                        <td class="text-left">{{ $project->name }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Project'))
                                    <a href="{{ route('admin.projects.edit', $project->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Project'))
                                    <a id="delete-project-btn" href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-project-btn"
                                       data-original-title="Delete"
                                       data-project-id="{{ $project->id }}"
                                       data-project-route="{{ route('admin.projects.delete', $project->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/projects.js') }}"></script>
@endsection