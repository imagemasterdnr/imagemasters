@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/projects') }}">Projects</a></li>
        <li><span href="javascript:void(0)">Edit Project</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-project',
            'route' => ['admin.projects.update', $project->id],
            'class' => 'form-horizontal ',
            'files' => true
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit Project "{{$project->name}}"</strong></h2>
                </div>
                {{--<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">--}}
                    {{--<label class="col-md-3 control-label" for="project_code">Code</label>--}}

                    {{--<div class="col-md-9">--}}
                        {{--<input type="text" class="form-control" id="project_code" name="code"--}}
                               {{--value="{{ Request::old('code') ? : $project->code }}"--}}
                               {{--placeholder="Enter project code..">--}}
                        {{--@if($errors->has('code'))--}}
                            {{--<span class="help-block animation-slideDown">{{ $errors->first('code') }}</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="project_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="project_name" name="name"
                               value="{{ Request::old('name') ? : $project->name }}"
                               placeholder="Enter project name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                {{--<div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">--}}
                    {{--<label class="col-md-3 control-label" for="project_slug">Slug</label>--}}

                    {{--<div class="col-md-9">--}}
                        {{--<input type="text" class="form-control" id="project_slug" name="slug"--}}
                               {{--value="{{ Request::old('slug') ? : $project->slug }}"--}}
                               {{--placeholder="Enter project slug..">--}}
                        {{--@if($errors->has('slug'))--}}
                            {{--<span class="help-block animation-slideDown">{{ $errors->first('slug') }}</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group{{ $errors->has('vimeo_link') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="vimeo_link">Vimeo Link</label>
                    <div class="col-md-9">
                        <textarea id="vimeo_link" name="vimeo_link" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter project vimeo link..">{!! Request::old('vimeo_link') ? : $project->vimeo_link !!}</textarea>
                        @if($errors->has('vimeo_link'))
                            <span class="help-block animation-slideDown">{{ $errors->first('vimeo_link') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="file">Overview Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Choose File <input type="file" name="file" style="display: none;">
                            </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('file'))
                            <span class="help-block animation-slideDown">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="file">{{--Overview Image--}}</label>
                    <div class="col-md-9">
                        <a href="{{ asset($project->image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ asset($project->image) }}" alt="{{ $project->image }}" class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="description">Description</label>

                    <div class="col-md-9">
                        <textarea id="description" name="description" rows="3"
                                  class="form-control"
                                  style="resize: vertical; min-height: 100px;"
                                  placeholder="Enter project description..">{!! Request::old('description') ? : $project->description !!}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Featured?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_featured" name="is_featured"
                                   value="1" {{ Request::old('is_featured') ? : ($project->is_featured ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($project->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ url('admin/projects') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('extrascripts')
    <script>
        @if (!empty($project->project_images))
            var sProjectImages = '<?php echo $project->project_images()->get()->toJson(); ?>';
        @endif
    </script>
    <script type="text/javascript" src="{{ asset('public/proui-backend/js/helpers/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/projects.js') }}"></script>
@endsection