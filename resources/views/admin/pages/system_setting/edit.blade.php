@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/system_settings') }}">System Settings</a></li>
        <li><span href="javascript:void(0)">Edit System Setting</span></li>
    </ul>
    <div class="row">
        <div class="block">
            <div class="block-title">
                <h2><i class="fa fa-pencil"></i> <strong>Edit System Setting "{{$system_setting->name}}"</strong></h2>
            </div>
            {{  Form::open([
                'method' => 'PUT',
                'id' => 'edit-system-setting',
                'route' => ['admin.system_settings.update', $system_setting->id],
                'class' => 'form-horizontal form-bordered',
                'files' => TRUE
                ])
            }}
            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="system_settings_code">Code</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="system_settings_code" name="code"
                           placeholder="Enter system setting code.."
                           value="{{  Request::old('code') ? : $system_setting->code }}"
                           readonly>
                    @if($errors->has('code'))
                        <span class="help-block animation-slideDown">{{ $errors->first('code') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="col-md-3 control-label" for="system_settings_name">Name</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" id="system_settings_name" name="name"
                           value="{{  Request::old('name') ? : $system_setting->name }}"
                           placeholder="Enter system setting name..">
                    @if($errors->has('name'))
                        <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>

            @if ($system_setting->code == 'SS0007')
                <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label"
                           for="value">Value</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                        <span class="btn btn-primary">
                            Choose File <input type="file" name="value" style="display: none;">
                        </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ asset($system_setting->value) }}" class="zoom img-thumbnail"
                           style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ asset($system_setting->value) }}" alt="{{ $system_setting->value}}"
                                 class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                    </div>
                </div>
            @else
                <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="system_settings_value">Value</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="system_settings_value" name="value"
                               value="{{  Request::old('value') ? : $system_setting->value }}"
                               placeholder="Enter system setting value..">
                        @if($errors->has('value'))
                            <span class="help-block animation-slideDown">{{ $errors->first('value') }}</span>
                        @endif
                    </div>
                </div>
            @endif
            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3">
                    <a href="{{ url('admin/system_settings') }}" class="btn btn-sm btn-warning">Cancel</a>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('extrascripts')
    @if ($system_setting->code == 'SS0007')
        <script>
            sSettingType = 'file';
        </script>
    @endif
    <script type="text/javascript" src="{{ asset('public/js/libraries/system_settings.js') }}"></script>
@endsection