@extends('admin.pages.index')

@section('content')
    <script>
        var deleteCategoryURI = "{{ route('admin.product_categories.delete', '') }}";
    </script>
    @if (auth()->user()->can('Create Product Category'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ url('admin/product_categories/create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Service Category</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i
                                    class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2><i class="fa fa-shopping-cart sidebar-nav-icon"></i>&nbsp;<strong>Service Categories</strong></h2>
        </div>
        <div class="alert alert-info alert-dismissable product_categories-empty {{$product_categories->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No service categories found.
        </div>
        <div class="table-responsive {{$product_categories->count() == 0 ? 'johnCena' : '' }}">
            <table id="product-categories-table"
                   class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($product_categories as $product_category)
                    <tr data-product_categories-template-id="{{$product_category->id}}">
                        <td class="text-center">{{ $product_category->id }}</td>
                        <td class="text-center"><strong>{{ $product_category->name }}</strong></td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Product Category'))
                                    <a href="{{ route('admin.product_categories.edit', $product_category->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Product Category'))
                                    <a id="delete-product_categories-btn" href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-product_categories-btn"
                                       data-original-title="Delete"
                                       data-product_categories-id="{{ $product_category->id }}"
                                       data-product_categories-route="{{ route('admin.product_categories.delete', $product_category->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/product_categories.js') }}"></script>
@endsection