@extends('admin.pages.index')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ url('admin/product_categories') }}">Service Categories</a></li>
        <li><span href="javascript:void(0)">Edit Service Category</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-product-category',
            'route' => ['admin.product_categories.update', $product_category->id],
            'class' => 'form-horizontal ',
            'files' => true
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit Service Category "{{$product_category->name}}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_category_code">Code</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_category_code" name="code"
                               value="{{ Request::old('code') ? : $product_category->code }}"
                               placeholder="Enter service category code..">
                        @if($errors->has('code'))
                            <span class="help-block animation-slideDown">{{ $errors->first('code') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_category_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_category_name" name="name"
                               value="{{ Request::old('name') ? : $product_category->name }}"
                               placeholder="Enter service category name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="banner_image">Banner Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="banner_image" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('banner_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('banner_image') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="banner_image">{{--Banner Image--}}</label>
                    <div class="col-md-9">
                        <a href="{{ asset($product_category->banner_image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ asset($product_category->banner_image) }}" alt="{{ $product_category->banner_image }}" class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('banner_video') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="banner_video">Banner Video</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="banner_video" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('banner_video'))
                            <span class="help-block animation-slideDown">{{ $errors->first('banner_video') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="banner_video">{{--Banner Image--}}</label>
                    <div class="col-md-9" data-toggle="lightbox-video">
                        <a href="{{ asset($product_category->banner_video) }}" class="gallery-link" style="cursor: default !important;">
                            {{ asset($product_category->banner_video) }}
                        </a>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('overview_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="overview_image">Overview Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="overview_image" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('overview_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('overview_image') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="overview_image">{{--Overview Image--}}</label>
                    <div class="col-md-9">
                        <a href="{{ asset($product_category->overview_image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ asset($product_category->overview_image) }}" alt="{{ $product_category->overview_image }}" class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="description">Description</label>
                    <div class="col-md-9">
                        <textarea id="description" name="description" rows="9"
                                  class="form-control ckeditor"
                                  placeholder="Enter service category description..">{!! Request::old('description') ? : $product_category->description !!}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($product_category->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ url('admin/product_categories') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="col-md-6">--}}
            {{--@include('admin.pages.product_category.image_fields')--}}
        {{--</div>--}}
        <div class="col-md-">
            @include('admin.pages.product_category.meta_fields')
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('extrascripts')
    <script>
        @if (!empty($product_category->product_category_images))
            var sProductCategoriesImages = '<?php echo $product_category->product_category_images()->get()->toJson(); ?>';
        @endif
    </script>
    <script type="text/javascript" src="{{ asset('public/proui-backend/js/helpers/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/product_categories.js') }}"></script>
@endsection