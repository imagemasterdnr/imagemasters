@extends('front.auth.index')

@section('content')
    @if (!empty($page))
        @php
            $item = $page;
            $item->name = 'reset password';
        @endphp
    @else
        @php
            $item = (object) ['name' => 'reset password'];
        @endphp
    @endif
    @include('front.pages.custom-page.sections.sub_banner_generic')

    <section class="main-subpage">
            <div class="main-subpage__wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gray-box contact-form form--fullwidth">
                        {{--<p> Lorem ipsum dolor sit amet, netus ut. Lacinia arcu tortor porttitor est tincidunt </p>--}}
                        <form id="form-reset" action="{{ route('customer.password.reset.post') }}" method="POST">
                            <ul class="form-box">
                                @if (Session::has('status'))
                                    <div class="alert alert-success">
                                        {{ Session::get('status') }}
                                    </div>
                                @endif
                                <input type="hidden" name="token" value="{{ $token }}">
                                <li class="form-group">
                                    <label>EMAIL*</label>
                                    <input type="text" name="email" value="{{ $email or old('email') }}">
                                    @if ($errors->has('email'))
                                        <span id="email-error" class="help-block animation-slideDown">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </li>
                                    <li class="form-group">
                                        <label>FIRSTNAME*</label>
                                        <input type="text" name="first_name" value="{{ old('first_name') }}">
                                        @if ($errors->has('first_name'))
                                            <span id="first_name-error" class="help-block animation-slideDown">
                                    {{ $errors->first('first_name') }}
                                </span>
                                        @endif
                                    </li>
                                    <li class="form-group">
                                        <label>LASTNAME*</label>
                                        <input type="text" name="last_name" value="{{ old('last_name') }}">
                                        @if ($errors->has('last_name'))
                                            <span id="last_name-error" class="help-block animation-slideDown">
                                    {{ $errors->first('last_name') }}
                                </span>
                                        @endif
                                    </li>
                                <li class="form-group">
                                    <label>PASSWORD*</label>
                                    <input type="password" id="password" name="password">
                                    @if ($errors->has('password'))
                                        <span id="password-error" class="help-block animation-slideDown">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </li>
                                <li class="form-group padding-left-more">
                                    <label>PASSWORD CONFIRMATION*</label>
                                    <input type="password" name="password_confirmation">
                                </li>
                                <li>
                                    <button type="submit" class="btn btn--width"> Reset Password</button>
                                </li>
                            </ul>
                        </form>
                        <div class="text-center message-text">
                            <a href="{{ url('customer/login') }}">
                                <small>Did you remember your password? Login here.</small>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('front.pages.custom-page.sections.instagram-feed')
@endsection