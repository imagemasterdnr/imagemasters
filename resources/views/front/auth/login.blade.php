@extends('front.auth.index')

@section('content')
    @if (!empty($page))
        @php
            $item = $page;
            $item->name = 'login';
        @endphp
    @else
        @php
            $item = (object) ['name' => 'login'];
        @endphp
    @endif
    @include('front.pages.custom-page.sections.sub_banner_generic')

    <section class="main-subpage">
        <div class="main-subpage__wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gray-box contact-form form--fullwidth">
                        {{--<p> Lorem ipsum dolor sit amet, netus ut. Lacinia arcu tortor porttitor est tincidunt </p>--}}
                        <form id="form-login" action="{{ route('customer.login.post') }}" method="POST">
                            <ul class="form-box">
                                @if (Session::has('status'))
                                    <div class="alert alert-success">
                                        {{ Session::get('status') }}
                                    </div>
                                @endif
                                {{--<li class="form-group">--}}
                                    {{--<label>EMAIL*</label>--}}
                                    {{--<input type="text" name="email" value="{{ old('email') }}">--}}
                                    {{--@if ($errors->has('email'))--}}
                                        {{--<span id="email-error" class="help-block animation-slideDown">--}}
                                            {{--{{ $errors->first('email') }}--}}
                                        {{--</span>--}}
                                    {{--@endif--}}
                                {{--</li>--}}
                                <li class="form-group">
                                    <label>FIRSTNAME*</label>
                                    <input type="text" name="first_name" value="{{ old('first_name') }}">
                                    @if ($errors->has('first_name'))
                                        <span id="first_name-error" class="help-block animation-slideDown">
                                        {{ $errors->first('first_name') }}
                                    </span>
                                    @endif
                                </li>
                                <li class="form-group">
                                    <label>LASTNAME*</label>
                                    <input type="text" name="last_name" value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                        <span id="last_name-error" class="help-block animation-slideDown">
                                        {{ $errors->first('last_name') }}
                                    </span>
                                    @endif
                                </li>
                                <li class="form-group">
                                    <label>PASSWORD*</label>
                                    <input type="password" name="password">
                                    @if ($errors->has('password'))
                                        <span id="password-error" class="help-block animation-slideDown">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </li>

                                <li>
                                    <div class="container">
                                        <div class="col-xs-6 text-left">
                                                <small>Remember me</small>
                                                <input type="checkbox" id="remember" name="remember" style="width: auto;vertical-align: sub;display: inline-block;">
                                        </div>
                                        <div class="col-xs-6 text-right">
                                                <div class="message-text">
                                                    <a href="{{ url('customer/password/email') }}">
                                                        <small>Forgot password?</small>
                                                    </a>
                                                </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn--width btn--float-center"> Login</button>
                                </li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('front.pages.custom-page.sections.instagram-feed')
@endsection

