@extends('front.auth.index')

@section('content')
    @if (!empty($page))
        @php
            $item = $page;
            $item->name = 'register';
        @endphp
    @else
        @php
            $item = (object) ['name' => 'register'];
        @endphp
    @endif
    @include('front.pages.custom-page.sections.sub_banner_generic')

    <section class="main-subpage register">
        <div class="main-subpage__wrapper container">
            <div class="row">
                <div class="col-md-12">

                    <div class="gray-box contact-form form--fullwidth">
                        {{--<p> Lorem ipsum dolor sit amet, netus ut. Lacinia arcu tortor porttitor est tincidunt </p>--}}
                        <form id="form-register" action="{{ route('customer.register.post') }}" method="POST">
                            <ul class="form-box">
                                @if (Session::has('status'))
                                    <div class="alert alert-success">
                                        {{ Session::get('status') }}
                                    </div>
                                @endif
                                <li class="form-group">
                                    <label>FIRSTNAME*</label>
                                    <input type="text" name="first_name" value="{{ old('first_name') }}">
                                    @if ($errors->has('first_name'))
                                        <span id="first_name-error" class="help-block animation-slideDown">
                                            {{ $errors->first('first_name') }}
                                        </span>
                                    @endif
                                </li>
                                <li class="form-group">
                                    <label>LASTNAME*</label>
                                    <input type="text" name="last_name" value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                        <span id="last_name-error" class="help-block animation-slideDown">
                                            {{ $errors->first('last_name') }}
                                        </span>
                                    @endif
                                </li>
                                {{--<li class="form-group">--}}
                                    {{--<label>USERNAME*</label>--}}
                                    {{--<input type="text" name="user_name" value="{{ old('user_name') }}">--}}
                                    {{--@if ($errors->has('user_name'))--}}
                                        {{--<span id="user_name-error" class="help-block animation-slideDown">--}}
                                            {{--{{ $errors->first('user_name') }}--}}
                                        {{--</span>--}}
                                    {{--@endif--}}
                                {{--</li>--}}
                                <li class="form-group">
                                    <label>EMAIL*</label>
                                    <input type="text" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span id="email-error" class="help-block animation-slideDown">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </li>
                                <li class="form-group">
                                    <label>PHONE*</label>
                                    <input type="text" name="phone" value="{{ old('phone') }}">
                                    @if ($errors->has('phone'))
                                        <span id="phone-error" class="help-block animation-slideDown">
                                            {{ $errors->first('phone') }}
                                        </span>
                                    @endif
                                </li>
                                <li class="form-group">
                                    <label>PASSWORD*</label>
                                    <input type="password" id="password" name="password">
                                    @if ($errors->has('password'))
                                        <span id="password-error" class="help-block animation-slideDown">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </li>
                                <li class="form-group padding-left-more">
                                    <label>PASSWORD CONFIRMATION*</label>
                                    <input type="password" name="password_confirmation">
                                </li>
                                <li>
                                    <button type="submit" class="btn btn--width btn--float-center"> Register</button>
                                </li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('front.pages.custom-page.sections.instagram-feed')
@endsection