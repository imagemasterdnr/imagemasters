@include('front.pages.template.template_start')
@include('front.pages.template.page_head')

<div class="page-content" id="page-content">
    @yield('content')
</div>

@include('front.pages.template.page_footer')
@include('front.pages.template.template_scripts')
<script type="text/javascript" src="{{ asset('public/js/libraries/front_login.js') }}"></script>
@include('front.pages.template.template_end')
@include('front.pages.flash')