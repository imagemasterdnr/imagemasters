<?php
/**
 * page_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
?>
<!-- Footer -->
<footer class="site-footer site-section">
    <div class="container">
        <!-- Footer Links -->
        <div class="row">

            <div class="col-lg-3 col-md-3 site-footer__item  site-footer__item--left">
                <div class="footer-logo">
                    <a href="{{ url('/') }}">
                        image masters
                    </a>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 site-footer__item  site-footer__item--right">
                <div class="row clear-padding">
                    <div class="col-md-4">
                        <aside>
                            <h4 class="footer-heading">Home</h4>
                            <ul class="footer-nav">
                                <li><a href="{{ url ('services') }}">Services</a></li>
                                <li><a href="{{ url ('projects') }}" class="{{ request()->is('tour_preview/*') ? 'hidden' : '' }}">Projects</a></li>
                                <li><a href="{{ url ('our-history') }}">Our History</a></li>
                                <li><a href="{{ url ('contact-us') }}">Contact Us</a></li>
                                <li><a href="{{ url ('customer/login') }}">Customer Login</a></li>
                                <li><a href="{{ url('schedule-now') }}" class="{{ request()->is('tour_preview/*') ? 'hidden' : '' }}">Schedule Now!</a></li>
                            </ul>
                        </aside>
                    </div>

                    <div class="col-md-4 {{ request()->is('tour_preview/*') ? 'hidden' : '' }}">
                        <aside>
                            <h4 class="footer-heading">Our Projects</h4>
                            <ul class="footer-nav">
                                @if (!empty($projects) && count($projects))
                                    @foreach($projects->take(6) as $project)
                                        <li>
                                            <a href="{{ url('projects?project_id=' . $project->id) }}">{{ $project->name }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </aside>
                    </div>

                    <div class="col-md-4 {{ request()->is('tour_preview/*') ? 'hidden' : '' }}">
                        <aside>
                            <h4 class="footer-heading">Contact Info</h4>
                            <ul class="footer-nav contact-list">
                                <li><a href="tel:{!! $seo_meta['phone'] !!}"><strong>Phone:</strong> {!! $seo_meta['phone'] !!} </a></li>
                                <li><a style="text-transform: none;" href="mailto:{!! $seo_meta['email'] !!}"><strong style="text-transform: uppercase;">Email:</strong>
                                        {!! $seo_meta['email'] !!} </a></li>
                                <li><a href="#" class="subscription-modal-btn" data-toggle="modal" data-target="#subscription-modal">Subscribe to
                                        Newsletter</a></li>
                            </ul>
                        </aside>

                        <aside class="margin-top50">
                            <h4 class="footer-heading">Social Media</h4>
                            <ul class="footer-nav social-list">
                                <li><a href="{!! $seo_meta['facebook_link'] !!}" target="_blank" class="fb">Fb </a></li>
                                <li><a href="{!! $seo_meta['twitter_link'] !!}" target="_blank" class="twitter">Twitter </a></li>
                                <li><a href="{!! $seo_meta['pinterest_link'] !!}" target="_blank" class="pinterest">Printerest </a></li>
                                <li><a href="{!! $seo_meta['instagram_link'] !!}" target="_blank" class="insta">Insta </a></li>
                            </ul>
                        </aside>
                    </div>

                </div>

                <div class="col-md-12 clear-padding copyright">
                        <span>
                            <p>Copyrights @
                                Image Master {{  date('Y') }}. All Rights  Reserved. Web Design by Dog and Rooster, Inc. </p>
                        </span>
                    <span><a href="{{ url('privacy-policy') }}">Privacy Policy</a></span>
                </div>
            </div>

        </div>
        <!-- END Footer Links -->
    </div>
</footer>
<!-- END Footer -->

<!-- The Modal -->
<div class="modal" id="subscription-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Subscribe to Newsletter</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form id="create-subscription" action="{{ route('save_subscription') }}" method="POST" class="text-center">
                    <ul class="form-box">
                        <li class="form-group">
                            <label>FIRSTNAME*</label>
                            <input type="text" name="first_name" value="">
                        </li>
                        <li class="form-group">
                            <label>LASTNAME*</label>
                            <input type="text" name="last_name" value="">
                        </li>
                        <li class="form-group">
                            <label>EMAIL*</label>
                            <input type="text" name="email" value="">
                        </li>
                        <li>
                            <button type="submit" class="btn btn--width"> Subscribe</button>
                        </li>
                    </ul>
                </form>
            </div>

            <!-- Modal footer -->
            {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--}}
            {{--</div>--}}

        </div>
    </div>
</div>

</div>
<!-- END Page Container -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>
