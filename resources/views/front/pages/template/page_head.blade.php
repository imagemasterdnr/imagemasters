<header class="main-navigation">

    {{-- mobile header start  --}}

    <section class="search-box">
        <div class="search-box__button"></div>
        <div class="search-box__wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <form class="search-form" action="{{ url('search') }}" method="GET">
                        <div class="form-group">
                            <a class="nav-link" type="submit" href="javascript:void(0)">
                                <i class="fa fa-search"></i>
                            </a>
                            <input type="text" name="keyword" id="keyword" placeholder="Search Keyword"
                                   value="{{ !empty($search_params) && isset($search_params['keyword']) ? $search_params['keyword'] : '' }}"
                                   spellcheck="false">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <div class="row main-navigation__mobile">
        <div class="col-xs-6 main-navigation__mobile--schedule {{ request()->is('tour_preview/*') ? 'hidden' : '' }}">
            <div class="schedule">
                <a href="{{ url('schedule-now') }}" class="btn btn-schedule"> Schedule Now! </a>
            </div>
        </div>
        <div class="col-xs-6 main-navigation__mobile--LoginRegister text-right {{ request()->is('tour_preview/*') ? 'hidden' : '' }}">
            <ul class="navigation-login grid-col">
                <li>
                    @if (auth()->check())
                        @if (auth()->user()->hasRole('Customer'))
                            <a href="#" class="{{--dropdown-toggle--}}" data-toggle="dropdown">Dashboard
                                <span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('customer.dashboard.account') }}">My Account</a>
                                <a class="dropdown-item" href="{{ route('customer.dashboard.products') }}">My
                                    Products</a>
                                <a class="dropdown-item" href="{{ route('customer.dashboard.profile') }}">Profile</a>
                                <a class="dropdown-item" href="{{ route('customer.dashboard.support') }}">Support</a>
                            </div>
                            <a href="javasript:void(0)">|</a>
                            <a href="{{ url('customer/logout') }}">Logout</a>
                        @else
                            <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                            <a href="javasript:void(0)">|</a>
                            <a href="{{ url('customer/logout') }}">Logout</a>
                        @endif
                    @else
                        <a href="{{ url('customer/login') }}"> <span><i class="fas fa-user"></i></span> Login</a>
                        <a href="javasript:void(0)">|</a>
                        <a href="{{ url('customer/register') }}">Register</a>
                    @endif
                </li>
                <li>
                    {{--<a href="javascript:void(0)">--}}
                    {{--<span><i class="fas fa-shopping-cart"></i> Cart</span>--}}
                    {{--</a>--}}
                </li>
            </ul>
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-12 main-navigation__mobile--navbar">
            <a class="navbar-brand logo main-navigation__mobile--logo" href="{{ url('/') }}"> <img
                        src="{{  url('/public/uploads/logo.png') }}" class="img-responsive"
                        alt="logo"> </a>

            <a href="javascript:void()" class="btn btn-mobile main-navigation__mobile--button {{ request()->is('tour_preview/*') ? 'hidden' : '' }}"> menu
                <i class="fas fa-bars"></i>
            </a>

            <ul class="navbar-nav navbar-right main-navigation__mobile--menu {{ request()->is('tour_preview/*') ? 'hidden' : '' }}">
                <li class="nav-item {{ !Request::is('customer/dashboard/*') && (Request::is('/') || Request::is('home')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/') }}">Home </a>
                </li>
                <li class="nav-item sub-arrow {{ Request::is('services') || Request::is('services/*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('services') }}">
                        Services

                    </a>
                    <a href="javascript:void()" class="btn--float">
                        <i class="fa fa-plus open--btn" aria-hidden="true"></i>
                        <i class="fa fa-times close--btn" aria-hidden="true"></i>
                    </a>
                    <div class="nav-item__submenu">
                        @include('front.pages.custom-page.sections.mega_menu_dynamic')
                    </div>
                </li>
                <li class="nav-item {{ Request::is('projects') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('projects') }}">
                        Projects
                    </a>
                    {{-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div> --}}
                </li>
                <li class="nav-item {{ Request::is('our-history') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('our-history') }}">Our History</a>
                </li>
                <li class="nav-item {{ Request::is('contact-us') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('contact-us') }}">Contact Us</a>
                </li>
                <li class="nav-item">
                    <a class="search-now" href="javascript:void(0)">
                        <i class="fa fa-search"></i> Search
                    </a>
                </li>


            </ul>
        </div>

    </div>
    {{-- mobile menu ending --}}


    <div class="fluid-container row main-navigation__desktop">

        <div class="col-md-9 main-navigation__item main-navigation__item--left">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand logo" href="{{ url('/') }}"> <img src="{{  url('/public/uploads/logo.png') }}"
                                                                         class="img-responsive"
                                                                         alt="logo"> </a>
                <div class="collapse navbar-collapse {{ request()->is('tour_preview/*') ? 'hidden' : '' }}" style="{{ request()->is('tour_preview/*') ? 'display:none !important;' : '' }}" id="navbarSupportedContent">
                    <ul class="navbar-nav navbar-right">
                        <li class="nav-item {{ !Request::is('customer/dashboard/*') && (Request::is('/') || Request::is('home')) ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('/') }}">Home </a>
                        </li>
                        <li class="mega-menu-btn nav-item{{ Request::is('services') || Request::is('services/*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('services') }}">Services  <i class="fas fa-sort-down"></i></a>
                        </li>
                        <li class="nav-item {{ Request::is('projects') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('projects') }}">
                                Projects
                            </a>
                            {{-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div> --}}
                        </li>
                        <li class="nav-item {{ Request::is('our-history') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('our-history') }}">Our History</a>
                        </li>
                        <li class="nav-item {{ Request::is('contact-us') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('contact-us') }}">Contact Us</a>
                        </li>

                        <li class="nav-item">
                            <a class="search-now" href="javascript:void(0)">
                                <i class="fa fa-search"></i>
                            </a>
                        </li>

                        {{-- <li class="nav-item {{ Request::is('search') ? 'active' : '' }}">
                            <form class="search-form" action="{{ url('search') }}" method="GET">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text" name="keyword" id="keyword" placeholder="Search Keyword" value="{{ !empty($search_params) && isset($search_params['keyword']) ? $search_params['keyword'] : '' }}">
                                    </div>
                                </div>
                                <a class="nav-link" href="javascript:void(0)">
                                    <i class="fa fa-search"></i>
                                </a>
                            </form>
                        </li> --}}


                    </ul>
                </div>


                {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button> --}}
            </nav>
        </div>

        <div class="col-md-3 main-navigation__item main-navigation__item--right grid-custom {{ request()->is('tour_preview/*') ? 'hidden' : '' }}">
            <div class=" grid-col grid-col2 grid-custom__wrapper">
                <div class="grid-col__item grid-col__item--left">
                    <div class="schedule">
                        <a href="{{ url('schedule-now') }}" class="btn btn-schedule"> Schedule Now! </a>
                    </div>
                </div>
                <div class="grid-col__item grid-col__item--right">
                    <ul class="navigation-login grid-col">
                        <li>
                            @if (auth()->check())
                                @if (auth()->user()->hasRole('Customer'))
                                    <a href="#" class="{{--dropdown-toggle--}}" data-toggle="dropdown">Dashboard
                                        <span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ route('customer.dashboard.account') }}">My
                                            Account</a>
                                        <a class="dropdown-item" href="{{ route('customer.dashboard.products') }}">My
                                            Products</a>
                                        <a class="dropdown-item" href="{{ route('customer.dashboard.profile') }}">Profile</a>
                                        <a class="dropdown-item" href="{{ route('customer.dashboard.support') }}">Support</a>
                                    </div>
                                    <a href="javasript:void(0)">|</a>
                                    <a href="{{ url('customer/logout') }}">Logout</a>
                                @else
                                    <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                                    <a href="javasript:void(0)">|</a>
                                    <a href="{{ url('customer/logout') }}">Logout</a>
                                @endif
                            @else
                                <a href="{{ url('customer/login') }}"> <span><i class="fas fa-user"></i></span>
                                    Login</a>
                                <a href="javasript:void(0)">|</a>
                                <a href="{{ url('customer/register') }}">Register</a>
                            @endif
                        </li>
                        <li>
                            {{--<a href="javascript:void(0)">--}}
                            {{--<span><i class="fas fa-shopping-cart"></i> Cart</span>--}}
                            {{--</a>--}}
                        </li>
                    </ul>
                </div>
            </div>


        </div>


    </div>

    <div class="sub-megamenu">
        <div class="sub-megamenu__background">
            <div class="row">
                <div class="col-md-6">
                    <div class="sub-megamenu__left">
                        <div class="sub-megamenu__wrapper">
                            <div class="article-main">
                                <div class="article-main__wrapper">
                                    <div class="article-main__label">Services</div>
                                    <div class="sub-megamenu__menus">
                                        @include('front.pages.custom-page.sections.mega_menu_dynamic')
                                    </div>
                                </div>
                            </div>
                            <div class="sub-megamenu__button">
                                <a class="btn primary-btn" href="{{ url('services') }}"> View all services </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="sub-megamenu__right">
                        <div class="sub-megamenu__right--ads" style="background-image: url('{{ $seo_meta['mega_menu_image'] }}') !important;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
<section class="invisible-mega"></section>