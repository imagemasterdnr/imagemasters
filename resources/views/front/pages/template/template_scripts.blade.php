<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>
<!-- global variables -->
<script>
    var sGlobalVariable = 'sample';
    /* used in designer image upload */
    var sBaseURI = '{{ url('/') }}';
    /* used in designer image upload */
    var iViewProjectId = null;
</script>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{asset('public/proui-frontend/js/vendor/jquery.min.js')}}"></script>
<script src="{{asset('public/proui-frontend/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('public/proui-frontend/js/plugins.js')}}"></script>
<script src="{{asset('public/proui-backend/js/plugins.js')}}"></script>
<script src="{{asset('public/proui-frontend/js/app.js')}}"></script>

<script>
    $(window).on('load', function () {
        var uiGallery = $('.video-frame');

        uiGallery.magnificPopup({
            delegate: 'a.project-display__video', // the selector for gallery item
            type: 'iframe',
            gallery: {
                enabled:true
            }
        });

        $("img.hidden_image")
            .on("load", function (res) {
                if ($(this).hasClass('hidden_image')) {
//                    console.log(res)
                    $(this).parents('.hidden_image_container:first').css('background-image', 'url(\'' + $(this).attr('src') + '\')');
//                    console.log('success')
                }
            })
            .on("error", function (res) {
                if ($(this).hasClass('hidden_image')) {
//                    console.log(res)
                    if ($(this).hasClass('banner_hidden_image')) {
                        $(this).parents('.hidden_image_container:first').css('background-image', 'url(\'{{ url(config('constants.placeholder_banner_image')) }}\')');
                    } else {
                        $(this).parents('.hidden_image_container:first').css('background-image', 'url(\'{{ url(config('constants.placeholder_image')) }}\')');
                    }
//                    console.log('error')
                }
            }).each(function () {
            if ($(this).hasClass('hidden_image')) {
//                console.log(this)
                if (this.complete && this.naturalWidth > 0) {
                    $(this).load();
                } else {
                    $(this).error();
                }
            }
        });
    });
</script>

<script src="{{asset('public/js/app.js')}}"></script>

<!-- Include platform js file -->
<script type="text/javascript" src="{{ asset('public/js/core/platform.js') }}"></script>
<!-- Include ajaxq js file -->
<script type="text/javascript" src="{{ asset('public/js/core/ajaxq.js') }}"></script>
<!-- Include centralAjax js file -->
<script type="text/javascript" src="{{ asset('public/js/core/centralAjax.js') }}"></script>
<!-- Include config js file -->
<script type="text/javascript" src="{{ asset('public/js/core/config.js') }}"></script>
<!-- Include main js file -->
{{--<script type="text/javascript" src="{{ asset('public/js/libraries/main.js') }}"></script>--}}
<!-- Include delete js file -->
{{--<script type="text/javascript" src="{{ asset('public/js/libraries/delete.js') }}"></script>--}}
<!-- Include sweetalert files -->
<link rel="stylesheet" type="text/css" href="{{ asset('public/node_modules/sweetalert/dist/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('public/node_modules/sweetalert/dist/sweetalert.min.js') }}"></script>
<!-- Include moment files -->
{{--<script type="text/javascript" src="{{ asset('public/node_modules/moment/moment.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('public/node_modules/moment-timezone/builds/moment-timezone.min.js') }}"></script>--}}
<!-- Include countdown files -->
<!-- countdown only -->
{{--<script type="text/javascript" src="{{ asset('public/node_modules/jquery-countdown/dist/jquery.countdown.js') }}"></script>--}}
<!-- with other functions -->
{{--<link rel="stylesheet" type="text/css" href="{{ asset('public/node_modules/jquery.countdown.package-2.1.0/css/jquery.countdown.css') }}">--}}
{{--<script type="text/javascript" src="{{ asset('public/node_modules/jquery.countdown.package-2.1.0/js/jquery.plugin.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('public/node_modules/jquery.countdown.package-2.1.0/js/jquery.countdown.js') }}"></script>--}}
<!-- include custom timer -->
{{--<script type="text/javascript" src="{{ asset('public/js/plugins/timer.js') }}"></script>--}}

{{--<!-- Include pusher js file -->--}}
{{--<script type="text/javascript" src="{{ asset('public/js/plugins/pusher.min.js') }}"></script>--}}
{{--<!-- Include websocket js file -->--}}
{{--<script type="text/javascript" src="{{ asset('public/js/core/websocket.js') }}"></script>--}}
{{--<!-- Include websocket_events js file -->--}}
{{--<script type="text/javascript" src="{{ asset('public/js/libraries/websocket_events.js') }}"></script>--}}

<script type="text/javascript" src="{{ asset('public/js/libraries/search.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/libraries/subscriptions.js') }}"></script>

@section('extrascripts')
@show