@extends('front.pages.index')

@section('content')
    @if (!empty($page))
        <?php $item = $page; ?>
        {{-- need to be updated to live slugs or by page type --}}
        @if ($page['slug'] == 'home')
            @include('front.pages.custom-page.home')
        @elseif ($page['slug'] == 'contact-us')
            @include('front.pages.custom-page.contact-us')
        @elseif ($page['slug'] == 'services')
            @if (!empty($service))
                @include('front.pages.custom-page.service-details')
            @else
                @include('front.pages.custom-page.services')
            @endif
        @elseif ($page['slug'] == 'our-history')
            @include('front.pages.custom-page.our-history')
        @elseif ($page['slug'] == 'products')
            @include('front.pages.custom-page.products')
        @elseif ($page['slug'] == 'projects')
            @include('front.pages.custom-page.projects')
        @elseif ($page['slug'] == 'search')
            @include('front.pages.custom-page.search')
        @elseif ($page['slug'] == 'schedule-now')
            @include('front.pages.custom-page.schedule-now')
        @else
            @include('front.pages.custom-page.default-page')
        @endif
    @else
        @include('front.pages.custom-page.home')
    @endif
@endsection
