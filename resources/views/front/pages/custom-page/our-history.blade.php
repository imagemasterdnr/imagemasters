@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="hightlight padding-top-bottom80">
    {{--{!! $item->content !!}--}}

    <div class="hightlight__wrapper project-grid">
        <div class="hightlight__item hightlight__item--left">
            <div class="hightlight__item--image">
                <div class="video-frame project-display">
                    <div class="project-display__image hidden_image_container"
                         style="background-color: #ddd !important; height: 100%; background-image: url('{!! !empty($item) && !empty($item->page_sections()->where('section', 'left_image')->first()) ?
                $item->page_sections()->where('section', 'left_image')->first()->content : '' !!}');">
                        <img class="hidden_image"
                             src="{!! !empty($item) && !empty($item->page_sections()->where('section', 'left_image')->first()) ?
                $item->page_sections()->where('section', 'left_image')->first()->content : '' !!}"
                             style="display: none;"/></div>
                    <div class="project-display__overlay">
                        <div class="project-display__overlay--content">&nbsp;</div>
                    </div>
                    {{--<a class="project-display__video mfp-iframe" data-project-id="1" href="{!! !empty($item) && !empty($item->page_sections()->where('section', 'video_vimeo_link')->first()) ?--}}
                {{--$item->page_sections()->where('section', 'video_vimeo_link')->first()->content : '' !!}">&nbsp;</a>--}}
                </div>
            </div>
        </div>
        <div class="hightlight__item hightlight__item--right">
            {!! !empty($item) && !empty($item->page_sections()->where('section', 'right_content')->first()) ?
                $item->page_sections()->where('section', 'right_content')->first()->content : '' !!}
        </div>
    </div>
</section>