@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="subpage-container project-grid">

    <div class="subpage-container__wrapper container projects-gallery">
        @if (!empty($projects) && count($projects))
            @foreach($projects as $project)
                <div class="col-md-4 project-container">
                    <div class="project-display">
                        <div class="project-display__image hidden_image_container" style="background-color: #ddd !important;"
                             {{--style="background-image:url('{{ !empty($project->image) && $project->image != '' ? s3_url($project->image) : '' }}'),
                                     url('{{ url(config('constants.placeholder_image')) }}');"--}}>
                            <img class="hidden_image" style="display: none;" src="{{ !empty($project->image) && $project->image != '' ? s3_url($project->image) : '' }}">
                        </div>
                        <div class="project-display__overlay">
                            <div class="project-display__overlay--content">
                                <a data-project-id="{{ $project->id }}"
                                            href="javascript:void(0)"
                                            {{--href="{{ !empty($project->vimeo_link) && $project->vimeo_link != '' ? $project->vimeo_link : 'https://vimeo.com/229490822?autoplay=1' }}"--}}>
                                        <h2>{{ !empty($project->name) && $project->name != '' ? $project->name : '' }}</h2>
                                 </a>
                                <p>{!! !empty($project->description) && $project->description != '' ? $project->description : '' !!}</p>
                            </div>
                            <a class="project-display__video mfp-iframe" data-project-id="{{ $project->id }}"
                            href="{{ !empty($project->vimeo_link) && $project->vimeo_link != '' ? $project->vimeo_link : 'https://vimeo.com/229490822?autoplay=1' }}">
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <div class="alert alert-warning text-center">
                    No projects found!
                </div>
            </div>
        @endif
    </div>

</section>

@section('extrascripts')
    @if (request()->has('project_id'))
        <script>
            iViewProjectId = "{{ request()->get('project_id') }}"
        </script>
    @endif
    <script type="text/javascript" src="{{ asset('public/js/libraries/front_projects.js') }}"></script>
@endsection