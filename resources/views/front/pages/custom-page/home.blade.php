@include('front.pages.custom-page.sections.main-banner')

{{-- {!! !empty($page) ? isset($page->content) ? $page->content : '' : '' !!} --}}


@include('front.pages.custom-page.sections.main-featured-video')

@include('front.pages.custom-page.sections.feature-project-sliding')

@include('front.pages.custom-page.sections.hightlight-project')

@include('front.pages.custom-page.sections.instagram-feed')