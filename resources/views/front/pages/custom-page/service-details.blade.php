@if (!empty($product))
    @php
        $item = $product;
        $item->name = $product->name;
        if (!empty($product->banner_image)) {
            $item->banner_image = $product->banner_image;
        }
    @endphp
@endif
@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="services-display services-display__single">

    <div class="services-display__wrapper container">

        @include('front.pages.custom-page.sections.calendar-btn')

        <div class="row">
            <div class="col-lg-4 col-md-12 services-display__single--left services-display__single--items">
                @if (isset($service->description) && $service->description != '')
                    <div class="services-display__overview-details padding-left15">
                        <div class="article-main">
                            <div class="article-main__wrapper">
                                <div class="article-main__label">
                                    product details
                                </div>
                                {!! $service->description !!}
                            </div>
                        </div>
                    </div>
                @endif

                <!--
                @if (isset($service->pricing_content) && $service->pricing_content != '')
                    <div class="services-display__pricing padding-left15">
                        {!! $service->pricing_content !!}
                    </div>
                @endif
                -->

                @if (!empty($product->project))
                    <div class="services-display__image">
                        <div class="image-display hidden_image_container" style="background-color: #ddd !important;"
                             {{--style="background-image:url('{{ !empty($product->project->image) && $product->project->image != '' ? s3_url($product->project->image) : '' }}'),
                                     url('{{ url(config('constants.placeholder_image')) }}');"--}}>
                            <img class="hidden_image" style="display: none;" src="{{ !empty($product->project->image) && $product->project->image != '' ? s3_url($product->project->image) : '' }}">
                        </div>
                        <div class="content-display padding-left15">
                            <div class="title">
                                sample project: {{ !empty($product->project->name) && $product->project->name != '' ? $product->project->name : '' }}
                            </div>
                            <a href="{{ url('projects?project_id=' . $product->project->id) }}" class="btn">View Project</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-lg-8 col-md-12 services-display__single--right services-display__single--item padding-space">
                <div class="{{--accordion-display--}}">
                    {!! isset($service->acuity_scheduling_link) && $service->acuity_scheduling_link != '' ? $service->acuity_scheduling_link : '' !!}
                </div>
            </div>
        </div>
    </div>
    @include('front.pages.custom-page.sections.related_services')
</section>