@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="subpage-container">
    <div class="subpage-container__wrapper container">
        <div class="col-md-12">
            {{--{!! !empty($page) ? isset($page->content) ? $page->content : '' : '' !!}--}}
            {!! !empty($page) && !empty($page->page_sections()->where('section', 'acuity_scheduling_link')->first()) ?
                $page->page_sections()->where('section', 'acuity_scheduling_link')->first()->content : '' !!}
        </div>
    </div>
</section>