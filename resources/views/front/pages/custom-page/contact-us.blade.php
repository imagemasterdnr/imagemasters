@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="feature-main contact-us">
    <div class="feature-main__wrapper container">

        @include('front.pages.custom-page.sections.calendar-btn')

        <div class="row">
            <div class="col-md-6 feature-main__item feature-main__item--label">
                {!! !empty($page) && !empty($page->page_sections()->where('section', 'content')->first()) ?
                    $page->page_sections()->where('section', 'content')->first()->content : '' !!}
                {{--{!! $item->content !!}--}}
            </div>

            <div class="col-md-6 feature-main__item feature-main__item--video">
                <div class="gray-box contact-form">
                    {!! !empty($page) && !empty($page->page_sections()->where('section', 'contact_form_title')->first()) ?
                        $page->page_sections()->where('section', 'contact_form_title')->first()->content : '' !!}
                    {{--@if (!empty($page) && $page->page_sections()->count() > 0 && $page->page_sections()->where('id', 4)->first())--}}
                        {{--@php--}}
                            {{--$section = $page->page_sections()->where('id', 4)->first();--}}
                        {{--@endphp--}}
                        {{--{!! $section->content !!}--}}
                    {{--@else--}}
                        {{--<p> Lorem ipsum dolor sit amet, netus ut. Lacinia arcu tortor porttitor est tincidunt... </p>--}}
                    {{--@endif--}}
                    <form id="create-contact" action="{{ route('save_contact') }}" method="POST">
                        <ul class="form-box">
                            <li class="form-group">
                                <label>Name*</label>
                                <input type="text" name="name">
                            </li>
                            {{--<li class="form-group">--}}
                                {{--<label>Company*</label>--}}
                                {{--<input type="text" name="company">--}}
                            {{--</li>--}}
                            <li class="form-group">
                                <label>EMAIL*</label>
                                <input type="text" name="email">
                            </li>
                            <li class="form-group">
                                <label>Phone*</label>
                                <input type="text" name="phone">
                            </li>
                            <li class="form-group">
                                <label>Subject*</label>
                                <input type="text" name="subject">
                            </li>
                            <li class="form-group">
                                <label>Message*</label>
                                <textarea name="message"></textarea>
                            </li>
                            <li>
                                <button type="submit" class="btn btn--width"> submit</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>


@include('front.pages.custom-page.sections.instagram-feed')

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/contacts.js') }}"></script>
@endsection