<div class="dashboard">
    <div class="dashboard__head">
        <div class="dashboard__head--title dashboard__head--inside no-underline">
            {{--<h2>{{ !empty($appointment->product) ? $appointment->product->name : '' }}</h2>--}}
            <h2>{!! $appointment['address'] !!}</h2>
            <div class="dashboard__status">
                {{--<div class="dashboard__status--dates dashboard__status--item">--}}
                {{--{{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}--}}
                {{--</div>--}}
                {{--<div class="dashboard__status--counts dashboard__status--item">--}}
                {{--{{ $appointment->attachments()->where('extension', '!=', 'mp4')->count() }} PHOTOS /--}}
                {{--{{ $appointment->attachments()->where('extension', '=', 'mp4')->count() }} VIDEOS--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="dashboard__body">
        <div class="dashboard__item view-album">
            <div class="row order-details__body">
                <div class="col-md-12">
                    {!! $appointment['video_link'] !!}
                </div>
            </div>
            {{-- packery plugin --}}
            @if (!empty($appointment->attachments) && count($appointment->attachments))
                @foreach ($appointment->attachments()->where('extension', '!=', 'mp4')->take(1)->get() as $attachment_key => $attachment)
                    <div class="slideshow" style="background-image: url('{{ s3_url($attachment->file) }}'), url('{{ url(config('constants.placeholder_image')) }}');
                            background-size: 100% auto !important;
                            background-position: center !important;">
                        {{--<img class="gallery-image-item" style="width:100%;height:100%;"--}}
                             {{--src="{{ s3_url($attachment->file) }}"--}}
                             {{--onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">--}}
                        <a href="javascript:void(0)" class="slideshow__play">
                            <span class="fa fa-play"></span>
                        </a>
                    </div>
                @endforeach
                <div class="grid-gallery" data-toggle="lightbox-gallery">
                    @foreach ($appointment->attachments()->get() as $attachment_key => $attachment)
                        <div class="grid-gallery__item">
                            @if ($attachment->extension == 'mp4')
                                <img class="gallery-image-item" data-video-id="{{ $attachment->id }}" data-video="1"
                                     video-src="{{ url($attachment->file) }}" src="{{ s3_url($attachment->file) }}"
                                     onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">
                            @else
                                <img class="gallery-image-item" src="{{ s3_url($attachment->file) }}"
                                     onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">
                            @endif
                            <div class="grid-gallery__overlay">
                                <a href="{{ s3_url($attachment->file) }}" class="btn btn-download gallery-link">
                                    <i class="fas fa-search"></i>
                                </a>
                                {{--<a href="{{ route('customer.dashboard.products.download_image', $attachment->id)/*url($attachment->file)*/ }}" class="btn btn-download download-image-btn"--}}
                                {{--download="{{ $attachment->name }}">--}}
                                {{--<i class="fas fa-download"></i>--}}
                                {{--</a>--}}
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="col-md-12">
                    <div class="alert alert-warning text-center">
                        There are no photos yet uploaded.
                    </div>
                </div>
            @endif
            {{-- packery plugin --}}
            <div class="row order-details__body">
                <div class="col-md-12">
                    {!! $appointment['google_map_link'] !!}
                </div>
                <div class="col-md-12">
                    <div class="col-md-12 property-details-container"
                         style="background-image: url('{{ s3_url($appointment->attachments()->where('extension', '!=', 'mp4')->take(1)->first()->file) }}');">
                        <div class="{{ $appointment['is_branded'] && !empty($appointment->user) ? 'col-md-7' : 'col-md-12' }} property-details-container__white-container">
                            <h3>{!! $appointment['address'] !!}</h3>
                            <table class="table {{--order-details__table order-details__table--information--}}">
                                <tbody>
                                <tr>
                                    <td>
                                        <b>{!! $appointment['beds'] !!}</b>
                                        <br>
                                        Beds
                                    </td>
                                    <td>
                                        <b>{!! $appointment['baths'] !!}</b>
                                        <br>
                                        Baths
                                    </td>
                                    <td>
                                        <b>{!! $appointment['availability_status'] !!}</b>
                                        <br>
                                        Availability Status
                                    </td>
                                    <td>
                                        <b>{!! $appointment['square_feet'] !!}</b>
                                        <br>
                                        Square Feet
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        @if ($appointment['is_branded'] && !empty($appointment->user))
                            <div class="col-md-1"></div>
                            <div class="col-md-4 property-details-container__white-container">
                                <table class="table {{--order-details__table order-details__table--information--}}">
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <img src="{{ asset($appointment->user->profile_image) }}"
                                                 class="img-thumbnail" style="height: 100px;">
                                            <h3>{{ $appointment->user->first_name .' '. $appointment->user->last_name }}</h3>
                                            <p class="text-left">
                                                <b>Position:</b> {{ $appointment->user->position }}
                                            </p>
                                            <p class="text-left">
                                                <b>Phone:</b> {{ $appointment->user->phone }}
                                            </p>
                                            <p class="text-left">
                                                <b>Address:</b> {{ $appointment->user->address }}
                                            </p>
                                            <p class="text-left">
                                                <b>License No:</b> {{ $appointment->user->license_no }}
                                            </p>
                                            @if ($appointment->user->website != '')
                                                <p class="text-left">
                                                    <a target="_blank" href="{{ $appointment->user->website }}">Visit my
                                                        website</a>
                                                </p>
                                            @endif
                                            <ul class="share-icons">
                                                {{-- share icon --}}
                                                {{-- mail --}}
                                                <li>
                                                    <a href="#" class="mail-modal-btn"
                                                       data-toggle="modal" data-target="#mail-modal">
                                                        <i class="fas fa-envelope"></i>
                                                    </a>
                                                </li>
                                                {{-- facebook --}}
                                                <li>
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(request()->url()) }}"
                                                       target="_blank">
                                                        <i class="fab fa-facebook"></i>
                                                    </a>
                                                </li>
                                                {{-- twitter --}}
                                                <li>
                                                    <a href="https://twitter.com/intent/tweet?url={{ urlencode(request()->url()) }}"
                                                       target="_blank">
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="modal" id="mail-modal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">

                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Send this virtual tour to a
                                                                friend!</h4>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>

                                                        <!-- Modal body -->
                                                        <div class="modal-body">
                                                            <form id="send-tour-preview"
                                                                  action="{{ route('tour_preview.send_mail', $appointment->appointment_id) }}"
                                                                  method="POST" class="text-center">
                                                                <input type="hidden" name="appointment_id"
                                                                       value="{{ $appointment->appointment_id }}">
                                                                <input type="hidden" name="address"
                                                                       value="{{ $appointment['address'] }}">
                                                                <input type="hidden" name="link"
                                                                       value="{{ urlencode(request()->url()) }}">
                                                                <ul class="form-box">
                                                                    <li class="form-group">
                                                                        <label>Your Friend's Email*</label>
                                                                        <input type="text" name="to_email" value="">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label>Your Friend's Name*</label>
                                                                        <input type="text" name="to_name" value="">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label>Your Email*</label>
                                                                        <input type="text" name="from_email" value="">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label>Your Name*</label>
                                                                        <input type="text" name="from_name" value="">
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label>Subject</label>
                                                                        <input type="text" name="subject"
                                                                               value="Check out this Virtual Tour"
                                                                               readonly>
                                                                    </li>
                                                                    <li class="form-group">
                                                                        <label>Message</label>
                                                                        <textarea name="message"
                                                                                  style="height:100px;"></textarea>
                                                                    </li>
                                                                    <li>
                                                                        <button type="submit" class="btn btn--width">
                                                                            Send
                                                                        </button>
                                                                    </li>
                                                                </ul>
                                                            </form>
                                                        </div>

                                                        <!-- Modal footer -->
                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--}}
                                                        {{--</div>--}}

                                                    </div>
                                                </div>
                                            </div>
                                            @if ($appointment->user->company_logo != '')
                                                <p class="text-center">
                                                    <img src="{{ asset($appointment->user->company_logo) }}"
                                                         class="img-thumbnail" style="height: 100px;">
                                                </p>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/node_modules/frame-grab.js/client/lib/rsvp.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/node_modules/frame-grab.js/client/frame-grab.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/plugins/jquery.slideshowify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/plugins/jquery.transit.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/front_products.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/front_tour_preview.js') }}"></script>
@endsection