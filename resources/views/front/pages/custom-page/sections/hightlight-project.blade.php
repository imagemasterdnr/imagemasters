
<section class="hightlight padding-top-bottom80">
    {!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_4')->first()) ?
            $item->page_sections()->where('section', 'home_section_4')->first()->content : '' !!}

    {{--@if (!empty($page) && $page->page_sections()->count() > 0 && $page->page_sections()->where('id', 3)->first())--}}
        {{--@php--}}
            {{--$section = $page->page_sections()->where('id', 3)->first();--}}
        {{--@endphp--}}
        {{--{!! $section->content !!}--}}
    {{--@else--}}
        {{--<div class="hightlight__wrapper">--}}

            {{--<div class="hightlight__item hightlight__item--left" data-aos="fade-right">--}}

                {{--<div class="hightlight__item--image">--}}
                    {{--<div class="hightlight__item--image__holder">--}}
                        {{--<img src="{{ url('/public/uploads/image.jpg') }}">--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}

            {{--<div class="hightlight__item hightlight__item--right" data-aos="fade-left">--}}

                {{--<div class="article-main padding-right30">--}}
                    {{--<div class="article-main__wrapper">--}}
                        {{--<div class="article-main__label">--}}
                            {{--Services We Provide--}}
                        {{--</div>--}}
                        {{--<h2>Outstanding <br> work at  <br> competative  <br> pricing</h2>--}}
                        {{--<strong>OUR APPROACH IS DEDICATED  <br>--}}
                            {{--PERSONAL SERVICE AND PUTTING OUT THE BEST  <br>--}}
                            {{--PHOTOS POSSIBLE FOR OUR VALUED CUSTOMERS.</strong>--}}
                        {{--<p>“These photos are awesome! You are right, the professional photographer can make a difference. You sure have a great team!"</p>--}}
                        {{--<h5>~Guy and Cherryl Johnston HOME SMART REALTY</h5>--}}
                        {{--<a href="#" class="btn primary-btn margin-top35">  View Services & Pricing </a>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--@endif--}}
</section>