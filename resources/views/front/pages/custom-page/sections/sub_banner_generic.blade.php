<section id="parallax1" class="banner sub-banner {{ request()->is('customer/*') && !request()->is('customer/login') && !request()->is('customer/register') && !request()->is('customer/password/*') ? 'banner-dashboard' : '' }}">
    <div class="banner__wrapper">
        <div class="banner__item hidden_image_container" style="background-color: #ddd !important;"
             {{--style="background-image:url('{{ !empty($item) && isset($item->banner_image) ? s3_url($item->banner_image) : '' }}'),
                url('{{ url(config('constants.placeholder_banner_image')) }}');"--}}>
                <img class="hidden_image banner_hidden_image" style="display: none;" src="{{ !empty($item) && isset($item->banner_image) ? s3_url($item->banner_image) : '' }}">
            <div class="container">
                <div class="col-md-12">
                    <div class="slider-content">
                        <h1>{{ !empty($item) ? isset($item->name) ? $item->name : '' : '' }}</h1>
                    </div>
                </div>
            </div>
            @if (!empty($item) && isset($item->banner_video))
                <video class="banner__item" style="width: 100%;" id="video-play" poster="{{ !empty($item) && isset($item->banner_image) ? s3_url($item->banner_image) : '' }}"  loop autoplay muted controlsList="nodownload"
                       webkitallowfullscreen mozallowfullscreen allowfullscreen>
                    <source type="video/mp4" src="{{ !empty($item) && isset($item->banner_video) ? s3_url($item->banner_video) : '' }}" />
                    {{--<source type="video/ogg" src="https://www.expresscorp.com/public/vid/HpVer19.Ogg" />--}}
                    {{--<source type="video/webm" src="https://www.expresscorp.com/public/vid/HpVer19.webm" />--}}

                    <!-- Flash fallback for non-HTML5 browsers without JavaScript -->
                    <object width="100%" height="400" type="application/x-shockwave-flash" data="flashmediaelement.swf">
                        <param name="movie" value="flashmediaelement.swf" />
                        <param name="flashvars" value="controls=false&file={{ !empty($item) && isset($item->banner_video) ? s3_url($item->banner_video) : '' }}" />
                        <!-- Image as a last resort -->
                        <img src="{{ !empty($item) && isset($item->banner_image) ? s3_url($item->banner_image) : '' }}" width="320" height="240" title="No video playback capabilities" />
                    </object>
                </video>
            @endif
        </div>
    </div>
</section>