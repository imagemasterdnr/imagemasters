<section class="instagram-feed">
    <div class="instagram-feed__wrapper instagram-feed-slide">
        @if (!empty($instagram_feed))
            @foreach ($instagram_feed as $instagram)
                <div class="instagram-feed__wrapper--item"
                     style="background:url('{{ $instagram->images->low_resolution->url }}')no-repeat;"></div>
            @endforeach
        @else
            <div class="instagram-feed__wrapper--item"
                 style="background:url('{{ url('/public/uploads/gallery1.jpg') }}')no-repeat;"></div>
            <div class="instagram-feed__wrapper--item"
                 style="background:url('{{ url('/public/uploads/gallery2.jpg') }}')no-repeat;"></div>
            <div class="instagram-feed__wrapper--item"
                 style="background:url('{{ url('/public/uploads/gallery3.jpg') }}')no-repeat;"></div>
            <div class="instagram-feed__wrapper--item"
                 style="background:url('{{ url('/public/uploads/gallery4.jpg') }}')no-repeat;"></div>
            <div class="instagram-feed__wrapper--item"
                 style="background:url('{{ url('/public/uploads/gallery5.jpg') }}')no-repeat;"></div>
            <div class="instagram-feed__wrapper--item"
                 style="background:url('{{ url('/public/uploads/gallery1.jpg') }}')no-repeat;"></div>
        @endif
    </div>
</section>
