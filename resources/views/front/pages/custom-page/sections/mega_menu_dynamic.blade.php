@foreach($header_services as $header_service)
    @if ($header_service['item_type'] == 'category')
        <ul>
            <li class="submenu__parent"><a href="{{ url('services/' . $header_service->slug) }}"><strong>{{ $header_service->name }}</strong></a></li>
            @php
                $header_products = $header_service->products()->where('is_active', 1)->get();
                   foreach ($header_products as $header_product) {
                       $header_product = App\Repositories\ProductRepository::getDataStatic($header_product);
                   }
            @endphp
            @foreach($header_products as $header_product)
                <li class="submenu__child"><a href="{{ url('services/' . $header_service->slug . '/' . $header_product->slug) }}">{{ $header_product->name }}</a></li>
            @endforeach
        </ul>
    @endif
@endforeach
<ul>
    @foreach($header_services as $header_service)
        @if ($header_service['item_type'] == 'product')
            <li class="submenu__parent"><a href="{{ url('services/' . $header_service->slug) }}"><strong>{{ $header_service->name }}</strong></a></li>
        @endif
    @endforeach
</ul>
{{--<ul>--}}
    {{--<li><a href="#"><strong>Photography</strong></a></li>--}}
    {{--<li><a href="#">Daytime Photography</a></li>--}}
    {{--<li><a href="#">Daytime Photography</a></li>--}}
    {{--<li><a href="#">Daytime Photography</a></li>--}}
    {{--<li><a href="#">Daytime Photography</a></li>--}}
{{--</ul>--}}
{{--<ul>--}}
    {{--<li><a href="#"><strong>Videography</strong></a></li>--}}
    {{--<li><a href="#">Walk-thru video</a></li>--}}
    {{--<li><a href="#">Premium Lifestyle Video</a></li>--}}
    {{--<li><a href="#">Zillow Walk-Thru Vide</a></li>--}}
{{--</ul>--}}
{{--<ul>--}}
    {{--<li><strong><a href="#">Aerial / Drone Photo & Video</a></strong></li>--}}
    {{--<li><a href="#">Aerial Video</a></li>--}}
    {{--<li><a href="#">Aerial Photography</a></li>--}}
{{--</ul>--}}
{{--<ul>--}}
    {{--<li><a href="#"><strong>Zillow 3D Home Tour</strong></a></li>--}}
    {{--<li><a href="#"><strong>3D Matterport Showcase</strong></a></li>--}}
    {{--<li><a href="#"><strong>CUstom Property Brochures</strong></a></li>--}}
{{--</ul>--}}