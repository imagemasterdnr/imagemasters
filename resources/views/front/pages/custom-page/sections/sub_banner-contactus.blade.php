
<section class="banner sub-banner">
        <div class="banner__wrapper">

            <div class="banner__item" style="background-image:url({{ s3_url('public/uploads/sub-banner-contactus.jpg') }});">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="slider-content">
                                <h1>Contact Us</h1>
                                {{-- <p>sample text </p> --}}
                            </div>
                        </div>
                    </div>
              </div>

        </div>
    </section>