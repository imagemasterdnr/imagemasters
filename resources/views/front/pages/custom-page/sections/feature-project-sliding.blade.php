<section class="feature-projects lightgray-background padding-top-bottom80">
    <div class="feature-projects__wrapper">

        {!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_3_title')->first()) ?
                    $item->page_sections()->where('section', 'home_section_3_title')->first()->content : '' !!}

        {{--@if (!empty($page) && $page->page_sections()->count() > 0 && $page->page_sections()->where('id', 2)->first())--}}
            {{--@php--}}
                {{--$section = $page->page_sections()->where('id', 2)->first();--}}
            {{--@endphp--}}
            {{--{!! $section->content !!}--}}
        {{--@else--}}
            {{--<div class="heading-primary text-center">--}}
                {{--<h2>Featured Projects</h2>--}}
                {{--<p>Lorem Ipsum Elite Dolor melis</p>--}}
            {{--</div>--}}
        {{--@endif--}}

        <div class="sliding-project"  data-aos="fade-up"
        data-aos-anchor-placement="center-bottom">
            <div class="variable-width">
                @if (!empty($projects) && count($projects))
                    @php
                        $loops = 1;
                    @endphp
                    @if (count($projects) < 4)
                        @php
                        if (count($projects) == 1) {
                            $loops = 4;
                        } else {
                            $loops = 2;
                        }
                        @endphp
                    @endif
                    @for ($x = 1; $x <= $loops; $x++)
                        @foreach($projects as $project)
                            <div class="sliding-project__item">
                                <div class="feature-image">
                                    <a href="{{ url('projects?project_id=' . $project->id) }}">
                                        <img src="{{ !empty($project->image) && $project->image != '' ? s3_url($project->image) : url(config('constants.placeholder_image')) }}"
                                        onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">
                                    </a>
                                </div>
                                <div class="text-content text-center">
                                    <a href="{{ url('projects?project_id=' . $project->id) }}">
                                        <h3>{{ !empty($project->name) && $project->name != '' ? $project->name : '' }}</h3>
                                    </a>
                                    <p>{!! !empty($project->description) && $project->description != '' ? $project->description : '' !!}</p>
                                    <a href="{{ url('projects?project_id=' . $project->id) }}" class="btn primary-btn"> View Project </a>
                                </div>
                            </div>
                        @endforeach
                    @endfor
                @else
                    <div class="col-md-12">
                        <div class="alert alert-warning text-center">
                            No projects found!
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>