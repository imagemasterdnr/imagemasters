@if (isset($service->previous) && isset($service->next))
<section class="arrow-button">
    <div class="arrow-button__wrapper container">

        <div class="row">
            <div class="col-xs-6 text-left arrow-button__item arrow-button__item--left">
                <a href="{{ isset($service->previous) ? $service->previous->slug : 'javascript:void(0)' }}" class="previous-service-btn">
                        <i class="fas fa-arrow-left"></i> PREVIOUS Services
                </a>
            </div>
            <div class="col-xs-6 text-right arrow-button__item arrow-button__item--right">
                <a href="{{ isset($service->next) ? $service->next->slug : 'javascript:void(0)' }}" class="next-service-btn">
                        <i class="fas fa-arrow-right"></i> NEXT Services
                </a>
            </div>
        </div>

    </div>
</section>
@endif