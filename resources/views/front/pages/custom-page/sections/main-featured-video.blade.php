<section class="feature-main">
    <div class="feature-main__wrapper container">

        @include('front.pages.custom-page.sections.calendar-btn')

        <div class="row">
            <div class="col-md-6 feature-main__item feature-main__item--label">
                {!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_left_content')->first()) ?
                                    $item->page_sections()->where('section', 'home_section_2_left_content')->first()->content : '' !!}
                <p>&nbsp;</p>
            </div>

            <div class="col-md-6 feature-main__item feature-main__item--video">
                <div class="video-main pull-top">
                    <div class="video-main__item">
                        {!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_video_link')->first()) ?
                                    $item->page_sections()->where('section', 'home_section_2_video_link')->first()->content : '' !!}
                        {{--<video controls="controls" controlslist="nodownload"--}}
                               {{--src="{!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_video')->first()) ?--}}
                                    {{--$item->page_sections()->where('section', 'home_section_2_video')->first()->content : '' !!}"--}}
                               {{--poster="{!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_video_image_thumbnail')->first()) ?--}}
                                    {{--$item->page_sections()->where('section', 'home_section_2_video_image_thumbnail')->first()->content : '' !!}"--}}
                               {{--style="max-width: 100%; height: auto;">&nbsp;</video>--}}
                        {{--<h4>{!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_video_label')->first()) ?--}}
                                    {{--$item->page_sections()->where('section', 'home_section_2_video_label')->first()->content : '' !!}</h4>--}}
                    </div>
                    <a class="btn primary-btn" href="{!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_video_button_link')->first()) ?
                                    $item->page_sections()->where('section', 'home_section_2_video_button_link')->first()->content : '' !!}">
                        {!! !empty($item) && !empty($item->page_sections()->where('section', 'home_section_2_video_button_label')->first()) ?
                                    $item->page_sections()->where('section', 'home_section_2_video_button_label')->first()->content : '' !!}
                    </a>
                </div>
            </div>
        </div>
        {{--@if (!empty($page) && $page->page_sections()->count() > 0 && $page->page_sections()->where('id', 1)->first())--}}
            {{--@php--}}
                {{--$section = $page->page_sections()->where('id', 1)->first();--}}
            {{--@endphp--}}
            {{--{!! $section->content !!}--}}
        {{--@else--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-6 feature-main__item feature-main__item--label" data-aos="zoom-in-right">--}}
                    {{--<div class="article-main padding-right20">--}}
                        {{--<div class="article-main__wrapper">--}}
                            {{--<div class="article-main__label">--}}
                                {{--Who we are--}}
                            {{--</div>--}}
                            {{--<h2>Beautiful Photos & Dedicated Service</h2>--}}
                            {{--<strong>We arrive on time, deliver photos fast and upload to MLS and other sites Free of charge,--}}
                                {{--no one else offers that service.</strong>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6 feature-main__item feature-main__item--video">--}}
                    {{--<div class="video-main pull-top">--}}
                        {{--<div class="video-main__item">--}}
                            {{--<img src="{{ url('public/uploads/video.jpg') }}">--}}
                            {{--<h4> Featured:Video Services Lorem IPSUM </h4>--}}
                        {{--</div>--}}

                        {{--<a href="{{ url('services') }}" class="btn primary-btn"> View Full list of Services </a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
    </div>
</section>