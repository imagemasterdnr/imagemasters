@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="subpage-container">
    <div class="subpage-container__wrapper container">
        @if (!empty($page) && $page->slug != 'schedule-now')
            @include('front.pages.custom-page.sections.calendar-btn')
        @endif

        <div class="col-md-12">
            {{--{!! !empty($page) ? isset($page->content) ? $page->content : '' : '' !!}--}}
            {!! !empty($page) && !empty($page->page_sections()->where('section', 'content')->first()) ?
                $page->page_sections()->where('section', 'content')->first()->content : '' !!}
        </div>

    </div>
</section>