@include('front.pages.custom-page.sections.sub_banner_generic')

<section class="subpage-container">
    <div class="subpage-container__wrapper container">
        <div class="col-md-12">
            <div class="alert alert-warning text-center {{ !empty($search_items) ? ($search_items['data']->count() == 0 ? '' : 'johnCena') : '' }}">
                No results found!
            </div>
            <div class="{{--table-responsive--}} {{ !empty($search_items) ? ($search_items['data']->count() == 0 ? 'johnCena' : '') : 'johnCena' }}">
                <table id="search-table"
                       class="table table-bordered table-striped table-vcenter">
                    <thead>
                    <tr role="row">
                        <th class="text-left">Results</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($search_items))
                        @foreach($search_items['data'] as $search_item)
                            <tr>
                                <td class="text-left">
                                    @if (!empty($search_item->item_type) && $search_item->item_type == 'product')
                                        <a href="{{ url('services/' . (!empty($search_item->product_categories()->first()) ? $search_item->product_categories()->first()->slug . '/' : '') . $search_item->slug) }}">
                                            {!! highlight_word((!empty($search_params) && isset($search_params['keyword']) ? $search_params['keyword'] : ''), $search_item->name) !!}
                                            <br>
                                        </a>
                                    @elseif (!empty($search_item->item_type) && $search_item->item_type == 'category')
                                        <a href="{{ url('services/' . $search_item->slug) }}">
                                            {!! highlight_word((!empty($search_params) && isset($search_params['keyword']) ? $search_params['keyword'] : ''), $search_item->name) !!}
                                            <br>
                                        </a>
                                    @elseif (!empty($search_item->item_type) && $search_item->item_type == 'project')
                                        <a href="{{ url('projects?project_id=' . $search_item->id) }}">
                                            {!! highlight_word((!empty($search_params) && isset($search_params['keyword']) ? $search_params['keyword'] : ''), $search_item->name) !!}
                                            <br>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>