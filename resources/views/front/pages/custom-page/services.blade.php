@if (!empty($product_category))
    @php
        $item->name = $product_category->name;
        if (!empty($product_category->banner_image)) {
            $item->banner_image = $product_category->banner_image;
        } else {
            $item->banner_image = '';
        }
        if (!empty($product_category->banner_video)) {
            $item->banner_video = $product_category->banner_video;
        } else {
            $item->banner_video = '';
        }
    @endphp
@endif
@include('front.pages.custom-page.sections.sub_banner_generic')
<section class="services-display">

    <div class="services-display__wrapper container">

        @include('front.pages.custom-page.sections.calendar-btn')

        <div class="services-headings">
            @if (empty($product_category))
                {{--{!! $item->content !!}--}}
                {!! !empty($item) && !empty($item->page_sections()->where('section', 'content')->first()) ?
                    $item->page_sections()->where('section', 'content')->first()->content : '' !!}
            @endif
        </div>


        <div class="row">
            @if (!empty($services) && count($services))
                @foreach($services as $service)
                    @php
                        if ($service->item_type == 'product') {
                            if (!empty($product_category)) {
                                $link = url('services/' . $product_category->slug . '/' . $service->slug);
                            } else {
                                $link = url('services/' . $service->slug);
                            }
                        } else {
                            $link = url('services/' . $service->slug);
                        }
                    @endphp
                    <div class="col-md-6 services-display__item" data-aos="fade-up"
                    data-aos-anchor-placement="top-bottom">
                        <div class="services-display__content">
                            <a href="{{ $link }}">
                                @if ($service->item_type == 'product')
                                    @if (!empty($service->overview_image))
                                        <div class="services-display--feature-image hidden_image_container" style="background-color: #ddd !important;"
                                             {{--style="background:url('{{ s3_url($service->overview_image) }}') no-repeat,
                                                     url('{{ url(config('constants.placeholder_image')) }}')no-repeat;"--}}>
                                            <img class="hidden_image" style="display: none;" src="{{ s3_url($service->overview_image) }}">
                                        </div>
                                    @else
                                        <div class="services-display--feature-image"
                                             style="background:url('{{ url(config('constants.placeholder_image')) }}') no-repeat;"></div>
                                    @endif
                                @else
                                    @if (!empty($service->overview_image))
                                        <div class="services-display--feature-image hidden_image_container" style="background-color: #ddd !important;"
                                             {{--style="background:url('{{ s3_url($service->overview_image) }}') no-repeat,
                                                     url('{{ url(config('constants.placeholder_image')) }}')no-repeat;"--}}>
                                            <img class="hidden_image" style="display: none;" src="{{ s3_url($service->overview_image) }}">
                                        </div>
                                    @else
                                        <div class="services-display--feature-image"
                                             style="background:url('{{ url(config('constants.placeholder_image')) }}') no-repeat;"></div>
                                    @endif
                                @endif
                            </a>
                            <div class="services-display--content">
                                <a href="{{ $link }}"><h2>{{ $service->name }}</h2></a>
                                <a href="{{ $link }}" class="btn secondary-btn"> View Details</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-md-12">
                    <div class="alert alert-warning text-center">
                        No services found!
                    </div>
                </div>
            @endif
            {{--<div class="col-md-6 services-display__item">--}}
            {{--<div class="services-display__content">--}}
            {{--<a href="#">--}}
            {{--<div class="services-display--feature-image"--}}
            {{--style="background:url('{{ url(config('constants.placeholder_image')) }}')no-repeat;"></div>--}}
            {{--</a>--}}
            {{--<div class="services-display--content">--}}
            {{--<a href="#"><h2>Daytime Photography</h2></a>--}}
            {{--<a href="#" class="btn secondary-btn"> View Details</a>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 services-display__item">--}}
            {{--<div class="services-display__content">--}}
            {{--<a href="#">--}}
            {{--<div class="services-display--feature-image"--}}
            {{--style="background:url('{{ url('/public/uploads/services-image2.jpg') }}')no-repeat;"></div>--}}
            {{--</a>--}}
            {{--<div class="services-display--content">--}}
            {{--<a href="#"><h2>Twilight Photography</h2></a>--}}
            {{--<a href="#" class="btn secondary-btn"> View Details</a>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 services-display__item">--}}
            {{--<div class="services-display__content">--}}
            {{--<a href="#">--}}
            {{--<div class="services-display--feature-image"--}}
            {{--style="background:url('{{ url('/public/uploads/services-image3.jpg') }}')no-repeat;"></div>--}}
            {{--</a>--}}
            {{--<div class="services-display--content">--}}
            {{--<a href="#"><h2>Premium Luxury Photography</h2></a>--}}
            {{--<a href="#" class="btn secondary-btn"> View Details</a>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 services-display__item">--}}
            {{--<div class="services-display__content">--}}
            {{--<a href="#">--}}
            {{--<div class="services-display--feature-image"--}}
            {{--style="background:url('{{ url('/public/uploads/services-image4.jpg') }}')no-repeat;"></div>--}}
            {{--</a>--}}
            {{--<div class="services-display--content">--}}
            {{--<a href="#"><h2>Aerial Photography</h2></a>--}}
            {{--<a href="#" class="btn secondary-btn"> View Details</a>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>