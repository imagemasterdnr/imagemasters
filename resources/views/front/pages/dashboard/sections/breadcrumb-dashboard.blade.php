<div class="dashboard-navigation">
    <div class="dashboard-navigation__wrapper container">
        <div class="row">
            <div class="col-md-12">
                <ul class="dashboard-navigation__parent">
                    <li class="dashboard-navigation__item {{ Request::is('customer/dashboard/account') ? 'active' : '' }}">
                        <a href="{{ route('customer.dashboard.account') }}">my account</a>
                    </li>
                    <li class="dashboard-navigation__item {{ Request::is('customer/dashboard/products') || Request::is('customer/dashboard/products/*') ? 'active' : '' }}">
                        <a href="{{ route('customer.dashboard.products') }}">my products</a>
                    </li>
                    <li class="dashboard-navigation__item {{ Request::is('customer/dashboard/profile') ? 'active' : '' }}">
                        <a href="{{ route('customer.dashboard.profile') }}">profile</a>
                    </li>
                    <li class="dashboard-navigation__item {{ Request::is('customer/dashboard/support') ? 'active' : '' }}">
                        <a href="{{ route('customer.dashboard.support') }}">support</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>