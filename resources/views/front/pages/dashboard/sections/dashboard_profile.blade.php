<div class="text-center dashboard dashboard__profile global-form-style profile-form form--fullwidth">
    <div class="dashboard__head">
        <div class="dashboard__head--title no-underline">
            <h2> Profile </h2>
        </div>
    </div>
    <div class="dashboard__body">
        <div class="dashboard__item view-album">
            {{  Form::open([
               'method' => 'POST',
               'id' => 'form-email',
               'route' => ['customer.dashboard.profile.email.put', auth()->user()->id],
               'class' => 'form-horizontal',
               'files' => TRUE
               ])
            }}
                <ul class="form-box">
                    @if (Session::has('status'))
                        <div class="alert alert-success">
                            {{ Session::get('status') }}
                        </div>
                    @endif
                    {{--<li class="form-group">--}}
                        {{--<label>FIRSTNAME*</label>--}}
                        {{--<input type="text" name="first_name" value="{{ auth()->user()->first_name }}" disabled style="background-color: #ddd;">--}}
                        {{--@if ($errors->has('first_name'))--}}
                            {{--<span id="first_name-error" class="help-block animation-slideDown">--}}
                                {{--{{ $errors->first('first_name') }}--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</li>--}}
                    {{--<li class="form-group">--}}
                        {{--<label>LASTNAME*</label>--}}
                        {{--<input type="text" name="last_name" value="{{ auth()->user()->last_name }}" disabled style="background-color: #ddd;">--}}
                        {{--@if ($errors->has('last_name'))--}}
                            {{--<span id="last_name-error" class="help-block animation-slideDown">--}}
                                {{--{{ $errors->first('last_name') }}--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</li>--}}
                    <li class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>EMAIL*</label>
                        <input type="text" name="email" value="{{ auth()->user()->email }}">
                        @if ($errors->has('email'))
                            <span id="email-error" class="help-block animation-slideDown">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        <label>PHONE*</label>
                        <input type="text" name="phone" value="{{ auth()->user()->phone }}">
                        @if ($errors->has('phone'))
                            <span id="phone-error" class="help-block animation-slideDown">
                            {{ $errors->first('phone') }}
                        </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
                        <label>POSITION</label>
                        <input type="text" name="position" value="{{ auth()->user()->position }}">
                        @if ($errors->has('position'))
                            <span id="position-error" class="help-block animation-slideDown">
                                {{ $errors->first('position') }}
                            </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label>ADDRESS</label>
                        <textarea type="text" name="address">{!! auth()->user()->address !!}</textarea>
                        @if ($errors->has('address'))
                            <span id="address-error" class="help-block animation-slideDown">
                            {{ $errors->first('address') }}
                        </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('license_no') ? 'has-error' : '' }}">
                        <label>LICENSE NO</label>
                        <input type="text" name="license_no" value="{{ auth()->user()->license_no }}">
                        @if ($errors->has('license_no'))
                            <span id="license_no-error" class="help-block animation-slideDown">
                                {{ $errors->first('license_no') }}
                            </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
                        <label>WEBSITE</label>
                        <input type="text" name="website" value="{{ auth()->user()->website }}">
                        @if ($errors->has('website'))
                            <span id="website-error" class="help-block animation-slideDown">
                            {{ $errors->first('website') }}
                        </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('profile_image') ? 'has-error' : '' }}">
                        <label>PROFILE IMAGE</label>
                        <input type="file" name="profile_image">
                        <a href="{{ asset(auth()->user()->profile_image) }}" class="zoom img-thumbnail"
                           style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ asset(auth()->user()->profile_image) }}" class="img-responsive center-block" style="max-width: 100px;"
                                onerror="this.src='{{ asset(config('constants.placeholder_image')) }}'">
                        </a>
                        @if ($errors->has('profile_image'))
                            <span id="profile_image-error" class="help-block animation-slideDown">
                                {{ $errors->first('profile_image') }}
                            </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('company_logo') ? 'has-error' : '' }}">
                        <label>COMPANY LOGO</label>
                        <input type="file" name="company_logo">
                        <a href="{{ asset(auth()->user()->company_logo) }}" class="zoom img-thumbnail"
                           style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ asset(auth()->user()->company_logo) }}" class="img-responsive center-block" style="max-width: 100px;"
                                 onerror="this.src='{{ asset(config('constants.placeholder_image')) }}'">
                        </a>
                        @if ($errors->has('company_logo'))
                            <span id="company_logo-error" class="help-block animation-slideDown">
                            {{ $errors->first('company_logo') }}
                        </span>
                        @endif
                    </li>
                    <li>
                        <button type="submit" class="btn btn--width"> Update</button>
                    </li>
                </ul>
            {{ Form::close() }}

            <br>
            <form id="form-password" action="{{ route('customer.dashboard.profile.password.post', auth()->user()->id) }}" method="POST">
                <ul class="form-box">
                    @if (Session::has('status'))
                        <div class="alert alert-success">
                            {{ Session::get('status') }}
                        </div>
                    @endif
                    <li class="form-group {{ $errors->has('old_password') ? 'has-error' : '' }}">
                        <label>OLD PASSWORD*</label>
                        <input type="password" name="old_password" value="">
                        @if ($errors->has('old_password'))
                            <span id="old_password-error" class="help-block animation-slideDown">
                                {{ $errors->first('old_password') }}
                            </span>
                        @endif
                    </li>
                    <li class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>NEW PASSWORD*</label>
                        <input type="password" id="password" name="password">
                        @if ($errors->has('password'))
                            <span id="password-error" class="help-block animation-slideDown">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </li>
                    <li class="form-group">
                        <label>NEW PASSWORD CONFIRMATION*</label>
                        <input type="password" name="password_confirmation">
                    </li>
                    <li>
                        <button type="submit" class="btn btn--width"> Update Password</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</div>
