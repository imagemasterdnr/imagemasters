<div class="dashboard">
    <div class="dashboard__head">
        <div class="dashboard__head--title no-underline">
            <h2> my products </h2>
        </div>
    </div>
    <div class="dashboard__body">
        <div class="dashboard__item products-view">
            @if ((!empty($appointments) && count($appointments)) || (!empty($cancelled_appointments) && count($cancelled_appointments)))
                <ul class="products-view__display">
                    @foreach($appointments->take(6) as $appointment)
                        <li>
                            <div class="product-thumbnail">
                                <a href="{{ route('customer.dashboard.products.view_album', $appointment->appointment_id) }}">
                                    <div class="product-thumbnail__image hidden_image_container"
                                         style="background-color: #ddd !important;"
                                            {{--style="background-image:url('{{ !empty($appointment->product->overview_image) && $appointment->product->overview_image != ''
                                            ? s3_url($appointment->product->overview_image) : '' }}'),
                                            url('{{ url(config('constants.placeholder_image')) }}');"--}}>
                                        <img class="hidden_image" style="display: none;" src="{{ !empty($appointment->product->overview_image) && $appointment->product->overview_image != ''
                                        ? s3_url($appointment->product->overview_image) : '' }}">
                                        <h4>{{ !empty($appointment->product) ? $appointment->product->name : '' }}</h4>
                                    </div>
                                </a>
                                <div class="product-thumbnail__date">
                                    {{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}
                                </div>
                                <div class="product-thumbnail__buttons">
                                    @include('front.pages.dashboard.sections.dashboard_appointment_block', compact('appointment'))
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @if (count($appointments) < 6)
                        @foreach($cancelled_appointments->take((6 - count($appointments))) as $appointment)
                            <li>
                                <div class="product-thumbnail">
                                    <a href="{{ route('customer.dashboard.products.view_album', $appointment->appointment_id) }}">
                                        <div class="product-thumbnail__image hidden_image_container"
                                             style="background-color: #ddd !important;"
                                                {{--style="background-image:url('{{ !empty($appointment->product->overview_image) && $appointment->product->overview_image != ''
                                                ? s3_url($appointment->product->overview_image) : '' }}'),
                                                url('{{ url(config('constants.placeholder_image')) }}');"--}}>
                                            <img class="hidden_image" style="display: none;" src="{{ !empty($appointment->product->overview_image) && $appointment->product->overview_image != ''
                                        ? s3_url($appointment->product->overview_image) : '' }}">
                                            <h4>{{ !empty($appointment->product) ? $appointment->product->name : '' }}</h4>
                                        </div>
                                    </a>
                                    <div class="product-thumbnail__date">
                                        {{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}
                                    </div>
                                    <div class="product-thumbnail__buttons">
                                        @include('front.pages.dashboard.sections.dashboard_appointment_block', compact('appointment'))
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ul>
            @else
                <div class="col-md-12">
                    <div class="alert alert-warning text-center">
                        No products found!
                    </div>
                </div>
            @endif
        </div>
    </div>
    @if ((count($appointments) + count($cancelled_appointments)) > 6)
        <div class="dashboard__footer text-right">
            <a href="{{ route('customer.dashboard.products') }}" class="btn"> VIEW ALL PRODUCTS </a>
        </div>
    @endif
</div>