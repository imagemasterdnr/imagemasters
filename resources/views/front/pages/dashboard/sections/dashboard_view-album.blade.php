<div class="dashboard">
    <div class="dashboard__head">
        <div class="dashboard__head--title dashboard__head--inside no-underline">
            <h2>{{ !empty($appointment->product) ? $appointment->product->name : '' }}</h2>
            <div class="dashboard__status">
                <div class="dashboard__status--dates dashboard__status--item">
                    {{ !empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime']) ? date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) : '' }}
                </div>
                <div class="dashboard__status--counts dashboard__status--item">
                    {{ $appointment->attachments()->where('extension', '!=', 'mp4')->count() }} PHOTOS /
                    {{ $appointment->attachments()->where('extension', '=', 'mp4')->count() }} VIDEOS
                </div>
            </div>

            @if (!empty($appointment->attachments) && count($appointment->attachments))
                <div class="dashboard__download">
                    <a class="text-decoration-none text-black-50 font-weight-bolder"
                       href="{{ route('customer.dashboard.products.download_images', $appointment->appointment_id) }}">
                        <i class="fas fa-download"></i> DOWNLOAD ALL
                    </a>
                </div>
            @endif

            <div class="dashboard__download" style="margin-right: 10px;">
                <a class="text-decoration-none text-black-50 font-weight-bolder copy-clipboard-btn"
                   href="javascript:void(0)"
                   data-custom-toggle="tooltip" data-trigger="focus" data-placement="top"
                   data-content="{{ route('tour_preview', $appointment->appointment_id) }}" title="Copied!"
                   data-text="{{ route('tour_preview', $appointment->appointment_id) }}">
                    <i class="fas fa-copy"></i> COPY LNK
                </a>
            </div>
            <div class="dashboard__download" style="margin-right: 10px;">
                <a class="text-decoration-none text-black-50 font-weight-bolder copy-clipboard-btn"
                   href="{{ route('customer.dashboard.products.edit_property_details', $appointment->appointment_id) }}">
                    <i class="fas fa-edit"></i> EDIT PROPERTY DETAILS
                </a>
            </div>
        </div>
    </div>
    <div class="dashboard__body">
        <div class="dashboard__item view-album">
            {{--<ul class="view-album__display msrItems">--}}
            {{--<div class="">--}}
            {{--@if ($appointment->attachments()->count())--}}
            {{--@for ($x = 0; $x < 3; $x++)--}}
            {{--@foreach ($appointment->attachments()->get() as $attachment_key => $attachment)--}}
            {{--<li class="itm{{$attachment_key+1}} msrItem msrItems__item"--}}
            {{--style="background-image:url('{{ asset($attachment->file) }}');">--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--@endfor--}}
            {{--@else--}}
            {{--<li class="itm1 msrItem msrItems__item"--}}
            {{--style="background-image:url('{{ url('/public/uploads/gallery5.jpg') }}');">--}}
            {{--<a class="video" video-url="https://vimeo.com/229490822">--}}
            {{--Vimeo--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="itm2 msrItem msrItems__item"--}}
            {{--style="background-image:url('{{ url('/public/uploads/gallery5.jpg') }}');">--}}
            {{--<a class="video" video-url="https://vimeo.com/243244233">--}}
            {{--Vimeo--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="itm3 msrItem msrItems__item"--}}
            {{--style="background-image:url('{{ url('/public/uploads/gallery5.jpg') }}');"></li>--}}
            {{--<li class="itm4 msrItem msrItems__item"></li>--}}
            {{--<li class="itm5 msrItem msrItems__item"></li>--}}
            {{--<li class="itm6 msrItem msrItems__item"></li>--}}
            {{--<li class="itm7 msrItem msrItems__item"></li>--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--</ul>--}}

            {{-- packery plugin --}}
            @if (!empty($appointment->attachments) && count($appointment->attachments))
                <div class="slideshow">
                    @foreach ($appointment->attachments()->where('extension', '!=', 'mp4')->take(1)->get() as $attachment_key => $attachment)
                        <img class="gallery-image-item" style="width:100%;height:100%;" src="{{ s3_url($attachment->file) }}" onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">
                        <a href="javascript:void(0)" class="slideshow__play">
                            <span class="fa fa-play"></span>
                        </a>
                    @endforeach
                </div>
                <div class="grid-gallery" data-toggle="lightbox-gallery">
                    @foreach ($appointment->attachments()->get() as $attachment_key => $attachment)
                        <div class="grid-gallery__item">
                            @if ($attachment->extension == 'mp4')
                                <img class="gallery-image-item" data-video-id="{{ $attachment->id }}" data-video="1" video-src="{{ url($attachment->file) }}" src="{{ s3_url($attachment->file) }}" onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">
                            @else
                                <img class="gallery-image-item" src="{{ s3_url($attachment->file) }}" onerror="this.src='{{ url(config('constants.placeholder_image')) }}'">
                            @endif
                            <div class="grid-gallery__overlay">
                                <a href="{{ s3_url($attachment->file) }}" class="btn btn-download gallery-link">
                                    <i class="fas fa-search"></i>
                                </a>
                                <a href="{{ route('customer.dashboard.products.download_image', $attachment->id)/*url($attachment->file)*/ }}" class="btn btn-download download-image-btn"
                                   download="{{ $attachment->name }}">
                                    <i class="fas fa-download"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="col-md-12">
                    <div class="alert alert-warning text-center">
                        There are no photos yet uploaded.
                    </div>
                </div>
            @endif
            {{-- packery plugin --}}
        </div>
    </div>
</div>