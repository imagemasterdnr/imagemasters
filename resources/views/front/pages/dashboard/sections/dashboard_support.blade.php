<div class="dashboard text-center dashboard__support">
    <div class="dashboard__image">
        <div class="dashboard__image--avatar">
        </div>
    </div>
    <div class="dashboard__head">
        <div class="dashboard__head--title text-center">
            <h2> NEED HELP? </h2>
        </div>
    </div>
    <div class="dashboard__body">
        <div class="row">
            <div class="col-md-4">
                @if (!empty($page) && $page->page_sections()->count() > 0 && $page->page_sections()->where('id', 4)->first())
                    @php
                        $section = $page->page_sections()->where('id', 4)->first();
                    @endphp
                    {!! $section->content !!}
                @else
                    {{-- <p> Lorem ipsum dolor sit amet, netus ut. Lacinia arcu tortor porttitor est tincidunt... </p> --}}
                @endif
                {!! !empty($page) && !empty($page->page_sections()->where('section', 'dashboard_support_content')->first()) ?
                    $page->page_sections()->where('section', 'dashboard_support_content')->first()->content : '' !!}
            </div>
            <div class="col-md-8">
                <div class="contact-form">
                    <form id="create-contact" action="{{ route('save_contact') }}" method="POST">
                                <ul class="form-box">
                                    <li class="form-group">
                                        <label>Name*</label>
                                        <input type="text" name="name" value="{{ auth()->user()->first_name .' '. auth()->user()->last_name }}">
                                    </li>
                                    {{--<li class="form-group">--}}
                                        {{--<label>Company*</label>--}}
                                        {{--<input type="text" name="company">--}}
                                    {{--</li>--}}
                                    <li class="form-group">
                                        <label>EMAIL*</label>
                                        <input type="text" name="email" value="{{ auth()->user()->email }}">
                                    </li>
                                    <li class="form-group">
                                        <label>Phone*</label>
                                        <input type="text" name="phone" value="{{ auth()->user()->phone }}">
                                    </li>
                                    <li class="form-group">
                                        <label>Subject*</label>
                                        <input type="text" name="subject">
                                    </li>
                                    <li class="form-group">
                                        <label>Message*</label>
                                        <textarea name="message"></textarea>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn btn--width"> submit</button>
                                    </li>
                                </ul>
                            </form>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard__footer" style="{{ request()->is('customer/dashboard/account') ? 'display:block' : '' }}">
        <a href="{{ url('customer/dashboard/support') }}" class="btn"> Contact us </a>
    </div>
</div>