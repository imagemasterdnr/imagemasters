<ul>
    @if ($appointment->status == 2)
        <li>
            <a href="javascript:void(0)">CANCELLED</a>
        </li>
    @else
        @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
        && !(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
            <li>
                <a id="cancel-appointment-btn" href="javascript:void(0)"
                   class="text-decoration-none text-black-50 font-weight-bolder cancel-appointment-btn"
                   data-appointment-id="{{ $appointment->appointment_id }}"
                   data-appointment-route="{{ route('customer.appointments.cancel_appointment', $appointment->appointment_id) }}">CANCEL
                    APPOINTMENT
                </a>
            </li>
        @else
            <li>
                <a href="{{ route('customer.dashboard.products.view_album', $appointment->appointment_id) }}">VIEW
                    ALBUM</a>
            </li>
        @endif
    @endif
    <li>
        <a href="{{ route('customer.dashboard.products.order_detail', $appointment->appointment_id) }}">ORDER
            DETAIL</a>
    </li>
</ul>