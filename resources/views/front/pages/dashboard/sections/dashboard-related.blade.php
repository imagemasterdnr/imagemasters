<div class="dashboard-related">
    @if (!empty($products) && count($products))
        <div class="dashboard-related__head">
            <div class="dashboard-related__head--title no-underline">
                <h2> products you may like </h2>
            </div>
        </div>
        <div class="dashboard-related__body">
            <ul>
                @foreach($products->random(4) as $product)
                    <li class="dashboard-related__item">
                        <a href="{{ url('services/' . $product->slug) }}">
                            <div class="dashboard-related__image hidden_image_container" style="background-color: #ddd !important;"
                                 {{--style="background-image:url('{{ !empty($product->overview_image) && $product->overview_image != '' ? s3_url($product->overview_image) : '' }}'),
                                         url('{{ url(config('constants.placeholder_image')) }}');"--}}>
                                <img class="hidden_image" style="display: none;" src="{{ !empty($product->overview_image) && $product->overview_image != '' ? s3_url($product->overview_image) : '' }}">
                                <h4>{{ $product->name }}</h4>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
</div>