<div class="order-details">
    <div class="order-details__wrapper container">

        <div class="row order-details__header">
            <div class="col-md-6">
                <h2>order details</h2>
                <div class="order-details__header--number">
                    <span>Order #</span>
                    <span>{!! $appointment['acuity_details']['id'] !!}</span>
                    @if ($appointment->status != 2)
                        @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                        && (date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                            {{ $appointment->attachments()->where('extension', '!=', 'mp4')->count() }} PHOTOS /
                            {{ $appointment->attachments()->where('extension', '=', 'mp4')->count() }} VIDEOS
                            <a class="text-decoration-none text-black-50 font-weight-bolder"
                               href="{{ route('customer.dashboard.products.view_album', $appointment->appointment_id) }}">VIEW
                                ALBUM</a> |
                            <a class="text-decoration-none text-black-50 font-weight-bolder"
                               href="{{ route('customer.dashboard.products.edit_property_details', $appointment->appointment_id) }}">EDIT PROPERTY DETAILS</a>
                        @endif
                    @endif
                </div>
            </div>
            <div class="col-md-6 text-center cancel-button">
                @if ($appointment->status != 2)
                    @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                        && !(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                        <a id="cancel-appointment-btn" href="javascript:void(0)"
                           class="cancel-appointment-btn btn btn-primary btn-payment"
                           data-appointment-id="{{ $appointment->appointment_id }}"
                           data-appointment-route="{{ route('customer.appointments.cancel_appointment', $appointment->appointment_id) }}">
                            Cancel Appointment
                        </a>
                    @endif
                @else
                    @if ($appointment['acuity_details']['canceled'])
                        <a href="javascript:void(0)"
                           class="btn btn-primary btn-payment">
                            Canceled
                        </a>
                    @endif
                @endif
            </div>
        </div>

        <div class="row order-details__body">
            <div class="col-md-6">
                <h3>order information</h3>
                <table class="table order-details__table order-details__table--information">
                    <tbody>
                    <tr>
                        <td>Service</td>
                        <td>{!! $appointment['acuity_details']['category'] !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! date('F d, Y h:i:s A', strtotime($appointment['acuity_details']['datetime'])) !!}</td>
                    </tr>
                    <tr>
                        <td>Photographer</td>
                        <td>{!! $appointment['acuity_details']['calendar'] !!}</td>
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td>{!! $appointment['acuity_details']['lastName'] !!}</td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td>{!! $appointment['acuity_details']['lastName'] !!}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>{!! $appointment['acuity_details']['phone'] !!}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{!! $appointment['acuity_details']['email'] !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>


            <div class="col-md-6">
                <h3>Appointment Details</h3>
                <table class="table order-details__table order-details__table--billing">
                    <tbody>
                    <tr>
                        <td>Size</td>
                        <td>
                            @if (isset($appointment['acuity_details']['appointment_type']) && !empty($appointment['acuity_details']['appointment_type']))
                                {!! $appointment['acuity_details']['appointment_type']['name'] !!}
                            @else

                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Add ons</td>
                        <td>
                            @if (isset($appointment['acuity_details']['add_ons']) && !empty($appointment['acuity_details']['add_ons']) && count($appointment['acuity_details']['add_ons']))
                            @foreach($appointment['acuity_details']['add_ons'] as $add_on)
                            {!! $add_on['name'] !!}</br>
                            @endforeach
                            @else
                                No add-ons
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Date</strong></td>
                        <td>{!! $appointment['acuity_details']['date'] !!}</td>
                    </tr>
                    <tr>
                        <td><strong>Time</strong></td>
                        <td>{!! $appointment['acuity_details']['time'] !!}</td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>${!! $appointment['acuity_details']['price'] !!}</td>
                    </tr>
                    <tr>
                        <td>Amount Paid</td>
                        <td>${!! $appointment['acuity_details']['amountPaid'] !!}</td>
                    </tr>
                    <tr>
                        <td>Notes</td>
                        <td>{!! $appointment['acuity_details']['notes'] !!}</td>
                    </tr>
                    @if (isset($appointment['acuity_details']['forms']) && !empty($appointment['acuity_details']['forms']) && count($appointment['acuity_details']['forms']))
                        @foreach($appointment['acuity_details']['forms'] as $form)
                            @foreach($form['values'] as $form_value)
                                <tr>
                                    <td>{!! ucwords(strtolower($form_value['name'])) !!}</td>
                                    <td>{!! $form_value['value'] !!}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

            <div class="col-md-6">
                <h3>Property Details</h3>
                <table class="table order-details__table order-details__table--information">
                    <tbody>
                    <tr>
                        <td>Beds</td>
                        <td>{!! $appointment['beds'] !!}</td>
                    </tr>
                    <tr>
                        <td>Baths</td>
                        <td>{!! $appointment['baths'] !!}</td>
                    </tr>
                    <tr>
                        <td>Availability Status</td>
                        <td>{!! $appointment['availability_status'] !!}</td>
                    </tr>
                    <tr>
                        <td>Square Feet</td>
                        <td>{!! $appointment['square_feet'] !!}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>{!! $appointment['address'] !!}</td>
                    </tr>
                    <tr>
                        <td>Google Map Iframe Link</td>
                        <td>{!! $appointment['google_map_link'] !!}</td>
                    </tr>
                    <tr>
                        <td>Video Link</td>
                        <td>{!! $appointment['video_link'] !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- end of wrapper of order details --}}
</div>