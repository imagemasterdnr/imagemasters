<div class="order-details">
    <div class="order-details__wrapper container">
        <div class="row order-details__header">
            <div class="col-md-6">
                <h2>Edit Property details</h2>
                <div class="order-details__header--number">
                    <span>Order #</span>
                    <span>{!! $appointment['acuity_details']['id'] !!}</span>
                    @if ($appointment->status != 2)
                        @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                        && (date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                            {{ $appointment->attachments()->where('extension', '!=', 'mp4')->count() }} PHOTOS /
                            {{ $appointment->attachments()->where('extension', '=', 'mp4')->count() }} VIDEOS
                            <a class="text-decoration-none text-black-50 font-weight-bolder"
                               href="{{ route('customer.dashboard.products.view_album', $appointment->appointment_id) }}">VIEW
                                ALBUM</a> |
                            <a class="text-decoration-none text-black-50 font-weight-bolder"
                               href="{{ route('customer.dashboard.products.order_detail', $appointment->appointment_id) }}">ORDER DETAILS</a>
                        @endif
                    @endif
                </div>
            </div>
            <div class="col-md-6 text-center cancel-button">
                @if ($appointment->status != 2)
                    @if (!empty($appointment['acuity_details']) && isset($appointment['acuity_details']['datetime'])
                        && !(date('Y-m-d H:i:s', strtotime($appointment['acuity_details']['datetime'])) <= Carbon::now()->toDateTimeString()))
                        <a id="cancel-appointment-btn" href="javascript:void(0)"
                           class="cancel-appointment-btn btn btn-primary btn-payment"
                           data-appointment-id="{{ $appointment->appointment_id }}"
                           data-appointment-route="{{ route('customer.appointments.cancel_appointment', $appointment->appointment_id) }}">
                            Cancel Appointment
                        </a>
                    @endif
                @else
                    @if ($appointment['acuity_details']['canceled'])
                        <a href="javascript:void(0)"
                           class="btn btn-primary btn-payment">
                            Canceled
                        </a>
                    @endif
                @endif
            </div>
        </div>

        <div class="dashboard__body">
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-form">
                        <form id="edit-property-detail" action="{{ route('customer.dashboard.products.update_property_details', $appointment->appointment_id) }}" method="POST">
                            <ul class="form-box">
                                <li class="form-group">
                                    <label>BEDS</label>
                                    <input type="text" name="beds" value="{{ $appointment->beds }}">
                                </li>
                                <li class="form-group">
                                    <label>BATHS</label>
                                    <input type="text" name="baths" value="{{ $appointment->baths }}">
                                </li>
                                <li class="form-group">
                                    <label>AVAILABILITY STATUS</label>
                                    <input type="text" name="availability_status" value="{{ $appointment->availability_status }}">
                                </li>
                                <li class="form-group">
                                    <label>SQUARE FEET</label>
                                    <input type="text" name="square_feet" value="{{ $appointment->square_feet }}">
                                </li>
                                <li class="form-group">
                                    <label>ADDRESS</label>
                                    <textarea name="address">{!! $appointment->address !!}</textarea>
                                </li>
                                <li class="form-group">
                                    <label>GOOGLE MAP IFRAME LINK</label>
                                    <textarea name="google_map_link">{!! $appointment->google_map_link !!}</textarea>
                                </li>
                                <li class="form-group">
                                    <label style="position: unset;">IS BRANDED?
                                        <input type="checkbox" id="is_branded" name="is_branded"
                                               style="width: auto;vertical-align: sub;display: inline-block;"
                                               value="1" {{ $appointment->is_branded ? 'checked' : '' }}>
                                    </label>
                                </li>
                                <li class="form-group">
                                    <label>VIDEO LINK</label>
                                    <textarea name="video_link">{!! $appointment->video_link !!}</textarea>
                                </li>
                                <li>
                                    <button type="submit" class="btn btn--width"> submit</button>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end of wrapper of order details --}}
</div>