<div class="dashboard text-center">
        <div class="dashboard__head">
            <div class="dashboard__head--title text-center">
                <h2> my profile </h2>
            </div>
        </div>
        <div class="dashboard__body">
            <ul>
                <li>{{ auth()->user()->first_name .' '. auth()->user()->last_name }}</li>
                {{--<li><span>VISA</span>•••1111</li>--}}
            </ul>
            <ul>
                <li>{{ auth()->user()->email }}</li>
                {{--<li><span>PASSWORD:</span> ••••••••</li>--}}
            </ul>
        </div>
        <div class="dashboard__footer">
            <a href="{{ route('customer.dashboard.profile')  }}" class="btn"> Edit </a>
        </div>
</div>
