@extends('front.pages.index')

@section('content')
@if (!empty($page))
    @php
        $item = $page;
    @endphp
@endif

@include('front.pages.custom-page.sections.sub_banner_generic')

@include('front.pages.dashboard.sections.breadcrumb-dashboard')

<section class="main-subpage main-dashboard">
    <div class="main-subpage__wrapper container">
        <div class="row">
            <div class="col-md-12 main-dashboard__left">
                {{-- display my products --}}
                @include('front.pages.dashboard.sections.dashboard_all-product')
            </div>
        </div>
        {{-- end of row --}}
    </div>
</section>
@endsection

@section('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/front_products.js') }}"></script>
@endsection