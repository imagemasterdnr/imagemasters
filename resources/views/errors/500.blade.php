<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>{!! $seo_meta['title'] !!}</title>

    <meta name="description" content="{!! $seo_meta['description'] !!}">
    <meta name="author" content="{!! $seo_meta['author'] !!}">
    <meta name="robots" content="{!! $seo_meta['robots'] !!}">
    <meta name="keywords" content="{!! $seo_meta['keywords'] !!}">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{!! $seo_meta['title'] !!}">
    <meta property="og:description" content="{!! $seo_meta['description'] !!}">
    <meta property="og:url" content="{!! url('') !!}">
    <meta property="og:site_name" content="{!! $seo_meta['name'] !!}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{asset('public/proui-backend/img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon57.png')}}" sizes="57x57">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon72.png')}}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon76.png')}}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon114.png')}}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon120.png')}}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon144.png')}}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon152.png')}}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{asset('public/proui-backend/img/icon180.png')}}" sizes="180x180">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{ asset('public/proui-backend/css/bootstrap.min.css') }}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{ asset('public/proui-backend/css/plugins.css') }}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{ asset('public/proui-backend/css/main.css') }}">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{ asset('public/proui-backend/css/themes.css') }}">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) -->
    <script src="{{ asset('public/proui-backend/js/vendor/modernizr.min.js') }}"></script>
</head>
<body>
<!-- Error Container -->
<div id="error-container">
    <div class="error-options">
        <h3><i class="fa fa-chevron-circle-left text-muted"></i> <a href="{{ (URL::current() == URL::previous()) ? url('') : URL::previous() }}">Go Back</a></h3>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1><i class="fa fa-cog fa-spin text-danger"></i> 500</h1>
            <h2 class="h3">Oops, we are sorry but our server encountered an internal error..<br>But do not worry, we are
                on it..</h2>
        </div>
    </div>
</div>
<!-- END Error Container -->
</body>
</html>