//   AOS.init();
  AOS.init({
    duration: 1200,
  })


  // go to top on refresh
  $(window).on('load', function(){
      if (window.location.href == sBaseURI + '/') {
        setTimeout(function () {
            $(window).scrollTop(0);
        }, 50);
      }
  });


  // calendar icon scroll to fix
  jQuery(window).scroll(function() {
  var scroll = jQuery(window).scrollTop();

  if (scroll >= 500) {
    jQuery(".btn-calendar").addClass("scrolling");
    } else {
    jQuery(".btn-calendar").removeClass("scrolling");
    }
  });


  // for menu mobile
  $(".main-navigation__mobile--button").click(function(){
    $(".main-navigation__mobile--menu").toggleClass('show-me');
  });



// contact us
  $("ul.form-box .form-group input").focus(function(){
    $(this).parent().addClass('active');

  }).blur(function(){
    $(this).parent().removeClass('active');
  })

  $("ul.form-box .form-group textarea").focus(function(){
    $(this).parent().addClass('active');

  }).blur(function(){
    $(this).parent().removeClass('active');
  })


// activate search box
  $(".search-now").click(function(){
    $(".search-box").toggleClass('search-visible');
    setTimeout(function() {
        var sValue = $('[name="keyword"]').val();
        $('[name="keyword"]').focus().val('').val(sValue);
    }, 100);
  });

  $(".search-box__button").click(function(){
    $(".search-box").toggleClass('search-visible');
  });


// $('.mega-menu-btn, .invisible-mega').on('mouseover', function (e) {
//     e.preventDefault();
//     $(".sub-megamenu").toggleClass('sub-megamenu-visible');
//     $(".invisible-mega").toggleClass('invisible-show');
//
//     $('.sub-megamenu-visible').focus();
// });

$('.mega-menu-btn').mouseenter(function (e) {
    e.preventDefault();
    $(".sub-megamenu").addClass('sub-megamenu-visible');
    $(".invisible-mega").addClass('invisible-show');
    $('.sub-megamenu-visible').focus();
});
$('.mega-menu-btn').mouseleave(function (e) {
    e.preventDefault();
    console.log($(e.relatedTarget))
    if ($(e.relatedTarget).hasClass('sub-megamenu-visible') || $(e.relatedTarget).hasClass('sub-megamenu__background')) {
        $('.sub-megamenu-visible .sub-megamenu__background').unbind();
        $('.sub-megamenu-visible .sub-megamenu__background').mouseenter(function () {
            $(".sub-megamenu").addClass('sub-megamenu-visible');
            $(".invisible-mega").addClass('invisible-show');
        });
        $('.sub-megamenu-visible .sub-megamenu__background').mouseleave(function () {
            console.log('menuhoverout')
            $(".sub-megamenu").removeClass('sub-megamenu-visible');
            $(".invisible-mega").removeClass('invisible-show');
        });
    } else {
        $(".sub-megamenu").removeClass('sub-megamenu-visible');
        $(".invisible-mega").removeClass('invisible-show');
    }
});

// for the mobile submenu
$(".nav-item.sub-arrow ").click(function(){
  $(".sub-arrow").toggleClass('display--submenu');
});
