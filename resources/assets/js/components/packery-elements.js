$(window).load(function () {
    $('.gallery').packery({
        itemSelector: '.gallery-item'
    });
    $(window).resize(function () {
        $('.gallery').packery({
            itemSelector: '.gallery-item'
        });
    });
});