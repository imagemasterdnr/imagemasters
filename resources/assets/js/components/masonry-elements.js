var time = undefined;
$(function(){
    //init
    $('.msrItems').msrItems({
        'colums': 4, //columns number

    });

    //update columns size on window resize
    $( window ).on('resize', function(e) {
        clearTimeout(time);
        time = setTimeout(function(){
            $('.msrItems').msrItems('refresh');
        }, 50);
    })

    //load new elements(without updating existing)
    // $('.msrItems').msrItems('doload', [
    //     '<div class="itm8 msrItem"></div>',
    //     '<div class="itm9 msrItem"></div>',
    //     '<div class="itm10 msrItem"></div>'
    // ]);
});