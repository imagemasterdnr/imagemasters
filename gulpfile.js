const elixir = require('laravel-elixir');

process.env.DISABLE_NOTIFIER = true;
elixir.config.sourcemaps = true;
elixir.config.css.autoprefix.options.browsers = ['> 2%', 'Last 42 versions', 'IE 9'];

elixir((mix) => {
    mix.sass('./resources/assets/scss/app.scss', 'public/css');
    mix.scripts([
            './node_modules/popper.js/dist/umd/popper.js',
            './node_modules/bootstrap/dist/js/bootstrap.js',
            './node_modules/slick-carousel/slick/slick.js',
            // './node_modules/masonry/test/masonry.js',

            './resources/assets/js/masonry/src/masonry.js',
            // './resources/assets/js/vimeo/video.popup.js',


            './resources/assets/js/components/slick-elements.js',
            // './resources/assets/js/components/masonry-elements.js',
            // './resources/assets/js/components/packery-elements.js',
            // './node_modules/packery/dist/packery.pkgd.js',
            // './resources/assets/js/components/vimeo-elements.js',
            './node_modules/aos/dist/aos.js',
            './resources/assets/js/app.js',
        ],
        'public/js/app.js');
});