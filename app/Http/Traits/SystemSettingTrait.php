<?php

namespace App\Http\Traits;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Project;
use App\Models\SeoMeta;
use App\Models\SystemSetting;
use Illuminate\Support\Facades\Log;

/**
 * Class SystemSettingTrait
 * @package App\Http\Traits
 * @author Randall Anthony Bondoc
 */
trait SystemSettingTrait
{

    /**
     * @param $code
     *
     * @return mixed
     */
    public function getSystemSettingByCode($code)
    {
        $system_setting = SystemSetting::where('code', $code)->first();
        return $system_setting;
    }

    /**
     * @return mixed
     */
    public function getSystemSettings()
    {
        $system_settings = SystemSetting::get();
        return $system_settings;
    }

    /**
     * Generate system generated codes by getting max id of the passed model then increment by one
     *
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @param  string $code
     *
     * @return mixed
     */
    public function generateSystemCode($model, $code = 'SS')
    {
        $max_code = $code . '0001';
        if ($model) {
            $max_id = $model->max('id');
            if ($max_id) {
                $max_code = substr($max_code, 0, -strlen($max_id)) . '' . ($max_id + 1);
            }
        }
        return $max_code;
    }

    /**
     * Get seo meta fields
     *
     * @param array $page
     *
     * @return mixed
     */
    public function getSeoMeta($page = [])
    {
        $seo_meta = [
            'name' => 'Laravel Template',
            'author' => 'ravbondoc',
            'robots' => 'noindex, nofollow',
            'title' => 'Laravel Template',
            'keywords' => 'Laravel Template',
            'description' => 'Laravel Template. ACL Integrated (Access Control List).',
            'mega_menu_image' => 'public/uploads/mega-image.jpg',
            'facebook_link' => 'https://www.facebook.com/',
            'twitter_link' => 'https://twitter.com/',
            'pinterest_link' => 'https://pinterest.com/',
            'instagram_link' => 'https://www.instagram.com/',
        ];

        if(env('APP_ENV') == 'prod' || env('APP_ENV') == 'prod_ssl') {
            $seo_meta['robots'] = 'index, follow';
        }

        $system_settings = $this->getSystemSettings();

        foreach ($system_settings as $system_setting) {
            if ($system_setting->code == 'SS0001') {
                $seo_meta['name'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0002') {
                $seo_meta['email'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0003') {
                $seo_meta['phone'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0004') {
                $seo_meta['title'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0005') {
                $seo_meta['keywords'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0006') {
                $seo_meta['description'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0007') {
                $seo_meta['mega_menu_image'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0008') {
                $seo_meta['facebook_link'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0009') {
                $seo_meta['twitter_link'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0010') {
                $seo_meta['pinterest_link'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0011') {
                $seo_meta['instagram_link'] = $system_setting->value;
            }
            if ($system_setting->code == 'SS0012') {
                $seo_meta['footer_copyright_text'] = $system_setting->value;
            }
        }

        if (isset($page) && !empty($page)) {
            if (isset($page->seo_meta_id)) {
                $page_seo_meta = SeoMeta::find($page->seo_meta_id);
                if (!empty($page_seo_meta)) {
                    $seo_meta['title'] = $page_seo_meta->meta_title;
                    $seo_meta['keywords'] = $page_seo_meta->meta_keywords;
                    $seo_meta['description'] = $page_seo_meta->meta_description;
                }
            }
        }

        view()->share(['seo_meta' => $seo_meta]);
        return $seo_meta;
    }

    public function getInstaFeed()
    {
        $instagram_feed = [];
        $access_token = "4860688627.bca3d6a.7a79772ce7e048919a52ff8fd6739a93";
        $user_id = "image_masters";
        $ig_client = new \GuzzleHttp\Client;
        $ig = "https://api.instagram.com/v1/users/" . $user_id . "/media/recent?access_token=" . $access_token;
        try {
            $ig_response = $ig_client->get($ig)->getBody();
            $response = json_decode($ig_response);
            $instagram_feed = $response->data;
        } catch (ClientException $ex) {
            Log::debug('Curl Client Exception:' . $ex->getMessage());
            $instagram_feed = [];
        } catch (\Exception $ex) {
            Log::debug('Curl Exception:' . $ex->getMessage());
            $instagram_feed = [];
        }
        return view()->share(['instagram_feed' => $instagram_feed]);
    }

    public function getInstaFeedToken()
    {
        $client_id = "";
        $client_secret = "";
        $redirect_url = "http://54.68.88.28/imagemasters";
        $access_token_url = "https://api.instagram.com/oauth/access_token/";

        $client = new \GuzzleHttp\Client;
        /*authorize*/
        /*copy this and paste in the browser to get the code to create access token*/
        $authorize_url = "https://api.instagram.com/oauth/authorize/?client_id=" . $client_id . "&client_secret=" . $client_secret . "&redirect_uri=" . $redirect_url . "&response_type=code&scope=basic+public_content";
//        dd($authorize_url);

        /*get access token*/
        $response = $client->post($access_token_url, [
            'form_params' => [
                "client_id" => $client_id,
                "client_secret" => $client_secret,
                "redirect_uri" => $redirect_url,
                "code" => ""/*use the code from authorize url*/,
                "grant_type" => "authorization_code"
            ]
        ])->getBody();

        dd(json_decode($response));
    }

    /**
     * Search by keyword
     *
     * @param  array $params
     *
     * @return mixed
     */
    public function searchKeyword($params = [])
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $keyword = isset($params['keyword']) && !empty($params['keyword']) && ($params['keyword'] != 'null') ? urldecode($params['keyword']) : '';
        $limit = isset($params['limit']) && !empty($params['limit']) && ($params['limit'] != 'null') ? $params['limit'] : 10;
        $sort = isset($params['sort']) && !empty($params['sort']) && ($params['sort'] != 'null') ? $params['sort'] : 'name';
        $orderby = isset($params['orderby']) && !empty($params['orderby']) && ($params['orderby'] != 'null') ? $params['orderby'] : 'asc';

        if ($keyword != '') {
            $product_categories = ProductCategory::orWhere('name', 'LIKE', '%' . $keyword . '%')
                ->where('is_active', '1')
                ->orderby($sort, $orderby)
                ->get();

            $product_categories->map(function ($item) {
                $item['item_type'] = 'category';
                return $item;
            });

            $products = Product::orWhere('name', 'LIKE', '%' . $keyword . '%')
                ->where('is_active', 1)
                ->orderby($sort, $orderby)
                ->get();

            $products->map(function ($item) {
                $item['item_type'] = 'product';
                return $item;
            });

            $projects = Project::orWhere('name', 'LIKE', '%' . $keyword . '%')
                ->where('is_active', 1)
                ->orderby($sort, $orderby)
                ->get();

            $projects->map(function ($item) {
                $item['item_type'] = 'project';
                return $item;
            });

            $items = collect([]);
            $items = $items->merge($product_categories)->merge($products)->merge($projects);

            if ($items->count()) {
                $response['data'] = $items;
                $response['status'] = TRUE;
            } else {
                $response['messages'][] = 'No results found.';
            }
        } else {
            $response['messages'][] = 'No results found.';
        }

        $response['data'] = collect($response['data']);
        return $response;
    }
}