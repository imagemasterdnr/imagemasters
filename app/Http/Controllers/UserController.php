<?php

namespace App\Http\Controllers;

use AcuityScheduling;
use App\Models\User;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Http\Request;
use Session;
use Spatie\Permission\Models\Role;

/**
 * Class UserController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles users.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param User $user_model
     * @param UserRepository $user_repository
     * @param Role $role_model
     */
    public function __construct(User $user_model,
                                Role $role_model,
                                UserRepository $user_repository
    )
    {
        /*
         * Model namespace
         * using $this->user_model can also access $this->user_model->where('id', 1)->get();
         * */
        $this->user_model = $user_model;
        $this->role_model = $role_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of posts with other data (related tables).
         * */
        $this->user_repository = $user_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read User')) {
            abort('401', '401');
        }

        $all_users = $this->user_model->get();
        $users = [];
        if (!Auth::user()->hasRole('Super Admin')) {
            foreach ($all_users as $all_user) {
                if (!$all_user->hasRole('Super Admin')) {
                    $users[] = $all_user;
                }
            }
        } else {
            $users = $all_users;
        }

        $filtered_users = [];
        foreach ($users as $user) {
            if (!$user->hasRole('Customer')) {
                $filtered_users[] = $user;
            }
        }

        $users = collect($filtered_users);

        return view('admin.pages.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPermissionTo('Create User')) {
            abort('401', '401');
        }

        $roles = $this->role_model->get();
        return view('admin.pages.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('Create User')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'user_name' => 'required|max:45|unique:users',
            'email' => 'required|email|max:45|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = $this->user_model->create($request->only('first_name', 'last_name', 'user_name', 'email',
            'password', 'photographer_calendar_id'));

        $roles = $request['roles'];
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = $this->role_model->where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r);
            }
        }

        return redirect()->route('admin.users.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'User successfully added.',
                'type' => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('Read User')) {
            abort('401', '401');
        }

        $user = $this->user_model->findOrFail($id);

        if (!Auth::user()->hasRole('Super Admin')) {
            if ($user->hasRole('Super Admin')) {
                abort('401', '401');
            }
        }

        $appointments = $user->my_appointments()->get();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        foreach ($appointments as $appointment) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $appointment['acuity_details'] = $acuity_details;
        }

        return view('admin.pages.user.show', compact('user', 'appointments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPermissionTo('Update User')) {
            abort('401', '401');
        }

        $user = $this->user_model->findOrFail($id);

        if (!Auth::user()->hasRole('Super Admin')) {
            if ($user->hasRole('Super Admin')) {
                abort('401', '401');
            }
        }

        if ($user->hasRole('Customer')) {
            abort('401', '401');
        }

        $roles = $this->role_model->get();

        return view('admin.pages.user.edit', compact('user', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('Update User')) {
            abort('401', '401');
        }

        $user = $this->user_model->findOrFail($id);

        if (!Auth::user()->hasRole('Super Admin')) {
            if ($user->hasRole('Super Admin')) {
                abort('401', '401');
            }
        }

        $this->validate($request, [
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'user_name' => 'required|max:45|unique:users,user_name,' . $id,
            'email' => 'required|email|max:45|unique:users,email,' . $id,
            'password' => 'required_if:change_password,==,1|min:6|confirmed',
        ]);

        if ($request->get('change_password') == '1') {
            $input = $request->only(['first_name', 'last_name', 'user_name', 'email', 'is_active', 'password', 'photographer_calendar_id']);
        } else {
            $input = $request->only(['first_name', 'last_name', 'user_name', 'email', 'is_active', 'photographer_calendar_id']);
        }

        $input['is_active'] = $input['is_active'] ? 1 : 0;
        $roles = $request['roles'];
        $user->fill($input)->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);
        } else {
            $user->roles()->detach();
        }
        return redirect()->route('admin.users.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'User successfully updated.',
                'type' => 'success'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPermissionTo('Delete User')) {
            abort('401', '401');
        }

        $user = $this->user_model->findOrFail($id);

        if (!Auth::user()->hasRole('Super Admin')) {
            if ($user->hasRole('Super Admin')) {
                abort('401', '401');
            }
        }

        $user->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'User successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return json_encode($response);
    }

    public function updatePassword(Request $request, $id)
    {
        if (!Auth::user()->hasRole('Customer')) {
            abort('401', '401');
        }

        if (auth()->user()->id != $id) {
            abort('401', '401');
        }

        $user = $this->user_model->findOrFail($id);
        $this->validate($request, [
            'old_password' => 'required|min:8|old_password:' . auth()->user()->password,
            'password' => 'required|min:8|confirmed',
        ]);

        $input = $request->only(['password']);
        $user->fill($input)->save();

        return redirect()->back()
            ->with('flash_message', [
                'title' => '',
                'message' => 'Password successfully updated.',
                'type' => 'success'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCustomersIndex()
    {
        if (!Auth::user()->hasPermissionTo('Read User')) {
            abort('401', '401');
        }

        $customers = $this->user_model
            ->role(['Customer'])
            ->get();

        return view('admin.pages.customer.index', compact('customers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getCustomersShow($id)
    {
        if (!Auth::user()->hasPermissionTo('Read User')) {
            abort('401', '401');
        }

        $customer = $this->user_model->findOrFail($id);

        if (!Auth::user()->hasRole('Super Admin')) {
            if ($customer->hasRole('Super Admin')) {
                abort('401', '401');
            }
        }

        $appointments = $customer->my_appointments()->get();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        foreach ($appointments as $appointment) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $appointment['acuity_details'] = $acuity_details;
        }

        return view('admin.pages.customer.show', compact('customer', 'appointments'));
    }
}
