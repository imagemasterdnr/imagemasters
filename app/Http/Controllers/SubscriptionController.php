<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use Auth;
use Illuminate\Http\Request;
use Newsletter;
use Session;

/**
 * Class SubscriptionController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class SubscriptionController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | subscription Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles subscriptions.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param subscription $subscription_model
     */
    public function __construct(Subscription $subscription_model,
                                Newsletter $newsletter)
    {
        /*
         * Model namespace
         * using $this->subscription_model can also access $this->subscription_model->where('id', 1)->get();
         * */
        $this->subscription_model = $subscription_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of subscriptions with other data (related tables).
         * */


        $this->newsletter = $newsletter;

//        $this->middleware(['isAdmin'], [
//            'except' => [
//                'saveSubscription',
//            ]
//        ]);
    }

    /**
     * Save the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function saveSubscription(Request $request)
    {
        $response = [
            'status' => FALSE,
            'message' => [],
            'data' => [],
        ];

        $input = $request->all();
        $is_subscribed = $this->newsletter->isSubscribed($request->get('email')); //returns a boolean
        if ($is_subscribed) {
            $response['message'][] = 'Email already existing!';
//            return redirect()->back()
//                ->with('flash_message', [
//                    'title' => '',
//                    'message' => 'Email already existing!',
//                    'type' => 'error'
//                ]);
        } else {
            $newsletter_response = $this->newsletter->subscribe($request->get('email'), [
                'FNAME' => $request->get('first_name'),
                'LNAME' => $request->get('last_name')
            ]);
            if ($newsletter_response) {
                $subscription = $this->subscription_model->create($input);
//                $this->sendConfirmationEmail($subscription);
                $response['message'][] = 'Thank you for subscribing to our newsletter!';
                $response['status'] = TRUE;
            } else {
                $response['message'][] = 'Please try again.';
//                return redirect()->back()
//                    ->with('flash_message', [
//                        'title' => '',
//                        'message' => 'Please try again.',
//                        'type' => 'error'
//                    ]);
            }
        }

        return json_encode($response);
        return redirect()->back()
            ->with('flash_message', [
                'title' => '',
                'message' => 'Thank you for subscribing to our newsletter!',
                'type' => 'success'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read Subscription')) {
            abort('401', '401');
        }

        $subscriptions = $this->subscription_model->get();

        return view('admin.pages.subscription.index', compact('subscriptions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('Read Subscription')) {
            abort('401', '401');
        }

        $subscription = $this->subscription_model->findOrFail($id);

        return view('admin.pages.subscription.show', compact('subscription'));
    }
}
