<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use App\Models\ProductCategoryImage;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\SeoMetaRepository;
use Auth;
use File;
use Illuminate\Http\Request;
use Session;
use Storage;

/**
 * Class ProductCategoryController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ProductCategoryController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | ProductCategory Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles product categories
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param ProductCategory $product_category_model
     * @param ProductCategoryRepository $product_category_repository
     * @param SeoMetaRepository $seo_meta_repository
     * @param ProductCategoryImage $product_category_image_model
     */
    public function __construct(ProductCategory $product_category_model,
                                ProductCategoryRepository $product_category_repository,
                                SeoMetaRepository $seo_meta_repository,
                                ProductCategoryImage $product_category_image_model
    )
    {
        /*
         * Model namespace
         * using $this->product_model can also access $this->product_model->where('id', 1)->get();
         * */
        $this->product_category_model = $product_category_model;
        $this->product_category_image_model = $product_category_image_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of products with other data (related tables).
         * */
        $this->product_category_repository = $product_category_repository;
        $this->seo_meta_repository = $seo_meta_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read Product Category')) {
            abort('401', '401');
        }

        $product_categories = $this->product_category_repository->getAll();

        return view('admin.pages.product_category.index', compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPermissionTo('Create Product Category')) {
            abort('401', '401');
        }

        return view('admin.pages.product_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('Create Product Category')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'code' => 'required|max:25|unique:products',
            'name' => 'required|max:75|unique:products',
//            'slug' => 'required|max:75|unique:products',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['description'] = stripslashes($input['description']);
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        $product_category = $this->product_category_model->create($input);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->product_category_repository->uploadFile($request->file('banner_image'), $product_category);
            $product_category->fill(['banner_image' => $file_upload_path])->save();
        }

        if ($request->hasFile('banner_video')) {
            $file_upload_path = $this->product_category_repository->uploadFile($request->file('banner_video'), $product_category);
            $product_category->fill(['banner_video' => $file_upload_path])->save();
        }

        if ($request->hasFile('overview_image')) {
            $file_upload_path = $this->product_category_repository->uploadFile($request->file('overview_image'), $product_category);
            $product_category->fill(['overview_image' => $file_upload_path])->save();
        }

//        /* product_category_images */
//        $product_category->product_category_images()->forceDelete();
//
//        if (isset($input['product_category_images'])) {
//            foreach ($input['product_category_images'] as $product_category_image) {
//                $product_category_image = json_decode(urldecode($product_category_image));
//                $new_product_category_image_data = [
//                    'name' => $product_category_image->name,
//                    'size' => $product_category_image->size,
//                    'file' => $product_category_image->file,
//                    'type' => isset($product_category_image->type) ? 1 : 0,
//                    'is_default' => $product_category_image->is_default
//                ];
//                $new_product_category_image = $product_category->product_category_images()->create($new_product_category_image_data);
//            }
//        }
//        /* product_category_images */

        return redirect()->route('admin.product_categories.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Product Category ' . $product_category->name . ' successfully added.',
                'type' => 'success'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPermissionTo('Update Product Category')) {
            abort('401', '401');
        }

        $product_category = $this->product_category_model->findOrFail($id);
        $seo_meta_fields = $product_category->seo_meta;

        return view('admin.pages.product_category.edit', compact('product_category', 'seo_meta_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('Update Product Category')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'code' => 'required|max:25|unique:products,code,' . $id,
            'name' => 'required|max:75|unique:products,name,' . $id,
//            'slug' => 'required|max:75|unique:products,slug,' . $id,
        ]);

        $product_category = $this->product_category_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;

        $input['description'] = stripslashes($input['description']);
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->product_category_repository->uploadFile($request->file('banner_image'), $product_category);
            $input['banner_image'] = $file_upload_path;
        }

        if ($request->hasFile('banner_video')) {
            $file_upload_path = $this->product_category_repository->uploadFile($request->file('banner_video'), $product_category);
            $input['banner_video'] = $file_upload_path;
        }

        if ($request->hasFile('overview_image')) {
            $file_upload_path = $this->product_category_repository->uploadFile($request->file('overview_image'), $product_category);
            $input['overview_image'] = $file_upload_path;
        }

        $product_category->fill($input)->save();

//        /* product_category_images */
//        $product_category->product_category_images()->forceDelete();
//
//        if (isset($input['product_category_images'])) {
//            foreach ($input['product_category_images'] as $product_category_image) {
//                $product_category_image = json_decode(urldecode($product_category_image));
//                $new_product_category_image_data = [
//                    'name' => $product_category_image->name,
//                    'size' => $product_category_image->size,
//                    'file' => $product_category_image->file,
//                    'type' => isset($product_category_image->type) ? 1 : 0,
//                    'is_default' => $product_category_image->is_default
//                ];
//                $new_product_category_image = $product_category->product_category_images()->create($new_product_category_image_data);
//            }
//        }
//        /* product_category_images */

        return redirect()->route('admin.product_categories.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Product Category ' . $product_category->name . ' successfully updated.',
                'type' => 'success'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPermissionTo('Delete Product Category')) {
            abort('401', '401');
        }

        $product_category = $this->product_category_model->findOrFail($id);
        $product_category->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Product Category successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return json_encode($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        /*multiple*/
        $files = $request->file('file');

        $this->validate($request, [
            'file.*.file' => 'mimetypes:image/gif, image/jpg, image/jpeg, image/png',
        ]);

        $file_paths = [];
        foreach ($files as $file) {
            $file_paths[] = $this->product_category_repository->uploadFile($file);
        }

        return response()->json($file_paths, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $file = $request->get('orig-src');

        $product_category_images = $this->product_category_image_model->where('file', $file)->get();
        if (!empty($product_category_images)) {
            foreach ($product_category_images as $product_category_image) {
                $product_category_image->delete();
            }
        }

        if (File::exists($file)) {
            File::delete($file);
            $response['status'] = TRUE;
            $s3 = Storage::disk('s3');
            $s3->delete($file);
        }

        return response()->json($response, 200);
    }
}
