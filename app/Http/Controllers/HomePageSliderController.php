<?php

namespace App\Http\Controllers;

use App\Models\HomePageSlider;
use App\Repositories\HomePageSliderRepository;
use Auth;
use File;
use Illuminate\Http\Request;
use Session;

/**
 * Class Home Page SliderController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class HomePageSliderController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Page Slider Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles home_page_sliders.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param HomePageSlider $home_page_slider_model
     * @param HomePageSliderRepository $home_page_slider_repository
     */
    public function __construct(HomePageSlider $home_page_slider_model,
                                HomePageSliderRepository $home_page_slider_repository
    )
    {
        /*
         * Model namespace
         * using $this->home_page_slider_model can also access $this->home_page_slider_model->where('id', 1)->get();
         * */
        $this->home_page_slider_model = $home_page_slider_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of home_page_sliders with other data (related tables).
         * */
        $this->home_page_slider_repository = $home_page_slider_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read Home Page Slider')) {
            abort('401', '401');
        }

        $home_page_sliders = $this->home_page_slider_repository->getAll();

        return view('admin.pages.home_page_slider.index', compact('home_page_sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPermissionTo('Create Home Page Slider')) {
            abort('401', '401');
        }

        return view('admin.pages.home_page_slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('Create Home Page Slider')) {
            abort('401', '401');
        }

        $this->validate($request, [
//            'name' => 'required|max:75|unique:home_page_sliders',
            'file' => 'required|mimes:gif,jpg,jpeg,png',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['description'] = stripslashes($input['description']);
        $home_page_slider = $this->home_page_slider_model->create($input);

        $file_upload_path = $this->home_page_slider_repository->uploadFile($request->file('file'), $home_page_slider);
        $home_page_slider->fill(['image' => $file_upload_path['data']['file_upload_path']])->save();

        return redirect()->route('admin.home_page_sliders.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Home Page Slider ' . $home_page_slider->name . ' successfully added.',
                'type' => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin/home_page_sliders');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPermissionTo('Update Home Page Slider')) {
            abort('401', '401');
        }

        $home_page_slider = $this->home_page_slider_model->findOrFail($id);
        $seo_meta_fields = $home_page_slider->seo_meta;

        return view('admin.pages.home_page_slider.edit', compact('home_page_slider', 'seo_meta_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('Update Home Page Slider')) {
            abort('401', '401');
        }

        $this->validate($request, [
//            'name' => 'required|max:75|unique:home_page_sliders,name,' . $id,
            'file' => 'mimes:gif,jpg,jpeg,png',
        ]);

        $home_page_slider = $this->home_page_slider_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['description'] = stripslashes($input['description']);

        if ($request->hasFile('file')) {
            $file_upload_path = $this->home_page_slider_repository->uploadFile($request->file('file'), $home_page_slider);
            $input['image'] = $file_upload_path['data']['file_upload_path'];
        }

        $home_page_slider->fill($input)->save();

        return redirect()->route('admin.home_page_sliders.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Home Page Slider ' . $home_page_slider->name . ' successfully updated.',
                'type' => 'success'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPermissionTo('Delete Home Page Slider')) {
            abort('401', '401');
        }

        $home_page_slider = $this->home_page_slider_model->findOrFail($id);
        $home_page_slider->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Home Page Slider successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return json_encode($response);
    }
}
