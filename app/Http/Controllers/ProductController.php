<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductImage;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\SeoMetaRepository;
use Auth;
use File;
use Illuminate\Http\Request;
use Session;
use Storage;

/**
 * Class ProductController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ProductController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Product Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles products.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param Product $product_model
     * @param ProductRepository $product_repository
     * @param ProductCategoryRepository $product_category_repository
     * @param SeoMetaRepository $seo_meta_repository
     * @param ProductImage $product_image_model
     * @param ProjectRepository $project_repository
     */
    public function __construct(Product $product_model,
                                ProductRepository $product_repository,
                                ProductCategoryRepository $product_category_repository,
                                SeoMetaRepository $seo_meta_repository,
                                ProductImage $product_image_model,
                                ProjectRepository $project_repository
    )
    {
        /*
         * Model namespace
         * using $this->product_model can also access $this->product_model->where('id', 1)->get();
         * */
        $this->product_model = $product_model;
        $this->product_image_model = $product_image_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of products with other data (related tables).
         * */
        $this->product_repository = $product_repository;
        $this->product_category_repository = $product_category_repository;
        $this->seo_meta_repository = $seo_meta_repository;
        $this->project_repository = $project_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read Product')) {
            abort('401', '401');
        }

        $products = $this->product_repository->getAll();

        return view('admin.pages.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPermissionTo('Create Product')) {
            abort('401', '401');
        }

        $product_categories = $this->product_category_repository->getAll();
        $projects = $this->project_repository->getAll();

        return view('admin.pages.product.create', compact('product_categories', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('Create Product')) {
            abort('401', '401');
        }

        $this->validate($request, [
//            'code' => 'required|max:25|unique:products',
            'name' => 'required|max:75|unique:products',
//            'slug' => 'required|max:75|unique:products',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['acuity_scheduling_link'] = stripslashes($input['acuity_scheduling_link']);
        $input['description'] = stripslashes($input['description']);
        $input['pricing_content'] = isset($input['pricing_content']) ? stripslashes($input['pricing_content']) : '';
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        $product = $this->product_model->create($input);

        /* product_categories */
        if (isset($input['product_categories'])) {
            $product->product_categories()->sync($input['product_categories']);
        } else {
            $product->product_categories()->detach();
        }
        /* product_categories */

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->product_repository->uploadFile($request->file('banner_image'), $product);
            $product->fill(['banner_image' => $file_upload_path])->save();
        }

        if ($request->hasFile('overview_image')) {
            $file_upload_path = $this->product_repository->uploadFile($request->file('overview_image'), $product);
            $product->fill(['overview_image' => $file_upload_path])->save();
        }

//        /* product_images */
//        $product->product_images()->forceDelete();
//
//        if (isset($input['product_images'])) {
//            foreach ($input['product_images'] as $product_image) {
//                $product_image = json_decode(urldecode($product_image));
//                $new_product_image_data = [
//                    'name' => $product_image->name,
//                    'size' => $product_image->size,
//                    'file' => $product_image->file,
//                    'type' => isset($product_image->type) ? 1 : 0,
//                    'is_default' => $product_image->is_default
//                ];
//                $new_product_image = $product->product_images()->create($new_product_image_data);
//            }
//        }
//        /* product_images */

        return redirect()->route('admin.products.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Product ' . $product->name . ' successfully added.',
                'type' => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin/products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPermissionTo('Update Product')) {
            abort('401', '401');
        }

        $product = $this->product_model->findOrFail($id);
        $product_categories = $this->product_category_repository->getAll();
        $seo_meta_fields = $product->seo_meta;
        $projects = $this->project_repository->getAll();

        return view('admin.pages.product.edit', compact('product', 'product_categories', 'seo_meta_fields', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('Update Product')) {
            abort('401', '401');
        }

        $this->validate($request, [
//            'code' => 'required|max:25|unique:products,code,' . $id,
            'name' => 'required|max:75|unique:products,name,' . $id,
//            'slug' => 'required|max:75|unique:products,slug,' . $id,
        ]);

        $product = $this->product_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['description'] = stripslashes($input['description']);
        $input['pricing_content'] = isset($input['pricing_content']) ? stripslashes($input['pricing_content']) : '';
        $input['acuity_scheduling_link'] = stripslashes($input['acuity_scheduling_link']);
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->product_repository->uploadFile($request->file('banner_image'), $product);
            $input['banner_image'] = $file_upload_path;
        }

        if ($request->hasFile('overview_image')) {
            $file_upload_path = $this->product_repository->uploadFile($request->file('overview_image'), $product);
            $input['overview_image'] = $file_upload_path;
        }

        $product->fill($input)->save();

        /* product_categories */
        if (isset($input['product_categories'])) {
            $product->product_categories()->sync($input['product_categories']);
        } else {
            $product->product_categories()->detach();
        }
        /* product_categories */

//        /* product_images */
//        $product->product_images()->forceDelete();
//
//        if (isset($input['product_images'])) {
//            foreach ($input['product_images'] as $product_image) {
//                $product_image = json_decode(urldecode($product_image));
//                $new_product_image_data = [
//                    'name' => $product_image->name,
//                    'size' => $product_image->size,
//                    'file' => $product_image->file,
//                    'type' => isset($product_image->type) ? 1 : 0,
//                    'is_default' => $product_image->is_default
//                ];
//                $new_product_image = $product->product_images()->create($new_product_image_data);
//            }
//        }
//        /* product_images */

        return redirect()->route('admin.products.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Product ' . $product->name . ' successfully updated.',
                'type' => 'success'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPermissionTo('Delete Product')) {
            abort('401', '401');
        }

        $product = $this->product_model->findOrFail($id);
        $product->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Product successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return json_encode($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        /*multiple*/
        $files = $request->file('file');

        $this->validate($request, [
            'file.*.file' => 'mimetypes:image/gif, image/jpg, image/jpeg, image/png',
        ]);

        $file_paths = [];
        foreach ($files as $file) {
            $file_paths[] = $this->product_repository->uploadFile($file);
        }

        return response()->json($file_paths, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $file = $request->get('orig-src');

        $product_images = $this->product_image_model->where('file', $file)->get();
        if (!empty($product_images)) {
            foreach ($product_images as $product_image) {
                $product_image->delete();
            }
        }

        if (File::exists($file)) {
            File::delete($file);
            $response['status'] = TRUE;
            $s3 = Storage::disk('s3');
            $s3->delete($file);
        }

        return response()->json($response, 200);
    }
}
