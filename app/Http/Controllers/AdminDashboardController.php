<?php

namespace App\Http\Controllers;
use AcuityScheduling;
use App\Models\Appointment;
use App\Models\Contact;
use App\Models\Subscription;

/**
 * Class AdminDashboardController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class AdminDashboardController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Admin Dashboard Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles admin dashboard.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param Appointment $appointment_model
     * @param Contact $contact_model
     * @param Subscription $subscription_model
     */
    public function __construct(Appointment $appointment_model,
                                Contact $contact_model,
                                Subscription $subscription_model
    )
    {
        /*
         * Model namespace
         * using $this->request_model can also access $this->request_model->where('id', 1)->get();
         * */
        $this->appointment_model = $appointment_model;
        $this->contact_model = $contact_model;
        $this->subscription_model = $subscription_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of requests with other data (related tables).
         * */

//        $this->middleware(['isAdmin']);

        /** Acuity API Connection **/
        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            return redirect()->to('admin/appointments');
            /* get photographer appointments only */
            $all_appointments_count = $this->appointment_model
                ->where('calendar_id', auth()->user()->photographer_calendar_id)
                ->count();
            $appointments = $this->appointment_model
                ->where('calendar_id', auth()->user()->photographer_calendar_id)
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();
        } else {
            $all_appointments_count = $this->appointment_model->count();
            $appointments = $this->appointment_model
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();
        }

        foreach ($appointments as $appointment) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $appointment['acuity_details'] = $acuity_details;
        }

        $all_contacts_count = $this->contact_model
            ->count();
        $contacts = $this->contact_model
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        $all_subscriptions_count = $this->subscription_model
            ->count();
        $subscriptions = $this->subscription_model
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        return view('admin.pages.dashboard.index', compact('appointments', 'all_appointments_count',
            'contacts', 'all_contacts_count',
            'subscriptions', 'all_subscriptions_count'
        ));
    }
}
