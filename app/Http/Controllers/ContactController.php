<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Auth;
use Illuminate\Http\Request;
use Session;

/**
 * Class ContactController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ContactController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | contact Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles contacts.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param contact $contact_model
     */
    public function __construct(Contact $contact_model)
    {
        /*
         * Model namespace
         * using $this->contact_model can also access $this->contact_model->where('id', 1)->get();
         * */
        $this->contact_model = $contact_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of contacts with other data (related tables).
         * */

//        $this->middleware(['isAdmin'], [
//            'except' => [
//                'saveContact',
//            ]
//        ]);
    }

    /**
     * Save the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function saveContact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
//            'company' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'subject' => 'required',
        ]);

        $input = $request->all();
        $contact = $this->contact_model->create($input);

        return redirect()->back()
            ->with('flash_message', [
                'title' => '',
                'message' => 'Thanks! We will get back to you soon.',
                'type' => 'success'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read Contact')) {
            abort('401', '401');
        }

        $contacts = $this->contact_model->get();

        return view('admin.pages.contact.index', compact('contacts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('Read Contact')) {
            abort('401', '401');
        }

        $contact = $this->contact_model->findOrFail($id);

        return view('admin.pages.contact.show', compact('contact'));
    }
}
