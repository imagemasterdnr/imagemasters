<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\SystemSettingTrait;
use App\Repositories\PageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, SystemSettingTrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/customer/dashboard';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct(PageRepository $page_repository,
                                UserRepository $user_repository,
                                ProjectRepository $project_repository,
                                ProductCategoryRepository $product_category_repository,
                                ProductRepository $product_repository
    )
    {
        $this->page_repository = $page_repository;
        $this->user_repository = $user_repository;
        $this->project_repository = $project_repository;
        $this->product_category_repository = $product_category_repository;
        $this->product_repository = $product_repository;

        $this->middleware('isFront.guest', ['except' => 'logout']);
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $page = $this->page_repository->getActivePageBySlug('customer/login');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_product_categories = $this->product_category_repository->getAllActive();
        $header_product_categories->map(function ($item) {
            $item['item_type'] = 'category';
            return $item;
        });
        $header_products = $this->product_repository->getAllActiveNoCategory();
        $header_products->map(function ($item) {
            $item['item_type'] = 'product';
            return $item;
        });

        $header_services = collect([]);
        $header_services = $header_services->merge($header_product_categories)->merge($header_products);
        return view('front.auth.login', compact('page', 'projects', 'header_services'));
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();

            /* only defined roles can login */
            if ($user->hasAnyRole(['Customer']) && $user->type == 1 && $user->is_active && $this->attemptLogin($request)) {
                $user->last_login = date('Y-m-d H:i:s');
                $user->save();
                return $this->sendLoginResponse($request);
            } else {
                $this->incrementLoginAttempts($request);
                return $this->sendFailedLoginResponse($request, 'auth.failed_status');
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/customer');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function credentials(Request $request)
    {
//        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
//            ? $this->username()
//            : 'user_name';

        return [
//            $field => $request->get($this->username()),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'password' => $request->password,
        ];
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $default = 'customer/dashboard';
        session()->put('logged_in_from', 'customer');

        $target_url = redirect()->intended($request->session()->get('url.intended', '/customer/dashboard'))->getTargetUrl();

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if (strpos($target_url, $default) != -1) {
            if (!strpos($target_url, 'admin') != -1) {
                return redirect($default);
            } else {
                return redirect()->intended($target_url);
            }
        } else {
            return redirect($default);
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
//            $this->username() => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'first_name';
    }
}
