<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\SystemSettingTrait;
use App\Models\User;
use App\Repositories\PageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, SystemSettingTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/customer/dashboard';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct(User $user_model,
                                Role $role_model,
                                PageRepository $page_repository,
                                UserRepository $user_repository,
                                ProjectRepository $project_repository,
                                ProductCategoryRepository $product_category_repository,
                                ProductRepository $product_repository
    )
    {
        /*
        * Model namespace
        * using $this->user_model can also access $this->user_model->where('id', 1)->get();
        * */
        $this->user_model = $user_model;
        $this->role_model = $role_model;
        $this->page_repository = $page_repository;
        $this->user_repository = $user_repository;
        $this->project_repository = $project_repository;
        $this->product_category_repository = $product_category_repository;
        $this->product_repository = $product_repository;

        $this->middleware('isFront.guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
//        $sections = $this->getSections();
        $page = $this->page_repository->getActivePageBySlug('customer/register');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_product_categories = $this->product_category_repository->getAllActive();
        $header_product_categories->map(function ($item) {
            $item['item_type'] = 'category';
            return $item;
        });
        $header_products = $this->product_repository->getAllActiveNoCategory();
        $header_products->map(function ($item) {
            $item['item_type'] = 'product';
            return $item;
        });

        $header_services = collect([]);
        $header_services = $header_services->merge($header_product_categories)->merge($header_products);
        return view('front.auth.register', compact('page', 'projects', 'header_services'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:25|unique_name:' . $data['last_name'],
            'last_name' => 'required|max:25',
//            'user_name' => 'required|max:45|unique:users',
            'email' => 'required|email|max:45',
            'phone' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        $user = $this->user_model->create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
//            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'phone' => isset($data['phone']) ? $data['phone'] : '',
            'password' => $data['password'],
            'type' => 1,
        ]);

        /* customer role */
        $roles = [4];
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = $this->role_model->where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r);
            }
        }

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $params = [
            'view' => 'email.registered',
            'user' => [
                'name' => $user->first_name . ' ' . $user->last_name,
                'email' => $user->email
            ],
            'subject' => 'Customer Registration',
            'attachments' => []
        ];
        $mail = $this->user_repository->emailRegistered($params);

        $params['view'] = 'email.registered_admin';
        $mail = $this->user_repository->emailRegisteredAdmin($params);

    }
}
