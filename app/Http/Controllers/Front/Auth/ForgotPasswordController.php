<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\SystemSettingTrait;
use App\Repositories\PageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProjectRepository;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/**
 * Class ForgotPasswordController
 * @package App\Http\Controllers\Auth
 */
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails, SystemSettingTrait;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct(PageRepository $page_repository,
                                ProjectRepository $project_repository,
                                ProductCategoryRepository $product_category_repository,
                                ProductRepository $product_repository
    )
    {
        $this->page_repository = $page_repository;
        $this->project_repository = $project_repository;
        $this->product_category_repository = $product_category_repository;
        $this->product_repository = $product_repository;

        $this->middleware('isFront.guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
//        $sections = $this->getSections();
        $page = $this->page_repository->getActivePageBySlug('customer/password/email');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_product_categories = $this->product_category_repository->getAllActive();
        $header_product_categories->map(function ($item) {
            $item['item_type'] = 'category';
            return $item;
        });
        $header_products = $this->product_repository->getAllActiveNoCategory();
        $header_products->map(function ($item) {
            $item['item_type'] = 'product';
            return $item;
        });

        $header_services = collect([]);
        $header_services = $header_services->merge($header_product_categories)->merge($header_products);
        return view('front.auth.passwords.email', compact('page', 'projects', 'header_services'));
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'first_name' => 'required', 'last_name' => 'required']);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email', 'first_name', 'last_name')
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }
}
