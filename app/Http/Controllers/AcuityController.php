<?php

namespace App\Http\Controllers;

use AcuityScheduling;
use App\Models\Appointment;
use App\Models\AppointmentAttachment;
use App\Models\Product;
use App\Models\User;
use App\Models\WebhookLog;
use App\Repositories\AppointmentRepository;
use App\Repositories\UserRepository;
use Auth;
use Chumper\Zipper\Zipper;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Session;
use Spatie\Permission\Models\Role;
use Storage;

/**
 * Class AcuityController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class AcuityController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Acuity Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles acuity API.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param WebhookLog $webhook_log_model
     * @param User $user_model
     * @param Appointment $appointment_model
     * @param AppointmentAttachment $appointment_attachment_model
     * @param Product $product_model
     * @param Role $role_model
     * @param UserRepository $user_repository
     * @param AppointmentRepository $appointment_repository
     */
    public function __construct(WebhookLog $webhook_log_model,
                                User $user_model,
                                Appointment $appointment_model,
                                AppointmentAttachment $appointment_attachment_model,
                                Product $product_model,
                                Role $role_model,
                                UserRepository $user_repository,
                                AppointmentRepository $appointment_repository)
    {
        /*
         * Model namespace
         * using $this->acuity_model can also access $this->acuity_model->where('id', 1)->get();
         * */
        $this->webhook_log_model = $webhook_log_model;
        $this->user_model = $user_model;
        $this->appointment_model = $appointment_model;
        $this->appointment_attachment_model = $appointment_attachment_model;
        $this->product_model = $product_model;
        $this->role_model = $role_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of acuity with other data (related tables).
         * */
        $this->user_repository = $user_repository;
        $this->appointment_repository = $appointment_repository;

        /** Acuity API Connection **/
        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));
    }

    public function getAppointments()
    {
//        $appointments = $this->acuity->request('/appointments');
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            /* get photographer appointments only */
            $appointments = $this->appointment_repository->getAllPhotographer();
        } else {
            $appointments = $this->appointment_repository->getAll();
        }

        foreach ($appointments as $appointment) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $appointment['acuity_details'] = $acuity_details;
        }

        return view('admin.pages.appointment.index', compact('appointments'));
    }

    public function getAppointmentById($id)
    {
        $appointment = $this->appointment_repository->getByAppointmentId($id);
        /* if you are a photographer */
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            /* if you are not the photographer of the appointment */
            if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
                abort('401', '401');
            }
        }

        $acuity_details = $this->acuity->request('/appointments/' . $id);

        if (isset($acuity_details['status_code'])) {
            abort('404', '404');
        }
        $appointment['acuity_details'] = $acuity_details;

        return view('admin.pages.appointment.show', compact('appointment', 'acuity_details'));
    }

    public function getAddAppointmentAttachments($id)
    {
        $appointment = $this->appointment_repository->getByAppointmentId($id);

        /* if you are a photographer */
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            /* if you are not the photographer of the appointment */
            if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
                abort('401', '401');
            } else if ($appointment->status == 2) {
                abort('401', '401');
            }
        } else {
            abort('401', '401');
        }

        $acuity_details = $this->acuity->request('/appointments/' . $id);

        if (isset($acuity_details['status_code'])) {
            abort('404', '404');
        }
        $appointment['acuity_details'] = $acuity_details;

        if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
            abort('401', '401');
        }

        return view('admin.pages.appointment.add_attachment', compact('appointment', 'acuity_details'));
    }

    public function postAddAppointmentAttachments(Request $request, $id)
    {
        $appointment = $this->appointment_model->findOrFail($id);
        /* if you are a photographer */
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            /* if you are not the photographer of the appointment */
            if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
                abort('401', '401');
            } else if ($appointment->status == 2) {
                abort('401', '401');
            }
        } else {
            abort('401', '401');
        }

        $input = $request->all();

        /* attachments */
        $appointment->attachments()->forceDelete();

        if (isset($input['appointment_images'])) {
            foreach ($input['appointment_images'] as $appointment_image) {
                $appointment_image = json_decode(urldecode($appointment_image));
                $new_appointment_image_data = [
                    'name' => $appointment_image->name,
                    'size' => $appointment_image->size,
                    'file' => $appointment_image->file,
                    'type' => isset($appointment_image->type) ? 1 : 0,
                    'is_default' => $appointment_image->is_default,
                    'extension' => $appointment_image->extension,
                    'thumbnail' => $appointment_image->thumbnail
                ];
                $new_appointment_image = $appointment->attachments()->create($new_appointment_image_data);
            }
        }
        /* attachments */

        $params = [
            'view' => 'email.attachment_added',
            'user' => [
                'name' => $appointment->user->first_name . ' ' . $appointment->user->last_name,
                'email' => $appointment->user->email
            ],
            'subject' => 'Attachments Added!',
            'attachments' => []
        ];
        $mail = $this->user_repository->emailAttachmentsAdded($params);

        return redirect()->route('admin.appointments.show', $appointment->appointment_id)
            ->with('flash_message', [
                'title' => '',
                'message' => 'Added attachment to ' . $appointment->appointment_id . ' successfully.',
                'type' => 'success'
            ]);
    }

    public function cancelAppointment(Request $request, $id)
    {
        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $appointment = $this->appointment_model->where('appointment_id', $id)->first();

        if (auth()->user()->hasAnyRole(['Photographer'])) {
            abort('401', '401');
        } else if (auth()->user()->hasAnyRole(['Customer'])) {
            if (auth()->user()->id != $appointment->user_id) {
                abort('401', '401');
            }
        }

        $acuity_appointment = $this->acuity->request('/appointments/' . $id . '/cancel', array(
            'method' => 'PUT',
            'query' => array(
                'admin' => auth()->user()->hasAnyRole(['Admin', 'Super Admin'])
            ),
            'data' => array(
                'cancelNote' => $request->get('note'),
            )
        ));

        if (isset($acuity_appointment['status_code']) && $acuity_appointment['status_code'] == 400) {
            $response['message'][] = $acuity_appointment['message'];
        } else {
            $appointment->status = 2;
            $appointment->save();

            $response['message'][] = 'Appointment ' . $appointment->appointment_id . ' canceled successfully.';
            $response['data']['id'] = $id;
            $response['status'] = TRUE;
        }
        return json_encode($response);
    }

    public function putClient(Request $request, $id)
    {
        if (!Auth::user()->hasRole('Customer')) {
            abort('401', '401');
        }

        if (auth()->user()->id != $id) {
            abort('401', '401');
        }

        $user = $this->user_model->findOrFail($id);
        $this->validate($request, [
            'email' => 'required|email|max:45',
            'phone' => 'required',
            'profile_image' => 'mimes:jpg,jpeg,png',
            'company_logo' => 'mimes:jpg,jpeg,png',
        ]);

        $input = $request->only(['email', 'phone', 'position', 'address', 'license_no', 'website']);
        $user->fill($input)->save();

        if ($request->hasFile('profile_image')) {
            $file_upload_path = $this->user_repository->uploadFile($request->file('profile_image'), /*'profile_image'*/null, 'user_profile_images');
            $user->fill(['profile_image' => $file_upload_path])->save();
        }
        if ($request->hasFile('company_logo')) {
            $file_upload_path = $this->user_repository->uploadFile($request->file('company_logo'), /*'company_logo'*/null, 'user_company_logos');
            $user->fill(['company_logo' => $file_upload_path])->save();
        }

        $acuity_user = $this->acuity->request('/clients', array(
            'method' => 'PUT',
            'query' => array(
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
            ),
            'data' => array(
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
            )
        ));

        return redirect()->back()
            ->with('flash_message', [
                'title' => '',
                'message' => 'Profile successfully updated.',
                'type' => 'success'
            ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        /*multiple*/
        $files = $request->file('file');

        $this->validate($request, [
            'file.*.file' => 'mimetypes:image/gif, image/jpg, image/jpeg, image/png',
        ]);

        $file_paths = [];
        foreach ($files as $file) {
            $file_paths[] = $this->appointment_repository->uploadFile($file);
        }

        return response()->json($file_paths, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $file = $request->get('orig-src');

        $appointment_images = $this->appointment_attachment_model->where('file', $file)->get();
        if (!empty($appointment_images)) {
            foreach ($appointment_images as $appointment_image) {
                $appointment_image->delete();
            }
        }

        if (File::exists($file)) {
            File::delete($file);
            $response['status'] = TRUE;
            $s3 = Storage::disk('s3');
            $s3->delete($file);
        }

        return response()->json($response, 200);
    }

    public function downloadImages($id)
    {
        $appointment = $this->appointment_model->where('appointment_id', $id)->first();
        $attachments = [];
//        /* download from local */
//        if (!empty($appointment)) {
//            $appointment_attachments = $appointment->attachments()->pluck('file')->toArray();
//            foreach ($appointment_attachments as $appointment_attachment) {
//                if (File::exists($appointment_attachment)) {
//                    $attachments[] = $appointment_attachment;
//                }
//            }
//        }
//
//        if (!empty($attachments) && count($attachments)) {
//            $zipper = new Zipper();
//            $filepath = 'public/uploads/appointment_zips/' . $id . ' - ' . time() . '.zip';
//            $zipper->make($filepath)->add($attachments)->close();
//            return response()->download($filepath)->deleteFileAfterSend(true);
//        }


        /* download from s3 */
        if (!empty($appointment)) {
            $appointment_attachments = $appointment->attachments()->get();
            foreach ($appointment_attachments as $appointment_attachment) {
                if (Storage::disk('s3')->exists($appointment_attachment->file)) {
                    $attachments[] = $appointment_attachment;
                }
            }
        }

        if (!empty($attachments) && count($attachments)) {
            $zipper = new Zipper();
            $filepath = 'public/uploads/appointment_zips/' . $id . ' - ' . time() . '.zip';
            $zipper->make($filepath);
            foreach($attachments as $attachment) {
                $zipper->addString($attachment->name, file_get_contents(s3_url($attachment->file)));
            }
            $zipper->close();
            return response()->download($filepath)->deleteFileAfterSend(true);
        }
        return redirect()->back()->with('flash_message', [
            'title' => '',
            'message' => 'Images not found.',
            'type' => 'error'
        ]);
    }

    public function downloadImageFromS3($id)
    {
        $attachment = $this->appointment_attachment_model->find($id);
        $file_path = Storage::disk('s3')->url($attachment->file);
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . basename($file_path));
        return readfile($file_path);
    }

    public function getAppointmentAddons()
    {
        $appointments = $this->acuity->request('/appointment-addons');
        dd($appointments);
    }

    public function getAppointmentTypes()
    {
        $appointments = $this->acuity->request('/appointment-types');
        dd($appointments);
    }

    public function getCalendars()
    {
        $appointments = $this->acuity->request('/calendars');
        dd($appointments);
    }

    public function getClients()
    {
        $clients = $this->acuity->request('/clients');
        dd($clients);
    }

    public function getForms()
    {
        $clients = $this->acuity->request('/forms');
        dd($clients);
    }

    public function webhooks(Request $request)
    {
        $this->acuity->verifyMessageSignature(env('ACUITY_API_KEY'));
        Log::debug('Webhooks:' . json_encode($request->all()));

        /*
         * save to webhook logs
         *
         *
         * get_appointments/:id  -> acuity api
         *
         * check first and last name if existing case insensitive regardless of logged in user because this is a background task
         *
         *
         * if exists
         * save appointment to existing user
         * else
         * create new user send random password to the email (8 chars)
         * save appointment to new user
         *
         *
         * appointments table
         * user_id
         * appt id
         * appt type id
         * calendar id
         * product id to be checked by category from api
         *
         *
         * appointment attachments
         * appt id
         * file
         * etc
         *
         *
         * else ???
         * */
        Log::debug('Webhook validation:' . json_encode($request->has('action') && $request->has('id') && $request->has('calendarID') && $request->has('appointmentTypeID')));
        if ($request->has('action') && $request->has('id') && $request->has('calendarID') && $request->has('appointmentTypeID')) {
            if ($request->get('action') == 'scheduled') {
                $webhook_data = [
                    'action' => $request->get('action'),
                    'appointment_id' => $request->get('id'),
                    'calendar_id' => $request->get('calendarID'),
                    'appointment_type_id' => $request->get('appointmentTypeID'),

                ];
                $webhook = $this->webhook_log_model->create($webhook_data);
                Log::debug('Webhook log:' . json_encode($webhook));

                $appointment_api = $this->acuity->request('/appointments/' . $request->get('id'));
                Log::debug('Webhook Appointment API:' . json_encode($appointment_api));
                if (!isset($appointment_api['status_code'])) {
                    $appointment_data = [
                        'appointment_id' => $appointment_api['id'],
                        'calendar_id' => $appointment_api['calendarID'],
                        'appointment_type_id' => $appointment_api['appointmentTypeID'],
                        'user_id' => 0,
                        'product_id' => 0,
                    ];

                    $user = $this->user_model
                        ->where('first_name', $appointment_api['firstName'])
                        ->where('last_name', $appointment_api['lastName'])
                        ->first();
                    Log::debug('Webhook User:' . json_encode($user));
                    if (!empty($user)) {
                        $appointment_data['user_id'] = $user->id;
                    } else {
                        $random_password = $this->user_repository->generateRandomPassword(8);
                        $user = $this->user_model
                            ->create([
                                'first_name' => $appointment_api['firstName'],
                                'last_name' => $appointment_api['lastName'],
                                'type' => 1,
                                'email' => $appointment_api['email'],
                                'password' => $random_password,
                            ]);
                        $roles = [4];
                        if (isset($roles)) {
                            foreach ($roles as $role) {
                                $role_r = $this->role_model->where('id', '=', $role)->firstOrFail();
                                $user->assignRole($role_r);
                            }
                        }
                        $appointment_data['user_id'] = $user->id;

                        $params = [
                            'view' => 'email.login_credentials',
                            'user' => [
                                'name' => $appointment_api['firstName'] . ' ' . $appointment_api['lastName'],
                                'email' => $appointment_api['email']
                            ],
                            'password_data' => [
                                'first_name' => $appointment_api['firstName'],
                                'last_name' => $appointment_api['lastName'],
                                'password' => $random_password
                            ],
                            'subject' => 'Login credentials',
                            'attachments' => []
                        ];
                        $mail = $this->user_repository->emailWebhook($params);
                        Log::debug('Webhook Email:' . json_encode($random_password));

                        $params = [
                            'view' => 'email.registered_admin',
                            'user' => [
                                'name' => $appointment_api['firstName'] . ' ' . $appointment_api['lastName'],
                                'email' => $appointment_api['email']
                            ],
                            'subject' => 'Customer Registration',
                            'attachments' => []
                        ];
                        $params['view'] = 'email.registered_admin';
                        $mail = $this->user_repository->emailRegisteredAdmin($params);
                    }

                    $product = $this->product_model
                        ->where('name', $appointment_api['category'])
                        ->first();
                    if (!empty($product)) {
                        $appointment_data['product_id'] = $product->id;
                    }

                    $appointment = $this->appointment_model->create($appointment_data);
                    Log::debug('Webhook Appointment:' . json_encode($appointment));
                }
            } else if ($request->get('action') == 'canceled') {
                $appointment = $this->appointment_model->where('appointment_id', $request->get('id'))->first();
                if (!empty($appointment)) {
                    $appointment->status = 2;
                    $appointment->save();
                }
            }
        }
    }

    public function editPropertyDetails($id)
    {
        $appointment = $this->appointment_repository->getByAppointmentId($id);

        /* if you are a photographer */
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            /* if you are not the photographer of the appointment */
            if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
                abort('401', '401');
            } else if ($appointment->status == 2) {
                abort('401', '401');
            }
        } else {
            abort('401', '401');
        }

        $acuity_details = $this->acuity->request('/appointments/' . $id);

        if (isset($acuity_details['status_code'])) {
            abort('404', '404');
        }
        $appointment['acuity_details'] = $acuity_details;

        if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
            abort('401', '401');
        }

        return view('admin.pages.appointment.edit_property_detail', compact('appointment', 'acuity_details'));
    }

    public function updatePropertyDetails(Request $request, $id)
    {
        $appointment = $this->appointment_model->findOrFail($id);
        /* if you are a photographer */
        if (auth()->user()->hasAnyRole(['Photographer'])) {
            /* if you are not the photographer of the appointment */
            if (auth()->user()->photographer_calendar_id != $appointment->calendar_id) {
                abort('401', '401');
            } else if ($appointment->status == 2) {
                abort('401', '401');
            }
        } else {
            abort('401', '401');
        }

        $input = $request->all();
        $input['is_branded'] = isset($input['is_branded']) ? 1 : 0;
        $appointment->fill($input)->save();

        return redirect()->route('admin.appointments.show', $appointment->appointment_id)
            ->with('flash_message', [
                'title' => '',
                'message' => 'Updated property details ' . $appointment->appointment_id . ' successfully.',
                'type' => 'success'
            ]);
    }
}
