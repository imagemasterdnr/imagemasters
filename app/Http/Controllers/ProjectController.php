<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectImage;
use App\Models\ProjectVideo;
use App\Repositories\ProjectRepository;
use App\Repositories\SeoMetaRepository;
use Auth;
use File;
use Illuminate\Http\Request;
use Session;

/**
 * Class ProjectController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ProjectController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Project Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles projects.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param Project $project_model
     * @param ProjectRepository $project_repository
     * @param SeoMetaRepository $seo_meta_repository
     * @param ProjectImage $project_image_model
     * @param ProjectVideo $project_video_model
     */
    public function __construct(Project $project_model,
                                ProjectRepository $project_repository,
                                SeoMetaRepository $seo_meta_repository,
                                ProjectImage $project_image_model,
                                ProjectVideo $project_video_model
    )
    {
        /*
         * Model namespace
         * using $this->project_model can also access $this->project_model->where('id', 1)->get();
         * */
        $this->project_model = $project_model;
        $this->project_image_model = $project_image_model;
        $this->project_video_model = $project_video_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of projects with other data (related tables).
         * */
        $this->project_repository = $project_repository;
        $this->seo_meta_repository = $seo_meta_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Read Project')) {
            abort('401', '401');
        }

        $projects = $this->project_repository->getAll();

        return view('admin.pages.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPermissionTo('Create Project')) {
            abort('401', '401');
        }

        return view('admin.pages.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('Create Project')) {
            abort('401', '401');
        }

        $this->validate($request, [
//            'code' => 'required|max:25|unique:projects',
            'name' => 'required|max:75|unique:projects',
//            'slug' => 'required|max:75|unique:projects',
            'file' => 'required|mimes:gif,jpg,jpeg,png',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['is_featured'] = isset($input['is_featured']) ? 1 : 0;
        $input['vimeo_link'] = stripslashes($input['vimeo_link']);
        $input['description'] = stripslashes($input['description']);
        $input['slug'] = str_slug($input['name']);

//        /* seo meta */
//        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
//        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
//        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
//        $input['seo_meta_id'] = $seo_meta->id;
//        /* seo meta */

        $project = $this->project_model->create($input);

        $file_upload_path = $this->project_repository->uploadFile($request->file('file'), $project);
        $project->fill(['image' => $file_upload_path['data']['file_upload_path']])->save();

//        /* project_images */
//        $project->project_images()->forceDelete();
//
//        if (isset($input['project_images'])) {
//            foreach ($input['project_images'] as $project_image) {
//                $project_image = json_decode(urldecode($project_image));
//                $new_project_image_data = [
//                    'name' => $project_image->name,
//                    'size' => $project_image->size,
//                    'file' => $project_image->file,
//                    'type' => $project_image->type,
//                    'is_default' => $project_image->is_default
//                ];
//                $new_project_image = $project->project_images()->create($new_project_image_data);
//            }
//        }
//        /* project_images */
//
//        /* project_videos */
//        $project->project_videos()->forceDelete();
//
//        if (isset($input['project_videos'])) {
//            foreach ($input['project_videos'] as $project_video) {
//                $project_video = json_decode(urldecode($project_video));
//                $new_project_video_data = [
//                    'name' => $project_video->name,
//                    'size' => $project_video->size,
//                    'file' => $project_video->file,
//                    'type' => $project_video->type,
//                    'is_default' => $project_video->is_default
//                ];
//                $new_project_video = $project->project_videos()->create($new_project_video_data);
//            }
//        }
//        /* project_videos */

        return redirect()->route('admin.projects.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Project ' . $project->name . ' successfully added.',
                'type' => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin/projects');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPermissionTo('Update Project')) {
            abort('401', '401');
        }

        $project = $this->project_model->findOrFail($id);
        $seo_meta_fields = $project->seo_meta;

        return view('admin.pages.project.edit', compact('project', 'seo_meta_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('Update Project')) {
            abort('401', '401');
        }

        $this->validate($request, [
//            'code' => 'required|max:25|unique:projects,code,' . $id,
            'name' => 'required|max:75|unique:projects,name,' . $id,
//            'slug' => 'required|max:75|unique:projects,slug,' . $id,
            'file' => 'mimes:gif,jpg,jpeg,png',
        ]);

        $project = $this->project_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['is_featured'] = isset($input['is_featured']) ? 1 : 0;
        $input['description'] = stripslashes($input['description']);
        $input['vimeo_link'] = stripslashes($input['vimeo_link']);
        $input['slug'] = str_slug($input['name']);

//        /* seo meta */
//        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
//        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
//        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
//        $input['seo_meta_id'] = $seo_meta->id;
//        /* seo meta */

        if ($request->hasFile('file')) {
            $file_upload_path = $this->project_repository->uploadFile($request->file('file'), $project);
            $input['image'] = $file_upload_path['data']['file_upload_path'];
        }

        $project->fill($input)->save();

//        /* project_images */
//        $project->project_images()->forceDelete();
//
//        if (isset($input['project_images'])) {
//            foreach ($input['project_images'] as $project_image) {
//                $project_image = json_decode(urldecode($project_image));
//                $new_project_image_data = [
//                    'name' => $project_image->name,
//                    'size' => $project_image->size,
//                    'file' => $project_image->file,
//                    'type' => $project_image->type,
//                    'is_default' => $project_image->is_default
//                ];
//                $new_project_image = $project->project_images()->create($new_project_image_data);
//            }
//        }
//        /* project_images */
//
//        /* project_videos */
//        $project->project_videos()->forceDelete();
//
//        if (isset($input['project_videos'])) {
//            foreach ($input['project_videos'] as $project_video) {
//                $project_video = json_decode(urldecode($project_video));
//                $new_project_video_data = [
//                    'name' => $project_video->name,
//                    'size' => $project_video->size,
//                    'file' => $project_video->file,
//                    'type' => $project_video->type,
//                    'is_default' => $project_video->is_default
//                ];
//                $new_project_video = $project->project_videos()->create($new_project_video_data);
//            }
//        }
//        /* project_videos */

        return redirect()->route('admin.projects.index')
            ->with('flash_message', [
                'title' => '',
                'message' => 'Project ' . $project->name . ' successfully updated.',
                'type' => 'success'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPermissionTo('Delete Project')) {
            abort('401', '401');
        }

        $project = $this->project_model->findOrFail($id);
        $project->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Project successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return json_encode($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        /*multiple*/
        $files = $request->file('file');

        $this->validate($request, [
            'file.*.file' => 'mimetypes:image/gif, image/jpg, image/jpeg, image/png',
        ]);

        $file_paths = [];
        foreach ($files as $file) {
            $file_paths[] = $this->project_repository->uploadFile($file);
        }

        return response()->json($file_paths, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $file = $request->get('orig-src');

        $project_images = $this->project_image_model->where('file', $file)->get();
        if (!empty($project_images)) {
            foreach ($project_images as $project_image) {
                $project_image->delete();
            }
        }

        if (File::exists($file)) {
            File::delete($file);
            $response['status'] = TRUE;
        }

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadVideo(Request $request)
    {
        /*multiple*/
        $files = $request->file('file');

        $this->validate($request, [
            'file.*.file' => 'mimetypes:image/gif, image/jpg, image/jpeg, image/png',
        ]);

        $file_paths = [];
        foreach ($files as $file) {
            $file_paths[] = $this->project_repository->uploadVideo($file);
        }

        return response()->json($file_paths, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteVideo(Request $request)
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $file = $request->get('orig-src');

        $project_videos = $this->project_video_model->where('file', $file)->get();
        if (!empty($project_videos)) {
            foreach ($project_videos as $project_video) {
                $project_video->delete();
            }
        }

        if (File::exists($file)) {
            File::delete($file);
            $response['status'] = TRUE;
        }

        return response()->json($response, 200);
    }
}
