<?php

namespace App\Http\Controllers;

use AcuityScheduling;
use App\Http\Traits\SystemSettingTrait;
use App\Repositories\AppointmentRepository;
use App\Repositories\PageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class FrontDashboardController extends Controller
{
    use SystemSettingTrait;

    /**
     * @param PageRepository $page_repository
     * @param AppointmentRepository $appointment_repository
     * @param ProjectRepository $project_repository
     * @param ProductCategoryRepository $product_category_repository
     * @param ProductRepository $product_repository
     * @param UserRepository $user_repository
     */
    public function __construct(PageRepository $page_repository,
                                AppointmentRepository $appointment_repository,
                                ProjectRepository $project_repository,
                                ProductCategoryRepository $product_category_repository,
                                ProductRepository $product_repository,
                                UserRepository $user_repository
    )
    {
        $this->page_repository = $page_repository;
        $this->appointment_repository = $appointment_repository;
        $this->project_repository = $project_repository;
        $this->product_category_repository = $product_category_repository;
        $this->product_repository = $product_repository;
        $this->user_repository = $user_repository;
    }

    public function account()
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();
        $appointments = auth()->user()->my_appointments()->get();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        foreach ($appointments as $appointment) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $appointment['acuity_details'] = $acuity_details;
            $appointment->product = $this->product_repository->getDataStatic($appointment->product);
        }

        $cancelled_appointments = $appointments->sortBy(function ($temp, $key) {
            return Carbon::parse($temp['acuity_details']['datetime'])->getTimestamp();
        })->where('status', '=', 2);

        $appointments = $appointments->sortBy(function ($temp, $key) {
            return Carbon::parse($temp['acuity_details']['datetime'])->getTimestamp();
        })->where('status', '!=', 2);

        $products = $this->product_repository->getAll();

        return view('front.pages.dashboard.account', compact('page', 'projects', 'appointments', 'products', 'cancelled_appointments'));
    }

    public function products()
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();

        $appointments = auth()->user()->my_appointments()->get();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        foreach ($appointments as $appointment) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $appointment['acuity_details'] = $acuity_details;
            $appointment->product = $this->product_repository->getDataStatic($appointment->product);
        }

        $cancelled_appointments = $appointments->sortBy(function ($temp, $key) {
            return Carbon::parse($temp['acuity_details']['datetime'])->getTimestamp();
        })->where('status', '=', 2);

        $appointments = $appointments->sortBy(function ($temp, $key) {
            return Carbon::parse($temp['acuity_details']['datetime'])->getTimestamp();
        })->where('status', '!=', 2);

        return view('front.pages.dashboard.products', compact('page', 'projects', 'appointments', 'cancelled_appointments'));
    }

    public function productViewAlbum($id)
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();

        $appointment = auth()->user()->my_appointments()->where('appointment_id', $id)->first();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
        $appointment['acuity_details'] = $acuity_details;

        return view('front.pages.dashboard.view_album', compact('page', 'projects', 'appointment'));
    }

    public function productOrderDetail($id)
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();

        $appointment = auth()->user()->my_appointments()->where('appointment_id', $id)->first();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        if (!empty($appointment)) {
            $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
            $totals = 0;
            if (!isset($acuity_details['status_code'])) {
                $acuity_add_ons = $this->acuity->request('/appointment-addons');
                $add_ons = [];
                foreach ($acuity_details['addonIDs'] as $addon_id) {
                    $matching_add_on_key = array_search($addon_id, array_column($acuity_add_ons, 'id'));
                    if ($matching_add_on_key >= 0) {
                        $totals += $acuity_add_ons[$matching_add_on_key]['price'];
                        $add_ons[] = $acuity_add_ons[$matching_add_on_key];
                    }
                }
                $acuity_details['add_ons'] = $add_ons;

                $acuity_appointment_types = $this->acuity->request('/appointment-types');
                $matching_appointment_type_key = array_search($acuity_details['appointmentTypeID'], array_column($acuity_appointment_types, 'id'));

                $appointment_type = $acuity_appointment_types[$matching_appointment_type_key];
                $acuity_details['appointment_type'] = $appointment_type;
                $totals += $appointment_type['price'];
                $acuity_details['totals'] = $totals;
            }
            $appointment['acuity_details'] = $acuity_details;
//            dd($acuity_details);
        } else {
            abort('404', '404');
        }

        return view('front.pages.dashboard.order_detail', compact('page', 'projects', 'appointment'));
    }

    public function profile()
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();
        return view('front.pages.dashboard.profile', compact('page', 'projects'));
    }

    public function support()
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();
        return view('front.pages.dashboard.support', compact('page', 'projects'));
    }

    public function getHeaderServices()
    {
        $header_product_categories = $this->product_category_repository->getAllActive();
        $header_product_categories->map(function ($item) {
            $item['item_type'] = 'category';
            return $item;
        });
        $header_products = $this->product_repository->getAllActiveNoCategory();
        $header_products->map(function ($item) {
            $item['item_type'] = 'product';
            return $item;
        });

        $header_services = collect([]);
        $header_services = $header_services->merge($header_product_categories)->merge($header_products);
        view()->share(['header_services' => $header_services]);
        return $header_services;
    }

    public function tourPreview($id)
    {
        $page = $this->page_repository->getActivePageBySlug('tour_preview');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();

        $appointment = $this->appointment_repository->getByAppointmentId($id);

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
        $appointment['acuity_details'] = $acuity_details;

        return view('front.pages.custom-page.tour-preview', compact('page', 'projects', 'appointment'));
    }

    public function editPropertyDetails($id)
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }
        $projects = $this->project_repository->getAllActive();
        $header_services = $this->getHeaderServices();

        $appointment = auth()->user()->my_appointments()->where('appointment_id', $id)->first();

        $this->acuity = new AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID'),
            'apiKey' => env('ACUITY_API_KEY')
        ));

        $acuity_details = $this->acuity->request('/appointments/' . $appointment->appointment_id);
        $appointment['acuity_details'] = $acuity_details;

        return view('front.pages.dashboard.edit_property_details', compact('page', 'projects', 'appointment'));
    }

    public function updatePropertyDetails(Request $request, $id)
    {
        $appointment = auth()->user()->my_appointments()->where('appointment_id', $id)->first();

        $input = $request->all();
        $input['is_branded'] = isset($input['is_branded']) ? 1 : 0;
        $appointment->fill($input)->save();

        return redirect()->back()
            ->with('flash_message', [
                'title' => '',
                'message' => 'Property Details successfully updated.',
                'type' => 'success'
            ]);
    }

    public function sendTourPreview(Request $request, $id)
    {
        $params = [
            'view' => 'email.tour_preview',
            'from' => [
                'name' => $request->get('from_name'),
                'email' => $request->get('from_email')
            ],
            'to' => [
                'name' => $request->get('to_name'),
                'email' => $request->get('to_email')
            ],
            'appointment_data' => [
                'appointment_id' => $request->get('appointment_id'),
                'address' => $request->get('address'),
                'link' => $request->get('link'),
            ],
            'message' => $request->get('message'),
            'subject' => $request->get('subject'),
            'attachments' => []
        ];
        $mail = $this->user_repository->emailTourPreview($params);

        return redirect()->back()
            ->with('flash_message', [
                'title' => '',
                'message' => 'Email successfully sent.',
                'type' => 'success'
            ]);
    }
}
