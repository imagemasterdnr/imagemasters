<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class FrontRedirectIfAuthenticated
 * @package App\Http\Middleware
 * @author Randall Anthony Bondoc
 */
class FrontRedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Session::get('logged_in_from') == 'admin') {
                return redirect('/admin/dashboard');
            } elseif (Session::get('logged_in_from') == 'customer') {
                return redirect('/customer/dashboard');
            }
        }

        return $next($request);
    }
}
