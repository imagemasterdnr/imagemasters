<?php

namespace App\Repositories;

use App\Models\Product;
use File;
use Storage;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class ProductRepository
{
    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/Product;
     */
    public function get($id)
    {
        $item = Product::findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\Product;
     */
    public function getData($item)
    {
        if (!empty($item)) {
//            $item['product_image'] = $item->product_images()->orderBy('is_default', 'DESC')->first();
            $product_categories = $item->product_categories()->select('id')->get()->toArray();
            $item['category_names'] = $item->product_categories()->pluck('name')->implode(', ');
            if (!empty($product_categories)) {
                $previous = Product::where('is_active', 1)
                    ->where('id', '<', $item->id)
                    ->whereHas('product_categories', function ($query) use ($product_categories) {
                        $query->whereIn('id', $product_categories);
                    })
                    ->max('id');
                if (empty($previous)) {
                    $previous = Product::where('is_active', 1)
                        ->where('id', '>', $item->id)
                        ->whereHas('product_categories', function ($query) use ($product_categories) {
                            $query->whereIn('id', $product_categories);
                        })
                        ->max('id');
                }
                $next = Product::where('is_active', 1)
                    ->where('id', '>', $item->id)
                    ->whereHas('product_categories', function ($query) use ($product_categories) {
                        $query->whereIn('id', $product_categories);
                    })
                    ->min('id');
                if (empty($next)) {
                    $next = Product::where('is_active', 1)
                        ->where('id', '<', $item->id)
                        ->whereHas('product_categories', function ($query) use ($product_categories) {
                            $query->whereIn('id', $product_categories);
                        })
                        ->min('id');
                }

                $item['previous'] = Product::where('is_active', 1)
                    ->find($previous);
                $item['next'] = Product::where('is_active', 1)
                    ->find($next);
            } else {
                $previous = Product::where('is_active', 1)
                    ->where('id', '<', $item->id)
                    ->whereDoesntHave('product_categories')
                    ->max('id');
                if (empty($previous)) {
                    $previous = Product::where('is_active', 1)
                        ->where('id', '>', $item->id)
                        ->whereDoesntHave('product_categories')
                        ->max('id');
                }
                $next = Product::where('is_active', 1)
                    ->where('id', '>', $item->id)
                    ->whereDoesntHave('product_categories')
                    ->min('id');
                if (empty($next)) {
                    $next = Product::where('is_active', 1)
                        ->where('id', '<', $item->id)
                        ->whereDoesntHave('product_categories')
                        ->min('id');
                }

                $item['previous'] = Product::where('is_active', 1)
                    ->find($previous);
                $item['next'] = Product::where('is_active', 1)
                    ->find($next);
            }
        }

        return $item;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\Product;
     */
    static function getDataStatic($item)
    {
        if (!empty($item)) {
//            $item['product_image'] = $item->product_images()->orderBy('is_default', 'DESC')->first();
            $product_categories = $item->product_categories()->select('id')->get()->toArray();
            $item['category_names'] = $item->product_categories()->pluck('name')->implode(', ');
            if (!empty($product_categories)) {
                $previous = Product::where('is_active', 1)
                    ->where('id', '<', $item->id)
                    ->whereHas('product_categories', function ($query) use ($product_categories) {
                        $query->whereIn('id', $product_categories);
                    })
                    ->max('id');
                if (empty($previous)) {
                    $previous = Product::where('is_active', 1)
                        ->where('id', '>', $item->id)
                        ->whereHas('product_categories', function ($query) use ($product_categories) {
                            $query->whereIn('id', $product_categories);
                        })
                        ->max('id');
                }
                $next = Product::where('is_active', 1)
                    ->where('id', '>', $item->id)
                    ->whereHas('product_categories', function ($query) use ($product_categories) {
                        $query->whereIn('id', $product_categories);
                    })
                    ->min('id');
                if (empty($next)) {
                    $next = Product::where('is_active', 1)
                        ->where('id', '<', $item->id)
                        ->whereHas('product_categories', function ($query) use ($product_categories) {
                            $query->whereIn('id', $product_categories);
                        })
                        ->min('id');
                }

                $item['previous'] = Product::where('is_active', 1)
                    ->find($previous);
                $item['next'] = Product::where('is_active', 1)
                    ->find($next);
            } else {
                $previous = Product::where('is_active', 1)
                    ->where('id', '<', $item->id)
                    ->whereDoesntHave('product_categories')
                    ->max('id');
                if (empty($previous)) {
                    $previous = Product::where('is_active', 1)
                        ->where('id', '>', $item->id)
                        ->whereDoesntHave('product_categories')
                        ->max('id');
                }
                $next = Product::where('is_active', 1)
                    ->where('id', '>', $item->id)
                    ->whereDoesntHave('product_categories')
                    ->min('id');
                if (empty($next)) {
                    $next = Product::where('is_active', 1)
                        ->where('id', '<', $item->id)
                        ->whereDoesntHave('product_categories')
                        ->min('id');
                }

                $item['previous'] = Product::where('is_active', 1)
                    ->find($previous);
                $item['next'] = Product::where('is_active', 1)
                    ->find($next);
            }
        }

        return $item;
    }

    /**
     * Get all instances
     *
     * @return App/Models/Product;
     */
    public function getAll()
    {
        $items = Product::get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all active
     *
     * @param  $id
     *
     * @return \App\Models\Product Collection;
     */
    public function getActive($id)
    {
        $item = Product::where('is_active', 1)->findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get all active
     *
     * @return \App\Models\Product Collection;
     */
    public function getAllActive()
    {
        $items = Product::where('is_active', 1)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all active
     *
     * @return \App\Models\Product Collection;
     */
    public function getAllActiveNoCategory()
    {
        $items = Product::where('is_active', 1)->whereDoesntHave('product_categories')->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all active with offset limit
     *
     * @param int $offset
     * @param int $limit
     *
     * @return \App\Models\Product Collection;
     */
    public function getAllActiveOffsetLimit($offset = 0, $limit = 16)
    {
        $items = Product::where('is_active', 1)->skip($offset)->take($limit)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get search with offset limit
     *
     * @param string $keyword
     * @param int %category
     * @param int $offset
     * @param int $limit
     *
     * @return \App\Models\Product Collection;
     */
    public function getSearchActiveOffsetLimit($keyword = '', $category = 0, $offset = 0, $limit = 16)
    {
        $items = Product::where('is_active', 1)
            ->where('name', 'LIKE', '%' . $keyword . '%');
        if ($category) {
            $items = $items->whereHas('product_categories', function ($query) use ($category) {
                $query->where('id', $category);
            });
        }
        $items = $items->skip($offset)->take($limit)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get search total
     *
     * @param string $keyword
     * @param int %category
     *
     * @return \App\Models\Product Collection;
     */
    public function getSearchActiveTotal($keyword = '', $category = 0)
    {
        $items = Product::where('is_active', 1)
            ->where('name', 'LIKE', '%' . $keyword . '%');
        if ($category) {
            $items = $items->whereHas('product_categories', function ($query) use ($category) {
                $query->where('id', $category);
            });
        }
        $items = $items->count();
        return $items;
    }

    /**
     * Get active
     *
     * @param  $slug
     *
     * @return \App\Models\Product Collection;
     */
    public function getActiveBySlug($slug)
    {
        $item = Product::where('slug', $slug)->where('is_active', 1)->first();
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file)
    {
        $extension = $file->getClientOriginalExtension();
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/product_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        $s3 = Storage::disk('s3');
        $s3->put($file_upload_path, file_get_contents($file_upload_path), 'public');
        return $file_upload_path;
//        $response = array(
//            'status' => FALSE,
//            'data' => array(),
//            'message' => array(),
//        );
//
//        $extension = $file->getClientOriginalExtension();
//        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
//        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
//        $file_path = '/uploads/product_images';
//        $directory = public_path() . $file_path;
//
//        if (!File::exists($directory)) {
//            File::makeDirectory($directory, 0775);
//        }
//
//        $file->move($directory, $file_name);
//        $file_upload_path = 'public' . $file_path . '/' . $file_name;
//        $s3 = Storage::disk('s3');
//        $s3->put($file_upload_path, file_get_contents($file_upload_path), 'public');
//        $response['data']['file_upload_path'] = $file_upload_path;
//        $response['data']['file_name'] = $file_name;
//        $response['data']['is_default'] = 0;
//        $response['data']['id'] = $file_upload_path;
//        return $response;
    }
}