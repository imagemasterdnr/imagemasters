<?php

namespace App\Repositories;

use App\Models\Project;
use File;
use Storage;

/**
 * Class ProjectRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class ProjectRepository
{
    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/Project;
     */
    public function get($id)
    {
        $item = Project::findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\Project;
     */
    public function getData($item)
    {
        if (!empty($item)) {
            $item['project_image'] = $item->project_images()->first();
        }

        return $item;
    }

    /**
     * Get all instances
     *
     * @return App/Models/Project;
     */
    public function getAll()
    {
        $items = Project::get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all active
     *
     * @param  $id
     *
     * @return \App\Models\Project Collection;
     */
    public function getActive($id)
    {
        $item = Project::where('is_active', 1)->findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get all active
     *
     * @return \App\Models\Project Collection;
     */
    public function getAllActive()
    {
        $items = Project::where('is_active', 1)->where('is_featured', 1)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all active with offset limit
     *
     * @param int $offset
     * @param int $limit
     *
     * @return \App\Models\Project Collection;
     */
    public function getAllActiveOffsetLimit($offset = 0, $limit = 16)
    {
        $items = Project::where('is_active', 1)->skip($offset)->take($limit)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get active
     *
     * @param  $slug
     *
     * @return \App\Models\Project Collection;
     */
    public function getActiveBySlug($slug)
    {
        $item = Project::where('slug', $slug)->where('is_active', 1)->first();
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file)
    {
        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $extension = $file->getClientOriginalExtension();
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/project_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        $s3 = Storage::disk('s3');
        $s3->put($file_upload_path, file_get_contents($file_upload_path), 'public');
        $response['data']['file_upload_path'] = $file_upload_path;
        $response['data']['file_name'] = $file_name;
        $response['data']['is_default'] = 0;
        $response['data']['id'] = $file_upload_path;
        return $response;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadVideo($file)
    {
        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $extension = $file->getClientOriginalExtension();
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/project_videos';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        $response['data']['file_upload_path'] = $file_upload_path;
        $response['data']['file_name'] = $file_name;
        $response['data']['is_default'] = 0;
        $response['data']['id'] = $file_upload_path;
        return $response;
    }
}