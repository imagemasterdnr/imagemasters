<?php

namespace App\Repositories;

use App\Models\ProductCategory;
use File;
use Storage;

/**
 * Class ProductCategoryRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class ProductCategoryRepository
{
    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/ProductCategory;
     */
    public function get($id)
    {
        $item = ProductCategory::findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get all instances
     *
     * @return App/Models/ProductCategory;
     */
    public function getAll()
    {
        $items = ProductCategory::get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all active
     *
     * @param  $id
     *
     * @return \App\Models\ProductCategory Collection;
     */
    public function getActive($id)
    {
        $item = ProductCategory::where('is_active', 1)->findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get all active
     *
     * @return \App\Models\ProductCategory Collection;
     */
    public function getAllActive()
    {
        $items = ProductCategory::where('is_active', 1)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get active category by slug
     *
     * @param  $slug
     *
     * @return App/Models/ProductCategory;
     */
    public function getActiveBySlug($slug)
    {
        $item = ProductCategory::where('slug', $slug)->where('is_active', 1)->first();
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\Product;
     */
    public function getData($item)
    {
        if (!empty($item)) {
//            $item['product_category_image'] = $item->product_category_images()->orderBy('is_default', 'DESC')->first();
        }
        return $item;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file)
    {
        $extension = $file->getClientOriginalExtension();
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/product_category_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        $s3 = Storage::disk('s3');
        $s3->put($file_upload_path, file_get_contents($file_upload_path), 'public');
        return $file_upload_path;
//        $response = array(
//            'status' => FALSE,
//            'data' => array(),
//            'message' => array(),
//        );
//
//        $extension = $file->getClientOriginalExtension();
//        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
//        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
//        $file_path = '/uploads/product_category_images';
//        $directory = public_path() . $file_path;
//
//        if (!File::exists($directory)) {
//            File::makeDirectory($directory, 0775);
//        }
//
//        $file->move($directory, $file_name);
//        $file_upload_path = 'public' . $file_path . '/' . $file_name;
//        $s3 = Storage::disk('s3');
//        $s3->put($file_upload_path, file_get_contents($file_upload_path), 'public');
//        $response['data']['file_upload_path'] = $file_upload_path;
//        $response['data']['file_name'] = $file_name;
//        $response['data']['is_default'] = 0;
//        $response['data']['id'] = $file_upload_path;
//        return $response;
    }
}