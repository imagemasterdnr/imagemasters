<?php

namespace App\Repositories;

use App\Models\Appointment;
use File;
use Storage;

/**
 * Class AppointmentRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class AppointmentRepository
{
    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/Appointment;
     */
    public function get($id)
    {
        $item = Appointment::findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\Appointment;
     */
    public function getData($item)
    {
        if (!empty($item)) {

        }

        return $item;
    }

    /**
     * Get all instances
     *
     * @return App/Models/Appointment;
     */
    public function getAll()
    {
        $items = Appointment::get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    public function getAllPhotographer()
    {
        $items = Appointment::where('calendar_id', auth()->user()->photographer_calendar_id)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/Appointment;
     */
    public function getByAppointmentId($id)
    {
        $item = Appointment::where('appointment_id', $id)->first();
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file)
    {
        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $extension = $file->getClientOriginalExtension();
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/appointment_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        $s3 = Storage::disk('s3');
        $s3->put($file_upload_path, file_get_contents($file_upload_path), 'public');
        $response['data']['file_upload_path'] = $file_upload_path;
        $response['data']['file_name'] = $file_name;
        $response['data']['extension'] = $extension;
        $response['data']['thumbnail'] = '';
        if ($extension == 'mp4') {
            $response['data']['thumbnail'] = 'asd';
        }
        $response['data']['is_default'] = 0;
        $response['data']['id'] = $file_upload_path;
        return $response;
    }
}