<?php

namespace App\Repositories;

use App\Http\Traits\SystemSettingTrait;
use App\Models\User;
use File;
use Mail;

/**
 * Class UserRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class UserRepository
{
    use SystemSettingTrait;

    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/User;
     */
    public function get($id)
    {
        $item = User::findOrFail($id);
        return $item;
    }

    /**
     * Generate random hash/code
     *
     * @param $random_string_length = 6
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function generateRandomPassword($random_string_length = 6)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < $random_string_length; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }

    /**
     * Send email
     *
     * @param array $params
     *
     * @return mixed
     */
    public function emailWebhook($params = null)
    {
        if (isset($params)) {
            $system_setting_name = $this->getSystemSettingByCode('SS0001');
            $system_setting_email = $this->getSystemSettingByCode('SS0002');

            $data = [
                'subject' => $params['subject'],
                'user' => [
                    'name' => $params['user']['name'],
                    'email' => $params['user']['email']
                ],
                'password_data' => $params['password_data'],
                'attachments' => $params['attachments']
            ];

            Mail::send($params['view'], compact('data'), function ($message) use ($data, $system_setting_name, $system_setting_email) {
                $message->from($system_setting_email->value/*config('constants.no_reply_email')*/, $system_setting_name->value);
                $message->bcc(config('constants.dnr_bcc'));
                $message->to($data['user']['email'], $data['user']['name']);
                $message->subject($data['subject']);
                if (isset($data['attachments']) && count($data['attachments'])) {
                    foreach ($data['attachments'] as $attachment) {
                        if (File::exists($attachment)) {
                            $message->attach($attachment);
                        }
                    }
                }
            });
        }
    }

    /**
     * Send email
     *
     * @param array $params
     *
     * @return mixed
     */
    public function emailRegistered($params = null)
    {
        if (isset($params)) {
            $system_setting_name = $this->getSystemSettingByCode('SS0001');
            $system_setting_email = $this->getSystemSettingByCode('SS0002');

            $data = [
                'subject' => $params['subject'],
                'user' => [
                    'name' => $params['user']['name'],
                    'email' => $params['user']['email']
                ],
                'attachments' => $params['attachments']
            ];

            Mail::send($params['view'], compact('data'), function ($message) use ($data, $system_setting_name, $system_setting_email) {
                $message->from($system_setting_email->value/*config('constants.no_reply_email')*/, $system_setting_name->value);
                $message->bcc(config('constants.dnr_bcc'));
                $message->to($data['user']['email'], $data['user']['name']);
                $message->subject($data['subject']);
                if (isset($data['attachments']) && count($data['attachments'])) {
                    foreach ($data['attachments'] as $attachment) {
                        if (File::exists($attachment)) {
                            $message->attach($attachment);
                        }
                    }
                }
            });
        }
    }

    /**
     * Send email
     *
     * @param array $params
     *
     * @return mixed
     */
    public function emailRegisteredAdmin($params = null)
    {
        if (isset($params)) {
            $system_setting_name = $this->getSystemSettingByCode('SS0001');
            $system_setting_email = $this->getSystemSettingByCode('SS0002');

            $data = [
                'subject' => $params['subject'],
                'user' => [
                    'name' => $params['user']['name'],
                    'email' => $params['user']['email']
                ],
                'attachments' => $params['attachments']
            ];

            Mail::send($params['view'], compact('data'), function ($message) use ($data, $system_setting_name, $system_setting_email) {
                $message->from($system_setting_email->value/*config('constants.no_reply_email')*/, $system_setting_name->value);
                $message->bcc(config('constants.dnr_bcc'));
                $message->to($system_setting_email->value, $system_setting_name->value);
                $message->subject($data['subject']);
                if (isset($data['attachments']) && count($data['attachments'])) {
                    foreach ($data['attachments'] as $attachment) {
                        if (File::exists($attachment)) {
                            $message->attach($attachment);
                        }
                    }
                }
            });
        }
    }

    /**
     * Send email
     *
     * @param array $params
     *
     * @return mixed
     */
    public function emailAttachmentsAdded($params = null)
    {
        if (isset($params)) {
            $system_setting_name = $this->getSystemSettingByCode('SS0001');
            $system_setting_email = $this->getSystemSettingByCode('SS0002');

            $data = [
                'subject' => $params['subject'],
                'user' => [
                    'name' => $params['user']['name'],
                    'email' => $params['user']['email']
                ],
                'attachments' => $params['attachments']
            ];

            Mail::send($params['view'], compact('data'), function ($message) use ($data, $system_setting_name, $system_setting_email) {
                $message->from($system_setting_email->value/*config('constants.no_reply_email')*/, $system_setting_name->value);
                $message->bcc(config('constants.dnr_bcc'));
                $message->to($data['user']['email'], $data['user']['name']);
                $message->subject($data['subject']);
                if (isset($data['attachments']) && count($data['attachments'])) {
                    foreach ($data['attachments'] as $attachment) {
                        if (File::exists($attachment)) {
                            $message->attach($attachment);
                        }
                    }
                }
            });
        }
    }

    /**
     * Upload and move file to directory
     *
     * @param $file
     * @param $type
     * @param $path
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file, $type = null, $path)
    {
        $extension = $file->getClientOriginalExtension();
        $file_name = substr((pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)), 0, 30) . '-' . time() . ($type ? '-' . $type : '') . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/' . $path;
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0777);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        return $file_upload_path;
    }

    /**
     * Send email
     *
     * @param array $params
     *
     * @return mixed
     */
    public function emailTourPreview($params = null)
    {
        if (isset($params)) {
            $data = [
                'subject' => $params['subject'],
                'from' => [
                    'name' => $params['from']['name'],
                    'email' => $params['from']['email']
                ],
                'to' => [
                    'name' => $params['to']['name'],
                    'email' => $params['to']['email']
                ],
                'appointment_data' => $params['appointment_data'],
                'message' => $params['message'],
                'attachments' => $params['attachments']
            ];

            Mail::send($params['view'], compact('data'), function ($message) use ($data) {
                $message->from($data['from']['email'], $data['from']['name']);
                $message->bcc(config('constants.dnr_bcc'));
                $message->to($data['to']['email'], $data['to']['name']);
                $message->subject($data['subject']);
                if (isset($data['attachments']) && count($data['attachments'])) {
                    foreach ($data['attachments'] as $attachment) {
                        if (File::exists($attachment)) {
                            $message->attach($attachment);
                        }
                    }
                }
            });
        }
    }
}