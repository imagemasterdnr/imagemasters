<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Project
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Project extends Model
{
    use SoftDeletes;

    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'slug',
        'description',
        'image',
        'is_active',
        'is_featured',
        'seo_meta_id',
        'vimeo_link'
    ];

    public function project_images()
    {
        return $this->hasMany('App\Models\ProjectImage', 'project_id');
    }

    public function project_videos()
    {
        return $this->hasMany('App\Models\ProjectVideo', 'project_id');
    }

    public function seo_meta()
    {
        return $this->hasOne('App\Models\SeoMeta', 'id', 'seo_meta_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'project_id');
    }
}