<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'slug',
        'description',
        'is_active',
        'seo_meta_id',
        'acuity_scheduling_link',
        'project_id',
        'pricing_content',
        'banner_image',
        'overview_image'
    ];

    public function product_categories()
    {
        return $this->belongsToMany('App\Models\ProductCategory', 'product_categories_matrix', 'product_id', 'product_category_id');
    }

    public function product_images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

    public function project()
    {
        return $this->hasOne('App\Models\Project', 'id', 'project_id');
    }

    public function seo_meta()
    {
        return $this->hasOne('App\Models\SeoMeta', 'id', 'seo_meta_id');
    }
}