<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProjectImage
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class ProjectImage extends Model
{
    use SoftDeletes;

    protected $table = 'project_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'name',
        'file',
        'size',
        'is_default',
        'type'
    ];

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }
}