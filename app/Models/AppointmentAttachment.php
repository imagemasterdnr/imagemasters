<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AppointmentAttachment
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class AppointmentAttachment extends Model
{
    use SoftDeletes;

    protected $table = 'appointment_attachments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'appointment_id',
        'name',
        'file',
        'size',
        'is_default',
        'type',
        'extension',
        'thumbnail'
    ];

    public function appointment()
    {
        return $this->belongsTo('App\Models\Appointment', 'appointment_id');
    }
}