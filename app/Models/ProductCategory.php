<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductCategory
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class ProductCategory extends Model
{
    use SoftDeletes;

    protected $table = 'product_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'slug',
        'description',
        'is_active',
        'seo_meta_id',
        'banner_image',
        'banner_video',
        'overview_image'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_categories_matrix', 'product_category_id', 'product_id');
    }

    public function product_category_images()
    {
        return $this->hasMany('App\Models\ProductCategoryImage', 'product_category_id');
    }

    public function seo_meta()
    {
        return $this->hasOne('App\Models\SeoMeta', 'id', 'seo_meta_id');
    }
}