<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Appointment
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Appointment extends Model
{
    use SoftDeletes;

    protected $table = 'appointments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'appointment_id',
        'calendar_id',
        'appointment_type_id',
        'user_id',
        'product_id',
        'beds',
        'baths',
        'availability_status',
        'square_feet',
        'address',
        'google_map_link',
        'video_link',
        'is_branded',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function photographer()
    {
        return $this->belongsTo('App\Models\User', 'calendar_id', 'photographer_calendar_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function attachments()
    {
        return $this->hasMany('App\Models\AppointmentAttachment', 'appointment_id');
    }
}