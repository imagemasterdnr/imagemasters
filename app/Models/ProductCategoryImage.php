<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductCategoryImage
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class ProductCategoryImage extends Model
{
    use SoftDeletes;

    protected $table = 'product_category_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_category_id',
        'name',
        'file',
        'size',
        'is_default',
        'type'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'product_category_id');
    }
}