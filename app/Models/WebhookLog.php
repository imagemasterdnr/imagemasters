<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WebhookLog
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class WebhookLog extends Model
{
    use SoftDeletes;

    protected $table = 'webhook_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'action',
        'appointment_id',
        'calendar_id',
        'appointment_type_id',
    ];
}