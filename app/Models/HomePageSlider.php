<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HomePageSlider
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class HomePageSlider extends Model
{
    use SoftDeletes;

    protected $table = 'home_page_sliders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'description',
        'image',
        'is_active',
    ];
}