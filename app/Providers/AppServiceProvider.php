<?php

namespace App\Providers;

use Hash;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        /* custom old password validation */
        Validator::extend('old_password', function ($attribute, $value, $parameters) {
            return Hash::check($value, auth()->user()->password);
        });
        Validator::replacer('old_password', function($message, $attribute, $rule, $parameters){
            return 'The old password you entered is incorrect.';
        });

        Validator::extend('unique_name', function ($attribute, $value, $parameters) {
            $count = \DB::table('users')->where('first_name', $value)
                ->where('last_name', $parameters[0])
                ->where('type', 1)
                ->count();
            return $count === 0;
        });
        Validator::replacer('unique_name', function($message, $attribute, $rule, $parameters){
            return 'The first name and last name should be unique.';
        });

        if(env('APP_ENV') == 'prod_ssl') {
            \URL::forceScheme('https');
        }

        \Artisan::call('view:clear');
        \Artisan::call('cache:clear');
        \Artisan::call('route:clear');
        \Artisan::call('config:clear');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
