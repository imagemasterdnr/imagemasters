<?php

/*
|--------------------------------------------------------------------------
| Main Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "main" middleware group. Now create something great!
|
*/

Route::group(
    [
        "middleware" => ['isAdmin', 'revalidate'],
        "prefix" => 'admin'
    ], function () {

    /* dashboard routes */
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });

    /* dashboard */
    Route::get('/dashboard', [
        'uses' => '\App\Http\Controllers\AdminDashboardController@index',
        'as' => 'admin.dashboard',
    ]);
    /* dashboard */

    /* users */
    Route::resource('/users', 'UserController', [
        'as' => 'admin'
    ]);

    Route::delete('/users/{id}/delete',
        ['as' => 'admin.users.delete',
            'uses' => '\App\Http\Controllers\UserController@destroy']
    );
    /* users */

    /* customers */

    Route::get('/customers',
        ['as' => 'admin.customers.index',
            'uses' => '\App\Http\Controllers\UserController@getCustomersIndex']
    );

    Route::get('/customers/{id}',
        ['as' => 'admin.customers.show',
            'uses' => '\App\Http\Controllers\UserController@getCustomersShow']
    );

    Route::delete('/customers/{id}/delete',
        ['as' => 'admin.customers.delete',
            'uses' => '\App\Http\Controllers\UserController@destroy']
    );
    /* customers */

    /* roles */
    Route::resource('/roles', 'RoleController', [
        'as' => 'admin'
    ]);

    Route::delete('/roles/{id}/delete',
        ['as' => 'admin.roles.delete',
            'uses' => '\App\Http\Controllers\RoleController@destroy']
    );
    /* roles */

    /* permissions */
    Route::resource('/permissions', 'PermissionController', [
        'as' => 'admin'
    ]);

    Route::delete('/permissions/{id}/delete',
        ['as' => 'admin.permissions.delete',
            'uses' => '\App\Http\Controllers\PermissionController@destroy']
    );
    /* permissions */

    /* permission groups */
    Route::resource('/permission_groups', 'PermissionGroupController', [
        'as' => 'admin'
    ]);

    Route::delete('/permission_groups/{id}/delete',
        ['as' => 'admin.permission_groups.delete',
            'uses' => '\App\Http\Controllers\PermissionGroupController@destroy']
    );
    /* permission groups */

    /* system settings */
    Route::resource('/system_settings', 'SystemSettingController', [
        'as' => 'admin',
    ]);

    Route::delete('/system_settings/{id}/delete',
        ['as' => 'admin.system_settings.delete',
            'uses' => '\App\Http\Controllers\SystemSettingController@destroy']
    );
    /* system settings */

    /* posts */
    Route::resource('/posts', 'PostController', [
        'as' => 'admin'
    ]);

    Route::delete('/posts/{id}/delete',
        ['as' => 'admin.posts.delete',
            'uses' => '\App\Http\Controllers\PostController@destroy']
    );
    /* posts */

    /* pages */
    Route::resource('/pages', 'PageController', [
        'as' => 'admin'
    ]);

    Route::delete('/pages/{id}/delete',
        ['as' => 'admin.pages.delete',
            'uses' => '\App\Http\Controllers\PageController@destroy']
    );
    /* pages */

    /* contacts */
    Route::resource('/contacts', 'ContactController', [
        'as' => 'admin'
    ]);
    /* contacts */

    /* product_categories */
    Route::post('/product_categories/upload_image', [
        'uses' => '\App\Http\Controllers\ProductCategoryController@uploadImage',
        'as' => 'admin.product_categories.upload_image'
    ]);
    Route::delete('/product_categories/delete_image', [
        'uses' => '\App\Http\Controllers\ProductCategoryController@deleteImage',
        'as' => 'admin.product_categories.delete_image'
    ]);

    Route::resource('/product_categories', 'ProductCategoryController', [
        'as' => 'admin'
    ]);

    Route::delete('/product_categories/{id}/delete',
        ['as' => 'admin.product_categories.delete',
            'uses' => '\App\Http\Controllers\ProductCategoryController@destroy']
    );
    /* product_categories */

    /* products */
    Route::post('/products/upload_image', [
        'uses' => '\App\Http\Controllers\ProductController@uploadImage',
        'as' => 'admin.products.upload_image'
    ]);
    Route::delete('/products/delete_image', [
        'uses' => '\App\Http\Controllers\ProductController@deleteImage',
        'as' => 'admin.products.delete_image'
    ]);

    Route::resource('/products', 'ProductController', [
        'as' => 'admin'
    ]);

    Route::delete('/products/{id}/delete',
        ['as' => 'admin.products.delete',
            'uses' => '\App\Http\Controllers\ProductController@destroy']
    );
    /* products */

    /* projects */
//    Route::post('/projects/upload_image', [
//        'uses' => '\App\Http\Controllers\ProjectController@uploadImage',
//        'as' => 'admin.projects.upload_image'
//    ]);
//    Route::delete('/projects/delete_image', [
//        'uses' => '\App\Http\Controllers\ProjectController@deleteImage',
//        'as' => 'admin.projects.delete_image'
//    ]);

    Route::resource('/projects', 'ProjectController', [
        'as' => 'admin'
    ]);

    Route::delete('/projects/{id}/delete',
        ['as' => 'admin.projects.delete',
            'uses' => '\App\Http\Controllers\ProjectController@destroy']
    );
    /* projects */

    /* subscriptions */
    Route::resource('/subscriptions', 'SubscriptionController', [
        'as' => 'admin'
    ]);
    /* subscriptions */

    /* home_page_sliders */
    Route::resource('/home_page_sliders', 'HomePageSliderController', [
        'as' => 'admin'
    ]);

    Route::delete('/home_page_sliders/{id}/delete',
        ['as' => 'admin.home_page_sliders.delete',
            'uses' => '\App\Http\Controllers\HomePageSliderController@destroy']
    );
    /* home_page_sliders */

    Route::get('/appointments', [
        'uses' => '\App\Http\Controllers\AcuityController@getAppointments',
        'as' => 'admin.appointments',
    ]);

    Route::get('/appointments/{id}', [
        'uses' => '\App\Http\Controllers\AcuityController@getAppointmentById',
        'as' => 'admin.appointments.show',
    ]);

    Route::post('/appointments/upload_image', [
        'uses' => '\App\Http\Controllers\AcuityController@uploadImage',
        'as' => 'admin.appointments.upload_image',
    ]);

    Route::delete('/appointments/delete_image', [
        'uses' => '\App\Http\Controllers\AcuityController@deleteImage',
        'as' => 'admin.appointments.delete_image',
    ]);

    Route::get('/appointments/{id}/add_attachments', [
        'uses' => '\App\Http\Controllers\AcuityController@getAddAppointmentAttachments',
        'as' => 'admin.appointments.add_attachments.get',
    ]);

    Route::post('/appointments/{id}/add_attachments', [
        'uses' => '\App\Http\Controllers\AcuityController@postAddAppointmentAttachments',
        'as' => 'admin.appointments.add_attachments.post',
    ]);

    Route::get('/appointments/{id}/edit_property_details', [
        'uses' => '\App\Http\Controllers\AcuityController@editPropertyDetails',
        'as' => 'admin.appointments.edit_property_details',
    ]);

    Route::post('/appointments/{id}/update_property_details', [
        'uses' => '\App\Http\Controllers\AcuityController@updatePropertyDetails',
        'as' => 'admin.appointments.update_property_details',
    ]);

    Route::post('/appointments/{id}/cancel', [
        'uses' => '\App\Http\Controllers\AcuityController@cancelAppointment',
        'as' => 'admin.appointments.cancel_appointment',
    ]);

    /* ckeditor image upload */
    Route::post('/ckeditor_image_upload',
        ['as' => 'admin.ckeditor_image_upload',
            'uses' => '\App\Http\Controllers\PageController@ckEditorImageUpload']
    );
    /* ckeditor image upload */
});


Route::group(
    [
        "middleware" => ['isFront', 'revalidate'],
        "prefix" => 'customer'
    ], function () {

    /* dashboard routes */
    Route::get('/', function () {
        return redirect('customer/dashboard/account');
    });

    Route::get('/dashboard', function () {
        return redirect('customer/dashboard/account');
    });

    Route::get('/dashboard/account', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@account',
        'as' => 'customer.dashboard.account',
    ]);

    Route::get('/dashboard/products', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@products',
        'as' => 'customer.dashboard.products',
    ]);

    Route::get('/dashboard/products/{id}/view_album', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@productViewAlbum',
        'as' => 'customer.dashboard.products.view_album',
    ]);

    Route::get('/dashboard/products/{id}/edit_property_details', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@editPropertyDetails',
        'as' => 'customer.dashboard.products.edit_property_details',
    ]);

    Route::post('/dashboard/products/{id}/update_property_details', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@updatePropertyDetails',
        'as' => 'customer.dashboard.products.update_property_details',
    ]);

    Route::get('/dashboard/products/{id}/view_album/download_images', [
        'uses' => '\App\Http\Controllers\AcuityController@downloadImages',
        'as' => 'customer.dashboard.products.download_images',
    ]);

    Route::get('/dashboard/products/view_album/download_image/{id}', [
        'uses' => '\App\Http\Controllers\AcuityController@downloadImageFromS3',
        'as' => 'customer.dashboard.products.download_image',
    ]);

    Route::get('/dashboard/products/{id}/order_detail', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@productOrderDetail',
        'as' => 'customer.dashboard.products.order_detail',
    ]);

    Route::post('/dashboard/products/{id}/cancel', [
        'uses' => '\App\Http\Controllers\AcuityController@cancelAppointment',
        'as' => 'customer.appointments.cancel_appointment',
    ]);

    Route::get('/dashboard/profile', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@profile',
        'as' => 'customer.dashboard.profile',
    ]);

    Route::post('/dashboard/profile/{id}/email', [
        'uses' => '\App\Http\Controllers\AcuityController@putClient',
        'as' => 'customer.dashboard.profile.email.put'
    ]);

    Route::post('/dashboard/profile/{id}/password', [
        'uses' => '\App\Http\Controllers\UserController@updatePassword',
        'as' => 'customer.dashboard.profile.password.post'
    ]);

    Route::get('/dashboard/support', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@support',
        'as' => 'customer.dashboard.support',
    ]);
});

Route::group(
    [
        "middleware" => [/*'isAdmin',*/ 'revalidate'],
        "prefix" => 'acuity'
    ], function () {

    Route::get('/appointments', [
        'uses' => '\App\Http\Controllers\AcuityController@getAppointments',
        'as' => 'acuity.appointments',
    ]);

    Route::get('/appointments/{id}', [
        'uses' => '\App\Http\Controllers\AcuityController@getAppointmentById',
        'as' => 'acuity.appointments.show',
    ]);

    Route::get('/appointment-types', [
        'uses' => '\App\Http\Controllers\AcuityController@getAppointmentTypes',
        'as' => 'acuity.appointment_types',
    ]);

    Route::get('/appointment-addons', [
        'uses' => '\App\Http\Controllers\AcuityController@getAppointmentAddons',
        'as' => 'acuity.appointment_addons',
    ]);

    Route::get('/calendars', [
        'uses' => '\App\Http\Controllers\AcuityController@getCalendars',
        'as' => 'acuity.calendars',
    ]);

    Route::get('/clients', [
        'uses' => '\App\Http\Controllers\AcuityController@getClients',
        'as' => 'acuity.clients',
    ]);

    Route::get('/forms', [
        'uses' => '\App\Http\Controllers\AcuityController@getForms',
        'as' => 'acuity.forms',
    ]);

    Route::post('/webhooks', [
        'uses' => '\App\Http\Controllers\AcuityController@webhooks',
        'as' => 'acuity.webhooks',
    ]);
});

Route::group(
    [
        "middleware" => ['revalidate'],
    ], function () {

    /* contacts */
    Route::post('/save_contact', [
        'uses' => '\App\Http\Controllers\ContactController@saveContact',
        'as' => 'save_contact'
    ]);
    /* contact */

    /* subscriptions */
    Route::post('/save_subscription', [
        'uses' => '\App\Http\Controllers\SubscriptionController@saveSubscription',
        'as' => 'save_subscription'
    ]);
    /* subscription */

    Route::get('/tour_preview/{id}', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@tourPreview',
        'as' => 'tour_preview',
    ]);

    Route::post('/tour_preview/{id}/send_mail', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@sendTourPreview',
        'as' => 'tour_preview.send_mail',
    ]);

    Route::get('/{slug?}/{slug_2?}/{slug_3?}', '\App\Http\Controllers\PageController@showPages');

});