<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();


Route::group(
    [
        "middleware" => ['revalidate'],
    ], function () {

    /*
    * Admin
    */
    /* Login Routes */
    Route::get('/admin/login', ['as' => 'admin.login', 'uses' => 'Admin\Auth\LoginController@showLoginForm']);
    Route::post('/admin/login', ['as' => 'admin.login.post', 'uses' => 'Admin\Auth\LoginController@login']);
    Route::post('/admin/logout', ['as' => 'admin.logout', 'uses' => 'Admin\Auth\LoginController@logout']);
    Route::get('/admin/logout', ['as' => 'admin.logout', 'uses' => 'Admin\Auth\LoginController@logout']);

    /* Registration Routes */
//    Route::get('/admin/register', ['as' => 'admin.register', 'uses' => 'Admin\Auth\RegisterController@showRegistrationForm']);
//    Route::post('/admin/register', ['as' => 'admin.register.post', 'uses' => 'Admin\Auth\RegisterController@register']);


    /*
     * Front
     */
    /* Login Routes */
    Route::get('/customer/login', ['as' => 'customer.login', 'uses' => 'Front\Auth\LoginController@showLoginForm']);
    Route::post('/customer/login', ['as' => 'customer.login.post', 'uses' => 'Front\Auth\LoginController@login']);
    Route::post('/customer/logout', ['as' => 'customer.logout', 'uses' => 'Front\Auth\LoginController@logout']);
    Route::get('/customer/logout', ['as' => 'customer.logout', 'uses' => 'Front\Auth\LoginController@logout']);

    /* Registration Routes */
    Route::get('/customer/register', ['as' => 'customer.register', 'uses' => 'Front\Auth\RegisterController@showRegistrationForm']);
    Route::post('/customer/register', ['as' => 'customer.register.post', 'uses' => 'Front\Auth\RegisterController@register']);

    /* Forgot Password Routes */
    Route::get('/customer/password/email', ['as' => 'customer.password.email', 'uses' => 'Front\Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('/customer/password/email', ['as' => 'customer.password.email.post', 'uses' => 'Front\Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('/customer/password/reset/{token}', ['as' => 'customer.password.reset', 'uses' => 'Front\Auth\ResetPasswordController@showResetForm']);
    Route::post('/customer/password/reset', ['as' => 'customer.password.reset.post', 'uses' => 'Front\Auth\ResetPasswordController@reset']);
});