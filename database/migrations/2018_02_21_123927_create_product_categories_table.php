<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 75);
            $table->string('name', 75);
            $table->string('slug', 75);
            $table->text('description');
            $table->string('banner_image', 255);
            $table->string('banner_video', 255);
            $table->string('overview_image', 255);
            $table->integer('seo_meta_id');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('product_categories');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}