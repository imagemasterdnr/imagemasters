<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_address_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('address', 125);
            $table->string('address_2', 125);
            $table->string('city', 125);
            $table->string('state', 125);
            $table->string('postal', 125);
            $table->string('country', 125);
            $table->integer('type')->comment('1-Billing, 2-Shipping')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('user_address_details');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}