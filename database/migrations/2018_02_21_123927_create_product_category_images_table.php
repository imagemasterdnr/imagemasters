<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id');
            $table->string('name', 75);
            $table->string('file', 255);
            $table->string('size', 75);
            $table->integer('is_default');
            $table->integer('type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('product_category_images');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}