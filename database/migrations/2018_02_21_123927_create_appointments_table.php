<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appointment_id');
            $table->string('calendar_id');
            $table->string('appointment_type_id');
            $table->string('user_id');
            $table->string('product_id');
            $table->string('beds');
            $table->string('baths');
            $table->string('availability_status');
            $table->string('square_feet');
            $table->text('address');
            $table->text('google_map_link');
            $table->text('video_link');
            $table->integer('is_branded');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}