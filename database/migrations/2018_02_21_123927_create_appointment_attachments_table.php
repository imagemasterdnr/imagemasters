<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appointment_id');
            $table->string('name', 75);
            $table->string('file', 255);
            $table->string('size', 75);
            $table->integer('is_default');
            $table->integer('type');
            $table->string('extension', 75);
            $table->string('thumbnail', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_attachments');
    }
}