<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'dtph',
                'name' => 'Daytime Photography',
                'slug' => 'daytime-photography',
                'description' => '<div class="single-heading">
<h4>DAYTIME PHOTOGRAPHY</h4>
</div>

<p>Photographer arrives during regular daylight hours and captures multiple photos of interiors and exteriors, we bring lighting to make sure the best features are well represented. If property is a condo/part of community with onsite amenities we usually include many photos of those features at no extra charge. Need special neighborhood scenes captured? No problem, we can take off-site photos at an additional cost...just ask prior to the appointment so we can set aside enough time on schedule.</p>
',
                'banner_image' => 'public/uploads/product_images/daytime-photography-01-1563408886.jpg',
                'overview_image' => 'public/uploads/product_images/daytime-photography-overview-01-1563409302.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Daytime+Photography" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>',
                'seo_meta_id' => 9,
                'project_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-01-31 16:18:55',
                'updated_at' => '2019-08-30 11:31:49',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'zill3d',
                'name' => 'Zillow 3D Home Tour',
                'slug' => 'zillow-3d-home-tour',
                'description' => '<div class="single-heading">
<h4>ZILLOW 3D HOME</h4>
</div>

<p>Zillow recently developed their 3D Home Virtual Tours (helps place your listing prominently on Zillow!)...we are able to produce these at a very affordable price so if you are thinking about Matterport but want a more cost effective product than Zillow 3D may be what you need. Listings with 3D HOME VIRTUAL TOURS are given increased visibility on Zillow and only Premier Agents or Zillow Certified Photographers can post 3D HOME TOURS! This includes a boost in Zillow\'s search order, as well as email notifications sent to potential buyers. These benefits can help you find more qualified buyers, as you’ll be talking with buyers who can view a video/tour of the home before contacting you.</p>
',
                'banner_image' => 'public/uploads/product_images/zillow-3d-photography-03-1564693781.jpg',
                'overview_image' => 'public/uploads/product_images/zillow-3d-photography-overview-01-1564693647.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Zillow+3D+Home+Tour" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 10,
                'project_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-01-31 16:35:53',
                'updated_at' => '2019-08-30 11:32:39',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'twlgph',
                'name' => 'Twilight/Sunset Photography',
                'slug' => 'twilightsunset-photography',
                'description' => '<div class="single-heading">
<h4>TWILIGHT/SUNSET</h4>
</div>

<p>We bring powerful strobe lighting and take several exposures per scene moving our lights so that everything is properly captured. Not to brag (ok, we are) but our twilight scenes are spectacular, these are not your set and shoot HDR without lighting. We hand craft these photos and the quality is evident, our experience and equipment result in sharp detail and realistic color.....no weird nuclear explosion look (we’ve all seen that). Yes, there are other photographers who offer twilight for cheaper......there is a reason something costs less. Sorry, we won’t do our twilight for lower price, have to say it as we are asked all the time....to get something to look this good takes time and talent.</p>

<p>Photographer arrives approximately 30 minutes prior to sunset and shoots until 30 minutes after sunset capturing approximately 6-10 scenes (less scenes on estate sized homes).</p>
',
                'banner_image' => 'public/uploads/product_images/twilight-photography-01-1563409026.jpg',
                'overview_image' => 'public/uploads/product_images/twilight-photography-overivew-01-1563409216.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Twilight+Photography" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 19,
                'project_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-02-28 10:44:05',
                'updated_at' => '2019-08-30 11:39:39',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'arlph',
                'name' => 'Aerial Photography',
                'slug' => 'aerial-photography',
                'description' => '<div class="single-heading">
<h4>AERIAL DRONE</h4>
</div>

<p>Our drone photographers are FAA licensed and insured.....beware, not everyone who offers these services are licensed! Aerial photography is not just for large properties, they can also provide a unique and dramatic prospective of a home when taken at 20-30 feet elevation.</p>
',
                'banner_image' => 'public/uploads/product_images/aerial-banner-01-1563409530.jpg',
                'overview_image' => 'public/uploads/product_images/aerial-overview-01-1563409531.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Aerial+Photography" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 20,
                'project_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-02-28 10:53:25',
                'updated_at' => '2019-08-30 11:37:16',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'prluxph',
                'name' => 'Premium Luxury Photography',
                'slug' => 'premium-luxury-photography',
                'description' => '<div class="single-heading">
<h4>PREMIUM LUXURY PHOTOGRAPHY</h4>
</div>

<p>When you need magazine quality images for your very best listings. Our photographer brings studio lighting, highest quality camera equipment and spends additional time creating perfect scenes onsite and during editing process. This product is custom bid and we provide you with an estimate prior to scheduled appointment; onsite visit for estimate is recommended for estate properties, we are happy to do this at no additional cost to properly assess the job. This is also a perfect product for architects, builders, designers, commercial brokers or your website and marketing.</p>
',
                'banner_image' => 'public/uploads/product_images/premium-banner-01-1563409765.jpg',
                'overview_image' => 'public/uploads/product_images/premium-overview-01-1563409765.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Premium+Luxury+Photography" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 21,
                'project_id' => 6,
                'is_active' => 1,
                'created_at' => '2019-02-28 10:59:01',
                'updated_at' => '2019-08-30 11:37:30',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => '',
                'name' => 'Aerial Video',
                'slug' => 'aerial-video',
                'description' => '<div class="single-heading">
<h4>AERIAL VIDEO</h4>
</div>

<p>Our drone photographers are FAA licensed and insured. Aerial flyover video captured on DJI Inspire 2 Drone for large parcels, estates, commercial properties or community features. Ideal if you have a land listing, commercial property (like apartment complex) or estate to show features from above like nothing else can ! ***You will be notified at time of appointment if property lies within FAA no-fly zone—near airports, government buildings, military bases, etc...with advanced notice many of these areas we can submit request for 1day permit and be allowed to fly (additional $75 fee incurred by customer).</p>

<div class="single-heading">
<h4>AERIAL PHOTO</h4>
</div>

<p>Our drone photographers are FAA licensed and insured. Aerial photography is not just for large properties, they can also provide a unique and dramatic prospective of a home when taken just 20-30 feet off the ground. Also ideal to showcase nearby community features like parks, beach, shopping or HOA amenities. ***You will be notified at time of appointment if property lies within FAA no-fly zone—near airports, government buildings, military bases, etc...with advanced notice many of these areas we can submit request for 1day permit and be allowed to fly (additional $75 fee incurred by customer).</p>
',
                'banner_image' => 'public/uploads/product_images/aerial-video-banner-01-1563470367.jpg',
                'overview_image' => 'public/uploads/product_images/aerial-video-overview-01-1563470367.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=9527363" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 22,
                'project_id' => 7,
                'is_active' => 1,
                'created_at' => '2019-03-01 16:57:59',
                'updated_at' => '2019-08-30 11:37:41',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => '',
                'name' => '3D Matterport Showcase',
                'slug' => '3d-matterport-showcase',
                'description' => '<div class="single-heading">
<h4>3D MATTERPORT</h4>
</div>

<p>Matterport 360 Tours are easy to navigate and buyers seem to LOVE them. If you have a listing that shows beautifully this is a great way to show it off. Matterport\'s Showcase 3.0 player offers many perspectives. Use “Dollhouse View” to see the whole property all at once, switch to “Inside View” for an interactive walkthrough experience - like you\'re actually there, “Floorplan” gives a birds-eye perspective of the property (“Floorplan” feature is additional cost).</p>
',
                'banner_image' => 'public/uploads/product_images/3d-banner-1567213631.jpg',
                'overview_image' => 'public/uploads/product_images/3d-matterport-overview-03-1567011084.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:3D+MATTERPORT+SHOWCASE" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 23,
                'project_id' => 6,
                'is_active' => 1,
                'created_at' => '2019-03-01 17:00:39',
                'updated_at' => '2019-08-30 18:07:12',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => '',
                'name' => 'GOOGLE 360',
                'slug' => 'google-360',
                'description' => '<div class="single-heading">
<h4>Google 360</h4>
</div>

<p>Google 360 tours are primarily designed for businesses as they link to Google maps—it is a fantastic marketing tool for any business open to the public as it displays information on Google maps, lets the viewer choose 360 tour as option to view at an address then click and go inside to take a look around. We can create as many 360 scenes as needed for your location. This can be apartment complexes, restaurants, retail establishments, businesses, anyone who wants stronger presence on Google so users can immerse themselves into your location.</p>
',
                'banner_image' => 'public/uploads/product_images/out-story-banner-1552019486.jpg',
                'overview_image' => 'public/uploads/product_images/2019-02-282-1551488531.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Custom+Brochure+Flyers" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 24,
                'project_id' => 6,
                'is_active' => 1,
                'created_at' => '2019-03-01 17:02:23',
                'updated_at' => '2019-08-30 11:38:04',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => '',
                'name' => 'Walk-Thru Video',
                'slug' => 'walk-thru-video',
                'description' => '<div class="single-heading">
<h4>WALK-THRU</h4>
</div>

<p>Walk-thru video tour of home and it’s exteriors (if in condo complex basic amenities are also featured). Music and text overlay included. This video is a GREAT product and very affordable, we have been able to bring the price down on these— quality will amaze you AND your client. Agent branded version is additional cost. We post video onto Zillow and MLS plus give you a link to add to websites, email marketing blasts and social media. Listings with video walkthroughs are given increased visibility on Zillow and only Premier Agents or Zillow Certified Photographers can post video!</p>
',
                'banner_image' => 'public/uploads/product_images/walk-thru-video-banner-01-1563471271.jpg',
                'overview_image' => 'public/uploads/product_images/walk-thru-video-overview-01-1563471272.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Walk-Thru+Video" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 25,
                'project_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-03-01 17:04:00',
                'updated_at' => '2019-08-30 11:38:15',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => '',
                'name' => 'Premium Lifestyle Video',
                'slug' => 'premium-lifestyle-video',
                'description' => '<div class="single-heading">
<h4>PREMIUM LIFESTYLE VIDEO</h4>
</div>

<p>When your listing needs the very best representation these videos are spectacular, evoking the true experience of touring the property by making it come to life. Product includes aerial flyover video of home and surrounding area shot on DJI Inspire 2 Drone, walk-thru of home and it’s surroundings captured on cinema camera and slider, detailed focus shots to bring the best features center stage, twilight video, custom motion graphics and overlay PLUS community scenes like nearby restaurants, shops and parks to convey the lifestyle your listing represents. We use the very best equipment and the quality is evident, this is like a movie of your listing !!! You can also include voiceover, agent interview, time-lapse scenes and many other eye catching features.</p>

<p>Clients and buyers are expecting more these days on luxury listings, wanting to fully view a home and it’s lifestyle before seeing in person, critical for out-of-town buyers. Not doing video could mean prospective buyer never sees the home.</p>

<p>THESE ARE AMAZING VIDEOS AND ALSO A GREAT PRESENTATION TOOL ON YOUR FUTURE LISTING APPTS AND “COMING SOON” MARKETING!</p>
',
                'banner_image' => 'public/uploads/product_images/premium-video-banner-01-1563471629.jpg',
                'overview_image' => 'public/uploads/product_images/premium-video-overview-01-1563471630.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Premium+Lifestyle+Video" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 26,
                'project_id' => 6,
                'is_active' => 1,
                'created_at' => '2019-03-08 11:20:54',
                'updated_at' => '2019-08-30 11:38:27',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'code' => '',
                'name' => 'Zillow Walk-Thru Video',
                'slug' => 'zillow-walk-thru-video',
                'description' => '<div class="single-heading">
<h4>ZILLOW WALK-THRU</h4>
</div>

<p>This new walk-thru product from Zillow is created through their Real Estate App and immediately uploaded to the system, we send you a link so it can also go on MLS and be used for other marketing. We capture on iPhone w/stabilizer and wide lens taking a complete walk through of your listing. Is not customized like regular walk-thru video product but is an affordable option and we are Zillow Certified Photographers. Listings with video walkthroughs are given increased visibility on Zillow and only Premier Agents or Zillow Certified Photographers can post video!</p>
',
                'banner_image' => 'public/uploads/product_images/backyard-house-lights-1552072915.jpg',
                'overview_image' => 'public/uploads/product_images/architecture-brick-building-209315-1552072915.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '<iframe src="https://app.acuityscheduling.com/schedule.php?owner=15118665&appointmentType=category:Zillow+Walk-Thru+Video" width="100%" height="800" frameBorder="0"></iframe>																																  <script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>
',
                'seo_meta_id' => 27,
                'project_id' => 7,
                'is_active' => 1,
                'created_at' => '2019-03-08 11:21:55',
                'updated_at' => '2019-08-30 11:38:42',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'code' => '',
                'name' => 'test service',
                'slug' => 'test-service',
                'description' => '<p>test</p>
',
                'banner_image' => 'public/uploads/product_images/Our-Story-01-1563407522-1568440683.jpg',
                'overview_image' => 'public/uploads/product_images/Our-Story-body0image-01-1563472028-1568440683.jpg',
                'pricing_content' => '',
                'acuity_scheduling_link' => '',
                'seo_meta_id' => 35,
                'project_id' => 6,
                'is_active' => 0,
                'created_at' => '2019-09-13 22:58:03',
                'updated_at' => '2019-09-13 23:02:43',
                'deleted_at' => '2019-09-13 23:02:43',
            ),
        ));
        
        
    }
}