<?php

use Illuminate\Database\Seeder;

class WebhookLogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('webhook_logs')->delete();
        
        \DB::table('webhook_logs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'action' => 'scheduled',
                'appointment_id' => '268115468',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9194304',
                'created_at' => '2019-02-19 14:29:04',
                'updated_at' => '2019-02-19 14:29:04',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'action' => 'scheduled',
                'appointment_id' => '268147329',
                'calendar_id' => '1992960',
                'appointment_type_id' => '5573986',
                'created_at' => '2019-02-19 16:41:01',
                'updated_at' => '2019-02-19 16:41:01',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'action' => 'scheduled',
                'appointment_id' => '268150423',
                'calendar_id' => '1992960',
                'appointment_type_id' => '5573997',
                'created_at' => '2019-02-19 16:58:15',
                'updated_at' => '2019-02-19 16:58:15',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'action' => 'scheduled',
                'appointment_id' => '268150648',
                'calendar_id' => '1992960',
                'appointment_type_id' => '9194304',
                'created_at' => '2019-02-19 16:59:24',
                'updated_at' => '2019-02-19 16:59:24',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'action' => 'scheduled',
                'appointment_id' => '268155428',
                'calendar_id' => '1992960',
                'appointment_type_id' => '9194304',
                'created_at' => '2019-02-19 17:26:32',
                'updated_at' => '2019-02-19 17:26:32',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'action' => 'scheduled',
                'appointment_id' => '268651790',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9194304',
                'created_at' => '2019-02-21 12:38:59',
                'updated_at' => '2019-02-21 12:38:59',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'action' => 'scheduled',
                'appointment_id' => '268667632',
                'calendar_id' => '1992960',
                'appointment_type_id' => '9194304',
                'created_at' => '2019-02-21 13:33:05',
                'updated_at' => '2019-02-21 13:33:05',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'action' => 'scheduled',
                'appointment_id' => '271725483',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9332353',
                'created_at' => '2019-03-06 12:04:16',
                'updated_at' => '2019-03-06 12:04:16',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'action' => 'scheduled',
                'appointment_id' => '271841677',
                'calendar_id' => '1893260',
                'appointment_type_id' => '9268676',
                'created_at' => '2019-03-06 21:50:21',
                'updated_at' => '2019-03-06 21:50:21',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'action' => 'scheduled',
                'appointment_id' => '272219962',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9377124',
                'created_at' => '2019-03-08 10:20:30',
                'updated_at' => '2019-03-08 10:20:30',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'action' => 'scheduled',
                'appointment_id' => '272220233',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9331537',
                'created_at' => '2019-03-08 10:21:32',
                'updated_at' => '2019-03-08 10:21:32',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'action' => 'scheduled',
                'appointment_id' => '272221584',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9365244',
                'created_at' => '2019-03-08 10:26:19',
                'updated_at' => '2019-03-08 10:26:19',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'action' => 'scheduled',
                'appointment_id' => '272318080',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9377124',
                'created_at' => '2019-03-08 17:43:01',
                'updated_at' => '2019-03-08 17:43:01',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'action' => 'scheduled',
                'appointment_id' => '272722696',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9331539',
                'created_at' => '2019-03-11 10:29:33',
                'updated_at' => '2019-03-11 10:29:33',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'action' => 'scheduled',
                'appointment_id' => '272723261',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9331515',
                'created_at' => '2019-03-11 10:31:00',
                'updated_at' => '2019-03-11 10:31:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'action' => 'scheduled',
                'appointment_id' => '273667079',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9365095',
                'created_at' => '2019-03-14 19:25:10',
                'updated_at' => '2019-03-14 19:25:10',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'action' => 'scheduled',
                'appointment_id' => '276401439',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9332353',
                'created_at' => '2019-03-27 10:36:53',
                'updated_at' => '2019-03-27 10:36:53',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'action' => 'scheduled',
                'appointment_id' => '281456843',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9365133',
                'created_at' => '2019-04-18 14:32:33',
                'updated_at' => '2019-04-18 14:32:33',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'action' => 'scheduled',
                'appointment_id' => '301667309',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9707359',
                'created_at' => '2019-07-17 15:44:49',
                'updated_at' => '2019-07-17 15:44:49',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'action' => 'scheduled',
                'appointment_id' => '302178092',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9194304',
                'created_at' => '2019-07-19 16:36:25',
                'updated_at' => '2019-07-19 16:36:25',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'action' => 'scheduled',
                'appointment_id' => '311873847',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9365018',
                'created_at' => '2019-08-27 12:25:20',
                'updated_at' => '2019-08-27 12:25:20',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'action' => 'scheduled',
                'appointment_id' => '316553587',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9527363',
                'created_at' => '2019-09-13 19:12:29',
                'updated_at' => '2019-09-13 19:12:29',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'action' => 'scheduled',
                'appointment_id' => '316553850',
                'calendar_id' => '1893260',
                'appointment_type_id' => '9527363',
                'created_at' => '2019-09-13 19:13:38',
                'updated_at' => '2019-09-13 19:13:38',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}