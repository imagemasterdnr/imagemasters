<?php

use Illuminate\Database\Seeder;

class HomePageSlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_page_sliders')->delete();
        
        \DB::table('home_page_sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => '',
                'name' => '',
                'image' => 'public/uploads/home_page_slider_images/homepage-slider-01-1563405154.jpg',
                'description' => '',
                'is_active' => 1,
                'created_at' => '2019-02-06 16:08:20',
                'updated_at' => '2019-09-16 14:25:03',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => '',
                'name' => 'Real Estate Photography 2',
                'image' => 'public/uploads/home_page_slider_images/apartment-architecture-art-276724-1552001433.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
                'is_active' => 1,
                'created_at' => '2019-02-06 16:10:42',
                'updated_at' => '2019-07-17 16:34:59',
                'deleted_at' => '2019-07-17 16:34:59',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => '',
                'name' => 'banner 3',
                'image' => 'public/uploads/home_page_slider_images/interior_design_style_design_home_villa_living_room_68248_1920x1080-1552001400.jpg',
                'description' => '',
                'is_active' => 1,
                'created_at' => '2019-03-07 15:30:00',
                'updated_at' => '2019-07-17 16:35:02',
                'deleted_at' => '2019-07-17 16:35:02',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => '',
                'name' => 'TEST',
                'image' => 'public/uploads/home_page_slider_images/Our-Story-01-1563407522-1568441736.jpg',
                'description' => 'ASRSAR',
                'is_active' => 0,
                'created_at' => '2019-09-13 23:15:36',
                'updated_at' => '2019-09-13 23:17:14',
                'deleted_at' => '2019-09-13 23:17:14',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => '',
                'name' => 'Second Slide',
                'image' => 'public/uploads/home_page_slider_images/animals-02-hp-1568669066.jpg',
                'description' => 'This is a test THis s a link',
                'is_active' => 1,
                'created_at' => '2019-09-16 14:24:26',
                'updated_at' => '2019-09-16 14:26:16',
                'deleted_at' => '2019-09-16 14:26:16',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => '',
                'name' => '2',
                'image' => 'public/uploads/home_page_slider_images/WilshireBoulevard-1568703353.jpg',
                'description' => 'test',
                'is_active' => 0,
                'created_at' => '2019-09-16 23:55:53',
                'updated_at' => '2019-09-16 23:57:08',
                'deleted_at' => '2019-09-16 23:57:08',
            ),
        ));
        
        
    }
}