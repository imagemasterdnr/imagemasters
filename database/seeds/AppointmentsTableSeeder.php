<?php

use Illuminate\Database\Seeder;

class AppointmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('appointments')->delete();
        
        \DB::table('appointments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'appointment_id' => '268115468',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9194304',
                'user_id' => '6',
                'product_id' => '1',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-02-19 14:29:05',
                'updated_at' => '2019-02-25 18:51:41',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'appointment_id' => '268147329',
                'calendar_id' => '1992960',
                'appointment_type_id' => '5573986',
                'user_id' => '7',
                'product_id' => '0',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-02-19 16:41:02',
                'updated_at' => '2019-02-26 19:03:53',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'appointment_id' => '268150423',
                'calendar_id' => '1992960',
                'appointment_type_id' => '5573997',
                'user_id' => '6',
                'product_id' => '0',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-02-19 16:58:15',
                'updated_at' => '2019-02-19 16:58:15',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'appointment_id' => '268150648',
                'calendar_id' => '1992960',
                'appointment_type_id' => '9194304',
                'user_id' => '8',
                'product_id' => '1',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-02-19 16:59:25',
                'updated_at' => '2019-02-26 18:37:20',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'appointment_id' => '268155428',
                'calendar_id' => '1992960',
                'appointment_type_id' => '9194304',
                'user_id' => '6',
                'product_id' => '1',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-02-19 17:26:32',
                'updated_at' => '2019-02-26 18:51:55',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'appointment_id' => '268651790',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9194304',
                'user_id' => '8',
                'product_id' => '1',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-02-21 12:38:59',
                'updated_at' => '2019-02-25 18:59:36',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'appointment_id' => '268667632',
                'calendar_id' => '1992960',
                'appointment_type_id' => '9194304',
                'user_id' => '13',
                'product_id' => '1',
                'beds' => 'N/A',
                'baths' => 'N/A',
                'availability_status' => 'Available',
                'square_feet' => '1800',
                'address' => '4220 W Overlook Dr, San Diego CA 92115',
                'google_map_link' => '<div class="mapouter"><div class="gmap_canvas"><iframewidth="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=4220%20W%20Overlook%20Dr%2C%20San%20Diego%20CA%2092115&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div>',
                'video_link' => '',
                'is_branded' => 1,
                'status' => 0,
                'created_at' => '2019-02-21 13:33:05',
                'updated_at' => '2019-10-24 13:09:27',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'appointment_id' => '271725483',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9332353',
                'user_id' => '13',
                'product_id' => '5',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-06 12:04:17',
                'updated_at' => '2019-03-06 12:04:17',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'appointment_id' => '271841677',
                'calendar_id' => '1893260',
                'appointment_type_id' => '9268676',
                'user_id' => '6',
                'product_id' => '1',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-06 21:50:22',
                'updated_at' => '2019-03-06 21:50:22',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'appointment_id' => '272219962',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9377124',
                'user_id' => '8',
                'product_id' => '2',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-08 10:20:31',
                'updated_at' => '2019-03-08 10:20:31',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'appointment_id' => '272220233',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9331537',
                'user_id' => '15',
                'product_id' => '0',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-08 10:21:33',
                'updated_at' => '2019-03-08 10:21:33',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'appointment_id' => '272221584',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9365244',
                'user_id' => '8',
                'product_id' => '0',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-08 10:26:20',
                'updated_at' => '2019-03-08 10:26:20',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'appointment_id' => '272318080',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9377124',
                'user_id' => '13',
                'product_id' => '2',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-03-08 17:43:01',
                'updated_at' => '2019-03-08 19:21:19',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'appointment_id' => '272722696',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9331539',
                'user_id' => '8',
                'product_id' => '0',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-11 10:29:33',
                'updated_at' => '2019-03-11 10:29:33',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'appointment_id' => '272723261',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9331515',
                'user_id' => '13',
                'product_id' => '0',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-11 10:31:00',
                'updated_at' => '2019-03-11 10:31:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'appointment_id' => '273667079',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9365095',
                'user_id' => '6',
                'product_id' => '4',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-14 19:25:10',
                'updated_at' => '2019-03-14 19:25:10',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'appointment_id' => '276401439',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9332353',
                'user_id' => '22',
                'product_id' => '5',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-03-27 10:36:54',
                'updated_at' => '2019-03-27 10:36:54',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'appointment_id' => '281456843',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9365133',
                'user_id' => '24',
                'product_id' => '4',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-04-18 14:32:34',
                'updated_at' => '2019-04-18 14:32:34',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'appointment_id' => '301667309',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9707359',
                'user_id' => '24',
                'product_id' => '10',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-07-17 15:44:49',
                'updated_at' => '2019-07-17 15:44:49',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'appointment_id' => '302178092',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9194304',
                'user_id' => '26',
                'product_id' => '1',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 0,
                'created_at' => '2019-07-19 16:36:28',
                'updated_at' => '2019-07-19 16:36:28',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'appointment_id' => '311873847',
                'calendar_id' => '1893234',
                'appointment_type_id' => '9365018',
                'user_id' => '13',
                'product_id' => '4',
                'beds' => '3',
                'baths' => '3',
                'availability_status' => 'Available',
                'square_feet' => '1800',
                'address' => '4220 W Overlook Dr, San Diego CA 92115',
                'google_map_link' => '<div class="mapouter"><div class="gmap_canvas"><iframe width="1080" height="585" id="gmap_canvas" src="https://maps.google.com/maps?q=4220%20W%20Overlook%20Dr%2C%20San%20Diego%20CA%2092115&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:585px;width:1080px;}.gmap_canvas {overflow:hidden;background:none!important;height:585px;width:1080px;}</style></div>',
                'video_link' => '',
                'is_branded' => 1,
                'status' => 0,
                'created_at' => '2019-08-27 12:25:20',
                'updated_at' => '2019-10-24 13:26:44',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'appointment_id' => '316553587',
                'calendar_id' => '1992935',
                'appointment_type_id' => '9527363',
                'user_id' => '27',
                'product_id' => '6',
                'beds' => '',
                'baths' => '',
                'availability_status' => '',
                'square_feet' => '',
                'address' => '',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-09-13 19:12:29',
                'updated_at' => '2019-09-13 19:12:52',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'appointment_id' => '316553850',
                'calendar_id' => '1893260',
                'appointment_type_id' => '9527363',
                'user_id' => '27',
                'product_id' => '6',
                'beds' => '3',
                'baths' => '2',
                'availability_status' => 'ASAP',
                'square_feet' => '300',
                'address' => 'South Carolina',
                'google_map_link' => '',
                'video_link' => '',
                'is_branded' => 0,
                'status' => 2,
                'created_at' => '2019-09-13 19:13:38',
                'updated_at' => '2019-09-13 19:59:57',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}