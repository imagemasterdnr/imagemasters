<?php

use Illuminate\Database\Seeder;

class ProductCategoriesMatrixTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_categories_matrix')->delete();
        
        \DB::table('product_categories_matrix')->insert(array (
            0 => 
            array (
                'product_category_id' => 1,
                'product_id' => 1,
            ),
            1 => 
            array (
                'product_category_id' => 1,
                'product_id' => 3,
            ),
            2 => 
            array (
                'product_category_id' => 1,
                'product_id' => 4,
            ),
            3 => 
            array (
                'product_category_id' => 1,
                'product_id' => 5,
            ),
            4 => 
            array (
                'product_category_id' => 2,
                'product_id' => 9,
            ),
            5 => 
            array (
                'product_category_id' => 2,
                'product_id' => 10,
            ),
            6 => 
            array (
                'product_category_id' => 2,
                'product_id' => 11,
            ),
            7 => 
            array (
                'product_category_id' => 3,
                'product_id' => 6,
            ),
            8 => 
            array (
                'product_category_id' => 3,
                'product_id' => 4,
            ),
            9 => 
            array (
                'product_category_id' => 4,
                'product_id' => 2,
            ),
            10 => 
            array (
                'product_category_id' => 4,
                'product_id' => 7,
            ),
            11 => 
            array (
                'product_category_id' => 4,
                'product_id' => 8,
            ),
            12 => 
            array (
                'product_category_id' => 5,
                'product_id' => 12,
            ),
            13 => 
            array (
                'product_category_id' => 1,
                'product_id' => 12,
            ),
            14 => 
            array (
                'product_category_id' => 2,
                'product_id' => 12,
            ),
            15 => 
            array (
                'product_category_id' => 3,
                'product_id' => 12,
            ),
            16 => 
            array (
                'product_category_id' => 4,
                'product_id' => 12,
            ),
        ));
        
        
    }
}