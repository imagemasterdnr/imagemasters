<?php

use Illuminate\Database\Seeder;

class ProductImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_images')->delete();
        
        \DB::table('product_images')->insert(array (
            0 => 
            array (
                'id' => 6,
                'product_id' => 4,
                'name' => 'product_category-image1-1551379982.jpg',
                'file' => 'public/uploads/product_images/product_category-image1-1551379982.jpg',
                'size' => '130725',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-02-28 10:53:25',
                'updated_at' => '2019-02-28 10:53:25',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 10,
                'product_id' => 2,
                'name' => 'Zillow3D_hometour-1551385781.jpg',
                'file' => 'public/uploads/product_images/Zillow3D_hometour-1551385781.jpg',
                'size' => '151729',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-02-28 12:29:44',
                'updated_at' => '2019-02-28 12:29:44',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 11,
                'product_id' => 6,
                'name' => 'product_category-image1-1551488278.jpg',
                'file' => 'public/uploads/product_images/product_category-image1-1551488278.jpg',
                'size' => '130725',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-01 16:57:59',
                'updated_at' => '2019-03-01 16:57:59',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 12,
                'product_id' => 7,
                'name' => '2019-02-281-1551488438.jpg',
                'file' => 'public/uploads/product_images/2019-02-281-1551488438.jpg',
                'size' => '71199',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-01 17:00:39',
                'updated_at' => '2019-03-01 17:00:39',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 13,
                'product_id' => 8,
                'name' => '2019-02-282-1551488531.jpg',
                'file' => 'public/uploads/product_images/2019-02-282-1551488531.jpg',
                'size' => '172571',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-01 17:02:23',
                'updated_at' => '2019-03-01 17:02:23',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 14,
                'product_id' => 9,
                'name' => 'Videography-1551488635.jpg',
                'file' => 'public/uploads/product_images/Videography-1551488635.jpg',
                'size' => '223884',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-01 17:04:00',
                'updated_at' => '2019-03-01 17:04:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 15,
                'product_id' => 1,
                'name' => '2019-02-28-1551987140.jpg',
                'file' => 'public/uploads/product_images/2019-02-28-1551987140.jpg',
                'size' => '144433',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-07 11:32:21',
                'updated_at' => '2019-03-07 11:32:21',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 16,
                'product_id' => 3,
                'name' => 'Videography-1551987165.jpg',
                'file' => 'public/uploads/product_images/Videography-1551987165.jpg',
                'size' => '223884',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-07 11:32:47',
                'updated_at' => '2019-03-07 11:32:47',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 17,
                'product_id' => 5,
                'name' => 'PuakeaBay-015-1551987257.jpg',
                'file' => 'public/uploads/product_images/PuakeaBay-015-1551987257.jpg',
                'size' => '193496',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-07 11:34:19',
                'updated_at' => '2019-03-07 11:34:19',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}