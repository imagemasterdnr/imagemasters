<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Miko Suarez',
                'email' => 'programmer1@dogandrooster.com',
                'phone' => '2611475954',
                'company' => 'DNR',
                'subject' => 'test',
                'message' => 'test',
                'created_at' => '2019-02-26 12:09:18',
                'updated_at' => '2019-02-26 12:09:18',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Randall Anthony',
                'email' => 'programmer10@dogandrooster.com',
                'phone' => '8586779931',
                'company' => 'Dog and Rooster',
                'subject' => 'TEST CONTACT US',
                'message' => 'Sample message',
                'created_at' => '2019-03-08 19:34:47',
                'updated_at' => '2019-03-08 19:34:47',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Miko',
                'email' => 'programmer1@dogandrooster.com',
                'phone' => '2611475954',
                'company' => 'DNR',
                'subject' => 'Test',
                'message' => 'TEST',
                'created_at' => '2019-03-14 13:43:35',
                'updated_at' => '2019-03-14 13:43:35',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Miko Suarez',
                'email' => 'programmer1@dogandrooster.com',
                'phone' => '2611475954',
                'company' => 'DNR',
                'subject' => 'test',
                'message' => 'TESTING DNR',
                'created_at' => '2019-03-25 14:37:58',
                'updated_at' => '2019-03-25 14:37:58',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Genie',
                'email' => 'officeassist+500@dogandrooster.com',
                'phone' => '61941515335',
                'company' => '',
                'subject' => 'Test',
                'message' => 'this is a test from dog and rooster',
                'created_at' => '2019-10-04 21:40:05',
                'updated_at' => '2019-10-04 21:40:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}