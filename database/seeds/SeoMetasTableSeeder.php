<?php

use Illuminate\Database\Seeder;

class SeoMetasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('seo_metas')->delete();
        
        \DB::table('seo_metas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'meta_title' => 'Home',
                'meta_keywords' => 'Home',
                'meta_description' => 'Home',
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'meta_title' => 'About Us',
                'meta_keywords' => 'About Us',
                'meta_description' => 'About Us',
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'meta_title' => 'Contact Us',
                'meta_keywords' => 'Contact Us',
                'meta_description' => 'Contact Us',
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'meta_title' => 'Page Not Found',
                'meta_keywords' => 'Page Not Found',
                'meta_description' => 'Page Not Found',
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'meta_title' => 'Services',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-01-24 22:12:19',
                'updated_at' => '2019-07-12 11:03:26',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'meta_title' => 'Our Story',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-01-25 15:13:30',
                'updated_at' => '2019-07-12 11:01:18',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-01-29 07:06:24',
                'updated_at' => '2019-01-29 07:06:24',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-01-31 16:16:57',
                'updated_at' => '2019-01-31 16:16:57',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-01-31 16:18:55',
                'updated_at' => '2019-01-31 16:18:55',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-01-31 16:35:53',
                'updated_at' => '2019-01-31 16:35:53',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-08 18:38:34',
                'updated_at' => '2019-02-08 18:38:34',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-08 18:38:45',
                'updated_at' => '2019-02-08 18:38:45',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-08 18:39:02',
                'updated_at' => '2019-02-08 18:39:02',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-08 18:39:23',
                'updated_at' => '2019-02-08 18:39:23',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-08 18:39:23',
                'updated_at' => '2019-02-08 18:39:23',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-22 16:10:51',
                'updated_at' => '2019-02-22 16:10:51',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-22 16:15:47',
                'updated_at' => '2019-02-22 16:15:47',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-28 10:10:22',
                'updated_at' => '2019-02-28 10:10:22',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'meta_title' => 'Twilight/Sunset Photography',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-28 10:44:05',
                'updated_at' => '2019-07-12 12:14:38',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-28 10:53:25',
                'updated_at' => '2019-02-28 10:53:25',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'meta_title' => 'Premium Luxury Photography',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-02-28 10:59:01',
                'updated_at' => '2019-07-12 12:18:04',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-01 16:57:59',
                'updated_at' => '2019-03-01 16:57:59',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-01 17:00:39',
                'updated_at' => '2019-03-01 17:00:39',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-01 17:02:23',
                'updated_at' => '2019-03-01 17:02:23',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-01 17:04:00',
                'updated_at' => '2019-03-01 17:04:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'meta_title' => 'Premium Lifestyle Video',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-08 11:20:54',
                'updated_at' => '2019-07-12 12:22:25',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'meta_title' => 'Zillow Walk-Thru Video',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-08 11:21:55',
                'updated_at' => '2019-07-12 12:20:44',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-11 21:50:52',
                'updated_at' => '2019-03-11 21:50:52',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-19 18:34:36',
                'updated_at' => '2019-03-19 18:34:36',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-03-21 12:11:28',
                'updated_at' => '2019-03-21 12:11:28',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-08-01 15:06:55',
                'updated_at' => '2019-08-01 15:06:55',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-09-13 22:44:27',
                'updated_at' => '2019-09-13 22:44:27',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-09-13 22:48:18',
                'updated_at' => '2019-09-13 22:48:18',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-09-13 22:51:28',
                'updated_at' => '2019-09-13 22:51:28',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-09-13 22:58:03',
                'updated_at' => '2019-09-13 22:58:03',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'meta_title' => '',
                'meta_keywords' => '',
                'meta_description' => '',
                'created_at' => '2019-09-13 23:28:21',
                'updated_at' => '2019-09-13 23:28:21',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}