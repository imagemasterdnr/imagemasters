<?php

use Illuminate\Database\Seeder;

class SystemSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('system_settings')->delete();
        
        \DB::table('system_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'SS0001',
                'name' => 'Website Name',
                'value' => 'Image Masters',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'SS0002',
                'name' => 'Website Email',
                'value' => 'cynthia@imagemastersusa.com',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'SS0003',
                'name' => 'Website Phone',
                'value' => '619-464-0656',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'SS0004',
                'name' => 'Default Meta Title',
                'value' => 'Default Meta Title',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'SS0005',
                'name' => 'Default Meta Keywords',
                'value' => 'Default Meta Keywords',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'SS0006',
                'name' => 'Default Meta Description',
            'value' => 'Dog and Rooster Website. ACL Integrated (Access Control List).',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'SS0007',
                'name' => 'Mega Menu Image',
                'value' => 'public/uploads/mega-image.jpg',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'SS0008',
                'name' => 'Facebook Link',
                'value' => 'https://www.facebook.com/#',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2019-09-17 16:19:55',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'SS0009',
                'name' => 'Twitter Link',
                'value' => 'https://twitter.com/',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'SS0010',
                'name' => 'Pinterest Link',
                'value' => 'https://www.pinterest.com/',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'SS0011',
                'name' => 'Instagram Link',
                'value' => 'https://www.instagram.com/',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}