<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_categories')->delete();
        
        \DB::table('product_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'Photography',
                'name' => 'Photography',
                'slug' => 'photography',
                'description' => '<p>asdasd</p>
',
                'banner_image' => 'public/uploads/product_category_images/photography-01-1563408712.jpg',
                'banner_video' => '',
                'overview_image' => 'public/uploads/product_category_images/photography-overview-01-1563409905.jpg',
                'seo_meta_id' => 8,
                'is_active' => 1,
                'created_at' => '2019-01-31 16:16:58',
                'updated_at' => '2019-07-17 17:31:45',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'Videography',
                'name' => 'Videography',
                'slug' => 'videography',
                'description' => '<p>Videography</p>
',
                'banner_image' => 'public/uploads/product_category_images/videography-banner-01-1563472582.jpg',
                'banner_video' => 'public/uploads/product_category_images/videography-1567209443.mp4',
                'overview_image' => 'public/uploads/product_category_images/videography-overview-01-1563472582.jpg',
                'seo_meta_id' => 18,
                'is_active' => 1,
                'created_at' => '2019-02-28 10:10:22',
                'updated_at' => '2019-08-30 16:57:35',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'aerialanddronephotovideo',
                'name' => 'Aerial/Drone Photo & Video',
                'slug' => 'aerialdrone-photo-video',
                'description' => '<p>Aerial/Drone Photo &amp; Video</p>
',
                'banner_image' => 'public/uploads/product_category_images/aerial-banner-02-1563470175.jpg',
                'banner_video' => '',
                'overview_image' => 'public/uploads/product_category_images/aerial-overview-02-1563470176.jpg',
                'seo_meta_id' => 30,
                'is_active' => 1,
                'created_at' => '2019-03-21 12:11:28',
                'updated_at' => '2019-07-18 10:16:16',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => '3dproducts',
                'name' => '3D Products',
                'slug' => '3d-products',
                'description' => '<p>3D Products</p>
',
                'banner_image' => 'public/uploads/product_category_images/aerial-banner-02-1563470175-1564697215.jpg',
                'banner_video' => '',
                'overview_image' => 'public/uploads/product_category_images/aerial-overview-02-1563470176-1564697216.jpg',
                'seo_meta_id' => 31,
                'is_active' => 1,
                'created_at' => '2019-08-01 15:06:55',
                'updated_at' => '2019-08-01 15:06:56',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => '123',
                'name' => 'Service test',
                'slug' => 'service-test',
                'description' => '<p>testing</p>
',
                'banner_image' => 'public/uploads/product_category_images/Our-Story-01-1563407522-1568440288.jpg',
                'banner_video' => '',
                'overview_image' => 'public/uploads/product_category_images/Our-Story-body0image-01-1563472028-1568440288.jpg',
                'seo_meta_id' => 34,
                'is_active' => 1,
                'created_at' => '2019-09-13 22:51:28',
                'updated_at' => '2019-09-13 23:25:25',
                'deleted_at' => '2019-09-13 23:25:25',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'testing',
                'name' => 'testing',
                'slug' => 'testing',
                'description' => '<p>fsads</p>
',
                'banner_image' => 'public/uploads/product_category_images/Our-Story-01-1563407522-1568442501.jpg',
                'banner_video' => '',
                'overview_image' => 'public/uploads/product_category_images/Our-Story-body0image-01-1563472028-1568442501.jpg',
                'seo_meta_id' => 36,
                'is_active' => 1,
                'created_at' => '2019-09-13 23:28:21',
                'updated_at' => '2019-09-13 23:29:49',
                'deleted_at' => '2019-09-13 23:29:49',
            ),
        ));
        
        
    }
}