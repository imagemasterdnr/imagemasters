<?php

use Illuminate\Database\Seeder;

class PageSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('page_sections')->delete();
        
        \DB::table('page_sections')->insert(array (
            0 => 
            array (
                'id' => 1,
                'page_id' => 1,
                'section' => 'home_section_2_left_content',
                'content' => '<div class="article-main padding-right20">
<div class="article-main__wrapper">
<div class="article-main__label">Who we are</div>

<h2>Beautiful Photos &amp; Dedicated Service</h2>
<strong>Ease of scheduling, prompt arrival to appointments and product delivered.</strong></div>
</div>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-07-12 10:59:14',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'page_id' => 1,
                'section' => 'home_section_2_video_link',
                'content' => '<iframe width="460" height="260" src="https://www.youtube.com/embed/M7lc1UVf-VE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
                'type' => 'textarea',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-02-27 21:19:05',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'page_id' => 1,
                'section' => 'home_section_2_video_image_thumbnail',
                'content' => 'public/uploads/video.jpg',
                'type' => 'file',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-02-27 21:19:05',
                'deleted_at' => '2019-09-18 18:20:11',
            ),
            3 => 
            array (
                'id' => 4,
                'page_id' => 1,
                'section' => 'home_section_2_video_label',
                'content' => 'Featured Video: What We Offer',
                'type' => 'input',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-08-01 13:54:22',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'page_id' => 1,
                'section' => 'home_section_2_video_button_label',
                'content' => 'VIEW FULL LIST OF SERVICES',
                'type' => 'input',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-02-27 21:19:05',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'page_id' => 1,
                'section' => 'home_section_2_video_button_link',
                'content' => 'http://54.68.88.28/imagemasters/services',
                'type' => 'input',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-02-27 21:19:05',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'page_id' => 1,
                'section' => 'home_section_3_title',
                'content' => '<div class="heading-primary text-center">
<h2>Featured Projects</h2>

<p>We have formed strong relationships with industry leaders, &nbsp;affiliated organizations like Union Tribune and all local Realtor Associations.</p>
</div>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-08-01 13:52:34',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'page_id' => 1,
                'section' => 'home_section_4',
                'content' => '<div class="hightlight__wrapper hightlight__middle">
<div class="hightlight__item hightlight__item--left">
<div class="hightlight__item--image">
<div class="hightlight__item--image__holder"><img src="http://54.68.88.28/imagemasters/public/uploads/ckeditor/5d2faec18e0d11563406017.jpg" style="width: 942px; height: 655px;" /></div>
</div>
</div>

<div class="hightlight__item hightlight__item--right">
<div class="article-main padding-right30">
<div class="article-main__wrapper">
<div class="article-main__label">Services We Provide</div>

<h2>QUALITY<br />
IS IN THE<br />
DETAILS</h2>
<strong>With an increasing number of photographers entering the industry, competition is fierce. Image Masters has worked with thousands of real estate professionals, we know what is needed to produce high quality and at affordable prices because of our volume.</strong> <a class="btn primary-btn margin-top35" href="http://54.68.88.28/imagemasters/services"> View Services &amp; Pricing </a></div>
</div>
</div>
</div>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-07-17 16:27:03',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'page_id' => 3,
                'section' => 'contact_form_title',
                'content' => '<p>Kindly provide details below and we will promptly respond</p>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-10-01 19:29:40',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'page_id' => 3,
                'section' => 'content',
                'content' => '<div class="article-main padding-right20">
<div class="article-main__wrapper">
<div class="article-main__label">GET IN TOUCH</div>

<h2>WE would love to hear from you</h2>

<ul class="contact-list">
<li><strong>phone:</strong> 619-464-0656</li>
<li><strong>EMAIL : </strong> cynthia@imagemastersusa.com</li>
<li><strong>Serving The Following Areas: </strong> <span>San Diego County</span> <span>Temecula Region</span> <span>Los Angeles/Orange County for Commercial Projects</span></li>
<li><a class="btn btn--primary" href="http://54.68.88.28/imagemasters/public/uploads/ckeditor/5d8002522ba941568670290.jpg">Download Blank Form</a></li>
</ul>
</div>
</div>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-10-01 19:29:40',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'page_id' => 5,
                'section' => 'content',
                'content' => '<div class="row">
<div class="col-md-6 feature-main__item feature-main__item--label">
<div class="article-main padding-right20">
<div class="article-main__wrapper">
<div class="article-main__label">What we offer</div>

<h2>High quality, low cost</h2>
<strong>We provide real estate photography and video products starting at just $129 including daytime, twilight/sunset, premium luxury, aerial/drone, 360 tours and agent headshots.</strong></div>
</div>
</div>

<div class="col-md-6 feature-main__item feature-main__item--label">
<div class="{{--article-main padding-right20--}}">
<div class="article-main__wrapper">
<p>Because of our high volume, we can often accommodate same-day photo appointment requests and rush turnaround when you need those images quickly (extra cost for same- day rush).</p>

<p>We also offer commercial photography for businesses, rental communities, retail and hospitality</p>

<p>All levels of product for your marketing needs</p>
</div>
</div>
</div>
</div>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-07-12 11:05:14',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'page_id' => 6,
                'section' => 'video_vimeo_link',
                'content' => 'https://vimeo.com/229490822',
                'type' => 'textarea',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-03-07 15:44:04',
                'deleted_at' => '2019-09-13 17:31:36',
            ),
            12 => 
            array (
                'id' => 13,
                'page_id' => 6,
                'section' => 'left_image',
                'content' => 'public/uploads/page_section_images/Our-Story-body0image-01-156347-1568435125.jpg',
                'type' => 'file',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-09-13 21:25:25',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'page_id' => 6,
                'section' => 'right_content',
                'content' => '<div class="article-main padding-right25">
<div class="article-main__wrapper font-third">
<div class="article-main__label">philosophy</div>

<h2>ESTABLISHED<br />
IN 1991</h2>
<strong>IMAGE MASTERS IS ONE OF THE LARGEST REAL ESTATE PHOTOGRAPHY COMPANIES IN SAN DIEGO.</strong>

<p>Established in 1991, Image Masters is a talented crew of artists and San Diego County\'s original real estate photography company...... providing images for over 100,000 homes.</p>

<p>Beautiful photos inevitably come from fine craftsmanship. We’ve poured years of cultivation into our art so that you can experience quality images and video. Most of our staff has been with us for years and our teamwork is evident in all stages of the process.</p>

<p>Our approach has always been reliable and honest service. We are fortunate to have formed strong relationships with industry leaders, affiliated organizations like Union Tribune and most local real estate brokerages, which has only sustained our commitment. We stand by our work.</p>

<p>Although the company has grown in size it remains a very hands- on labor of love for owner, Cynthia Hays. She is directly involved in much of the daily business operations. “We never want to get to a point where we aren’t in touch with the things that made us want to do this in the first place”. Her fascination with how people interact with architecture and that an artfully captured photo can leave them wanting more inspires the subtle details in how Image Masters produces their work.</p>

<p>We love what we do.</p>
<a class="btn primary-btn margin-top35" href="http://54.68.88.28/imagemasters/services"> View Services &amp; Pricing </a></div>
</div>
',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-09-16 14:40:18',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'page_id' => 13,
                'section' => 'acuity_scheduling_link',
                'content' => '<iframe frameborder="0" height="800" src="https://app.acuityscheduling.com/schedule.php?owner=15118665" width="100%"></iframe><script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script>',
                'type' => 'textarea',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-03-07 15:44:04',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'page_id' => 14,
                'section' => 'content',
                'content' => '<p>&nbsp;</p><p>Lorem ipsum dolor sit amet, vitae conceptam honestatis vis eu, ex qui atqui molestie consequuntur, ubique dolorum ancillae te eum. Id veri assum similique per, et qui iusto volumus alienum, his ex sumo tibique mandamus. An laoreet impedit facilis quo, quis esse praesent quo ut. Regione legendos in usu, minimum verterem ut pro, in sed prima essent.</p><p>Verterem vituperata complectitur an vix, ad summo habemus pertinacia has, adhuc graeci vim in. Te lorem appareat laboramus ius, mei animal feugait ea, ea tincidunt instructior mel. Ne lobortis nominati delicatissimi his. Ei deserunt dissentiet eos. Ex abhorreant incorrupte eos, malis zril legere his ea, no accusam insolens eam.</p><p>Pro tale minimum lobortis in, usu ut vero expetendis instructior. Verear facilisi sed ad. Veritus efficiendi disputando vis an. Cu atqui eloquentiam has, nam an idque forensibus.</p><p>Cum dicam corpora consetetur ei. Eleifend suavitate iudicabit id eum. Pri stet mutat interesset et, oporteat maluisset definiebas sed in. Et habeo deserunt philosophia pro, eam ne option commune.</p><p>Et denique salutatus ius, sed ut commodo verterem ullamcorper. Sensibus perpetua ius eu. Ornatus antiopam no nec. Pri ex legendos pertinax accusamus, nulla viris scaevola vel an, eu per posse essent similique. Vel cu nominavi adipiscing, dicam fabellas est an, cu perfecto tacimates democritum eos. Soluta dissentias est eu, mel lorem quaerendum an.</p><p>No utinam efficiantur his. Tota tractatos moderatius ut mel. Nibh veri ancillae duo cu. Eu cum aeterno postulant vulputate, nec doming sapientem id, legere iriure detraxit ex vim. Vis id labitur euripidis, vix postulant corrumpit et, copiosae senserit cum eu.</p><p>Scaevola honestatis nec et. Cu causae vivendum deterruisset eam, et nam vero feugait, his aliquip eleifend eu. Esse apeirian te vel, ut option philosophia nam. An vis voluptua definiebas, quod labore ex sea, no usu menandri persequeris. Has ei legimus ocurreret, sit et sensibus consequat efficiantur, eu pri exerci labitur. Eum in vide sale signiferumque.</p><p>Menandri urbanitas ei usu. Atomorum imperdiet forensibus vix et. Habeo nulla nec ea. Dissentias deterruisset at mel, sit in error torquatos. Nec ad cibo malis recusabo. Nominati antiopam ne mel, duis putant labitur usu an, labitur omittam commune has cu.</p><p>Pro iusto legere omnium at, detracto ocurreret sit ex, id eum omittam tractatos. Ius an pertinax erroribus corrumpit, debet inermis scribentur eu mel. Eu nostrud elaboraret est. Ut facer conceptam disputationi per, modo molestie efficiendi et sed. Vix et nonumes volutpat facilisis, ne cum idque copiosae. Sea at causae vidisse, eam at dolore latine alterum.</p><p>Cum an vocent eruditi sensibus, nam an putent habemus. Quis omnesque expetendis in pro, eum omnesque verterem salutandi eu. Delectus urbanitas cum an, cu eum quis cetero similique. Quas partem vim in.</p><p>Vel ut option sadipscing. Modus facete est cu, vel oporteat sententiae ut, eu utinam appellantur eum. Est aperiam sanctus ut, te posse tantas vim, ad omittam intellegebat ius. Hinc iuvaret forensibus mel id. Ne nostrum accusamus democritum vim, vis ex elitr nullam perfecto. Tamquam moderatius ad pro, harum appareat usu cu. Esse omnium vituperata no has.</p>',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-03-07 15:44:04',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'page_id' => 12,
                'section' => 'dashboard_support_content',
                'content' => '<div class="article-main"><ul class="contact-list"><li><strong>phone:</strong> 619-464-0656</li><li><strong>EMAIL : </strong> cynthia@imagemastersusa.com</li><li><strong>Serving The Following Areas: </strong> <span>San Diego</span> <span>Los Angelas</span> <span>Ramona</span> <span>Escondido</span></li></ul></div>',
                'type' => 'ckeditor',
                'created_at' => '2019-02-09 09:09:39',
                'updated_at' => '2019-03-07 15:44:04',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}