<?php

use Illuminate\Database\Seeder;

class PermissionGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_groups')->delete();
        
        \DB::table('permission_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Roles',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Permissions',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Permission Groups',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Users',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'System Settings',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Pages',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Posts',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Product Categories',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Products',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Projects',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Contacts',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Subscriptions',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Home Page Sliders',
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}