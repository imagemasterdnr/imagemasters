<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionGroupsTableSeeder::class);
        $this->call(UserHasRolesTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(SystemSettingsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PageTypesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(SeoMetasTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);
        $this->call(ProductCategoriesMatrixTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(HomePageSlidersTableSeeder::class);
        $this->call(PageSectionsTableSeeder::class);
        $this->call(ProductCategoryImagesTableSeeder::class);
        $this->call(ProductImagesTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->call(AppointmentAttachmentsTableSeeder::class);
        $this->call(AppointmentsTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(ProjectImagesTableSeeder::class);
        $this->call(ProjectVideosTableSeeder::class);
        $this->call(SubscriptionsTableSeeder::class);
        $this->call(UserAddressDetailsTableSeeder::class);
        $this->call(UserHasPermissionsTableSeeder::class);
        $this->call(WebhookLogsTableSeeder::class);
    }
}
