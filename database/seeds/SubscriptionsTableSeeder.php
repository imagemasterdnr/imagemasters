<?php

use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('subscriptions')->delete();
        
        \DB::table('subscriptions')->insert(array (
            0 => 
            array (
                'id' => 2,
                'first_name' => 'Randall',
                'last_name' => 'Anthony',
                'email' => 'programmer10@dogandrooster.com',
                'created_at' => '2019-03-06 14:41:31',
                'updated_at' => '2019-03-06 14:41:31',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 3,
                'first_name' => 'TEST',
                'last_name' => 'ONLY',
                'email' => 'test1@dogandrooster.com',
                'created_at' => '2019-03-08 19:43:04',
                'updated_at' => '2019-03-08 19:43:04',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'first_name' => 'Miko ',
                'last_name' => 'Suarez',
                'email' => 'programmer1@dogandrooster.com',
                'created_at' => '2019-03-14 13:46:07',
                'updated_at' => '2019-03-14 13:46:07',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'first_name' => 'Vincent ',
                'last_name' => 'Mcmahon',
                'email' => 'officeassist+200@dogandrooster.com',
                'created_at' => '2019-09-13 19:53:46',
                'updated_at' => '2019-09-13 19:53:46',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}