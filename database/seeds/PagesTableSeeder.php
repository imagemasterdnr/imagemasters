<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Home',
                'slug' => 'home',
                'content' => '',
                'banner_description' => '',
                'banner_image' => '',
                'is_active' => 1,
                'seo_meta_id' => 1,
                'page_type_id' => 1,
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'About Us',
                'slug' => 'about-us',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner.jpg',
                'is_active' => 1,
                'seo_meta_id' => 2,
                'page_type_id' => 2,
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => '2019-01-24 20:31:45',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Contact Us',
                'slug' => 'contact-us',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/Contact-Us-01-1563407613.jpg',
                'is_active' => 1,
                'seo_meta_id' => 3,
                'page_type_id' => 3,
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-07-17 16:53:33',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Page Not Found',
                'slug' => '404',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner.jpg',
                'is_active' => 1,
                'seo_meta_id' => 4,
                'page_type_id' => 2,
                'created_at' => '2019-01-24 20:31:45',
                'updated_at' => '2019-01-24 20:31:45',
                'deleted_at' => '2019-01-24 20:31:45',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Services',
                'slug' => 'services',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/services-01-1563407301.jpg',
                'is_active' => 1,
                'seo_meta_id' => 5,
                'page_type_id' => 2,
                'created_at' => '2019-01-24 22:12:19',
                'updated_at' => '2019-07-17 16:48:22',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'our-history',
                'slug' => 'our-history',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/Our-Story-01-1563407522-1568435125.jpg',
                'is_active' => 1,
                'seo_meta_id' => 6,
                'page_type_id' => 0,
                'created_at' => '2019-01-25 15:13:30',
                'updated_at' => '2019-09-13 21:25:25',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'projects',
                'slug' => 'projects',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/projects-01-1563407495.jpg',
                'is_active' => 1,
                'seo_meta_id' => 7,
                'page_type_id' => 0,
                'created_at' => '2019-01-29 07:06:24',
                'updated_at' => '2019-07-17 16:51:35',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Login',
                'slug' => 'customer/login',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/login-register-01-1563472778.jpg',
                'is_active' => 1,
                'seo_meta_id' => 11,
                'page_type_id' => 0,
                'created_at' => '2019-02-08 18:38:34',
                'updated_at' => '2019-07-18 10:59:38',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Register',
                'slug' => 'customer/register',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/login-register-01-1563472797.jpg',
                'is_active' => 1,
                'seo_meta_id' => 12,
                'page_type_id' => 0,
                'created_at' => '2019-02-08 18:38:45',
                'updated_at' => '2019-07-18 10:59:57',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Forgot Password',
                'slug' => 'customer/password/email',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner.jpg',
                'is_active' => 1,
                'seo_meta_id' => 13,
                'page_type_id' => 0,
                'created_at' => '2019-02-08 18:39:02',
                'updated_at' => '2019-02-08 18:39:02',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Reset Password',
                'slug' => 'customer/password/reset',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner.jpg',
                'is_active' => 1,
                'seo_meta_id' => 14,
                'page_type_id' => 0,
                'created_at' => '2019-02-08 18:39:23',
                'updated_at' => '2019-02-08 18:39:23',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Dashboard',
                'slug' => 'customer/dashboard',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner.jpg',
                'is_active' => 1,
                'seo_meta_id' => 15,
                'page_type_id' => 0,
                'created_at' => '2019-02-08 18:39:23',
                'updated_at' => '2019-02-08 18:39:23',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Schedule Now',
                'slug' => 'schedule-now',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/schedule-01-1563407851.jpg',
                'is_active' => 1,
                'seo_meta_id' => 16,
                'page_type_id' => 0,
                'created_at' => '2019-02-22 16:10:51',
                'updated_at' => '2019-07-17 16:57:31',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Privacy Policy',
                'slug' => 'privacy-policy',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/backyard-house-lights-1552325888.jpg',
                'is_active' => 1,
                'seo_meta_id' => 17,
                'page_type_id' => 0,
                'created_at' => '2019-02-22 16:15:47',
                'updated_at' => '2019-03-11 10:39:19',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'order-details1',
                'slug' => 'order-details1',
                'content' => '',
                'banner_description' => '<p>test</p>
',
                'banner_image' => '',
                'is_active' => 1,
                'seo_meta_id' => 28,
                'page_type_id' => 0,
                'created_at' => '2019-03-11 21:50:52',
                'updated_at' => '2019-03-11 21:50:52',
                'deleted_at' => '2019-03-11 10:39:19',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Search',
                'slug' => 'search',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner-projects-1553045676.jpg',
                'is_active' => 1,
                'seo_meta_id' => 29,
                'page_type_id' => 0,
                'created_at' => '2019-03-19 18:34:36',
                'updated_at' => '2019-03-19 18:34:37',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Tour Preview',
                'slug' => 'tour_preview',
                'content' => '',
                'banner_description' => '',
                'banner_image' => 'public/uploads/banner_images/sub-banner-projects-1553045676.jpg',
                'is_active' => 1,
                'seo_meta_id' => 0,
                'page_type_id' => 0,
                'created_at' => '2019-03-19 18:34:36',
                'updated_at' => '2019-03-19 18:34:37',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'test page',
                'slug' => 'test-page',
                'content' => '<p>test lorem</p>
',
                'banner_description' => '<p>test banner</p>
',
                'banner_image' => 'public/uploads/banner_images/Our-Story-01-1563407522-1568439867.jpg',
                'is_active' => 1,
                'seo_meta_id' => 32,
                'page_type_id' => 0,
                'created_at' => '2019-09-13 22:44:27',
                'updated_at' => '2019-09-13 22:46:44',
                'deleted_at' => '2019-09-13 22:46:44',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'testing',
                'slug' => 'testing',
                'content' => '<p>lorem ipsum dolor kismet&nbsp;lorem ipsum dolor kismetlorem ipsum dolor kismetlorem ipsum dolor kismetlorem ipsum dolor kismet</p>
',
                'banner_description' => '<p>lorem ipsum dolor kismet&nbsp;lorem ipsum dolor kismetlorem ipsum dolor kismetlorem ipsum dolor kismetlorem ipsum dolor kismet</p>
',
                'banner_image' => 'public/uploads/banner_images/Our-Story-01-1563407522-1568440098.jpg',
                'is_active' => 1,
                'seo_meta_id' => 33,
                'page_type_id' => 0,
                'created_at' => '2019-09-13 22:48:18',
                'updated_at' => '2019-09-13 22:50:15',
                'deleted_at' => '2019-09-13 22:50:15',
            ),
        ));
        
        
    }
}