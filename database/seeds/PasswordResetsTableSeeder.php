<?php

use Illuminate\Database\Seeder;

class PasswordResetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('password_resets')->delete();
        
        \DB::table('password_resets')->insert(array (
            0 => 
            array (
                'email' => 'officeassist+200@dogandrooster.com',
                'token' => '$2y$10$gErLF5Neg4X3dbfrRtg/luosTxL45JR9ivnsuu9kcvCd3QXLukQTa',
                'created_at' => '2019-10-01 18:55:35',
            ),
        ));
        
        
    }
}