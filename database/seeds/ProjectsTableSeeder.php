<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('projects')->delete();
        
        \DB::table('projects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => '',
                'name' => 'Del Mar Hotel',
                'slug' => 'del-mar-hotel',
                'image' => 'public/uploads/project_images/services1.jpg',
                'description' => 'Aerial / 360',
                'vimeo_link' => 'https://vimeo.com/229490822',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-01-31 20:41:40',
                'updated_at' => '2019-08-30 11:28:37',
                'deleted_at' => '2019-08-30 11:28:37',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => '',
                'name' => 'Pinnacle Living at the Marina District',
                'slug' => 'pinnacle-living-at-the-marina-district',
                'image' => 'public/uploads/project_images/services2.jpg',
                'description' => '360',
                'vimeo_link' => 'https://vimeo.com/6370469',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-01-31 20:58:56',
                'updated_at' => '2019-08-30 11:28:41',
                'deleted_at' => '2019-08-30 11:28:41',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => '',
                'name' => '2149 Avenir Drive',
                'slug' => '2149-avenir-drive',
                'image' => 'public/uploads/project_images/services3-1552103860.jpg',
                'description' => '360 / Daylight',
                'vimeo_link' => 'https://vimeo.com/253989945',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-03-08 19:57:40',
                'updated_at' => '2019-08-30 11:28:43',
                'deleted_at' => '2019-08-30 11:28:43',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => '',
                'name' => 'Testing',
                'slug' => 'testing',
                'image' => 'public/uploads/project_images/bookingicon-1552596881.JPG',
                'description' => 'Project Testing',
                'vimeo_link' => 'https://vimeo.com/6370469',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-03-14 13:54:41',
                'updated_at' => '2019-03-14 13:56:27',
                'deleted_at' => '2019-03-14 13:56:27',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => '',
                'name' => 'WALK-THRU VIDEO',
                'slug' => 'walk-thru-video',
                'image' => 'public/uploads/project_images/project1-1567189579.JPG',
                'description' => 'WALK-THRU VIDEO',
                'vimeo_link' => 'https://vimeo.com/334925479 ',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-08-30 11:26:19',
                'updated_at' => '2019-09-27 11:55:29',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => '',
                'name' => 'PREMIUM LIFESTYLE VIDEO',
                'slug' => 'premium-lifestyle-video',
                'image' => 'public/uploads/project_images/project2-1567189611.JPG',
                'description' => 'PREMIUM LIFESTYLE VIDEO',
                'vimeo_link' => 'https://vimeo.com/329693166',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-08-30 11:26:51',
                'updated_at' => '2019-08-30 11:26:51',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => '',
                'name' => 'AERIAL VIDEO',
                'slug' => 'aerial-video',
                'image' => 'public/uploads/project_images/project3-1567189691.JPG',
                'description' => 'AERIAL VIDEO',
                'vimeo_link' => 'https://vimeo.com/179835651',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-08-30 11:28:11',
                'updated_at' => '2019-08-30 11:28:12',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => '',
                'name' => 'TEST PROJECT',
                'slug' => 'test-project',
                'image' => 'public/uploads/project_images/Our-Story-body0image-01-1563472028-1568441237.jpg',
                'description' => 'TEST DESCRIPTIONXV',
                'vimeo_link' => 'https://vimeo.com/329693166',
                'seo_meta_id' => 0,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-09-13 23:07:17',
                'updated_at' => '2019-09-13 23:11:15',
                'deleted_at' => '2019-09-13 23:11:15',
            ),
        ));
        
        
    }
}