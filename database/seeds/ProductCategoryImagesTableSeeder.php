<?php

use Illuminate\Database\Seeder;

class ProductCategoryImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_category_images')->delete();
        
        \DB::table('product_category_images')->insert(array (
            0 => 
            array (
                'id' => 4,
                'product_category_id' => 1,
                'name' => '2019-02-28-1551488329.jpg',
                'file' => 'public/uploads/product_category_images/2019-02-28-1551488329.jpg',
                'size' => '144433',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-01 16:58:52',
                'updated_at' => '2019-03-01 16:58:52',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'product_category_id' => 2,
                'name' => 'Videography-1551377420.jpg',
                'file' => 'public/uploads/product_category_images/Videography-1551377420.jpg',
                'size' => '223884',
                'is_default' => 0,
                'type' => 0,
                'created_at' => '2019-03-01 17:06:34',
                'updated_at' => '2019-03-01 17:06:34',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}