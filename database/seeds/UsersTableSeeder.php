<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'status' => 0,
                'type' => 0,
                'email' => 'programmer10@dogandrooster.com',
                'phone' => '',
                'user_name' => 'programmer10',
                'password' => '$2y$10$MeFrmdDdbCcPt/weQgAtEeHTYaXzFT/6xFJU7qtqcesBwqnxNZsVa',
                'first_name' => 'Programmer',
                'middle_name' => '',
                'last_name' => 'Ten',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2018-03-01 13:33:36',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-03-01 13:14:04',
                'updated_at' => '2018-03-01 13:38:29',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'status' => 0,
                'type' => 0,
                'email' => 'test1@dogandrooster.net',
                'phone' => '',
                'user_name' => 'webmaster',
                'password' => '$2y$10$a0tj40FzHq8rnRrwRryn8ePtPp7YaVYX7z9cC/LEB02s2MU1U5E/C',
                'first_name' => 'Webmaster',
                'middle_name' => '',
                'last_name' => 'Webmaster',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-10-14 04:00:59',
                'token' => NULL,
                'remember_token' => 'u02KhqJpGBBDVtRz5Hh9C4Eb0Bi1iBRwj1GtPg4ZmbMGQciQykIbMXsXPpuM',
                'created_at' => '2018-03-01 13:14:04',
                'updated_at' => '2019-10-14 04:00:59',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'status' => 0,
                'type' => 0,
                'email' => 'admin_user@dogandrooster.com',
                'phone' => '',
                'user_name' => 'admin_user',
                'password' => '$2y$10$PCgLgtZDgqVirBtbTUBnkuh71XLmyPhD6BkVRzDfS3qY/APKBPEFq',
                'first_name' => 'Admin',
                'middle_name' => '',
                'last_name' => 'One',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2018-03-01 13:35:39',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-03-01 13:14:04',
                'updated_at' => '2018-03-01 13:35:39',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'status' => 0,
                'type' => 0,
                'email' => 'photographer_user@dogandrooster.com',
                'phone' => '',
                'user_name' => 'photographer_user',
                'password' => '$2y$10$jocg95ExUYOLoAgk7VT5hekjI2ro3EetXQxJcyF7SLxidKgTzFYzS',
                'first_name' => 'Photohrapher',
                'middle_name' => '',
                'last_name' => 'One',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '1893234',
                'is_active' => 1,
                'last_login' => '2019-08-01 16:14:34',
                'token' => NULL,
                'remember_token' => '9gObHkzJIepkLN5cphCZ3SlKf7FD7SICpyylbCZfYQcVJZkwFkf60Vd676lT',
                'created_at' => '2018-03-01 13:14:04',
                'updated_at' => '2019-08-01 16:14:34',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'status' => 0,
                'type' => 1,
                'email' => 'customer_user@dogandrooster.com',
                'phone' => '',
                'user_name' => 'customer_user',
                'password' => '$2y$10$dRLdTwcJWQj678CqNImRSOyYDrAZL5V2xH25dd4AGaFUNir/PMFj6',
                'first_name' => 'Customer',
                'middle_name' => '',
                'last_name' => 'One',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-05-08 16:19:23',
                'token' => NULL,
                'remember_token' => 'nOizlb6roPFdY77T3QvfMo2ii5V4YICSVbj3yrgb8EHmBT5R7pxAkgWLjFfv',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2019-05-08 16:19:23',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer10@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$uWpCmDtPMaPICZWNkB3k8u/NUfkNs43bLCN5UdAmhPobn7vF5wpe2',
                'first_name' => 'Customer',
                'middle_name' => '',
                'last_name' => 'Two',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-07-17 22:45:46',
                'token' => NULL,
                'remember_token' => 'LrXW2PIhmIL5pSSA1CidET5ywjioqBrZUuMNybA7afX6xNAQSt7EVQYPEdVO',
                'created_at' => '2019-02-19 14:29:04',
                'updated_at' => '2019-07-17 22:45:46',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer10@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$fFSD8QJ/u/rBrj/t592hsOAfmhbpG7X7Ep23RSH8A1xslTVjz9fpG',
                'first_name' => 'Customer',
                'middle_name' => '',
                'last_name' => 'Three',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-02-20 00:41:02',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-02-19 16:41:02',
                'updated_at' => '2019-02-19 16:41:02',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer1@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$sqkhn6HyeNPrKl.BYY2hFelUR09UWL0AoudOfZEw1C1CUMMViJPyC',
                'first_name' => 'Miko',
                'middle_name' => '',
                'last_name' => 'Suarez',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-08 20:22:34',
                'token' => NULL,
                'remember_token' => '4FMUACcgDWda2KTVUzvh6moC84MgUsShOwbYWFFcf7x6ko0uDfFGr1LCtFVC',
                'created_at' => '2019-02-19 16:59:25',
                'updated_at' => '2019-03-08 20:22:34',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'status' => 0,
                'type' => 0,
                'email' => 'test123@dogandrooster.net',
                'phone' => '',
                'user_name' => 'cynthia_hays',
                'password' => '$2y$10$uwTwF1TdF5fUrcME.8Xw6e8mRkko3BI/Ub949CLQ3wVb3mJcuBz2q',
                'first_name' => 'Cynthia',
                'middle_name' => '',
                'last_name' => 'Hayes',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '1893234',
                'is_active' => 1,
                'last_login' => '2019-10-14 04:01:24',
                'token' => NULL,
                'remember_token' => '5rpQxVY8Daoa8AWJ1FfN0V29HmFcewMwoUy6Dht5R0FGbbylxRICSoHTUylj',
                'created_at' => '2019-02-19 17:02:13',
                'updated_at' => '2019-10-14 04:01:24',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'status' => 0,
                'type' => 0,
                'email' => 'test123+1@dogandrooster.net',
                'phone' => '',
                'user_name' => 'dave_murphy',
                'password' => '$2y$10$nD/SMD4VmMGk39xaVVBQbeZViz6LpdD9TyOrDYCpCuTjwAPGVAoLy',
                'first_name' => 'Dave',
                'middle_name' => '',
                'last_name' => 'Murphy',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '1992935',
                'is_active' => 1,
                'last_login' => '2019-02-21 20:22:44',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-02-21 12:22:44',
                'updated_at' => '2019-02-21 12:25:06',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'status' => 0,
                'type' => 0,
                'email' => 'test123+2@dogandrooster.net',
                'phone' => '',
                'user_name' => 'jin_cho',
                'password' => '$2y$10$xWiffIu8njSqztko7EyXYebeEj8gdLeN6dSIqyHw8sLydi4O4WyUe',
                'first_name' => 'Jin',
                'middle_name' => '',
                'last_name' => 'Cho',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '1992960',
                'is_active' => 1,
                'last_login' => '2019-03-08 20:31:56',
                'token' => NULL,
                'remember_token' => 'jKYm6RXTlGMwjd5hEFm58eVjMSUoAKuSSLJ7JhFr4opVEb4wpMyT1v8NjQAz',
                'created_at' => '2019-02-21 12:23:44',
                'updated_at' => '2019-03-08 20:31:56',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'status' => 0,
                'type' => 0,
                'email' => 'test123+3@dogandrooster.net',
                'phone' => '',
                'user_name' => 'kaleb_tholen',
                'password' => '$2y$10$X4atpWxrTdxO/6gZhHZ/8.zZJhSyb92xAXTODJWJDL3Bje46E2EtK',
                'first_name' => 'Kaleb',
                'middle_name' => '',
                'last_name' => 'Tholen',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '1893260',
                'is_active' => 1,
                'last_login' => '2019-02-21 20:24:31',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-02-21 12:24:31',
                'updated_at' => '2019-02-21 12:25:27',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer1@dogandrooster.com',
                'phone' => '8586779931',
                'user_name' => '',
                'password' => '$2y$10$sqkhn6HyeNPrKl.BYY2hFelUR09UWL0AoudOfZEw1C1CUMMViJPyC',
                'first_name' => 'DNRMiko',
                'middle_name' => '',
                'last_name' => 'DNRSuarez',
                'position' => 'Agent',
                'address' => '4220 W Overlook Dr, San Diego CA 92115',
                'license_no' => '00123456',
                'website' => 'dogandrooster.com',
                'company_logo' => 'public/uploads/user_company_logos/images-1566536301.jpg',
                'profile_image' => 'public/uploads/user_profile_images/750-1936-pug-life-ebay-1566536301.jpg',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-10-24 13:02:29',
                'token' => NULL,
                'remember_token' => 'QIq2UcYUjKyvlKhAx3v8RKTwlm6LIRQab6KvTo60dATbJ2CRbcFT806Nnn4a',
                'created_at' => '2019-02-21 12:30:27',
                'updated_at' => '2019-10-24 13:02:29',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer1+2@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$EnqGlcUtKOW0Ce1UVRGE7e1S78g/.AIQaNwRP/CzWNCxrQMAktERm',
                'first_name' => 'dnr',
                'middle_name' => '',
                'last_name' => 'miko',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-05 19:13:36',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-03-05 11:13:36',
                'updated_at' => '2019-03-05 11:13:36',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'status' => 0,
                'type' => 1,
                'email' => 'asdasd@gmail.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$Vq5bMl07/VcEj9Bn/kmwSOKHQaQ/n4eM1cwoldH5dZbYnx.C114xq',
                'first_name' => 'asdasdasdasdasd',
                'middle_name' => '',
                'last_name' => 'asadsad',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-08 18:21:32',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-03-08 10:21:32',
                'updated_at' => '2019-03-08 10:21:32',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'status' => 0,
                'type' => 0,
                'email' => 'test@dogandrooster.com',
                'phone' => '',
                'user_name' => 'dnr_test',
                'password' => '$2y$10$3ZXwU45Rf5lJPiAXXAwKy.RxPOoTMpRcbbJ7pU7KIC5rWA6lBwOpe',
                'first_name' => 'TEST',
                'middle_name' => '',
                'last_name' => 'ONLY',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-09 01:14:57',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-03-08 17:14:57',
                'updated_at' => '2019-03-08 17:14:57',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer1+1@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$hhu1yMvBYJ0S1ckKNRx88el09QmU4ahPFVbVIksDrYkMlvvn7FlL6',
                'first_name' => 'RMiko',
                'middle_name' => '',
                'last_name' => 'NSuarez',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-14 21:40:04',
                'token' => NULL,
                'remember_token' => 'ibtsZvwN6f21lrvtL0FPF7zJsLGhlzZ8ALkya1Hb6DUpgnNqw5Zv1nXUs7k9',
                'created_at' => '2019-03-14 14:40:04',
                'updated_at' => '2019-03-14 14:40:04',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'status' => 0,
                'type' => 1,
                'email' => 'test123@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$SK/6UFquuIrp9E3peTKGhuWlE5o4ikeh.vnmpNGKuNWuHk4COsD72',
                'first_name' => 'Chadwick',
                'middle_name' => '',
                'last_name' => 'Test',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-15 01:12:55',
                'token' => NULL,
                'remember_token' => '8onOML4xFddgSwkmlhrgKsAVngstgFYpD1w39ZzuwMFhe9qsurt67WoqtfkS',
                'created_at' => '2019-03-14 18:12:55',
                'updated_at' => '2019-03-14 18:12:55',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'status' => 0,
                'type' => 1,
                'email' => 'somulyja@mailinator.net',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$S30xGxyu5wm2nLbWjto8p.NnWKF12JO4lc63C.PES/rO2ZIPdVnAK',
                'first_name' => 'Brittany',
                'middle_name' => '',
                'last_name' => 'Mills',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-15 01:16:22',
                'token' => NULL,
                'remember_token' => 'N0U4SunyesmKkAvnOLdo9fW1fPUoBaMFybQrJVrob3uxxLnOEIOB8FYsfXg2',
                'created_at' => '2019-03-14 18:16:22',
                'updated_at' => '2019-03-14 18:16:22',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'status' => 0,
                'type' => 1,
                'email' => 'qoxajeno@mailinator.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$2LnZGwxkmiWzczvfQLHBGOpATdNXmWbABedY.IBmrcZm0w8rsSuiC',
                'first_name' => 'Anne',
                'middle_name' => '',
                'last_name' => 'Sims',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-15 01:18:37',
                'token' => NULL,
                'remember_token' => 'cin9GF9zFpi00d3efPB4dONywvHhIf32xfnCau6LFab2zCtuzlg2sQhlgDDR',
                'created_at' => '2019-03-14 18:18:37',
                'updated_at' => '2019-03-14 18:18:37',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer1+3@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$f18y39w5e5nAHYVhcZkvCujZKZG7mt/P14CmoZp2f9tRIh1zSfZHC',
                'first_name' => 'RM',
                'middle_name' => '',
                'last_name' => 'Suarez',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-15 17:06:24',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-03-15 10:06:24',
                'updated_at' => '2019-03-15 10:06:24',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'status' => 0,
                'type' => 1,
                'email' => 'programmer1+3@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$YBOjjlDzgP8JDB2eehUFSu8LtVKYUiJByBckYjsMxj9HTWUg0e.Oy',
                'first_name' => 'Roldan',
                'middle_name' => '',
                'last_name' => 'Suarez',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-03-27 17:23:04',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-03-27 10:23:04',
                'updated_at' => '2019-03-27 10:23:04',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'status' => 0,
                'type' => 1,
                'email' => 'designer@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$MmquZkBCekrxtJV.wkA4w.8lL67CdjxrMqTf.qJgbb0SQ6pytFxs6',
                'first_name' => 'tomoko',
                'middle_name' => '',
                'last_name' => 'burke',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-08-01 16:52:49',
                'token' => NULL,
                'remember_token' => 'fADql5MOhXWqWNUR0N2hQVLOUm0dnJYBhwNRYW2pwDvZ1iO057F9WYNQosn0',
                'created_at' => '2019-04-10 16:05:37',
                'updated_at' => '2019-08-01 16:52:49',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'status' => 0,
                'type' => 1,
                'email' => 'carolynn@dogandrooster.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$6h6dJMyAA6dlyu1G6uIMwuic0EdRhpD8Cmqb.onf2adhMisvQSvXW',
                'first_name' => 'carolynn',
                'middle_name' => '',
                'last_name' => 'edmonson',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-09-17 16:20:59',
                'token' => NULL,
                'remember_token' => 'vwLCfADDwpeCcc3VudP7mRIi89Lct3iaaMEW2uMPo2uJq0vfNMWTzWd3evSC',
                'created_at' => '2019-04-11 11:29:04',
                'updated_at' => '2019-09-17 16:20:59',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'status' => 0,
                'type' => 0,
                'email' => 'programmer1222222@dogandrooster.com',
                'phone' => '',
                'user_name' => 'mikosuarez',
                'password' => '$2y$10$LYZ6E21R3/3f.mEoERr8fu5F72uUH61SJWiCEa3I7OsoeoPTArPDq',
                'first_name' => 'Miko',
                'middle_name' => '',
                'last_name' => 'Suarez',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '123',
                'is_active' => 1,
                'last_login' => '2019-07-17 15:36:45',
                'token' => NULL,
                'remember_token' => 'yU8t2PYa4W63spWCjEJ9GdV65fc2LZiC6zbWE2E6VXpSzpQYuZBRS7tE8Axm',
                'created_at' => '2019-04-30 11:23:08',
                'updated_at' => '2019-07-17 15:36:45',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'status' => 0,
                'type' => 1,
                'email' => 'cynthia@imagemastersusa.com',
                'phone' => '',
                'user_name' => '',
                'password' => '$2y$10$rkeMnRqqMQNWV7I7eW0aW.4F3J9QcKjC7YjCCei.WCH47XsAc2vBW',
                'first_name' => 'Cyn',
                'middle_name' => '',
                'last_name' => 'Hays',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-07-19 23:36:26',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-07-19 16:36:26',
                'updated_at' => '2019-07-19 16:36:26',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'status' => 0,
                'type' => 1,
                'email' => 'officeassist+200@dogandrooster.com',
            'phone' => '+1 (543) 426-8528',
                'user_name' => '',
                'password' => '$2y$10$pPKYg4Fc7O6vazTlK5mq.e3An3dPoYg3kyzKkBbBG2UdHQ9afmlYO',
                'first_name' => 'Vincent',
                'middle_name' => '',
                'last_name' => 'Mcmahon',
                'position' => 'office assistant',
                'address' => 'oberline drive san diego ca',
                'license_no' => '85694-5641854',
                'website' => '',
                'company_logo' => 'public/uploads/user_company_logos/REPUBLICLOGO-1568426388.png',
                'profile_image' => 'public/uploads/user_profile_images/profile-2398782_960_720-1568426388.png',
                'photographer_calendar_id' => '',
                'is_active' => 1,
                'last_login' => '2019-09-13 18:53:30',
                'token' => NULL,
                'remember_token' => '4FezeTDyAwKhyQnI6QORQClMOawmksCri0iymJCVmW1CRWZCiHZXRFKJxkWc',
                'created_at' => '2019-09-13 18:30:17',
                'updated_at' => '2019-09-13 19:01:17',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'status' => 0,
                'type' => 0,
                'email' => 'cajudeberi@mailinator.net',
                'phone' => '',
                'user_name' => 'cajudeberi',
                'password' => '$2y$10$HHbnAJsOMBPZAgY.e1qlGOstQ/WxQBoDmIrfqBRGpnyvcJwjfSBOS',
                'first_name' => 'Elvis',
                'middle_name' => '',
                'last_name' => 'Quinn',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => NULL,
                'is_active' => 1,
                'last_login' => '2019-09-23 23:43:42',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2019-09-23 16:43:42',
                'updated_at' => '2019-09-23 16:43:58',
                'deleted_at' => '2019-09-23 16:43:58',
            ),
            28 => 
            array (
                'id' => 29,
                'status' => 0,
                'type' => 1,
                'email' => 'officeassist+600@dogandrooster.com',
                'phone' => '6195815118',
                'user_name' => '',
                'password' => '$2y$10$1/qrQhnUq5bUIrNo/ZNlsODi5pgoN07dKxefsYux94ItpFtZDGMIq',
                'first_name' => 'Humpty',
                'middle_name' => '',
                'last_name' => 'Dumpty',
                'position' => '',
                'address' => '',
                'license_no' => '',
                'website' => '',
                'company_logo' => '',
                'profile_image' => '',
                'photographer_calendar_id' => NULL,
                'is_active' => 1,
                'last_login' => '2019-10-04 21:44:07',
                'token' => NULL,
                'remember_token' => '9bIdUB5Kr4s4J0E6E7kHRY3E26NlIiA2AzWej9JV1Rrq9C8YwVXtFIKJ0mOM',
                'created_at' => '2019-10-04 21:43:16',
                'updated_at' => '2019-10-04 21:44:07',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}