<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Create Role',
                'permission_group_id' => 1,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Read Role',
                'permission_group_id' => 1,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Update Role',
                'permission_group_id' => 1,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Delete Role',
                'permission_group_id' => 1,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Create Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Read Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Update Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Delete Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Create Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Read Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Update Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Delete Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Create User',
                'permission_group_id' => 4,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Read User',
                'permission_group_id' => 4,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Update User',
                'permission_group_id' => 4,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Delete User',
                'permission_group_id' => 4,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Create System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Read System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Update System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Delete System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Create Page',
                'permission_group_id' => 6,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Read Page',
                'permission_group_id' => 6,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Update Page',
                'permission_group_id' => 6,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Delete Page',
                'permission_group_id' => 6,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Create Post',
                'permission_group_id' => 7,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Read Post',
                'permission_group_id' => 7,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Update Post',
                'permission_group_id' => 7,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Delete Post',
                'permission_group_id' => 7,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Create Product Category',
                'permission_group_id' => 8,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Read Product Category',
                'permission_group_id' => 8,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Update Product Category',
                'permission_group_id' => 8,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Delete Product Category',
                'permission_group_id' => 8,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Create Product',
                'permission_group_id' => 9,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Read Product',
                'permission_group_id' => 9,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Update Product',
                'permission_group_id' => 9,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Delete Product',
                'permission_group_id' => 9,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Create Project',
                'permission_group_id' => 10,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Read Project',
                'permission_group_id' => 10,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Update Project',
                'permission_group_id' => 10,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Delete Project',
                'permission_group_id' => 10,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Read Contact',
                'permission_group_id' => 11,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Read Subscription',
                'permission_group_id' => 12,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Create Home Page Slider',
                'permission_group_id' => 13,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Read Home Page Slider',
                'permission_group_id' => 13,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Update Home Page Slider',
                'permission_group_id' => 13,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Delete Home Page Slider',
                'permission_group_id' => 13,
                'created_at' => '2019-02-18 22:11:00',
                'updated_at' => '2019-02-18 22:11:00',
            ),
        ));
        
        
    }
}