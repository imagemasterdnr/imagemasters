(function () {
    "use strict";
    /* declare global variables within the class */
    var uiSubscriptionsTable,
        uiCreateSubscriptionForm,
        uiSubscriptionsModalBtn,
        uiSubscriptionsModal,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain subscription events
     *
     * */
    CPlatform.prototype.subscription = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiSubscriptionsTable = $('#subscriptions-table');
            uiCreateSubscriptionForm = $('#create-subscription');
            uiSubscriptionsModalBtn = $('.subscription-modal-btn');
            uiSubscriptionsModal = $('#subscription-modal');

            /* subscriptions table initialize datatable */
            uiSubscriptionsTable.DataTable({
                "order": [[0, "desc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });

            uiSubscriptionsModalBtn.on('click', function () {
                uiSubscriptionsModal.modal('show');
            });

            /* create subscription validation */
            uiCreateSubscriptionForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    // platform.show_spinner($(form).find('[type="submit"]'), true);
                    // form.submit();
                    var oParams = {
                        data: {
                            'email' : $(form).find('[name="email"]').val(),
                            'first_name' : $(form).find('[name="first_name"]').val(),
                            'last_name' : $(form).find('[name="last_name"]').val(),
                        },
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                    };
                    console.log(oParams)
                    _ajax(oParams, function (oData) {
                        var sMessage = '';
                        for (var x in oData.message) {
                            var sMsg = oData.message[x];
                            sMessage += sMsg;
                        }
                        if (oData.status) {
                            uiSubscriptionsModal.modal('hide');
                            $(form).find('[type="text"]').val('');
                            swal({
                                title: "",
                                text: sMessage,
                                type: "success",
                                allowEscapeKey: true,
                                allowOutsideClick: true,
                                //            confirmButtonColor: "#DD6B55",
                            }, function () {

                            });
                        } else {
                            sMessage = '<span id="email-error" class="help-block animation-slideDown">' + sMessage + '</span>';
                            $(form).find('[name="email"]').closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                            $(form).find('[name="email"]').parents('.form-group').append(sMessage);
                        }
                    }, $(form).find('[type="submit"]'));
                    return false;
                },
                rules: {
                    'email': {
                        required: true,
                        email: true
                    },
                    'first_name': {
                        required: true
                    },
                    'last_name': {
                        required: true
                    },
                },
                messages: {
                    'email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.',
                    },
                    'first_name': {
                        required: 'Firstname is required.'
                    },
                    'last_name': {
                        required: 'Lastname is required.'
                    },
                }
            });
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.subscription.initialize();
});