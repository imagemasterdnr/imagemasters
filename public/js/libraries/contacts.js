(function () {
    "use strict";
    /* declare global variables within the class */
    var uiContactsTable,
        uiCreateContactForm,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain contact events
     *
     * */
    CPlatform.prototype.contact = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiContactsTable = $('#contacts-table');
            uiCreateContactForm = $('#create-contact');

            /* contacts table initialize datatable */
            uiContactsTable.DataTable({
                "order": [[0, "desc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });

            /* create contact validation */
            uiCreateContactForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'email': {
                        required: true,
                        email: true
                    },
                    'message': {
                        required: true
                    },
                    'name': {
                        required: true
                    },
                    // 'company': {
                    //     required: true
                    // },
                    'phone': {
                        required: true
                    },
                    'subject': {
                        required: true
                    },
                },
                messages: {
                    'email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.',
                    },
                    'message': {
                        required: 'Message is required.'
                    },
                    'name': {
                        required: 'Name is required.'
                    },
                    // 'company': {
                    //     required: 'Company is required.'
                    // },
                    'phone': {
                        required: 'Phone is required.'
                    },
                    'subject': {
                        required: 'Subject is required.'
                    },
                }
            });
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.contact.initialize();
});