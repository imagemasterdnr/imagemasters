(function () {
    "use strict";
    /* declare global variables within the class */
    var uiHomePageSlidersTable,
        uiCreateHomePageSliderForm,
        uiEditHomePageSliderForm,
        uiInputFile,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /* private fileselect function that will check/validate files input */
    function _fileselect(element, numFiles, label, ext) {
        var input = $(element).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (element.val() == '') {
            _fileselect_error(element, input, 'File is required.');
        } else {
            if (ext == 'jpeg' || ext == 'jpg' || ext == 'png') {
                element.closest('.form-group').find('.help-block').remove();
                element.closest('.form-group').removeClass('has-success has-error');

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) {
                        console.log(log);
                    }
                }
            }
            else {
                _fileselect_error(element, input, 'The upload file must be a file of type: jpeg, jpg, png.');
            }
        }
    }

    /* private fileselect_error function that will append/display error messages of file input */
    function _fileselect_error(element, input, msg) {
        element.closest('.form-group').find('.help-block').remove();
        element.closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        element.parents('.form-group > div').append('<span id="file-error" class="help-block animation-slideDown">' + msg + '</span>');
        element.val('');
        input.val('');
    }

    /*
     * This js file will only contain home-page-slider events
     *
     * */
    CPlatform.prototype.home_page_slider = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiHomePageSlidersTable = $('#home-page-sliders-table');
            uiCreateHomePageSliderForm = $('#create-home-page-slider');
            uiEditHomePageSliderForm = $('#edit-home-page-slider');
            uiInputFile = $('input[type="file"]');

            /* home-page-sliders table initialize datatable */
            var uiHomePageSlidersDataTable = uiHomePageSlidersTable.DataTable({
                "order": [[0, "asc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });

            /* create home-page-slider validation */
            uiCreateHomePageSliderForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group:first > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                    $(e).closest('.form-group').find('.chosen-container').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.select2-container').removeClass('has-success has-error').addClass('has-error');
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                    e.closest('.form-group').find('.chosen-container').removeClass('has-success has-error');
                    e.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    // 'name': {
                    //     required: true
                    // },
                    'file': {
                        required: true
                    },
                },
                messages: {
                    // 'name': {
                    //     required: 'Name is required.'
                    // },
                    'file': {
                        required: 'Image is required.'
                    },
                }
            });

            /* edit home-page-slider validation */
            uiEditHomePageSliderForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group:first > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                    $(e).closest('.form-group').find('.chosen-container').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.select2-container').removeClass('has-success has-error').addClass('has-error');
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                    e.closest('.form-group').find('.chosen-container').removeClass('has-success has-error');
                    e.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    // 'name': {
                    //     required: true
                    // },
                    'file': {
                        required: true
                    },
                },
                messages: {
                    // 'name': {
                    //     required: 'Name is required.'
                    // },
                    'file': {
                        required: 'Image is required.'
                    },
                }
            });

            /* delete home-page-slider button ajax */
            $('body').on('click', '.delete-home-page-slider-btn', function (e) {
                e.preventDefault();
                var self = $(this);
                /* open confirmation modal */
                swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    //confirmButtonColor: "#27ae60",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true,
                    allowOutsideClick: true
                }, function (isConfirm) {
                    /* if confirmed, send request ajax */
                    if (isConfirm) {
                        var oParams = {
                            'data': {'id': self.attr('data-home-page-slider-id')},
                            'url': self.attr('data-home-page-slider-route')
                        };
                        platform.delete.delete(oParams, function (oData) {
                            /* check return of ajax */
                            if (platform.var_check(oData)) {
                                /* check status if success */
                                if (oData.status > 0) {
                                    /* if status is true, render success messages */
                                    if (platform.var_check(oData.message)) {
                                        for (var x in oData.message) {
                                            var message = oData.message[x];
                                            swal({
                                                'title': "Deleted!",
                                                'text': message,
                                                'type': "success"
                                                //'confirmButtonColor': "#DD6B55",
                                            }, function () {
                                                /* remove home-page-slider container */
                                                $('[data-home-page-slider-template-id="' + oData.data.id + '"]').remove();

                                                /* check if there are other home-page-sliders to hide the table header and show the no home-page-sliders found */
                                                if ($('[data-home-page-slider-template-id]').length == 0) {
                                                    $('.home-page-slider-empty').removeClass('johnCena');
                                                    $('.table-responsive').addClass('johnCena');
                                                }
                                            });
                                        }
                                    }
                                }
                                else {
                                    /* if status is false, render error messages */
                                    if (platform.var_check(oData.message)) {
                                        for (var x in oData.message) {
                                            var message = oData.message[x];
                                            swal({
                                                'title': "Error!",
                                                'text': message,
                                                'type': "error"
                                                //'confirmButtonColor': "#DD6B55",
                                            }, function () {

                                            });
                                        }
                                    }
                                }
                            }
                        }, self);
                    }
                });
            });

            /* input file event */
            uiInputFile.on('change', function () {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, ''),
                    sValue = $(this).val(),
                    ext = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
                _fileselect($(this), numFiles, label, ext);
            });
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.home_page_slider.initialize();
});