(function () {
    "use strict";
    /* declare global variables within the class */
    var uiGallery,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain front_project events
     *
     * */
    CPlatform.prototype.front_project = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiGallery = $('.projects-gallery');

            uiGallery.magnificPopup({
                delegate: 'a.project-display__video', // the selector for gallery item
                type: 'iframe',
                gallery: {
                    enabled:true
                }
            });

            if (platform.var_check(iViewProjectId)) {
                var iProjectIndex = $('.project-container').find('a.project-display__video[data-project-id]').index($('a.project-display__video[data-project-id="' + iViewProjectId + '"]'));
                uiGallery.magnificPopup('open');
                uiGallery.magnificPopup('goTo', iProjectIndex);
                // $('[data-project-id="' + iViewProjectId + '"]').trigger('click');
            }
        },
    }
}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.front_project.initialize();
});