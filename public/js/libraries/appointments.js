(function () {
    "use strict";
    /* declare global variables within the class */
    var uiAppointmentsTable,
        uiUploadImageDropzone,
        sUploadImageUrl,
        sDeleteImageUrl,
        uiAddAttachmentForm,
        uiPropertyDetailsForm,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain appointment events
     *
     * */
    CPlatform.prototype.appointment = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiAppointmentsTable = $('#appointments-table');
            uiUploadImageDropzone = {};
            sUploadImageUrl = sBaseURI + '/admin/appointments/upload_image';
            sDeleteImageUrl = sBaseURI + '/admin/appointments/delete_image';
            uiAddAttachmentForm = $('#add-attachment');
            uiPropertyDetailsForm = $('#edit-property-detail');

            uiPropertyDetailsForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                onkeyup:false,
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {

                },
                messages: {

                }
            });

            /* appointments table initialize datatable */
            uiAppointmentsTable.DataTable({
                "order": [[0, "desc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [5]
                }]
            });

            if (uiAddAttachmentForm.length) {
                platform.appointment.attachment_upload();

                if (platform.var_check(sAppointmentImages)) {
                    var oAppointmentImages = platform.parse_json(sAppointmentImages);
                    for (var x in oAppointmentImages) {
                        var appointment_image = oAppointmentImages[x];
                        var sImageUrl = sBaseURI + '/' + appointment_image.file;
                        var oMockFile = {
                            name: appointment_image.name,
                            size: appointment_image.size,
                            path: appointment_image.file,
                            url: sImageUrl
                        };
                        var oResponse = [
                            {
                                data: {
                                    img_ctr: 0,
                                    file_upload_path: appointment_image.file,
                                    file_name: appointment_image.name,
                                    file_size: appointment_image.size,
                                    is_default: appointment_image.is_default,
                                    extension: appointment_image.extension,
                                    thumbnail: appointment_image.thumbnail,
                                }
                            }
                        ];
                        if (platform.var_check(uiUploadImageDropzone.files)) {
                            uiUploadImageDropzone.files.push(oMockFile);
                            uiUploadImageDropzone.emit("addedfile", oMockFile);
                            uiUploadImageDropzone.emit("thumbnail", oMockFile, sImageUrl);
                            uiUploadImageDropzone.emit("success", oMockFile, oResponse);
                            uiUploadImageDropzone.emit("complete", oMockFile);
                        }
                    }
                }
            }

            $('body').on('click', '.cancel-appointment-btn', function (e) {
                e.preventDefault();
                var self = $(this);
                /* open confirmation modal */
                swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to cancel this appointment?",
                    type: "warning",
                    showCancelButton: true,
                    //confirmButtonColor: "#27ae60",
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    // showLoaderOnConfirm: true,
                    allowOutsideClick: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        swal.enableButtons();
                        swal({
                            title: "Cancel Note",
                            html: true,
                            text: "<textarea id='cancel-note' class='form-control' style='resize:vertical; max-height: 200px; min-height: 100px;'></textarea>",
                            showCancelButton: true,
                            //confirmButtonColor: "#27ae60",
                            confirmButtonText: "Cancel now!",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            showLoaderOnConfirm: true,
                            allowOutsideClick: true
                        }, function (isConfirm) {
                            /* if confirmed, send request ajax */
                            if (isConfirm) {
                                var sNote = $('#cancel-note').val();
                                var oParams = {
                                    'data': {
                                        'id': self.attr('data-appoinment-id'),
                                        'note': sNote
                                    },
                                    'url': self.attr('data-appointment-route'),
                                    'type': 'POST',
                                };
                                _ajax(oParams, function (oData) {
                                    /* check return of ajax */
                                    if (platform.var_check(oData)) {
                                        /* check status if success */
                                        if (oData.status > 0) {
                                            /* if status is true, render success messages */
                                            if (platform.var_check(oData.message)) {
                                                for (var x in oData.message) {
                                                    var message = oData.message[x];
                                                    swal({
                                                        'title': "Canceled!",
                                                        'text': message,
                                                        'type': "success"
                                                        //'confirmButtonColor': "#DD6B55",
                                                    }, function () {
                                                        self.remove();
                                                    });
                                                }
                                            }
                                        }
                                        else {
                                            /* if status is false, render error messages */
                                            if (platform.var_check(oData.message)) {
                                                for (var x in oData.message) {
                                                    var message = oData.message[x];
                                                    swal({
                                                        'title': "Error!",
                                                        'text': message,
                                                        'type': "error"
                                                        //'confirmButtonColor': "#DD6B55",
                                                    }, function () {

                                                    });
                                                }
                                            }
                                        }
                                    }
                                }, self);
                            }
                        });
                    }
                });
            });

            $('[data-toggle="lightbox-gallery"]').each(function(){
                $(this).magnificPopup({
                    delegate: 'a.gallery-link',
                    type: 'image',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%" title="%title%"></button>',
                        tPrev: 'Previous',
                        tNext: 'Next',
                        tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
                    },
                    image: {titleSrc: 'title'},
                    callbacks: {
                        beforeOpen: function () {
                        },
                        afterClose: function () {
                        },
                        elementParse: function(item) {
                            var ext = item.src.substring(item.src.lastIndexOf('.') + 1).toLowerCase();
                            if (ext == 'mp4') {
                                item.type = 'iframe';
                            }
                        }
                    },
                });
            });

            if ($('[data-video="1"]').length) {
                $('[data-video="1"]').each(function () {
                    var uiThis = $(this);
                    var file = {
                        url: uiThis.attr('src')
                    };
                    FrameGrab.blob_to_video(file).then(
                        function videoRendered(videoEl) {
                            var frameGrab = new FrameGrab({video: videoEl});
                            frameGrab.grab('img', 1, 240).then(
                                function frameGrabbed(itemEntry) {
                                   uiThis.attr('src', itemEntry.container.src);
                                },
                                function frameFailedToGrab(reason) {
                                    console.log("Can't grab the video frame from file: " +
                                        file.name + ". Reason: " + reason);
                                }
                            );
                        },
                        function videoFailedToRender(reason) {
                            console.log("Can't convert the file to a video element: " +
                                file.name + ". Reason: " + reason);
                        }
                    );
                });
            }
        },

        /* ATTACHMENT START */
        /*
         * This function will upload the attachment to the server
         *
         * */
        attachment_upload: function (fnCallback) {
            Dropzone.options.uploadImage = false;
            Dropzone.autoDiscover = false;

            uiUploadImageDropzone = new Dropzone("#upload-image", {
                url: sUploadImageUrl,
                method: "post",
                parallelUploads: 1,
                uploadMultiple: true,
                acceptedFiles: "image/gif, image/jpg, image/jpeg, image/png, video/mp4",
                paramName: "file",
                maxFilesize: 10,
                // autoProcessQueue: false,
                accept: function (file, done) {
                    done();
                },
                init: function () {
                    var iErrorCtr = 0;
                    var iImgCtr = 0;
                    this.on("error", function (file, errorMessage) {
                        console.log(errorMessage)
                        if (!file.accepted) {
                            var _this = this;
                            _this.removeFile(file);
                            if (!file.type.match('image.*')) {
                                iErrorCtr++;
                            }
                        }
                    });

                    this.on("processing", function () {
                        iErrorCtr = 0;
                        iImgCtr = 0;
                    });

                    this.on("success", function (file, response) {
                        var _this = this;
                        $(file.previewElement).find('[data-dz-name]').text(response[iImgCtr].data.file_name);
                        file.new_name = response[iImgCtr].data.file_name;
                        file.new_path = response[iImgCtr].data.file_upload_path;
                        file.is_default = response[iImgCtr].data.is_default ? response[iImgCtr].data.is_default : 0;
                        file.extension = response[iImgCtr].data.extension ? response[iImgCtr].data.extension : '';
                        file.thumbnail = response[iImgCtr].data.thumbnail ? response[iImgCtr].data.thumbnail : '';

                        var oFile = {
                            name: file.new_name,
                            file: file.new_path,
                            size: file.size,
                            is_default: file.is_default,
                            extension: file.extension,
                            thumbnail: file.thumbnail,
                        };

                        var sFile = encodeURIComponent(JSON.stringify(oFile));

                        $(file.previewElement).append('<div class="view-delete-btn-group btn-group btn-group-xs col-md-12"></div>');
                        // $(file.previewElement).append('<div class="is-default-btn-group btn-group btn-group-xs col-md-12"></div>');

                        var uiViewButton = Dropzone.createElement('<a href="' + sBaseURI + '/' + file.new_path + '" ' +
                            'class="zoom img-thumbnail btn btn-info text-center col-md-6" ' +
                            'style="cursor: pointer !important;" ' +
                            'data-toggle="lightbox-image"><i class="fa fa-search"></a>');
                        $(file.previewElement).find('.view-delete-btn-group').append(uiViewButton);
                        // $('[data-toggle="lightbox-image"]').magnificPopup({type: 'image', image: {titleSrc: 'title'}});
                        $('#upload-image').each(function () {
                            $(this).magnificPopup({
                                delegate: '[data-toggle="lightbox-image"]',
                                type: 'image',
                                gallery: {
                                    enabled: true,
                                    navigateByImgClick: true,
                                    arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%" title="%title%"></button>',
                                    tPrev: 'Previous',
                                    tNext: 'Next',
                                    tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
                                },
                                image: {
                                    titleSrc: 'title'
                                },
                                callbacks: {
                                    beforeOpen: function () {
                                    },
                                    afterClose: function () {
                                    },
                                    elementParse: function(item) {
                                        var ext = item.src.substring(item.src.lastIndexOf('.') + 1).toLowerCase();
                                        if (ext == 'mp4') {
                                            item.type = 'iframe';
                                        }
                                    }
                                },
                            });
                        });

                        var uiRemoveButton = Dropzone.createElement('<button class="btn btn-danger text-center col-md-6 dz-remove-image-button"><i class="fa fa-trash"></i></button>');
                        $(file.previewElement).find('.view-delete-btn-group').append(uiRemoveButton);
                        $(uiRemoveButton).on("click", function (e) {
                            e.preventDefault();
                            var uiThis = $(this);
                            var oParams = {
                                'orig-src': response[iImgCtr].data.file_upload_path,
                            };
                            $('[name="appointment_images[]"][data-file="' + file.new_path + '"]').remove();
                            platform.appointment.attachment_delete(oParams, sDeleteImageUrl, uiThis, function (oData) {
                                _this.removeFile(file);
                            });
                        });

                        if (uiAddAttachmentForm.length) {
                            uiAddAttachmentForm.append('<input type="hidden" name="appointment_images[]" ' +
                                'data-is-default="' + file.is_default + '" data-file="' + file.new_path + '" value="' + sFile + '">');
                        }

                        var uiSetDefaultBtn = Dropzone.createElement('<button class="btn btn-success text-center col-md-12 set-default-btn">' + (file.is_default == 1 ? 'Default' : 'Set as default') + '</button>');
                        $(file.previewElement).find('.is-default-btn-group').append(uiSetDefaultBtn);

                        $(uiSetDefaultBtn).on("click", function (e) {
                            e.preventDefault();
                            var uiThis = $(this);
                            $.each($('[name="appointment_images[]"]'), function () {
                                var oValue = platform.parse_json(decodeURIComponent($(this).val()));
                                oValue.is_default = 0;
                                var sFile = encodeURIComponent(JSON.stringify(oValue));
                                $(this).attr('data-is-default', 0).attr('value', sFile);
                                $('.set-default-btn').text('Set as default');
                            });


                            oFile.is_default = 1;
                            var sFile = encodeURIComponent(JSON.stringify(oFile));
                            $('[name="appointment_images[]"][data-file="' + file.new_path + '"]').attr('data-is-default', 1).attr('value', sFile);
                            uiThis.text('Default');
                        });

                        if (platform.var_check(oFile.extension) && oFile.extension == 'mp4') {
                            if (!platform.var_check(file.url)) {
                                file.url = sBaseURI + '/' + file.new_path;
                            }
                            FrameGrab.blob_to_video(file).then(
                                function videoRendered(videoEl) {
                                    var frameGrab = new FrameGrab({video: videoEl});
                                    frameGrab.grab('img', 1, 240).then(
                                        function frameGrabbed(itemEntry) {
                                            $(file.previewElement).find('.dz-image').find('img').attr('src', itemEntry.container.src);
                                        },
                                        function frameFailedToGrab(reason) {
                                            console.log("Can't grab the video frame from file: " +
                                                file.name + ". Reason: " + reason);
                                        }
                                    );
                                },
                                function videoFailedToRender(reason) {
                                    console.log("Can't convert the file to a video element: " +
                                        file.name + ". Reason: " + reason);
                                }
                            );
                        }
                    });

                    this.on("complete", function (file) {
                        $(file.previewElement).find('.dz-progress').hide();
                    });
                }
            });
            $('#upload-image').addClass('dropzone');

            if (typeof(fnCallback) == 'function') {
                fnCallback();
            }
        },

        /*
         * This function will delete the attachment from the server
         *
         * */
        attachment_delete: function (oData, sUrl, uiBtn, fnCallback) {
            var oParams = {
                type: "DELETE",
                data: oData,
                url: sUrl,
            };
            _ajax(oParams, function (oData) {
                if (typeof(fnCallback) == 'function') {
                    fnCallback(oData);
                }
            }, uiBtn);
        },
        /* ATTACHMENT END */
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.appointment.initialize();
});