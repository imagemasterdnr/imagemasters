(function () {
    "use strict";
    /* declare global variables within the class */
    var uiPropertyDetailsForm,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain front_products events
     *
     * */
    CPlatform.prototype.front_products = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiPropertyDetailsForm = $('#edit-property-detail');

            $('body').on('click', '.cancel-appointment-btn', function (e) {
                e.preventDefault();
                var self = $(this);
                /* open confirmation modal */
                swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to cancel this appointment?",
                    type: "warning",
                    showCancelButton: true,
                    //confirmButtonColor: "#27ae60",
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    // showLoaderOnConfirm: true,
                    allowOutsideClick: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        swal.enableButtons();
                        swal({
                            title: "Cancel Note",
                            html: true,
                            text: "<textarea id='cancel-note' class='form-control' style='resize:vertical; max-height: 200px; min-height: 100px;'></textarea>",
                            showCancelButton: true,
                            //confirmButtonColor: "#27ae60",
                            confirmButtonText: "Cancel now!",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            showLoaderOnConfirm: true,
                            allowOutsideClick: true
                        }, function (isConfirm) {
                            /* if confirmed, send request ajax */
                            if (isConfirm) {
                                var sNote = $('#cancel-note').val();
                                var oParams = {
                                    'data': {
                                        'id': self.attr('data-appoinment-id'),
                                        'note': sNote
                                    },
                                    'url': self.attr('data-appointment-route'),
                                    'type': 'POST',
                                };
                                _ajax(oParams, function (oData) {
                                    /* check return of ajax */
                                    if (platform.var_check(oData)) {
                                        /* check status if success */
                                        if (oData.status > 0) {
                                            /* if status is true, render success messages */
                                            if (platform.var_check(oData.message)) {
                                                for (var x in oData.message) {
                                                    var message = oData.message[x];
                                                    swal({
                                                        'title': "Canceled!",
                                                        'text': message,
                                                        'type': "success"
                                                        //'confirmButtonColor': "#DD6B55",
                                                    }, function () {
                                                        // self.remove();
                                                        location.reload();
                                                    });
                                                }
                                            }
                                        }
                                        else {
                                            /* if status is false, render error messages */
                                            if (platform.var_check(oData.message)) {
                                                for (var x in oData.message) {
                                                    var message = oData.message[x];
                                                    swal({
                                                        'title': "Error!",
                                                        'text': message,
                                                        'type': "error"
                                                        //'confirmButtonColor': "#DD6B55",
                                                    }, function () {

                                                    });
                                                }
                                            }
                                        }
                                    }
                                }, self);
                            }
                        });
                    }
                });
            });

            if (typeof($.slideshowify) == 'function') {
                if ($('[data-toggle="lightbox-gallery"] img').length) {
                    $('.slideshow__play').on('click', function () {
                        $(this).siblings('img').remove();
                        $(this).remove();
                        $('[data-toggle="lightbox-gallery"] img').slideshowify({parentEl: '.slideshow'});
                        $('[data-toggle="lightbox-gallery"] img').show();
                    });
                }
            }

            $('[data-toggle="lightbox-gallery"]').each(function () {
                $(this).magnificPopup({
                    delegate: 'a.gallery-link',
                    type: 'image',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%" title="%title%"></button>',
                        tPrev: 'Previous',
                        tNext: 'Next',
                        tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
                    },
                    image: {
                        titleSrc: function (item) {
                            var uiClone = $(item.el).parents('.grid-gallery__item').find('.download-image-btn').clone();
                            uiClone.find('i').after(' Download');
                            uiClone.css({'font-size': '12px', 'padding': '0', 'font-weight': 'normal'});
                            return uiClone;
                        }
                    },
                    callbacks: {
                        beforeOpen: function () {
                        },
                        afterClose: function () {
                        },
                        elementParse: function (item) {
                            var ext = item.src.substring(item.src.lastIndexOf('.') + 1).toLowerCase();
                            if (ext == 'mp4') {
                                item.type = 'iframe';
                            }
                        }
                    },
                });
            });

            if ($('[data-video="1"]').length) {
                $('[data-video="1"]').each(function () {
                    var uiThis = $(this);
                    var file = {
                        url: uiThis.attr('video-src')
                    };
                    FrameGrab.blob_to_video(file).then(
                        function videoRendered(videoEl) {
                            var frameGrab = new FrameGrab({video: videoEl});
                            frameGrab.grab('img', 1, 330).then(
                                function frameGrabbed(itemEntry) {
                                    uiThis.attr('src', itemEntry.container.src);
                                    console.log($('[data-video-id="' + uiThis.attr('data-video-id') + '"]'))
                                    $('[data-video-id="' + uiThis.attr('data-video-id') + '"]').attr('src', itemEntry.container.src)
                                },
                                function frameFailedToGrab(reason) {
                                    console.log("Can't grab the video frame from file: " +
                                        file.name + ". Reason: " + reason);
                                }
                            );
                        },
                        function videoFailedToRender(reason) {
                            console.log("Can't convert the file to a video element: " +
                                file.name + ". Reason: " + reason);
                        }
                    );
                });
            }

            $('[data-custom-toggle="tooltip"]').tooltip();
            $('[data-custom-toggle="tooltip"]').tooltip('hide');
            $('.copy-clipboard-btn').on('click', function () {
                var uiTempInput = $("<input>");
                var uiThis = $(this);
                var sText = uiThis.attr('data-text');
                $("body").append(uiTempInput);
                uiTempInput.val(sText).select();
                document.execCommand("copy");
                uiTempInput.remove();

                $('[data-custom-toggle="tooltip"]').tooltip('show');
                setTimeout(function () {
                    $('[data-custom-toggle="tooltip"]').tooltip('hide');
                }, 1000);
            });

            uiPropertyDetailsForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                onkeyup:false,
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {

                },
                messages: {

                }
            });
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.front_products.initialize();
});