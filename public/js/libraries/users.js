(function () {
    "use strict";
    /* declare global variables within the class */
    var uiChangePasswordRadio,
        uiChangePasswordContainer,
        uiUsersTable,
        uiCreateUserForm,
        uiEditUserForm,
        uiRoleRadio,
        uiCalendarIDContainer,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain user events
     *
     * */
    CPlatform.prototype.user = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiChangePasswordRadio = $('#change_password');
            uiChangePasswordContainer = $('.change-pass-container'); uiUsersTable = $('#users-table');
            uiCreateUserForm = $('#create-user');
            uiEditUserForm = $('#edit-user');
            uiRoleRadio = $('[name="roles[]"]');
            uiCalendarIDContainer= $('.calendar-id-container');

            /* users table initialize datatable */
            var uiUsersDatatable = uiUsersTable.DataTable({
                "order": [[0, "asc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [4]
                }],
                "sDom": "<'row'<'col-sm-6 col-xs-5'l><'col-sm-6 col-xs-7'f><'col-xs-6 filter-container'><'col-xs-6 text-right' B>r>t<'row'<'col-sm-4 col-xs-4'i><'col-sm-8 col-xs-8 clearfix'p>>",
                "buttons": [
                    {
                        extend: 'csv',
                        className: 'btn btn-primary btn-csv',
                        filename: 'Users_' + moment().format('YYYYMMDDHHmmss'),
                        messageTop: null,
                        title: '',
                        // footer: true,
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ],
                            // modifier: {
                            //     page: 'current'
                            // }
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-primary btn-xls',
                        filename: 'Users_' + moment().format('YYYYMMDDHHmmss'),
                        messageTop: null,
                        title: '',
                        // footer: true,
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ],
                            // modifier: {
                            //     page: 'current'
                            // }
                        }
                    },
                ],
                "initComplete": function () {
                    /* create a container for additional filers */
                    // $('.dataTables_filter').parents().first().after('<div class="filter-container width-100percent"></div>');
                    /*role filter*/
                    this.api().columns(3).every(function () {
                        var column = this;

                        /* create a select/input text/input range */
                        var sRoleFilter = 'custom_role_filter';
                        var uiRoleSelect = '<!--<div class="col-sm-6">--> ' +
                            '<div class="dataTables_custom_role_filter"> ' +
                            '<label><select ' +
                            'name="'+sRoleFilter+'" ' +
                            'aria-controls="'+sRoleFilter+'" class="form-control">' +
                            '<option value="">All</option> ';

                        column.data().unique().sort().each(function (d, j) {
                            if (d != '') {
                                uiRoleSelect += '<option value="' + d + '">' + d + '</option>';
                            }
                        });

                        uiRoleSelect += '</select></label></div><!--</div>-->';

                        /* append to created container */
                        $('.filter-container').append(uiRoleSelect);

                        /* create an on change event for the filter */
                        $('[name="'+sRoleFilter+'"]').on('change', function () {
                            var val = $(this).val();
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
                    });
                    /*role filter end*/

                    var dtButtons = $('.dt-buttons')
                    dtButtons.find('button').each(function (e) {
                        console.log()
                        if ($(this).hasClass('btn-csv')) {
                            $(this).find('span').prepend('<i class="fa fa-file-text-o"></i> ');
                        }
                        else if ($(this).hasClass('btn-xls')) {
                            $(this).find('span').prepend('<i class="fa fa-file-excel-o"></i> ');
                        }

                        $('.dataTables_wrapper').find('.buttons-container').append($(this));
                    });
                }
            });

            /*user change password*/
            uiChangePasswordRadio.on('change', function (e) {
                if ($(this).is(':checked')) {
                    uiChangePasswordContainer.slideDown();
                    uiChangePasswordContainer.find('input').removeAttr('disabled').removeClass('johnCena');
                } else {
                    uiChangePasswordContainer.slideUp();
                    uiChangePasswordContainer.find('input').attr('disabled', 'disabled').addClass('johnCena');
                }
            });

            /* create user validation */
            uiCreateUserForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'first_name': {
                        required: true,
                        maxlength: 25
                    },
                    'last_name': {
                        required: true,
                        maxlength: 25
                    },
                    'user_name': {
                        required: true,
                        maxlength: 45
                    },
                    'email': {
                        required: true,
                        email: true,
                        maxlength: 45
                    },
                    'password': {
                        required: true,
                        minlength: 6
                    },
                    'password_confirmation': {
                        equalTo: uiCreateUserForm.find('#password')
                    }
                },
                messages: {
                    'first_name': {
                        required: 'Firstname is required.',
                        maxlength: 'Please enter no more than 25 characters.',
                    },
                    'last_name': {
                        required: 'Lastname is required.',
                        maxlength: 'Please enter no more than 25 characters.',
                    },
                    'user_name': {
                        required: 'Username is required.',
                        maxlength: 'Please enter no more than 45 characters.',
                    },
                    'email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.',
                        maxlength: 'Please enter no more than 45 characters.',
                    },
                    'password': {
                        required: 'Password is required.',
                        minlength: 'Your password must be at least 6 characters long.'
                    },
                    'password_confirmation': {
                        equalTo: 'Please enter the same password as above.'
                    }
                }
            });

            /* edit user validation */
            uiEditUserForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'first_name': {
                        required: true,
                        maxlength: 25
                    },
                    'last_name': {
                        required: true,
                        maxlength: 25
                    },
                    'user_name': {
                        required: true,
                        maxlength: 45
                    },
                    'email': {
                        required: true,
                        email: true,
                        maxlength: 45
                    },
                    'password': {
                        required: {
                            depends: function () {
                                return uiChangePasswordRadio.is(':checked');
                            }
                        },
                        minlength: {
                            param: 6,
                            depends: function () {
                                return uiChangePasswordRadio.is(':checked');
                            }
                        }
                    },
                    'password_confirmation': {
                        equalTo: {
                            param: uiEditUserForm.find('#password'),
                            depends: function () {
                                return uiChangePasswordRadio.is(':checked');
                            }
                        },
                    }
                },
                messages: {
                    'first_name': {
                        required: 'Firstname is required.',
                        maxlength: 'Please enter no more than 25 characters.',
                    },
                    'last_name': {
                        required: 'Lastname is required.',
                        maxlength: 'Please enter no more than 25 characters.',
                    },
                    'user_name': {
                        required: 'Username is required.',
                        maxlength: 'Please enter no more than 45 characters.',
                    },
                    'email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.',
                        maxlength: 'Please enter no more than 45 characters.',
                    },
                    'password': {
                        required: 'Password is required.',
                        minlength: 'Your password must be at least 6 characters long.'
                    },
                    'password_confirmation': {
                        equalTo: 'Please enter the same password as above.'
                    }
                }
            });

            /* delete user button ajax */
            $('body').on('click', '.delete-user-btn', function (e) {
                e.preventDefault();
                var self = $(this);
                /* open confirmation modal */
                swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    //confirmButtonColor: "#27ae60",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true,
                    allowOutsideClick: true
                }, function (isConfirm) {
                    /* if confirmed, send request ajax */
                    if (isConfirm) {
                        var oParams = {
                            'data': {'id': self.attr('data-user-id')},
                            'url': self.attr('data-user-route')
                        };
                        platform.delete.delete(oParams, function (oData) {
                            /* check return of ajax */
                            if (platform.var_check(oData)) {
                                /* check status if success */
                                if (oData.status > 0) {
                                    /* if status is true, render success messages */
                                    if (platform.var_check(oData.message)) {
                                        for (var x in oData.message) {
                                            var message = oData.message[x];
                                            swal({
                                                'title': "Deleted!",
                                                'text': message,
                                                'type': "success"
                                                //'confirmButtonColor': "#DD6B55",
                                            }, function () {
                                                /* remove user container */
                                                $('[data-user-template-id="' + oData.data.id + '"]').remove();

                                                /* check if there are other users to hide the table header and show the no users found */
                                                if ($('[data-user-template-id]').length == 0) {
                                                    $('.user-empty').removeClass('johnCena');
                                                    $('.table-responsive').addClass('johnCena');
                                                }
                                            });
                                        }
                                    }
                                }
                                else {
                                    /* if status is false, render error messages */
                                    if (platform.var_check(oData.message)) {
                                        for (var x in oData.message) {
                                            var message = oData.message[x];
                                            swal({
                                                'title': "Error!",
                                                'text': message,
                                                'type': "error"
                                                //'confirmButtonColor': "#DD6B55",
                                            }, function () {

                                            });
                                        }
                                    }
                                }
                            }
                        }, self);
                    }
                });
            });

            /*calendar id*/
            uiRoleRadio.on('change', function (e) {
                if ($(this).val() == 3) {
                    uiCalendarIDContainer.slideDown();
                    uiCalendarIDContainer.find('input').removeAttr('disabled');
                } else {
                    uiCalendarIDContainer.slideUp();
                    uiCalendarIDContainer.find('input').attr('disabled', 'disabled');
                }
            });
            if ($('[name="roles[]"]:checked').val() == 3) {
                uiCalendarIDContainer.slideDown();
                uiCalendarIDContainer.find('input').removeAttr('disabled');
            } else {
                uiCalendarIDContainer.slideUp();
                uiCalendarIDContainer.find('input').attr('disabled', 'disabled');
            }
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.user.initialize();
});