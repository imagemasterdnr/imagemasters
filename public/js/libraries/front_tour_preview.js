(function () {
    "use strict";
    /* declare global variables within the class */
    var uiSendTourPreviewForm,
        uiMailModalBtn,
        uiMailModal,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain front_tour_preview events
     *
     * */
    CPlatform.prototype.front_tour_preview = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiSendTourPreviewForm = $('#send-tour-preview');
            uiMailModalBtn = $('.mail-modal-btn');
            uiMailModal = $('#mail-modal');

            uiMailModalBtn.on('click', function () {
                uiMailModal.modal('show');
            });

            uiSendTourPreviewForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                onkeyup: false,
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'to_email' : {
                        required: true,
                        email: true,
                    },
                    'to_name' : {
                        required: true,
                    },
                    'from_email' : {
                        required: true,
                        email: true,
                    },
                    'from_name' : {
                        required: true,
                    },
                },
                messages: {
                    'to_email' : {
                        required: 'Your friend\'s email is required.',
                        email: 'Please enter a valid email address.',
                    },
                    'to_name' : {
                        required: 'Your friend\'s name is required.',
                    },
                    'from_email' : {
                        required: 'Your email is required.',
                        email: 'Please enter a valid email address.',
                    },
                    'from_name' : {
                        required: 'Your name is required.',
                    },
                }
            });
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.front_tour_preview.initialize();
});