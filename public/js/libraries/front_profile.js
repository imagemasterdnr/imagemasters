(function () {
    "use strict";
    /* declare global variables within the class */
    var uiPassword,
        uiEmail,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain front_profile events
     *
     * */
    CPlatform.prototype.front_profile = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiPassword = $('#form-password');
            uiEmail = $('#form-email');

            uiPassword.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                onkeyup:false,
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'old_password': {
                        required: true
                    },
                    'password': {
                        required: true,
                        minlength: 8
                    },
                    'password_confirmation': {
                        equalTo: uiPassword.find('#password')
                    },
                },
                messages: {
                    'old_password': {
                        required: 'Please provide old password'
                    },
                    'password': {
                        required: 'Please provide a new password',
                        minlength: 'Your new password must be at least 8 characters long'
                    },
                    'password_confirmation': {
                        equalTo: 'Please enter the same password as above'
                    },
                }
            });

            uiEmail.validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'span',
                onkeyup:false,
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'email': {
                        required: true,
                        email: true,
                        maxlength: 45
                    },
                    'phone': {
                        required: true,
                    },
                },
                messages: {
                    'email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.',
                        maxlength: 'Please enter no more than 45 characters.',
                    },
                    'phone': {
                        required: 'Phone is required.',
                    },
                }
            });

        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.front_profile.initialize();
});