(function () {
    "use strict";
    var uiSearchForm, uiSearchTable, filler;

    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };
            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    CPlatform.prototype.search = {
        initialize: function () {
            uiSearchForm = $('.search-form');
            uiSearchTable = $('#search-table');
            platform.search.search_form();
            platform.search.search_table();
        }, search_form: function () {
            uiSearchForm.each(function () {
                var uiForm = $(this);
                uiForm.find('.nav-link').on('click', function () {
                    uiForm.submit();
                });

                uiForm.validate({
                    errorClass: 'help-block animation-slideDown',
                    errorElement: 'span',
                    onkeyup: false,
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.form-group').find('.help-block').remove();
                    },
                    success: function (e) {
                        e.closest('.form-group').removeClass('has-success has-error');
                        e.closest('.form-group').find('.help-block').remove();
                    },
                    submitHandler: function (form) {
                        platform.show_spinner($(form).find('[type="submit"]'), true);
                        form.submit();
                    },
                    rules: {'keyword': {required: true},},
                    messages: {'keyword': {required: 'Keyword is required.'},}
                });
            });
        }, search_table: function () {
            $('.spinner').hide();
            $('.hide-after-load:not(.johnCena)').show();
            var uiSearchDatatable = uiSearchTable.DataTable({
                "order": [],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": false,
                "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}]
            });
        },
    }
}());
$(window).on('load', function () {
    platform.search.initialize();
});