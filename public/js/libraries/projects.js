(function () {
    "use strict";
    /* declare global variables within the class */
    var uiProjectsTable,
        uiCreateProjectForm,
        uiEditProjectForm,
        uiUploadImageDropzone,
        sUploadImageUrl,
        sDeleteImageUrl,
        uiInputFile,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /* private fileselect function that will check/validate files input */
    function _fileselect(element, numFiles, label, ext) {
        var input = $(element).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (element.val() == '') {
            _fileselect_error(element, input, 'File is required.');
        } else {
            if (ext == 'jpeg' || ext == 'jpg' || ext == 'png') {
                element.closest('.form-group').find('.help-block').remove();
                element.closest('.form-group').removeClass('has-success has-error');

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) {
                        console.log(log);
                    }
                }
            }
            else {
                _fileselect_error(element, input, 'The upload file must be a file of type: jpeg, jpg, png.');
            }
        }
    }

    /* private fileselect_error function that will append/display error messages of file input */
    function _fileselect_error(element, input, msg) {
        element.closest('.form-group').find('.help-block').remove();
        element.closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        element.parents('.form-group > div').append('<span id="file-error" class="help-block animation-slideDown">' + msg + '</span>');
        element.val('');
        input.val('');
    }

    /*
     * This js file will only contain project events
     *
     * */
    CPlatform.prototype.project = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiProjectsTable = $('#projects-table');
            uiCreateProjectForm = $('#create-project');
            uiEditProjectForm = $('#edit-project');
            uiUploadImageDropzone = {};
            sUploadImageUrl = sBaseURI + '/admin/projects/upload_image';
            sDeleteImageUrl = sBaseURI + '/admin/projects/delete_image';
            uiInputFile = $('input[type="file"]');

            /* projects table initialize datatable */
            var uiProjectsDataTable = uiProjectsTable.DataTable({
                "order": [[0, "asc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });

            /* create project validation */
            uiCreateProjectForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group:first > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                    $(e).closest('.form-group').find('.chosen-container').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.select2-container').removeClass('has-success has-error').addClass('has-error');
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                    e.closest('.form-group').find('.chosen-container').removeClass('has-success has-error');
                    e.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    // 'code': {
                    //     required: true
                    // },
                    'name': {
                        required: true
                    },
                    'vimeo_link': {
                        required: true
                    },
                    // 'slug': {
                    //     required: true,
                    //     special_char: true
                    // },
                },
                messages: {
                    // 'code': {
                    //     required: 'Code is required.'
                    // },
                    'name': {
                        required: 'Name is required.'
                    },
                    'vimeo_link': {
                        required: 'Vimeo Link is required.'
                    },
                    // 'slug': {
                    //     required: 'Slug is required.',
                    //     special_char: 'Slug must not contain special characters except for underscore and dash.',
                    // },
                }
            });

            /* edit project validation */
            uiEditProjectForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group:first > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                    $(e).closest('.form-group').find('.chosen-container').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.select2-container').removeClass('has-success has-error').addClass('has-error');
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                    e.closest('.form-group').find('.chosen-container').removeClass('has-success has-error');
                    e.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    // 'code': {
                    //     required: true
                    // },
                    'name': {
                        required: true
                    },
                    'vimeo_link': {
                        required: true
                    },
                    // 'slug': {
                    //     required: true,
                    //     special_char: true
                    // },
                },
                messages: {
                    // 'code': {
                    //     required: 'Code is required.'
                    // },
                    'name': {
                        required: 'Name is required.'
                    },
                    'vimeo_link': {
                        required: 'Vimeo Link is required.'
                    },
                    // 'slug': {
                    //     required: 'Slug is required.',
                    //     special_char: 'Slug must not contain special characters except for underscore and dash.',
                    // },
                }
            });

            /* delete project button ajax */
            $('body').on('click', '.delete-project-btn', function (e) {
                e.preventDefault();
                var self = $(this);
                /* open confirmation modal */
                swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    //confirmButtonColor: "#27ae60",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true,
                    allowOutsideClick: true
                }, function (isConfirm) {
                    /* if confirmed, send request ajax */
                    if (isConfirm) {
                        var oParams = {
                            'data': {'id': self.attr('data-project-id')},
                            'url': self.attr('data-project-route')
                        };
                        platform.delete.delete(oParams, function (oData) {
                            /* check return of ajax */
                            if (platform.var_check(oData)) {
                                /* check status if success */
                                if (oData.status > 0) {
                                    /* if status is true, render success messages */
                                    if (platform.var_check(oData.message)) {
                                        for (var x in oData.message) {
                                            var message = oData.message[x];
                                            swal({
                                                'title': "Deleted!",
                                                'text': message,
                                                'type': "success"
                                                //'confirmButtonColor': "#DD6B55",
                                            }, function () {
                                                /* remove project container */
                                                $('[data-project-template-id="' + oData.data.id + '"]').remove();

                                                /* check if there are other projects to hide the table header and show the no projects found */
                                                if ($('[data-project-template-id]').length == 0) {
                                                    $('.project-empty').removeClass('johnCena');
                                                    $('.table-responsive').addClass('johnCena');
                                                }
                                            });
                                        }
                                    }
                                }
                                else {
                                    /* if status is false, render error messages */
                                    if (platform.var_check(oData.message)) {
                                        for (var x in oData.message) {
                                            var message = oData.message[x];
                                            swal({
                                                'title': "Error!",
                                                'text': message,
                                                'type': "error"
                                                //'confirmButtonColor': "#DD6B55",
                                            }, function () {

                                            });
                                        }
                                    }
                                }
                            }
                        }, self);
                    }
                });
            });

            /* input file event */
            uiInputFile.on('change', function () {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, ''),
                    sValue = $(this).val(),
                    ext = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
                _fileselect($(this), numFiles, label, ext);
            });

            // if (uiCreateProjectForm.length || uiEditProjectForm.length) {
            //     platform.project.attachment_upload();
            //
            //     if (platform.var_check(sProjectImages)) {
            //         var oProjectImages = platform.parse_json(sProjectImages);
            //         for (var x in oProjectImages) {
            //             var project_image = oProjectImages[x];
            //             var sImageUrl = sBaseURI + '/' + project_image.file;
            //             var oMockFile = {
            //                 name: project_image.name,
            //                 size: project_image.size,
            //                 path: project_image.file,
            //                 url: sImageUrl
            //             };
            //             var oResponse = [
            //                 {
            //                     data: {
            //                         img_ctr: 0,
            //                         file_upload_path: project_image.file,
            //                         file_name: project_image.name,
            //                         file_size: project_image.size,
            //                         is_default: project_image.is_default,
            //                     }
            //                 }
            //             ];
            //             if (platform.var_check(uiUploadImageDropzone.files)) {
            //                 uiUploadImageDropzone.files.push(oMockFile);
            //                 uiUploadImageDropzone.emit("addedfile", oMockFile);
            //                 uiUploadImageDropzone.emit("thumbnail", oMockFile, sImageUrl);
            //                 uiUploadImageDropzone.emit("success", oMockFile, oResponse);
            //                 uiUploadImageDropzone.emit("complete", oMockFile);
            //             }
            //         }
            //     }
            // }
        },

        /* ATTACHMENT START */
        /*
         * This function will upload the attachment to the server
         *
         * */
        attachment_upload: function (fnCallback) {
            Dropzone.options.uploadImage = false;
            Dropzone.autoDiscover = false;

            uiUploadImageDropzone = new Dropzone("#upload-image", {
                url: sUploadImageUrl,
                method: "post",
                parallelUploads: 1,
                uploadMultiple: true,
                acceptedFiles: "image/gif, image/jpg, image/jpeg, image/png",
                paramName: "file",
                maxFilesize: 10,
                // autoProcessQueue: false,
                accept: function (file, done) {
                    done();
                },
                init: function () {
                    var iErrorCtr = 0;
                    var iImgCtr = 0;
                    this.on("error", function (file, errorMessage) {
                        console.log(errorMessage)
                        if (!file.accepted) {
                            var _this = this;
                            _this.removeFile(file);
                            if (!file.type.match('image.*')) {
                                iErrorCtr++;
                            }
                        }
                    });

                    this.on("processing", function () {
                        iErrorCtr = 0;
                        iImgCtr = 0;
                    });

                    this.on("success", function (file, response) {
                        var _this = this;
                        $(file.previewElement).find('[data-dz-name]').text(response[iImgCtr].data.file_name);
                        file.new_name = response[iImgCtr].data.file_name;
                        file.new_path = response[iImgCtr].data.file_upload_path;
                        file.is_default = response[iImgCtr].data.is_default ? response[iImgCtr].data.is_default : 0;

                        var oFile = {
                            name: file.new_name,
                            file: file.new_path,
                            size: file.size,
                            is_default: file.is_default,
                        };
                        var sFile = encodeURIComponent(JSON.stringify(oFile));

                        $(file.previewElement).append('<div class="view-delete-btn-group btn-group btn-group-xs col-md-12"></div>');
                        $(file.previewElement).append('<div class="is-default-btn-group btn-group btn-group-xs col-md-12"></div>');

                        var uiViewButton = Dropzone.createElement('<a href="' + sBaseURI + '/' + file.new_path + '" ' +
                            'class="zoom img-thumbnail btn btn-info text-center col-md-6" ' +
                            'style="cursor: pointer !important;" ' +
                            'data-toggle="lightbox-image"><i class="fa fa-search"></a>');
                        $(file.previewElement).find('.view-delete-btn-group').append(uiViewButton);
                        // $('[data-toggle="lightbox-image"]').magnificPopup({type: 'image', image: {titleSrc: 'title'}});
                        $('#upload-image').each(function () {
                            $(this).magnificPopup({
                                delegate: '[data-toggle="lightbox-image"]',
                                type: 'image',
                                gallery: {
                                    enabled: true,
                                    navigateByImgClick: true,
                                    arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%" title="%title%"></button>',
                                    tPrev: 'Previous',
                                    tNext: 'Next',
                                    tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
                                },
                                image: {
                                    titleSrc: 'title'
                                },
                                callbacks: {
                                    beforeOpen: function () {
                                    },
                                    afterClose: function () {
                                    },
                                },
                            });
                        });

                        var uiRemoveButton = Dropzone.createElement('<button class="btn btn-danger text-center col-md-6 dz-remove-image-button"><i class="fa fa-trash"></i></button>');
                        $(file.previewElement).find('.view-delete-btn-group').append(uiRemoveButton);
                        $(uiRemoveButton).on("click", function (e) {
                            e.preventDefault();
                            var uiThis = $(this);
                            var oParams = {
                                'orig-src': response[iImgCtr].data.file_upload_path,
                            };
                            $('[name="project_images[]"][data-file="' + file.new_path + '"]').remove();
                            platform.project.attachment_delete(oParams, sDeleteImageUrl, uiThis, function (oData) {
                                _this.removeFile(file);
                            });
                        });

                        if (uiCreateProjectForm.length) {
                            uiCreateProjectForm.append('<input type="hidden" name="project_images[]" ' +
                                'data-is-default="' + file.is_default + '" data-file="' + file.new_path + '" value="' + sFile + '">');
                        }
                        if (uiEditProjectForm.length) {
                            uiEditProjectForm.append('<input type="hidden" name="project_images[]" ' +
                                'data-is-default="' + file.is_default + '" data-file="' + file.new_path + '" value="' + sFile + '">');
                        }

                        var uiSetDefaultBtn = Dropzone.createElement('<button class="btn btn-success text-center col-md-12 set-default-btn">' + (file.is_default == 1 ? 'Default' : 'Set as default') + '</button>');
                        $(file.previewElement).find('.is-default-btn-group').append(uiSetDefaultBtn);

                        $(uiSetDefaultBtn).on("click", function (e) {
                            e.preventDefault();
                            var uiThis = $(this);
                            $.each($('[name="project_images[]"]'), function () {
                                var oValue = platform.parse_json(decodeURIComponent($(this).val()));
                                oValue.is_default = 0;
                                var sFile = encodeURIComponent(JSON.stringify(oValue));
                                $(this).attr('data-is-default', 0).attr('value', sFile);
                                $('.set-default-btn').text('Set as default');
                            });


                            oFile.is_default = 1;
                            var sFile = encodeURIComponent(JSON.stringify(oFile));
                            $('[name="project_images[]"][data-file="' + file.new_path + '"]').attr('data-is-default', 1).attr('value', sFile);
                            uiThis.text('Default');
                        });
                    });

                    this.on("complete", function (file) {
                        $(file.previewElement).find('.dz-progress').hide();
                    });
                }
            });
            $('#upload-image').addClass('dropzone');

            if (typeof(fnCallback) == 'function') {
                fnCallback();
            }
        },

        /*
         * This function will delete the attachment from the server
         *
         * */
        attachment_delete: function (oData, sUrl, uiBtn, fnCallback) {
            var oParams = {
                type: "DELETE",
                data: oData,
                url: sUrl,
            };
            _ajax(oParams, function (oData) {
                if (typeof(fnCallback) == 'function') {
                    fnCallback(oData);
                }
            }, uiBtn);
        },
        /* ATTACHMENT END */
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.project.initialize();
});