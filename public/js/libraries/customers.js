(function () {
    "use strict";
    /* declare global variables within the class */
    var uiChangePasswordRadio,
        uiChangePasswordContainer,
        uiUsersTable,
        uiCreateUserForm,
        uiEditUserForm,
        uiRoleRadio,
        uiCalendarIDContainer,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain customer events
     *
     * */
    CPlatform.prototype.customer = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiChangePasswordRadio = $('#change_password');
            uiChangePasswordContainer = $('.change-pass-container'); uiUsersTable = $('#customers-table');
            uiCreateUserForm = $('#create-customer');
            uiEditUserForm = $('#edit-customer');
            uiRoleRadio = $('[name="roles[]"]');
            uiCalendarIDContainer= $('.calendar-id-container');

            /* customers table initialize datatable */
            var uiUsersDatatable = uiUsersTable.DataTable({
                "order": [[0, "asc"]],
                "paging": true,
                "pageLength": 10,
                "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
                "ordering": true,
                "info": true,
                "searching": true,
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [2]
                }],
                "sDom": "<'row'<'col-sm-6 col-xs-5'l><'col-sm-6 col-xs-7'f><'col-xs-6 filter-container'><'col-xs-6 text-right' B>r>t<'row'<'col-sm-4 col-xs-4'i><'col-sm-8 col-xs-8 clearfix'p>>",
                "buttons": [
                    {
                        extend: 'csv',
                        className: 'btn btn-primary btn-csv',
                        filename: 'Users_' + moment().format('YYYYMMDDHHmmss'),
                        messageTop: null,
                        title: '',
                        // footer: true,
                        exportOptions: {
                            columns: [ 0, 1 ],
                            // modifier: {
                            //     page: 'current'
                            // }
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-primary btn-xls',
                        filename: 'Users_' + moment().format('YYYYMMDDHHmmss'),
                        messageTop: null,
                        title: '',
                        // footer: true,
                        exportOptions: {
                            columns: [ 0, 1 ],
                            // modifier: {
                            //     page: 'current'
                            // }
                        }
                    },
                ],
                "initComplete": function () {
                    var dtButtons = $('.dt-buttons')
                    dtButtons.find('button').each(function (e) {
                        console.log()
                        if ($(this).hasClass('btn-csv')) {
                            $(this).find('span').prepend('<i class="fa fa-file-text-o"></i> ');
                        }
                        else if ($(this).hasClass('btn-xls')) {
                            $(this).find('span').prepend('<i class="fa fa-file-excel-o"></i> ');
                        }

                        $('.dataTables_wrapper').find('.buttons-container').append($(this));
                    });
                }
            });


        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.customer.initialize();
});