<?php

return [
    "placeholder_image" => "public/uploads/default-image.jpg",
    "placeholder_banner_image" => "public/uploads/default-image-banner.jpg",
    "dnr_bcc" => ["programmer10@dogandrooster.com", "programmer1@dogandrooster.com"],
];